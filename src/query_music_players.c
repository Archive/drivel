/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * Copyright 2002-2004 Todd Kulesza, Davyd Madeley
 *
 * Authors:
 * 		Davyd Madeley <davyd@ucc.asn.au>
 *
 * -*- mode: c; c-basic-offset: 4; tab-size: 8; -*-
 *
 */

#include <config.h>

#include <dlfcn.h>
#include <gnome.h>
#include <libbonobo.h>

#include "query_music_players.h"

#ifdef HAVE_DBUS
#include <glib-object.h>
#include <dbus/dbus.h>
#include <dbus/dbus-glib.h>
#endif

/* ---- definitions ---- */
static void *xmms_dl = NULL; /* for dlopen() */
static void *beep_dl = NULL; /* for dlopen() */

static void
add_music_entry (GtkListStore *store, gchar *entry, GdkPixbuf *pixbuf, gboolean *changed, gboolean prepend);

static gboolean
xmms_init (void);
static gchar
*xmms_query (void);

static gboolean
beep_init (void);
static gchar
*beep_query (void);

#ifdef HAVE_DBUS
static DBusGConnection *	get_session_bus (void);
static gchar *			dbus_get_playing_rb (void);
static gchar *			dbus_get_playing_muine (void);
static gboolean			dbus_service_exists (const gchar *path);
#endif

/* ---- end definitions ---- */

void
query_music_players (GtkListStore *playing)
{
	gboolean entries;
	GdkPixbuf *pixbuf;

	gtk_list_store_clear (playing);
	entries = FALSE;

	if (xmms_init ())
	{
		pixbuf = gdk_pixbuf_new_from_file (DATADIR G_DIR_SEPARATOR_S "pixmaps"
				G_DIR_SEPARATOR_S "drivel" G_DIR_SEPARATOR_S "xmms.xpm", NULL);
		add_music_entry (playing, xmms_query (), pixbuf, &entries, FALSE);
	}
	if (beep_init ())
	{
		/* XXX - Just use the xmms.xpm for now. */
                pixbuf = gdk_pixbuf_new_from_file (DATADIR G_DIR_SEPARATOR_S "pixmaps"
                                G_DIR_SEPARATOR_S "drivel" G_DIR_SEPARATOR_S "xmms.xpm", NULL);

		add_music_entry (playing, beep_query (), NULL, &entries, FALSE);
	}

#ifdef HAVE_DBUS
	gchar *np = dbus_get_playing_rb ();

	if (np != NULL) {
	    pixbuf = gtk_icon_theme_load_icon (gtk_icon_theme_get_default (),
					       "rhythmbox", 16, 0, NULL);
	    add_music_entry (playing, np, pixbuf, &entries, FALSE);
	}


	np = dbus_get_playing_muine ();
	if (np != NULL) {
	    pixbuf = gtk_icon_theme_load_icon (gtk_icon_theme_get_default (),
					       "muine", 16, 0, NULL);
	    add_music_entry (playing, np, pixbuf, &entries, FALSE);
	}
#endif

	if (entries)
		add_music_entry (playing, g_strdup (""), NULL, NULL, TRUE);
	else
		/* FIXME:
		 * I would like to make this menu item insensitive, however due to
		 * an API issue in GTK, this will not be possible yet.
		 * See GNOME bug #135875
		 */
		add_music_entry (playing, g_strdup (_("No song playing")), NULL, NULL, FALSE);
}

static void
add_music_entry (GtkListStore *store, gchar *entry, GdkPixbuf *pixbuf, gboolean *changed, gboolean prepend)
{
	GtkTreeIter iter;

	if (entry)
	{
		if (prepend)
			gtk_list_store_prepend (store, &iter);
		else
			gtk_list_store_append (store, &iter);
		gtk_list_store_set (store, &iter,
				0, entry,
				1, pixbuf,
				-1);
		if (changed)
			*changed = TRUE;
	}
}

/*
 * xmms_init() and xmms_query() are based off
 * music_xmms_grab() and xmms_load_ok() from logjam.
 *
 * logjam - a GTK client for LiveJournal.
 * Copyright (C) 2000-2003 Evan Martin <evan@livejournal.com>
 * 
 * Imported May 3, 2003 by Todd Kulesza <todd@dropline.net>
 * Modified May 5, 2004 by Davyd Madeley <davyd@ucc.asn.au>
 */
static gboolean
xmms_init (void)
{
	if (xmms_dl == NULL)
	{
		xmms_dl = dlopen ("libxmms.so", RTLD_LAZY);

		if (!xmms_dl)
			xmms_dl = dlopen ("libxmms.so.1", RTLD_LAZY);
		if (!xmms_dl)
			return FALSE; /* no library; don't use xmms. */
	}
	return TRUE;
}

static gchar
*xmms_query (void)
{
	gchar *raw_title, *normalized_title=NULL;
	gssize pos;
	gsize bytes_read, bytes_written;

	static gint (*xrgpp)(gint) = NULL;
	static gchar* (*xrgpt)(gint, gint) = NULL;

	if (xrgpp == NULL || xrgpt == NULL) {
		xrgpp = dlsym (xmms_dl, "xmms_remote_get_playlist_pos");
		if (dlerror ())
			return NULL;
		xrgpt = dlsym (xmms_dl, "xmms_remote_get_playlist_title");
		if (dlerror ())
			return NULL;
	}

	pos = xrgpp (0);
	raw_title = xrgpt (0, pos);
	/* the string from XMMS is in the locale encoding. We need to 
	 * convert this to UTF8
	 */
	if (raw_title) {
		normalized_title = g_locale_to_utf8(raw_title, strlen(raw_title), &bytes_read, &bytes_written, NULL);
		free(raw_title);
	}
	return normalized_title;
}

/*
 * beep_init() and beep_query() are based off
 * xmms_init() and xmms_query() which are based off
 * music_xmms_grab() and xmms_load_ok() from logjam.
 *
 * logjam - a GTK client for LiveJournal.
 * Copyright (C) 2000-2003 Evan Martin <evan@livejournal.com>
 *
 * Imported May 3, 2003 by Todd Kulesza <todd@dropline.net>
 * Modified May 5, 2004 by Davyd Madeley <davyd@ucc.asn.au>
 * Modified November 18, 2004 by Adam Gregoire <bsdunx@gmail.com>
 */
static gboolean
beep_init (void)
{
	if (beep_dl == NULL)
	{
		beep_dl = dlopen ("libbeep.so", RTLD_LAZY);

		if (!beep_dl)
			beep_dl = dlopen ("libbeep.so.2", RTLD_LAZY);
		if (!beep_dl)
			return FALSE; /* no library; don't use beep. */
	}
	return TRUE;
}

static gchar
*beep_query (void)
{
	gchar *raw_title, *normalized_title=NULL;
        gssize pos;
        gsize bytes_read, bytes_written;

	static gint (*xrgpp)(gint) = NULL;
	static gchar* (*xrgpt)(gint, gint) = NULL;

	if (xrgpp == NULL || xrgpt == NULL) {
		xrgpp = dlsym (beep_dl, "xmms_remote_get_playlist_pos");
		if (dlerror ())
			return NULL;
		xrgpt = dlsym (beep_dl, "xmms_remote_get_playlist_title");
		if (dlerror ())
			return NULL;
	}

	pos = xrgpp (0);
	raw_title = xrgpt (0, pos);
        /* the string from XMMS is in the locale encoding. We need to
	 * convert this to UTF8
	 */
	if (raw_title) {
		normalized_title = g_locale_to_utf8(raw_title, strlen(raw_title), &bytes_read, &bytes_written, NULL);
		free(raw_title);
	}
	return normalized_title;
}

/*
 * routines for talking to Rhythmbox 
 * TODO:              Perhaps xmms or bmp can also talk via D-Bus?
 */
#ifdef HAVE_DBUS

static DBusGConnection *
get_session_bus (void)
{
    DBusGConnection  *conn;
    GError 	     *error;

    error = NULL;
    conn = dbus_g_bus_get (DBUS_BUS_SESSION, &error);

    if (conn == NULL) {
	g_printerr ("Failed to open connection to bus: %s\n",
		    error->message);
	g_error_free (error);
	return NULL;
    }

    return conn;
}


static gboolean 
dbus_service_exists (const gchar *name)
{
    GError *error = NULL;
    DBusGProxy *proxy;
    gchar **services;
    gchar **p;
    gboolean retval = FALSE;

    DBusGConnection *conn = get_session_bus ();
    g_return_val_if_fail (conn != NULL, FALSE);

    proxy = dbus_g_proxy_new_for_name (conn, DBUS_SERVICE_DBUS,
				       DBUS_PATH_DBUS,
				       DBUS_INTERFACE_DBUS);

    if (!dbus_g_proxy_call (proxy, "ListNames", &error, G_TYPE_INVALID,
			    G_TYPE_STRV, &services, G_TYPE_INVALID))
    {
	if (error->domain == DBUS_GERROR && error->code == DBUS_GERROR_REMOTE_EXCEPTION)
	    g_printerr ("Caught remote method exception %s: %s",
			dbus_g_error_get_name (error),
			error->message);
	else
	    g_printerr ("Error: %s\n", error->message);

	g_error_free (error);
	return FALSE;
    }

    for (p = services; *p; ++p) {
	if (strcmp (*p, name) == 0) {
	    retval = TRUE;
	    break;
	}
    } 

    g_strfreev (services);
    return retval;
}


static gchar *
dbus_get_playing_muine (void)
{
    gchar *strings = NULL;
    GError *error = NULL;
    DBusGProxy *proxy;

    DBusGConnection *conn = get_session_bus ();
    g_return_val_if_fail (conn != NULL, NULL);

    if (!dbus_service_exists ("org.gnome.Muine"))
	return NULL;

    proxy = dbus_g_proxy_new_for_name (conn, "org.gnome.Muine",
				       "/org/gnome/Muine/Player",
				       "org.gnome.Muine.Player");
    if (!dbus_g_proxy_call (proxy, "GetCurrentSong", &error, G_TYPE_INVALID,
			    G_TYPE_STRING, &strings, G_TYPE_INVALID))
    {
	if (error->domain == DBUS_GERROR && error->code == DBUS_GERROR_REMOTE_EXCEPTION)
	    g_printerr ("Caught remote method exception %s: %s",
			dbus_g_error_get_name (error), error->message);
	else
	    g_printerr ("Error: %s\n", error->message);

	g_error_free (error);
	return NULL;
    }
    
    g_return_val_if_fail (strings != NULL, NULL);
    
    gchar *str = strings;
    gchar *p = strings;
    gchar *title = "Unknown Title";
    gchar *artist = "Unknown Artist";

    while (*p) {
	if (*p == '\n') {
	    *p = '\0';
	    printf ("[%s]\n", str);
	    if (g_ascii_strncasecmp (str, "artist: ", strlen ("artist: ")) == 0)
		artist = g_strdup ((gchar *) str + strlen ("artist: "));
	    else if (g_ascii_strncasecmp (str, "title:", strlen ("title:")) == 0)
		title = g_strdup ((gchar *) str + strlen ("title: "));
	    str = ++p;
	}
	else 
	    ++p;
    }
    return (g_strdup_printf ("%s - %s", artist, title));
}


static gchar *
dbus_get_playing_rb (void)
{
    gchar *np = NULL;
    GError *error = NULL;
    DBusGProxy *proxy;
    gchar *playing_uri;
    GHashTable *song_details;

    DBusGConnection *conn = get_session_bus ();
    g_return_val_if_fail (conn != NULL, NULL);

    if (!dbus_service_exists ("org.gnome.Rhythmbox")) 
	return NULL;
  
    /* Step 1: get the URI of the currently playing song */
    proxy = dbus_g_proxy_new_for_name (conn, "org.gnome.Rhythmbox",
				       "/org/gnome/Rhythmbox/Player",
				       "org.gnome.Rhythmbox.Player");
    error = NULL;
    if (!dbus_g_proxy_call (proxy, "getPlayingUri", &error, G_TYPE_INVALID,
			    G_TYPE_STRING, &playing_uri, G_TYPE_INVALID))
    {
	if (error->domain == DBUS_GERROR && error->code == DBUS_GERROR_REMOTE_EXCEPTION)
	    g_printerr ("Caught remote method exception %s: %s",
			dbus_g_error_get_name (error),
			error->message);
	else
	    g_printerr ("Error: %s\n", error->message);

	g_error_free (error);
	return NULL;
    }
    
    if (strlen (playing_uri? playing_uri : "") == 0)
	return NULL;

    /* Step 2: knowing the URI, get the details for the song currently playing */
    proxy = dbus_g_proxy_new_for_name (conn, "org.gnome.Rhythmbox",
				       "/org/gnome/Rhythmbox/Shell",
				       "org.gnome.Rhythmbox.Shell");
    error = NULL;
    if (!dbus_g_proxy_call (proxy, "getSongProperties", &error,
			    G_TYPE_STRING, 
			    playing_uri,
			    G_TYPE_INVALID,
			    dbus_g_type_get_map ("GHashTable", 
						 G_TYPE_STRING, 
						 G_TYPE_VALUE), 
			    &song_details,
			    G_TYPE_INVALID))
    {
	if (error->domain == DBUS_GERROR && error->code == DBUS_GERROR_REMOTE_EXCEPTION)
	    g_printerr ("Caught remote method exception %s: %s",
			dbus_g_error_get_name (error),
			error->message);
	else
	    g_printerr ("Error: %s\n", error->message);

	g_error_free (error);
	return NULL;
    }
    g_return_val_if_fail (song_details != NULL, NULL);

    GValue *artist_v = g_hash_table_lookup (song_details, "artist");
    GValue *title_v = g_hash_table_lookup (song_details, "title");
    g_return_val_if_fail ((artist_v != NULL) && (title_v != NULL), NULL);

    gchar *artist = (gchar *) g_value_get_string (artist_v);
    gchar *title = (gchar *) g_value_get_string (title_v);

    if (! artist)
	artist = "Unknown Artist";
    if (! title)
	title = "Unknown Title";

    np = g_strdup_printf ("%s - %s", artist, title);
    g_hash_table_unref (song_details);

    printf ("get_playing_rhythmbox: %s\n", np);
    return np;
}

#endif /* HAVE_DBUS */
