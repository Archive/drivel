/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * Copyright 2004 Todd Kulesza
 *
 * Authors:
 * 		Todd Kulesza <todd@dropline.net>
 *		Davyd Madeley <davyd@ucc.asn.au>
 */

#include <config.h>

#include <stdarg.h>
#include <string.h>
#include <glib.h>
#include <libsoup/soup.h>

#include "utils.h"
#include "drivel_request.h"

static void
add_item (DrivelRequest *dr, const gchar *key, const gchar *value)
{
	DrivelRequestItem *dri;
	
	dri = g_new0 (DrivelRequestItem, 1);
	dri->key = g_strdup (key);
	dri->value = g_strdup (value);
	
	dr->items = g_slist_prepend (dr->items, dri);
	
	return;
}

/* Create an empty DrivelRequest */
DrivelRequest*
drivel_request_new (void)
{
	DrivelRequest *dr;
	
	dr = g_new0 (DrivelRequest, 1);
	dr->items = NULL;
	dr->current = NULL;
	dr->type = REQUEST_TYPE_NONE;
	dr->protocol = REQUEST_PROTOCOL_NONE;
	dr->api = BLOG_API_UNKNOWN;
	dr->request_values = g_hash_table_new_full (g_str_hash, g_str_equal,
			g_free, g_free);
	dr->data = NULL;
	dr->msg = NULL;

	return dr;
}

/* Create a DrivelRequest with a type and multiple items, terminated by NULL */
DrivelRequest*
drivel_request_new_with_items (DrivelRequestType type, 
		DrivelRequestProtocol protocol, DrivelBlogAPI api, const gchar *uri, ...)
{
	DrivelRequest *dr;
	va_list ap;
	const gchar *key, *value;
	
	dr = drivel_request_new ();
	drivel_request_set_type (dr, type);
	drivel_request_set_protocol (dr, protocol);
	drivel_request_set_api (dr, api);
	drivel_request_set_uri (dr, uri);
	
	va_start (ap, uri);
	
	do
	{
		key = va_arg (ap, gchar*);
		if (key)
		{
			value = va_arg (ap, gchar*);
			add_item (dr, key, value);
		}
	} while (key);
	
	va_end (ap);
	
	return dr;
}

/* Create a DrivelRequest with a type and SoupMessage */
DrivelRequest*
drivel_request_new_with_msg (DrivelRequestType type, 
		DrivelRequestProtocol protocol, DrivelBlogAPI api, const gchar *uri, SoupMessage *msg)
{
	DrivelRequest *dr;
	
	dr = drivel_request_new ();
	drivel_request_set_type (dr, type);
	drivel_request_set_protocol (dr, protocol);
	drivel_request_set_api (dr, api);
	drivel_request_set_uri (dr, uri);
	drivel_request_set_msg (dr, msg);
	
	return dr;
}

/* Add multiple items to a DrivelRequest.  Terminate the list with NULL. */
void
drivel_request_add_items (DrivelRequest *dr, ...)
{
	va_list ap;
	const gchar *key, *value;
	
	g_return_if_fail (dr);
	
	va_start (ap, dr);
	
	do
	{
		key = va_arg (ap, gchar*);
		if (key)
		{
			value = va_arg (ap, gchar*);
			add_item (dr, key, value);
		}
	} while (key);
	
	va_end (ap);
	
	return;
}

/* Remove multiple items by their key names.  Terminate the list with NULL. */
void
drivel_request_remove_items (DrivelRequest *dr, ...)
{
	va_list ap;
	const gchar *key;
	GSList *current;
	gboolean found;
	
	g_return_if_fail (dr);
	
	do
	{
		key = va_arg (ap, gchar*);
		if (key)
		{
			found = FALSE;
			for (current = dr->items; current && !found; current = current->next)
			{
				DrivelRequestItem *item;
				
				item = current->data;
				if (!strcmp (item->key, key))
				{
					found = TRUE;
					g_free (item->key);
					g_free (item->value);
					g_free (item);
					dr->items = g_slist_delete_link (dr->items, current);
				}
			}
		}
	} while (key);
	
	va_end (ap);
	
	return;
}

/* Set the SoupMessage object */
void
drivel_request_set_msg (DrivelRequest *dr, SoupMessage *msg)
{
	g_return_if_fail (dr);

	dr->msg = msg;

	return;
}

/* Get the SoupMessage object */
SoupMessage*
drivel_request_get_msg (DrivelRequest *dr)
{
	g_return_val_if_fail (dr, NULL);

	return (dr->msg);
}

/* Set the type of request */
void
drivel_request_set_type (DrivelRequest *dr, DrivelRequestType type)
{
	g_return_if_fail (dr);
	
	dr->type = type;
	
	return;
}

/* Get the type of request */
DrivelRequestType
drivel_request_get_type (DrivelRequest *dr)
{
	g_return_val_if_fail (dr, REQUEST_TYPE_NONE);
	
	return (dr->type);
}

/* Set the network protocol */
void
drivel_request_set_protocol (DrivelRequest *dr, DrivelRequestProtocol protocol)
{
	g_return_if_fail (dr);
	
	dr->protocol = protocol;
	
	return;
}

/* Get the network protocol */
DrivelRequestProtocol
drivel_request_get_protocol (DrivelRequest *dr)
{
	g_return_val_if_fail (dr, REQUEST_PROTOCOL_NONE);
	
	return (dr->protocol);
}

/* Set the journal API */
void
drivel_request_set_api (DrivelRequest *dr, DrivelBlogAPI api)
{
	g_return_if_fail (dr);
	
	dr->api = api;
	
	return;
}

/* Get the journal API */
DrivelBlogAPI
drivel_request_get_api (DrivelRequest *dr)
{
	g_return_val_if_fail (dr, BLOG_API_UNKNOWN);
	
	return dr->api;
}

/* Set the returned network data */
void
drivel_request_set_data (DrivelRequest *dr, DrivelRequestData *data)
{
	g_return_if_fail (dr);
	
	dr->data = data;
	
	return;
}

/* Get a pointer to the returned network data */
DrivelRequestData*
drivel_request_get_data (DrivelRequest *dr)
{
	g_return_val_if_fail (dr, NULL);
	
	return (dr->data);
}

/* Insert a key/value pair into the request_values hash table */
void
drivel_request_value_insert (DrivelRequest *dr, const gchar *key, const gchar *value)
{
	g_return_if_fail (dr);
	
	g_hash_table_insert (dr->request_values, g_strdup (key), g_strdup (value));
	
	return;
}

/* Lookup a value given a key in the request_values hash table */
const gchar*
drivel_request_value_lookup (DrivelRequest *dr, const gchar *key)
{
	gchar *value;
	
	g_return_val_if_fail (dr, NULL);
	
	value = g_hash_table_lookup (dr->request_values, key);
	
	return value;
}

/* Clear the request_values hash table */
void
drivel_request_clear_values (DrivelRequest *dr)
{
	g_return_if_fail (dr);
	
	hash_table_clear (dr->request_values);
	
	return;
}

/* Replace the existing hash table with a user-supplied table */
void
drivel_request_set_values (DrivelRequest *dr, GHashTable *table)
{
	g_return_if_fail (dr);
	g_return_if_fail (table);
	
	g_hash_table_destroy (dr->request_values);
	dr->request_values = table;
	
	return;
}

/* Set the DrivelRequest to the first item, returns FALSE if no items are
   available. */
gboolean
drivel_request_start (DrivelRequest *dr)
{
	g_return_val_if_fail (dr, FALSE);
	
	dr->current = dr->items;
	
	return (!(!(dr->current)));
}

/* Iterate to the next item in the list, returns FALSE if no items are
   available. */
gboolean
drivel_request_next (DrivelRequest *dr)
{
	g_return_val_if_fail (dr, FALSE);
	
	dr->current = dr->current->next;
	
	return (!(!(dr->current)));
}

/* Returns the current item in the list, or NULL if no items are available. */
DrivelRequestItem*
drivel_request_get_current_item (DrivelRequest *dr)
{
	DrivelRequestItem *item;
	
	g_return_val_if_fail (dr, NULL);
	
	if (dr->current)
		item = dr->current->data;
	else
		item = NULL;
	
	return item;
}

/* Looks for 'key' in 'items', returns 'value' if found */
const gchar*
drivel_request_item_lookup (DrivelRequest *dr, const gchar *key)
{
	DrivelRequestItem *item;
	gboolean valid;
	const gchar *value = NULL;
	
	g_return_val_if_fail (dr, NULL);
	
	valid = drivel_request_start (dr);
	while (valid)
	{
		item = drivel_request_get_current_item (dr);
		if (!strcmp (key, item->key))
		{
			value = item->value;
			valid = FALSE;
		}
		else
			valid = drivel_request_next (dr);
	}
	
	return value;
}

/* Set the URI to connect to */
void
drivel_request_set_uri (DrivelRequest *dr, const gchar *uri)
{
	g_return_if_fail (dr);
	g_return_if_fail (uri);
	
	g_free (dr->uri);
	dr->uri = g_strdup (uri);
	
	return;
}

/* Get the URI of the server */
const gchar*
drivel_request_get_uri (DrivelRequest *dr)
{
	g_return_val_if_fail (dr, NULL);
	
	return (dr->uri);
}

/* Frees the memory used by the object and the list. */
void
drivel_request_free (DrivelRequest *dr)
{
	GSList *current;
	DrivelRequestItem *item;
	
	g_return_if_fail (dr);
	
	for (current = dr->items; current; current = current->next)
	{
		item = current->data;
		g_free (item->key);
		g_free (item->value);
		g_free (item);
	}
	g_slist_free (dr->items);
	g_hash_table_destroy (dr->request_values);
	if (dr->data)
	{
		g_free (dr->data->data);
		g_free (dr->data);
	}
	if (dr->msg)
	{
		g_object_unref (dr->msg);
	}
	g_free (dr);
	
	return;
}
