/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * Copyright 2004 Todd Kulesza
 *
 * Authors:
 * 		Todd Kulesza <todd@dropline.net>
 */

/* Drivel's MT interface is modeled off of Sherzod Ruzmetov's 
 * Net::MovableType PERL class. */

#define _XOPEN_SOURCE /* glibc2 needs this */

#include <sys/types.h>
#include <config.h>

#include <time.h>
#include <string.h>
#include <libxml/parser.h>

#include "drivel.h"
#include "drivel_request.h"
#include "journal.h"
#include "network.h"
#include "xmlrpc.h"
#include "blog_mt.h"

extern DrivelClient *dc;

gboolean mt_ext_cat = FALSE; /* MT category extension */
gboolean mt_ext_pub = FALSE; /* MT publish extension */
DrivelRequest* (*getcategories_method)(const gchar *, const gchar *, 
				       const gchar *, const gchar *,
				       const gchar *) = NULL;

static void
parse_mtsupported_request (SoupMessage *msg, SoupMessage *target_msg)
{
	SoupXmlrpcResponse *response = NULL;

	g_return_if_fail (msg);

	debug ("movabletype parse_supported_request()");

	/* If we fault, assume we're using the MetaWeblog API */
	if ((!SOUP_STATUS_IS_SUCCESSFUL (msg->status_code)) ||
	    (!(response = soup_xmlrpc_message_parse_response (SOUP_XMLRPC_MESSAGE (msg)))) ||
	    (soup_xmlrpc_response_is_fault (response)))
	{
		if (response)
			g_object_unref (response);

		mt_ext_cat = FALSE;
		mt_ext_pub = FALSE;
		getcategories_method = blog_mwl_build_getcategories_request;
	}
	/* Check for MovableType category support */
	else 
	{
		if (msg->response.body && strstr (msg->response.body, "mt.getCategoryList"))
		{
			mt_ext_cat = TRUE;
			getcategories_method = blog_mt_build_getcategories_request;
		}
		/* Check for MetaWeblog category support */
		else
		{
			mt_ext_cat = FALSE;
			getcategories_method = blog_mwl_build_getcategories_request;
		}

		if (msg->response.body && strstr (msg->response.body, "mt.publishPost"))
		{
			mt_ext_pub = TRUE;
		}
		else
		{
			mt_ext_pub = FALSE;
		}
	}

	net_enqueue_msg (target_msg);

	return;
}

static void
build_mtsupported_request (const gchar *url, SoupMessage *target_msg)
{
	SoupXmlrpcMessage *msg;

	msg = xmlrpc_start ("mt.supportedMethods", url);
	xmlrpc_end (msg);
	
	g_signal_connect (G_OBJECT (msg), "finished",
			  G_CALLBACK (parse_mtsupported_request), target_msg);

	net_enqueue_msg (SOUP_MESSAGE (msg));

	return;
}

static void
parse_login_request (SoupMessage *msg, gpointer data)
{
	SoupXmlrpcResponse *response;
	SoupXmlrpcValue *value;
	SoupXmlrpcValueArrayIterator *array_iter;
	GtkTreeIter iter;

	g_return_if_fail (msg);

	debug ("movabletype parse_login_request()");

	/* Clear out categories from other users */
	gtk_list_store_clear (dc->category_store);
	gtk_list_store_append (dc->category_store, &iter);
	gtk_list_store_set (dc->category_store, &iter, 
			STORE_CATEGORY_NAME, _("None"),
			STORE_CATEGORY_ID, "none",
			STORE_CATEGORY_BLOG, "",
			-1);
	
	if (!(response = net_msg_get_response (SOUP_XMLRPC_MESSAGE (msg))))
		return;
	
	value = soup_xmlrpc_response_get_value (response);
	if (!value)
	{
		g_warning ("No value in reponse");
		return;
	}

	add_account_to_list (dc);
	dc->journals = 0;

	soup_xmlrpc_value_array_get_iterator (value, &array_iter);
	while (soup_xmlrpc_value_array_iterator_get_value (array_iter, &value) && value)
	{
		GHashTable *table;
		gchar *url, *id, *name;
		DrivelJournal *dj;

		soup_xmlrpc_value_get_struct (value, &table);

		value = g_hash_table_lookup (table, "url");
		soup_xmlrpc_value_get_string (value, &url);
		value = g_hash_table_lookup (table, "blogid");
		soup_xmlrpc_value_get_string (value, &id);
		value = g_hash_table_lookup (table, "blogName");
		soup_xmlrpc_value_get_string (value, &name);
		
		dj = drivel_journal_new ();
		dj->name = name;
		dj->uri_view = url;
		dj->id = id;
		dj->type = JOURNAL_TYPE_USER;
		dc->journal_list = g_slist_prepend (dc->journal_list, dj);

		g_hash_table_destroy (table);

		dc->journals++;
		array_iter = soup_xmlrpc_value_array_iterator_next (array_iter);

		/* get the categories for this journal */
		getcategories_method (dc->user->username, 
				      dc->user->password, 
				      dc->user->server, 
				      dj->name, 
				      dj->id);

		/* get the recent entries for this journal */
		blog_mt_build_getevents_request (dc->user->username,
						 dc->user->password,
						 dc->user->server,
						 dj->id,
						 FALSE);
	}

	dc->journal_list = g_slist_sort (dc->journal_list, (GCompareFunc)sort_journals);

	gtk_widget_hide (dc->login_window);
	journal_window_build (dc);

	g_object_unref (response);

	return;
}

DrivelRequest*
blog_mt_build_login_request (const gchar *url, const gchar *username, 
			     const gchar *password)
{
	SoupXmlrpcMessage *msg;

	msg = xmlrpc_start ("blogger.getUsersBlogs", url);
	xmlrpc_add_string_param (msg, DRIVEL_APPKEY);
	xmlrpc_add_string_param (msg, username);
	xmlrpc_add_string_param (msg, password);
	xmlrpc_end (msg);
	
	g_signal_connect (G_OBJECT (msg), "finished",
			  G_CALLBACK (parse_login_request), NULL);

	/* figure out which methods are supported by the server */
	build_mtsupported_request (dc->user->server, SOUP_MESSAGE (msg));

	return NULL;
}

static void
set_cat_mt (const gchar *postid)
{
	GtkTreeModel *model;
	GtkTreeIter iter;
	gchar *category = NULL;

	model = gtk_combo_box_get_model (GTK_COMBO_BOX (dc->journal_category));
	gtk_combo_box_get_active_iter (GTK_COMBO_BOX (dc->journal_category), &iter);
	gtk_tree_model_get (model, &iter, 1, &category, -1);

	/* If a category was selected, set it on the server */
	if (category)
	{
		blog_mt_build_setpostcategories_request (dc->user->username,
							 dc->user->password, 
							 dc->user->server, 
							 postid, category);
		g_free (category);
	}

	return;
}

static void
set_cat_mwl (SoupXmlrpcMessage *msg)
{
	GtkTreeModel *model;
	GtkTreeIter iter;
	gchar *category = NULL;

	model = gtk_combo_box_get_model (GTK_COMBO_BOX (dc->journal_category));
	gtk_combo_box_get_active_iter (GTK_COMBO_BOX (dc->journal_category), &iter);
	gtk_tree_model_get (model, &iter, 1, &category, -1);

	if (category)
	{
		xmlrpc_start_array_member (msg, "categories");
		soup_xmlrpc_message_write_string (msg, category);
		xmlrpc_end_array_member (msg);
		
		g_free (category);
	}

	return;
}

/* Parse the server's response to our post request */

static void
parse_postevent_request (SoupMessage *msg, gpointer data)
{
	SoupXmlrpcResponse *response;
	SoupXmlrpcValue *value;
	gchar *postid;

	g_return_if_fail (msg);

	debug ("movabletype parse_postevent_request()");

	if (!(response = net_msg_get_response (SOUP_XMLRPC_MESSAGE (msg))))
		return;
	
	value = soup_xmlrpc_response_get_value (response);
	if (!value)
	{
		g_warning ("No value in reponse");
		return;
	}

	/* First check for a "postid" value (set in the editevent method) since
	   metaWeblog.editPost returns a boolean, not the postid */
	postid = g_object_get_data (G_OBJECT (msg), "postid");
	if (!postid)
		soup_xmlrpc_value_get_string (value, &postid);

	if (mt_ext_cat)
	{
		set_cat_mt (postid);
	}
	
	if (mt_ext_pub)
	{
		blog_mt_build_publish_request (dc->user->username,
					       dc->user->password, 
					       dc->user->server, postid);
	}
	else
	{
		journal_finished_post (dc);
	}

	g_free (postid);
	g_object_unref (response);

	return;
}

/* Build a DrivelRequest with all of the content needed by metaWeblog.newPost() */

DrivelRequest*
blog_mt_build_postevent_request (const gchar *username, const gchar *password,
				 const gchar *uri, const gchar *blogid, 
				 gboolean publish, const gchar *title, 
				 const gchar *content)
{
	SoupXmlrpcMessage *msg;

	debug ("blog_mt_build_postevent_request()");

	/* If we're not using the MT publish extension, we need to publish here */
	if (!mt_ext_pub)
		publish = TRUE;

	msg = xmlrpc_start ("metaWeblog.newPost", uri);
	xmlrpc_add_string_param (msg, blogid);
	xmlrpc_add_string_param (msg, username);
	xmlrpc_add_string_param (msg, password);
	xmlrpc_start_struct_param (msg);
	xmlrpc_add_string_member (msg, "title", title);
	xmlrpc_add_string_member (msg, "description", content);
	if (!mt_ext_cat)
	{
		set_cat_mwl (msg);
	}
	xmlrpc_end_struct_param (msg);
	xmlrpc_add_bool_param (msg, publish);
	xmlrpc_end (msg);
	
	g_signal_connect (G_OBJECT (msg), "finished",
			  G_CALLBACK (parse_postevent_request), NULL);

	net_enqueue_msg (SOUP_MESSAGE (msg));

	return NULL;
}

DrivelRequest *
blog_mt_build_editevent_request (const gchar *username, const gchar *password,
		const gchar *uri, const gchar *postid, gboolean publish,
		const gchar *title, const gchar *content)
{
	SoupXmlrpcMessage *msg;

	debug ("blog_mt_build_editevent_request()");

	/* If we're not using the MT publish extension, we need to publish here */
	if (!mt_ext_pub)
		publish = TRUE;

	msg = xmlrpc_start ("metaWeblog.editPost", uri);
	xmlrpc_add_string_param (msg, postid);
	xmlrpc_add_string_param (msg, username);
	xmlrpc_add_string_param (msg, password);
	xmlrpc_start_struct_param (msg);
	xmlrpc_add_string_member (msg, "title", title);
	xmlrpc_add_string_member (msg, "description", content);
	if (!mt_ext_cat)
	{
		set_cat_mwl (msg);
	}
	xmlrpc_end_struct_param (msg);
	xmlrpc_add_bool_param (msg, publish);
	xmlrpc_end (msg);
	
	g_signal_connect (G_OBJECT (msg), "finished",
			  G_CALLBACK (parse_postevent_request), NULL);

	g_object_set_data (G_OBJECT (msg), "postid", g_strdup (postid));

	net_enqueue_msg (SOUP_MESSAGE (msg));

	return NULL;
}

/* Parse the server's list of categories */

static void
parse_mt_getcategories_request (SoupMessage *msg, gpointer data)
{
	SoupXmlrpcResponse *response;
	SoupXmlrpcValue *value;
	SoupXmlrpcValueArrayIterator *array_iter;
	GtkTreeIter iter;
	const gchar *journal_name;

	debug ("movabletype parse_mt_getcategories_request()\n");

	if (!(response = net_msg_get_response (SOUP_XMLRPC_MESSAGE (msg))))
		return;
	
	value = soup_xmlrpc_response_get_value (response);
	if (!value)
	{
		g_warning ("No value in reponse");
		return;
	}

	journal_name = g_object_get_data (G_OBJECT (msg), "journal_name");

	soup_xmlrpc_value_array_get_iterator (value, &array_iter);
	while (soup_xmlrpc_value_array_iterator_get_value (array_iter, &value) 
	       && value && journal_name)
	{
		GHashTable *table;
		gchar *id, *name;

		soup_xmlrpc_value_get_struct (value, &table);

		value = g_hash_table_lookup (table, "categoryId");
		soup_xmlrpc_value_get_string (value, &id);
		value = g_hash_table_lookup (table, "categoryName");
		soup_xmlrpc_value_get_string (value, &name);

		if (id && name)
		{
			gtk_list_store_append (dc->category_store, &iter);
			gtk_list_store_set (dc->category_store, &iter, 
					    STORE_CATEGORY_NAME, name,
					    STORE_CATEGORY_ID, id,
					    STORE_CATEGORY_BLOG, journal_name,
					    -1);			
		}

		g_free (id);
		g_free (name);
		g_hash_table_destroy (table);

		array_iter = soup_xmlrpc_value_array_iterator_next (array_iter);
	}

	gtk_tree_model_filter_refilter (GTK_TREE_MODEL_FILTER (dc->category_store_filtered));
	gtk_combo_box_set_active (GTK_COMBO_BOX (dc->journal_category), 0);

	g_object_unref (response);
	
	return;
}

/* Get the list of categories from the server */

DrivelRequest*
blog_mt_build_getcategories_request (const gchar *username, 
				     const gchar *password, const gchar *uri, 
				     const gchar *journal_name, const gchar *blogid)
{
	SoupXmlrpcMessage *msg;

	debug ("blog_mt_build_getcategories_request()");

	msg = xmlrpc_start ("mt.getCategoryList", uri);
	xmlrpc_add_string_param (msg, blogid);
	xmlrpc_add_string_param (msg, username);
	xmlrpc_add_string_param (msg, password);
	xmlrpc_end (msg);
	
	g_signal_connect (G_OBJECT (msg), "finished",
			  G_CALLBACK (parse_mt_getcategories_request), NULL);

	g_object_set_data_full (G_OBJECT (msg), "journal_name", g_strdup (journal_name), g_free);

	net_enqueue_msg (SOUP_MESSAGE (msg));

	return NULL;
}

static void
get_cat (const gchar *key, SoupXmlrpcValue *value, const gchar *journal_name)
{
	GtkTreeIter iter;

	gtk_list_store_append (dc->category_store, &iter);
	gtk_list_store_set (dc->category_store, &iter,
			    STORE_CATEGORY_NAME, key,
			    STORE_CATEGORY_ID, key,
			    STORE_CATEGORY_BLOG, journal_name,
			    -1);

	return;
}

/* Parse the server's list of categories */

static void
parse_mwl_getcategories_request (SoupMessage *msg, gpointer data)
{
	SoupXmlrpcResponse *response;
	SoupXmlrpcValue *value;
	GHashTable *table;
	const gchar *journal_name;

	debug ("movabletype parse_mwl_getcategories_request()\n");

	if (!(response = net_msg_get_response (SOUP_XMLRPC_MESSAGE (msg))))
		return;
	
	value = soup_xmlrpc_response_get_value (response);
	if (!value)
	{
		g_warning ("No value in reponse");
		return;
	}

	journal_name = g_object_get_data (G_OBJECT (msg), "journal_name");

	soup_xmlrpc_value_get_struct (value, &table);

	g_hash_table_foreach (table, (GHFunc)get_cat, journal_name);

	g_hash_table_destroy (table);

	gtk_tree_model_filter_refilter (GTK_TREE_MODEL_FILTER (dc->category_store_filtered));
	gtk_combo_box_set_active (GTK_COMBO_BOX (dc->journal_category), 0);

	g_object_unref (response);
	
	return;
}

/* Get the list of categories from the server */

DrivelRequest*
blog_mwl_build_getcategories_request (const gchar *username, 
				      const gchar *password, const gchar *uri, 
				      const gchar *journal_name, const gchar *blogid)
{
	SoupXmlrpcMessage *msg;

	debug ("blog_mwl_build_getcategories_request()");

	msg = xmlrpc_start ("metaWeblog.getCategories", uri);
	xmlrpc_add_string_param (msg, blogid);
	xmlrpc_add_string_param (msg, username);
	xmlrpc_add_string_param (msg, password);
	xmlrpc_end (msg);
	
	g_signal_connect (G_OBJECT (msg), "finished",
			  G_CALLBACK (parse_mwl_getcategories_request), NULL);

	g_object_set_data_full (G_OBJECT (msg), "journal_name", g_strdup (journal_name), g_free);

	net_enqueue_msg (SOUP_MESSAGE (msg));

	return NULL;
}

/* Parse the server's response to our setpostcategories request */

static void
parse_setpostcategories_request (SoupMessage *msg, gpointer data)
{
	SoupXmlrpcResponse *response;

	debug ("movabletype parse_setpostcategories_request()\n");

	if (!(response = net_msg_get_response (SOUP_XMLRPC_MESSAGE (msg))))
		return;
	
	g_object_unref (response);

	return;
}

/* Set the categories for a specific post */

DrivelRequest*
blog_mt_build_setpostcategories_request (const gchar *username, 
					 const gchar *password, const gchar *uri, 
					 const gchar *postid, const gchar *category)
{
	SoupXmlrpcMessage *msg;

	debug ("blog_mt_build_setpostcategories_request()");

	msg = xmlrpc_start ("mt.setPostCategories", uri);
	xmlrpc_add_string_param (msg, postid);
	xmlrpc_add_string_param (msg, username);
	xmlrpc_add_string_param (msg, password);
	xmlrpc_start_array_param (msg);
	soup_xmlrpc_message_start_struct (msg);
	xmlrpc_add_string_member (msg, "categoryId", category);
	xmlrpc_add_bool_member (msg, "isPrimary", TRUE);
	soup_xmlrpc_message_end_struct (msg);
	xmlrpc_end_array_param (msg);
	xmlrpc_end (msg);
	
	g_signal_connect (G_OBJECT (msg), "finished",
			  G_CALLBACK (parse_setpostcategories_request), NULL);

	net_enqueue_msg (SOUP_MESSAGE (msg));

	return NULL;
}

static void
parse_publish_request (SoupMessage *msg, gpointer data)
{
	SoupXmlrpcResponse *response;

	debug ("movabletype parse_publish_request()\n");

	if (!(response = net_msg_get_response (SOUP_XMLRPC_MESSAGE (msg))))
		return;
	
	g_object_unref (response);

	journal_finished_post (dc);
	
	return;
}

DrivelRequest*
blog_mt_build_publish_request (const gchar *username, const gchar *password,
			       const gchar *uri, const gchar *postid)
{
	SoupXmlrpcMessage *msg;

	debug ("blog_mt_build_publish_request()");

	msg = xmlrpc_start ("mt.publishPost", uri);
	xmlrpc_add_string_param (msg, postid);
	xmlrpc_add_string_param (msg, username);
	xmlrpc_add_string_param (msg, password);
	xmlrpc_end (msg);
	
	g_signal_connect (G_OBJECT (msg), "finished",
			  G_CALLBACK (parse_publish_request), NULL);

	net_enqueue_msg (SOUP_MESSAGE (msg));

	return NULL;
}

static void
parse_getevents_request (SoupMessage *msg, gpointer data)
{
	SoupXmlrpcResponse *response;
	SoupXmlrpcValue *value;
	SoupXmlrpcValueArrayIterator *array_iter;
	const gchar *last_entry;

	debug ("movabletype parse_getevents_request()\n");

	if (!(response = net_msg_get_response (SOUP_XMLRPC_MESSAGE (msg))))
		return;

	value = soup_xmlrpc_response_get_value (response);
	if (!value)
	{
		g_warning ("No value in reponse");
		return;
	}

	last_entry = g_object_get_data (G_OBJECT (msg), "last_entry");

	soup_xmlrpc_value_array_get_iterator (value, &array_iter);
	while (soup_xmlrpc_value_array_iterator_get_value (array_iter, &value) && value)
	{
		GHashTable *table;
		DrivelJournalEntry *entry = journal_entry_new ();

		soup_xmlrpc_value_get_struct (value, &table);

		value = g_hash_table_lookup (table, "postid");
		soup_xmlrpc_value_get_string (value, &entry->postid);
		value = g_hash_table_lookup (table, "description");
		soup_xmlrpc_value_get_string (value, &entry->content);
		value = g_hash_table_lookup (table, "userid");
		soup_xmlrpc_value_get_string (value, &entry->userid);
		value = g_hash_table_lookup (table, "title");
		if (value)
			soup_xmlrpc_value_get_string (value, &entry->subject);
		value = g_hash_table_lookup (table, "dateCreated");
		soup_xmlrpc_value_get_datetime (value, &entry->date_posted);
		
		if (last_entry)
		{
			journal_edit_entry (dc, entry->postid, entry->content, NULL, NULL,
					    entry->subject, NULL, NULL, NULL, NULL, NULL,
					    NULL, NULL);
			blog_mt_build_getpostcategories_request (dc->user->username,
								 dc->user->password,
								 dc->user->server,
								 g_strdup (entry->postid));
		}
		else
		{
			g_array_append_val (dc->recent_entries, entry);
		}

		g_hash_table_destroy (table);

		array_iter = soup_xmlrpc_value_array_iterator_next (array_iter);
	}
	
	if (!last_entry)
		journal_refresh_recent_entries (dc);

	g_object_unref (response);

	return;
}

DrivelRequest*
blog_mt_build_getevents_request (const gchar *username, const gchar *password,
				 const gchar *uri, const gchar *blogid, gboolean last_entry)
{
	SoupXmlrpcMessage *msg;
	gint howmany;

	debug ("blog_mt_build_getevents_request()");

	if (last_entry)
		howmany = 1;
	else
		howmany = DRIVEL_N_RECENT_POSTS;

	msg = xmlrpc_start ("metaWeblog.getRecentPosts", uri);
	xmlrpc_add_string_param (msg, blogid);
	xmlrpc_add_string_param (msg, username);
	xmlrpc_add_string_param (msg, password);
	xmlrpc_add_int_param (msg, howmany);
	xmlrpc_end (msg);
	
	g_signal_connect (G_OBJECT (msg), "finished",
			  G_CALLBACK (parse_getevents_request), NULL);

	g_object_set_data_full (G_OBJECT (msg), "blogid", g_strdup (blogid), g_free);
	if (last_entry)
		g_object_set_data_full (G_OBJECT (msg), "last_entry", g_strdup ("true"), g_free);

	net_enqueue_msg (SOUP_MESSAGE (msg));

	return NULL;
}

static void
parse_getpostcategories_request (SoupMessage *msg, gpointer data)
{
	SoupXmlrpcResponse *response;
	SoupXmlrpcValue *value;
	SoupXmlrpcValueArrayIterator *array_iter;
	gboolean cats = FALSE;

	debug ("movabletype parse_getpostcategories_request()\n");

	if (!(response = net_msg_get_response (SOUP_XMLRPC_MESSAGE (msg))))
		return;

	value = soup_xmlrpc_response_get_value (response);
	if (!value)
	{
		g_warning ("No value in reponse");
		return;
	}

	soup_xmlrpc_value_array_get_iterator (value, &array_iter);
	while (soup_xmlrpc_value_array_iterator_get_value (array_iter, &value) && value)
	{
		GHashTable *table;
		gchar *id;
		gboolean valid;
		GtkTreeIter iter;
		gint index = 0;
					
		soup_xmlrpc_value_get_struct (value, &table);
		
		value = g_hash_table_lookup (table, "categoryId");
		soup_xmlrpc_value_get_string (value, &id);

		valid = gtk_tree_model_get_iter_first (GTK_TREE_MODEL (dc->category_store), &iter);
		while (valid)
		{
			gchar *store_id;

			gtk_tree_model_get (GTK_TREE_MODEL (dc->category_store), &iter,
					    STORE_CATEGORY_ID, &store_id,
					    -1);
			if (store_id && !strcmp (store_id, id))
				valid = FALSE;
			else
			{
				index++;
				valid = gtk_tree_model_iter_next (GTK_TREE_MODEL (dc->category_store), &iter);
			}
			
			g_free (store_id);
		}
		
		gtk_combo_box_set_active (GTK_COMBO_BOX (dc->journal_category), index);

		cats = TRUE;
		g_free (id);
		g_hash_table_destroy (table);
		array_iter = soup_xmlrpc_value_array_iterator_next (array_iter);
	}

	if (!cats)
		gtk_combo_box_set_active (GTK_COMBO_BOX (dc->journal_category), 0);

	g_object_unref (response);

	return;
}

DrivelRequest*
blog_mt_build_getpostcategories_request (const gchar *username, const gchar *password, 
					 const gchar *uri, const gchar *postid)
{
	SoupXmlrpcMessage *msg;

	debug ("blog_mt_build_getpostcategories_request()");

	msg = xmlrpc_start ("mt.getPostCategories", uri);
	xmlrpc_add_string_param (msg, postid);
	xmlrpc_add_string_param (msg, username);
	xmlrpc_add_string_param (msg, password);
	xmlrpc_end (msg);
	
	g_signal_connect (G_OBJECT (msg), "finished",
			  G_CALLBACK (parse_getpostcategories_request), NULL);

	net_enqueue_msg (SOUP_MESSAGE (msg));

	return NULL;
}
