/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * Copyright 2005 Todd Kulesza
 *
 * Authors:
 * 		Todd Kulesza <todd@dropline.net>
 */

#include <config.h>

#include <string.h>
#include <libxml/parser.h>
#include <glib.h>

#include "atom.h"

/* Create a new AtomEntry item */
AtomEntry*
atom_entry_new (void)
{
	AtomEntry *entry;
	
	entry = g_new0 (AtomEntry, 1);
	entry->content = NULL;
	entry->id = NULL;
	entry->issued = NULL;
	entry->link = NULL;
	entry->title = NULL;
	
	return entry;
}

/* Free an AtomEntry item */
void
atom_entry_free (AtomEntry *entry)
{
	g_return_if_fail (entry);
	
	g_free (entry->content);
	g_free (entry->id);
	g_free (entry->issued);
	g_free (entry->link);
	g_free (entry->title);
	g_free (entry);
	
	return;
}

/* Build an XML packet from the AtomEntry data */
gchar*
atom_build_packet (AtomEntry *entry)
{
	xmlDocPtr doc;
	xmlNodePtr node, child;
	xmlChar *xml;
	gint bufsize;
	
	/* build the root entry node */
	doc = xmlNewDoc ((xmlChar *)"1.0");
	doc->encoding = xmlStrdup((xmlChar *)"UTF-8");
	doc->standalone = 1;
	node = xmlNewNode (NULL, (xmlChar *)"entry");
	xmlNewProp (node, (xmlChar *)"xmlns", (xmlChar *)"http://www.w3.org/2005/Atom");
	xmlDocSetRootElement (doc, node);
	
	/* add the AtomEntry elements */
	child = xmlNewTextChild (node, NULL, (xmlChar *)"title", (xmlChar *)entry->title);
	xmlNewProp (child, (xmlChar *)"type", (xmlChar *)"text/html");
/* 	xmlNewProp (child, (xmlChar *)"mode", (xmlChar *)"escaped"); */
/* 	xmlNewTextChild (node, NULL, (xmlChar *)"issued", (xmlChar *)entry->issued); */
	child = xmlNewTextChild (node, NULL, (xmlChar *)"generator", (xmlChar *)"Drivel");
	xmlNewProp (child, (xmlChar *)"version", (xmlChar *)VERSION);
	xmlNewProp (child, (xmlChar *)"uri", (xmlChar *)"http://www.dropline.net/drivel/");
	child = xmlNewTextChild (node, NULL, (xmlChar *)"content", (xmlChar *)entry->content);
	xmlNewProp (child, (xmlChar *)"type", (xmlChar *)"text/html");
	xmlNewProp (child, (xmlChar *)"mode", (xmlChar *)"escaped");
	child = xmlNewChild (node, NULL, (xmlChar *)"author", NULL);
	xmlNewTextChild (child, NULL, (xmlChar *)"name", " ");
	xmlNewTextChild (child, NULL, (xmlChar *)"email", " ");

	/* output the XML */
	xmlDocDumpFormatMemoryEnc (doc, &xml, &bufsize, "UTF-8", 0);
	
	xmlFreeDoc (doc);
	
	return (gchar *)xml;
}

/* Remove the <?xml> and <div> tags and convert <br/> to line-breaks */
static gchar*
tidy_xhtml (const gchar *xhtml)
{
	gchar *text, *xml, *bracket;
	gchar **lines;
	const gchar *tidy, *tag;

	tidy = xhtml;
	
	/* Remove the <?xml> tag */
	xml = strstr (tidy, "<?xml");
	if (xml)
	{
		bracket = strstr (xml, ">");
		if (bracket)
			tidy = ++bracket;
	}
	
	/* Remove the surrounding <div> tags */
	xml = strstr (tidy, "<div xmlns=\"http://www.w3.org/1999/xhtml\">");
	if (xml)
	{
		bracket = strstr (xml, ">");
		if (bracket)
			tidy = ++bracket;
	}
	/* If there was no div namespace tag, remove the surrounding content tags */
	else
	{
		xml = strstr (tidy, "<content");
		if (xml)
		{
			bracket = strstr (xml, ">");
			if (bracket)
				tidy = ++bracket;
		}
	}

	text = g_strdup (tidy);

	/* Search the last 20 characters for closing tags */
	tag = tidy + (strlen (tidy) - 20);
	xml = strstr (tag, "</div>");
	if (xml)
		text[xml - tidy] = '\0';
	/* If there was no closing div tag, look for a closing content tag */
	else
	{
		tag = tidy + (strlen (tidy) - 20);
		xml = strstr (tag, "</content>");
		if (xml)
			text[xml - tidy] = '\0';
	}

	/* Convert <br/> to line breaks */
	lines = g_strsplit (text, "\n", 0);
	g_free (text);
	text = g_strjoinv ("", lines);
	lines = g_strsplit (text, "<br/>", 0);
	g_free (text);
	text = g_strjoinv ("\n", lines);
	g_strfreev (lines);
	
	return text;
}

/* Parse an XML packet into an AtomEntry */
AtomEntry*
atom_entry_parse (xmlDocPtr doc, xmlNodePtr node)
{
	AtomEntry *entry;
	
	entry = atom_entry_new ();
	
	while (node)
	{
		if (!xmlStrcasecmp (node->name, (xmlChar *)"content"))
		{
			xmlDocPtr content;
			xmlNodePtr root;
			xmlChar *buffer;
			gint size;
			
			content = xmlNewDoc ((xmlChar *)"1.0");
			root = xmlDocCopyNode (node, content, 1);
			xmlDocSetRootElement (content, root);
			xmlDocDumpFormatMemoryEnc (content, &buffer, &size, "UTF-8", 0);
			entry->content = tidy_xhtml ((gchar *)buffer);

			xmlFree (buffer);
			xmlFreeDoc (content);
		}
		
		if (!xmlStrcasecmp (node->name, (xmlChar *)"id"))
			entry->id = (gchar *)xmlNodeListGetString (doc, node->xmlChildrenNode, 1);
		
		if (!xmlStrcasecmp (node->name, (xmlChar *)"issued"))
			entry->issued = (gchar *)xmlNodeListGetString (doc, node->xmlChildrenNode, 1);
		
		if (!xmlStrcasecmp (node->name, (xmlChar *)"link"))
		{
			gchar *rel = (gchar *)xmlGetProp (node, (xmlChar *)"rel");
			if (!xmlStrcasecmp ((xmlChar *)rel, (xmlChar *)"edit"))
				entry->link = (gchar *)xmlGetProp (node, (xmlChar *)"href");
			g_free (rel);
		}
		
		if (!xmlStrcasecmp (node->name, (xmlChar *)"title"))
			entry->title = (gchar *)xmlNodeListGetString (doc, node->xmlChildrenNode, 1);
		
		node = node->next;
	}
	
	return entry;
}
