/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * Copyright 2002-2004 Todd Kulesza
 *
 * Authors:
 * 		Todd Kulesza <todd@dropline.net>
 */

#include <config.h>

#include <stdio.h>
#include <unistd.h>
#define __USE_XOPEN
#include <time.h>
#include <gnome.h>
#include <glade/glade-xml.h>
#include <gdk-pixbuf/gdk-pixdata.h>
#include <gtksourceview/gtksourceview.h>
#include <gtksourceview/gtksourcelanguagesmanager.h>
#include <gtksourceview/gtksourcelanguage.h>
#include <string.h>
#include <strings.h>
#include <pwd.h>
#include <libxml/parser.h>
#ifdef HAVE_GTKSPELL
#include <gtkspell/gtkspell.h>
#endif /* HAVE_GTKSPELL */

#include "egg-recent.h"
#include "egg-datetime.h"
#include "drivel_request.h"
#include "blog_advogato.h"
#include "blog_atom.h"
#include "blog_blogger.h"
#include "blog_lj.h"
#include "blog_mt.h"
#include "insert_poll_dialog.h"
#include "query_music_players.h"
#include "tray.h"
#include "drivel.h"
#include "about.h"
#include "login.h"
#include "network.h"
#include "dialogs.h"
#include "journal.h"

typedef enum
{
	MENU_POST,
	MENU_UPDATE,
	MENU_DELETE,
	MENU_LAST,
	MENU_FRIENDS,
	MENU_HISTORY,
	MENU_SECURITY,
	MENU_ACTIVE_JOURNAL,
	MENU_VIEW_OPTIONS,
	OPTION_PICTURE
} WidgetType;

extern GMutex *net_mutex;

DrivelJournalProp*
journal_prop_new (void)
{
	DrivelJournalProp *prop;
	
	prop = g_new0 (DrivelJournalProp, 1);
	
	prop->id = NULL;
	prop->name = NULL;
	prop->value = NULL;
	
	return prop;
}

void
journal_prop_free (DrivelJournalProp *prop)
{
	if (!prop)
		return;
	
	g_free (prop->id);
	g_free (prop->name);
	g_free (prop->value);
	g_free (prop);
	
	return;
}

DrivelJournalEntry*
journal_entry_new (void)
{
	DrivelJournalEntry *entry;
	
	entry = g_new0 (DrivelJournalEntry, 1);
	
	entry->subject = NULL;
	entry->content = NULL;
	entry->security = NULL;
	entry->security_mask = NULL;
	entry->postid = NULL;
	entry->userid = NULL;
	entry->issued = NULL;
	entry->link = NULL;
	entry->properties = g_array_new (FALSE, TRUE, sizeof (DrivelJournalProp *));
	
	return entry;
}

void
journal_entry_free (DrivelJournalEntry *entry)
{
	if (!entry)
		return;
	
	g_free (entry->subject);
	g_free (entry->content);
	g_free (entry->security);
	g_free (entry->security_mask);
	g_free (entry->postid);
	g_free (entry->userid);
	g_free (entry->issued);
	g_free (entry->link);
	while (entry->properties->len)
	{
		DrivelJournalProp *prop;
		
		prop = g_array_index (entry->properties, DrivelJournalProp*, 0);
		g_array_remove_index (entry->properties, 0);
		journal_prop_free (prop);
	}
	
	g_free (entry);
	
	return;
}

static gchar*
journal_entry_get_prop (DrivelJournalEntry *entry, gchar *name)
{
	gchar *value = NULL;
	
	if (entry->properties)
	{
		gint i;
		
		for (i = 0; (i < entry->properties->len) && !value; i++)
		{
			DrivelJournalProp *prop;
			
			prop = g_array_index (entry->properties, DrivelJournalProp*, i);
			if (!strcasecmp (prop->name, name))
				value = g_strdup (prop->value);
		}
	}
	
	return value;
}

/* Callback for loading the specified recent entry into the journal editor */
static void
recent_entry_cb (GtkAction *action, gpointer data)
{
	gchar *mood, *music, *picture, *eventtime, *comments, *autoformat;
	DrivelJournalEntry *entry = (DrivelJournalEntry*) data;
	DrivelClient *dc = g_object_get_data (G_OBJECT (action), "dc");
	
	mood = journal_entry_get_prop (entry, "mood");
	music = journal_entry_get_prop (entry, "music");
	picture = journal_entry_get_prop (entry, "picture");
	eventtime = journal_entry_get_prop (entry, "eventtime");
	comments = journal_entry_get_prop (entry, "comments");
	autoformat = journal_entry_get_prop (entry, "autoformat");
	
	journal_edit_entry (dc, entry->postid, entry->content, entry->security, 
				entry->security_mask, entry->subject, mood, music, picture, 
				eventtime, comments, autoformat, entry->link);
	
	return;
}

static void
recent_entry_refresh_cb (GtkAction *action, gpointer data)
{
	DrivelClient *dc = (DrivelClient *)data;
	
	clear_recent_entries (dc->recent_entries);
	
	/* get the recent entries for this journal */
	switch (dc->user->api)
	{
	case BLOG_API_ATOM:
		blog_atom_build_getevents_request (dc->user->username,
						   dc->user->password, dc->active_journal->uri_feed, FALSE);
		break;
	case BLOG_API_BLOGGER:
		blog_blogger_build_getevents_request (dc->user->username,
						      dc->user->password, dc->user->server, 
						      dc->active_journal->id, DRIVEL_N_RECENT_POSTS);
		break;
	case BLOG_API_LJ:
		blog_lj_build_getevents_request (dc->user->username, 
						 dc->user->server, 0, FALSE, FALSE, "lastn", NULL, 0, 0, 0, 
						 DRIVEL_N_RECENT_POSTS, NULL, 0, dc->active_journal);
		break;
	case BLOG_API_MT:
		blog_mt_build_getevents_request (dc->user->username,
						 dc->user->password, dc->user->server, 
						 dc->active_journal->id, FALSE);
		break;
	default:
		break;
	}
	
	return;
}

/* Refresh the Recent Entries menu */
void
journal_refresh_recent_entries (DrivelClient *dc)
{
	GtkAction *action;
	GtkActionGroup *action_group;
	gint i, j;
	gchar *blogid, **ui_descriptions, *ui_description;
	GError *error;
	static guint merge_id = 0;

	if (!dc->active_journal)
		return;
	
	debug ("journal_refresh_recent_entries()");

	/* Remove the existing Recent Entries menu. */
	/* For some reason, we need to call gtk_ui_manager_get_ui() before
	 * building the new menu, otherwise it places the "Refresh" menuitem
	 * at the top of the menu. */
	if (merge_id)
	{
		gchar *ui;
		gtk_ui_manager_remove_ui (dc->menus, merge_id);
		ui = gtk_ui_manager_get_ui (dc->menus);
		g_free (ui);
	}

	switch (dc->user->api)
	{
		case BLOG_API_ATOM:
			blogid = g_strdup (dc->active_journal->uri_feed);
			break;
		case BLOG_API_BLOGGER:
		case BLOG_API_MT:
			blogid = g_strdup (dc->active_journal->id);
			break;
		case BLOG_API_LJ: 
			blogid = g_strdup (dc->active_journal->name);
			break;
		default:
			blogid = NULL;
			break;
	}
	
	ui_descriptions = g_new0 (gchar *, DRIVEL_N_RECENT_POSTS + 3);
	ui_descriptions[0] = g_strdup (
		"<ui>"
		"  <menubar name='MainMenu'>"
		"    <menu action='JournalMenu'>"
		"      <menu action='RecentEntries'>");
	action_group = gtk_action_group_new ("RecentEntryActions");
	gtk_action_group_set_translation_domain (action_group, GETTEXT_PACKAGE);

	for (i = 0, j = 1; (i < dc->recent_entries->len) && 
		(j < DRIVEL_N_RECENT_POSTS + 1); i++)
	{
		DrivelJournalEntry *entry;
		
		entry = g_array_index (dc->recent_entries, DrivelJournalEntry*, i);
		if (!strcmp (blogid, entry->userid))
		{
			gchar *name, *label;
			
			name = g_strdup_printf ("RecentEntriesItem%d", j);
			if (entry->subject && (entry->subject[0] != '\0'))
				label = g_strdup (entry->subject);
			else
				label = title_from_content (entry->content);
			action = gtk_action_new (name, label, 
					N_("Open this entry for editing"), NULL);
			g_object_set_data (G_OBJECT (action), "dc", dc);
			g_signal_connect (G_OBJECT (action), "activate",
						G_CALLBACK (recent_entry_cb), entry);
			gtk_action_group_add_action (action_group, action);
			
			ui_descriptions[j] = g_strdup_printf ("<menuitem action='%s'/>",
					name);
			j++;
		}
	}

	for (i = j; i < DRIVEL_N_RECENT_POSTS + 1; i++)
		ui_descriptions[i] = g_strdup ("");
	
	action = gtk_action_new ("RefreshRecentEntries", NULL, 
			N_("Refresh this list"), GTK_STOCK_REFRESH);
	g_signal_connect (G_OBJECT (action), "activate",
			G_CALLBACK (recent_entry_refresh_cb), dc);
	gtk_action_group_add_action (action_group, action);
	
	ui_descriptions[DRIVEL_N_RECENT_POSTS + 1] = g_strdup (
		"        <separator name='refresh'/>"
		"        <menuitem action='RefreshRecentEntries'/>"
		"      </menu>"
		"    </menu>"
		"  </menubar>"
		"</ui>");
	ui_descriptions[DRIVEL_N_RECENT_POSTS + 2] = NULL;
	
	ui_description = g_strjoinv (NULL, ui_descriptions);
	gtk_ui_manager_insert_action_group (dc->menus, action_group, 0);
	
	error = NULL;
	merge_id = gtk_ui_manager_add_ui_from_string (dc->menus, ui_description, -1, &error);
	if (!merge_id)
	{
		g_message ("building recent entries menu failed: %s", error->message);
		g_error_free (error);
	}
	
	g_strfreev (ui_descriptions);
	
	return;
}

void
remove_autosave (DrivelClient *dc)
{
	gchar *autosave_path, *filename;
	
	debug ("remove_autosave()");
	
	filename = g_strdup_printf ("autosave_%s", dc->user->username);
	autosave_path = g_build_filename (dc->config_directory, filename, NULL);
	
	gnome_vfs_unlink (autosave_path);
	dc->modified_autosave = FALSE;
	
	g_free (filename);
	g_free (autosave_path);
	
	return;
}

static void
set_journal_title (const DrivelJournal *active_journal, GtkWindow *window, DrivelBlogAPI api)
{
	gchar *title;
	
	g_return_if_fail (active_journal);
	g_return_if_fail (window);

	debug ("set_journal_title()");
	
	switch (api)
	{
		case BLOG_API_LJ:
		case BLOG_API_ADVOGATO:
		{
			if (active_journal->type == JOURNAL_TYPE_USER)
				title = g_strdup_printf (_("%s's Journal - Drivel"), 
							 active_journal->description);
			else
				title = g_strdup_printf (_("The %s Community - Drivel"), 
							 active_journal->name);
			
			break;
		}
		case BLOG_API_ATOM:
		case BLOG_API_BLOGGER:
		case BLOG_API_MT:
		{

			title = g_strdup_printf ("%s - Drivel", active_journal->name);
			break;
		}
		case BLOG_API_OFFLINE:
		{
			title = g_strdup (_("Offline - Drivel"));
			break;
		}
		default:
		{
			title = NULL;
			break;
		}
	}
	
	gtk_window_set_title (window, title);
	
	g_free (title);
	
	return;
}

static gboolean
supported_by_api (WidgetType w, DrivelBlogAPI api)
{
	gboolean retval = TRUE;
	
	switch (w)
	{
		case MENU_POST:
		{
			switch (api)
			{
				case BLOG_API_OFFLINE: retval = FALSE; break;
				default: break;
			}
			break;
		}
		case MENU_UPDATE:
		{
			switch (api)
			{
				case BLOG_API_OFFLINE: retval = FALSE; break;
				default: break;
			}
			break;
		}
		case MENU_DELETE:
		{
			switch (api)
			{
				case BLOG_API_ADVOGATO:
				case BLOG_API_OFFLINE: retval = FALSE; break;
				default: break;
			}
			break;
		}
		case MENU_LAST:
		{
			switch (api)
			{
				case BLOG_API_OFFLINE: retval = FALSE; break;
				default: break;
			}
			break;
		}
		case MENU_FRIENDS:
		{
			switch (api)
			{
				case BLOG_API_ADVOGATO:
				case BLOG_API_ATOM:
				case BLOG_API_BLOGGER:
				case BLOG_API_MT:
				case BLOG_API_OFFLINE: retval = FALSE; break;
				default: break;
			}
			break;
		}
		case MENU_HISTORY:
		{
			switch (api)
			{
				case BLOG_API_ADVOGATO:
				case BLOG_API_ATOM:
				case BLOG_API_BLOGGER:
				case BLOG_API_MT:
				case BLOG_API_OFFLINE: retval = FALSE; break;
				default: break;
			}
			break;
		}
		case MENU_SECURITY:
		{
			switch (api)
			{
				case BLOG_API_ADVOGATO:
				case BLOG_API_ATOM:
				case BLOG_API_BLOGGER:
				case BLOG_API_MT:
				case BLOG_API_OFFLINE: retval = FALSE; break;
				default: break;
			}
			break;
		}
		case MENU_ACTIVE_JOURNAL:
		{
			switch (api)
			{
				case BLOG_API_ADVOGATO: retval = FALSE; break;
				default: break;
			}
			break;
		}
		case MENU_VIEW_OPTIONS:
		{
			switch (api)
			{
				case BLOG_API_ADVOGATO:
				case BLOG_API_ATOM:
				case BLOG_API_BLOGGER: 
				case BLOG_API_OFFLINE: retval = FALSE; break;
				default: break;
			}
			break;
		}
		case OPTION_PICTURE:
		{
			switch (api)
			{
				case BLOG_API_ADVOGATO:
				case BLOG_API_ATOM:
				case BLOG_API_BLOGGER:
				case BLOG_API_MT:
				case BLOG_API_OFFLINE: retval = FALSE; break;
				default: break;
			}
			break;
		}
		default: break;
	}

	return retval;
}

static gchar*
parseElement (xmlDocPtr doc, xmlNodePtr cur)
{
	xmlChar *key;
	gchar *value;

	key = xmlNodeListGetString (doc, cur->xmlChildrenNode, 1);
	value = g_strdup ((gchar *)key);
	xmlFree (key);

	return value;
}

static gchar*
parseAttribute (xmlNodePtr cur, const char *attribute)
{
	xmlChar *attrib;
	gchar *value;

	attrib = xmlGetProp (cur, (xmlChar *)attribute);
	value = g_strdup ((gchar *)attrib);
	xmlFree (attrib);

	return value;
}

static gboolean
open_file (const gchar *filename, DrivelClient *dc)
{
	gchar input_data [4096];
	GnomeVFSHandle *handle;
	GnomeVFSFileSize bytes_read;
	GnomeVFSResult result;
	GtkTextBuffer *buffer;
	xmlDocPtr doc;
	xmlNodePtr cur;
	gchar *element;
	gboolean from_autosave = FALSE;
	GString *string;
	
	debug ("open_file()");
	
	/* if 'filename' is not null, this was called from the "Open Draft" dialog.
	 * otherwise, it was called by the autosave function. */
	if (filename)
		result = gnome_vfs_open (&handle, filename, GNOME_VFS_OPEN_READ);
	else
	{
		if (dc->config_directory)
		{
			gchar *file, *autosave;
			
			file = g_strdup_printf ("autosave_%s", dc->user->username);
			autosave = g_build_filename (dc->config_directory, file, NULL);
			result = gnome_vfs_open (&handle, autosave, GNOME_VFS_OPEN_READ);
			
			g_free (autosave);
			g_free (file);
			
			if (result != GNOME_VFS_OK)
				return FALSE;
			else
				from_autosave = TRUE;
		}
		else
			return FALSE;
	}
	
	if (result != GNOME_VFS_OK)
	{
		display_gnomevfs_error_dialog (dc, result);
		return FALSE;
	}
	
	result = gnome_vfs_seek (handle, GNOME_VFS_SEEK_START, 0);
	if (result != GNOME_VFS_OK)
	{
		display_gnomevfs_error_dialog (dc, result);
		gnome_vfs_close (handle);
		return FALSE;
	}
	
	string = g_string_new ("");
	do
	{
		result = gnome_vfs_read (handle, input_data, 4096, &bytes_read);
		if (result != GNOME_VFS_OK)
		{
			display_gnomevfs_error_dialog (dc, result);
			gnome_vfs_close (handle);
			return FALSE;
		}
		g_string_append_len (string, input_data, bytes_read);
	}
	while (bytes_read == 4096); /* if we read an entire block, reiterate */
	
	result = gnome_vfs_close (handle);
	if (result != GNOME_VFS_OK)
	{
		display_gnomevfs_error_dialog (dc, result);
		return FALSE;
	}

	/*
	 * it's useful to set these here
	 * if they have the appropriate tags we can override these defaults
	 */
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dc->journal_autoformat), FALSE);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dc->journal_comment), FALSE);
	gtk_combo_box_set_active (GTK_COMBO_BOX (dc->journal_security), 0);
	gtk_combo_box_set_active (GTK_COMBO_BOX (dc->journal_picture), 0);

	doc = xmlReadMemory (string->str, string->len, NULL, NULL, XML_PARSE_NOBLANKS);
	g_string_free (string, TRUE);

	if (doc == NULL)
	{
		display_open_error_dialog (dc, filename);
		return FALSE;
	}

	cur = xmlDocGetRootElement (doc);
	
	if (!cur || xmlStrcmp (cur->name, (const xmlChar *) "entry"))
	{
		display_open_error_dialog (dc, filename);
		xmlFreeDoc (doc);
		return FALSE;
	}
	
	gtk_source_buffer_begin_not_undoable_action (GTK_SOURCE_BUFFER (dc->buffer));
	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (dc->journal_text));
	
	cur = cur->xmlChildrenNode;
	while (cur != NULL)
	{
		if (!xmlStrcmp (cur->name, (const xmlChar *) "subject"))
		{
			element = parseElement (doc, cur);
			gtk_entry_set_text (GTK_ENTRY (dc->journal_subject), element);
			g_free (element);
		}
		else if (!xmlStrcmp (cur->name, (const xmlChar *) "event"))
		{
			element = parseElement (doc, cur);
			gtk_text_buffer_set_text (buffer, element, strlen (element));
			g_free (element);
		}
		else if (!xmlStrcmp (cur->name, (const xmlChar *) "music"))
		{
			element = parseElement (doc, cur);
			gtk_entry_set_text (GTK_ENTRY (GTK_BIN (dc->journal_music)->child), element);
			g_free (element);
		}
		else if (!xmlStrcmp (cur->name, (const xmlChar *) "mood"))
		{
			element = parseElement (doc, cur);
			gtk_entry_set_text (GTK_ENTRY (GTK_BIN (dc->journal_mood)->child), element);
			g_free (element);
		}
		else if (!xmlStrcmp (cur->name, (const xmlChar *) "security"))
		{
			element = parseAttribute (cur, "type");
			if (!strcmp (element, "friends"))
			{
				gtk_combo_box_set_active (GTK_COMBO_BOX (dc->journal_security), 1);
			}
			else if (!strcmp (element, "private"))
			{
				gtk_combo_box_set_active (GTK_COMBO_BOX (dc->journal_security), 2);
			}
			else if (!strcmp (element, "custom"))
			{
				g_message ("DEBUG: security type 'custom' not implemented, defaulting to private\n");
				gtk_combo_box_set_active (GTK_COMBO_BOX (dc->journal_security), 2);
			}
			else
			{
				g_message ("DEBUG: unknown security type '%s'\n", element);
			}
			g_free (element);
		}
		else if (!xmlStrcmp (cur->name, (const xmlChar *) "pickeyword"))
		{
			GtkTreeIter iter;
			gchar *value;

			element = parseElement (doc, cur);
			gtk_tree_model_get_iter_first (GTK_TREE_MODEL (dc->picture_store), &iter);

			do
			{
				gtk_tree_model_get (GTK_TREE_MODEL (dc->picture_store), &iter,
						1, &value,
						-1);
				if (!strcmp (element, value))
				{
					gtk_combo_box_set_active_iter (GTK_COMBO_BOX (dc->journal_picture), &iter);
					break;
				}
			}
			while (gtk_tree_model_iter_next (GTK_TREE_MODEL (dc->picture_store), &iter));
			
			g_free (element);
		}
		else if (!xmlStrcmp (cur->name, (const xmlChar *) "preformatted"))
		{
			gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dc->journal_autoformat), TRUE);
		}
		else if (!xmlStrcmp (cur->name, (const xmlChar *) "comments"))
		{
			element = parseAttribute (cur, "type");
			if (!strcmp (element, "disable"))
			{
				gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dc->journal_comment), TRUE);
			}
			else if (!strcmp (element, "noemail"))
			{
				g_message ("DEBUG: comment type 'noemail' not currently supported\n");
			}
			else
			{
				g_message ("DEBUG: unknown comment type '%s'\n", element);
			}
			g_free (element);
		}
		else if (!xmlStrcmp (cur->name, (const xmlChar *) "backdated"))
		{
			gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dc->journal_backdate), TRUE);
		}
		else if (!xmlStrcmp (cur->name, (const xmlChar *) "time"))
		{
			struct tm time;
			
			element = parseElement (doc, cur);
			if (!strptime (element, "%Y-%m-%d %H:%M:%S", &time))
				g_message ("DEBUG: something wrong with time string '%s'\n", element);
			g_free (element);
			egg_datetime_set_from_struct_tm (EGG_DATETIME (dc->journal_date), &time);
		}
		else
		{
			g_message ("DEBUG: unknown tag %s\n", cur->name);
		}
		cur = cur->next;
	}
	
	xmlFreeDoc (doc);
	
	dc->modified = from_autosave;
	remove_autosave (dc);
	
	gtk_source_buffer_end_not_undoable_action (GTK_SOURCE_BUFFER (dc->buffer));
	
	return TRUE;
}

static void
open_draft_cb (GtkWidget *widget, gpointer data)
{
	GtkWidget *dialog;
	GtkFileFilter *filter;
	DrivelClient *dc = (DrivelClient *) data;

	if (!display_save_dialog_proceed (dc))
		return;
	
	dialog = gtk_file_chooser_dialog_new (_("Drivel - Open Draft"), 
										  GTK_WINDOW (dc->current_window), 
										  GTK_FILE_CHOOSER_ACTION_OPEN,
										  GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
										  GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT,
										  NULL);
	gtk_dialog_set_default_response (GTK_DIALOG (dialog), GTK_RESPONSE_ACCEPT);

	filter = gtk_file_filter_new ();
	gtk_file_filter_set_name (filter, _("All XML Files"));
	gtk_file_filter_add_mime_type (filter, "text/xml");
	gtk_file_chooser_add_filter (GTK_FILE_CHOOSER (dialog), filter);
	filter = gtk_file_filter_new ();
	gtk_file_filter_set_name (filter, _("Drafts"));
	gtk_file_filter_add_mime_type (filter, "application/x-drivel");
	gtk_file_chooser_add_filter (GTK_FILE_CHOOSER (dialog), filter);
	gtk_file_chooser_set_filter (GTK_FILE_CHOOSER (dialog), filter);
	
	if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT)
	{
		gchar *filename, *uri;
		
		filename = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (dialog));
		open_file (filename, dc);
		
		uri = gnome_vfs_get_uri_from_local_path (filename);
		egg_recent_model_add (dc->recent_model, uri);
		g_free (uri);
		
		if (dc->draft_filename)
			g_free (dc->draft_filename);
		dc->draft_filename = g_strdup (filename);

		g_free (filename);
	}
	
	gtk_widget_destroy (dialog);
	
	return;
}

static gboolean
open_draft_recent_cb (GtkAction *action, gpointer data)
{
	EggRecentItem *item;
	gchar *uri;
	gboolean retval = TRUE;
	DrivelClient *dc = (DrivelClient *)data;
	
	item = egg_recent_view_uimanager_get_item (dc->recent_view, action);
	uri = egg_recent_item_get_uri (item);
	if (open_file (uri, dc))
		egg_recent_model_add (dc->recent_model, uri);
	else
		retval = FALSE;
	
	g_free (uri);
	
	return retval;
}

static gboolean
save_file (const gchar *filename, DrivelClient *dc)
{
	xmlDocPtr doc;
	xmlNodePtr cur, child;
	xmlChar *xmlbuf;
	int bufsize;
	gchar *output_data, *value;
	GtkTextBuffer *buffer;
	GtkTextIter start, end;
	GtkTreeIter iter;
	GnomeVFSHandle *handle;
	GnomeVFSFileSize written;
	GnomeVFSResult result;
	gint group, allowmask = 0;
	
	/* set up the document */
	doc = xmlNewDoc ((xmlChar *)"1.0");
	cur = xmlNewNode (NULL, (xmlChar *)"entry");
	xmlDocSetRootElement (doc, cur);
	
	/* populate XML */
	value = gtk_editable_get_chars (GTK_EDITABLE (dc->journal_subject), 0, -1);
	if (value)
	{
		if (strlen (value))
			xmlNewTextChild (cur, NULL, (xmlChar *)"subject", (xmlChar *)value);
		g_free (value);
	}
	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (dc->journal_text));
	gtk_text_buffer_get_bounds (buffer, &start, &end);
	value = gtk_text_buffer_get_text (buffer, &start, &end, FALSE);
	if (value)
	{
		if (strlen (value))
			xmlNewTextChild (cur, NULL, (xmlChar *)"event", (xmlChar *)value);
		g_free (value);
	}
	value = gtk_editable_get_chars (GTK_EDITABLE (GTK_BIN (dc->journal_mood)->child), 0, -1);
	if (value)
	{
		if (strlen (value))
			xmlNewTextChild (cur, NULL, (xmlChar *)"mood", (xmlChar *)value);
		g_free (value);
	}
	value = gtk_editable_get_chars (GTK_EDITABLE (GTK_BIN (dc->journal_music)->child), 0, -1);
	if (value)
	{
		if (strlen (value))
			xmlNewTextChild (cur, NULL, (xmlChar *)"music", (xmlChar *)value);
		g_free (value);
	}
	if (gtk_combo_box_get_active_iter (GTK_COMBO_BOX (dc->journal_picture), &iter))
	{
		gtk_tree_model_get (GTK_TREE_MODEL (dc->picture_store), &iter,
				1, &value,
				-1);
		if (value && strlen (value) && gtk_combo_box_get_active (GTK_COMBO_BOX (dc->journal_picture)))
			xmlNewTextChild (cur, NULL, (xmlChar *)"pickeyword", (xmlChar *)value);
	}
	if (gtk_combo_box_get_active_iter (GTK_COMBO_BOX (dc->journal_security), &iter))
	{
		gtk_tree_model_get (GTK_TREE_MODEL (dc->security_store), &iter, 
				STORE_SECURITY_NUM, &group, -1);
		switch (group)
		{
			case -10:
			{
				value = g_strdup ("public");
				break;
			}
			case -5:
			{
				value = g_strdup ("private");
				break;
			}
			case 0:
			{
				value = g_strdup ("friends");
				break;
			}
			default:
			{
				value = g_strdup ("custom");
				allowmask = 1 << group;
				break;
			}
		}
	}
	else
		value = NULL;
	if (value)
	{
		child = xmlNewTextChild (cur, NULL, (xmlChar *)"security", NULL);
		xmlNewProp (child, (xmlChar *)"type", (xmlChar *)value);
		g_free (value);
		value = g_strdup_printf ("%d", allowmask);
		xmlNewProp (child, (xmlChar *)"allowmask", (xmlChar *)value);
		g_free (value);
	}
	if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dc->journal_autoformat)))
		xmlNewTextChild (cur, NULL, (xmlChar *)"preformatted", NULL);
	if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dc->journal_comment)))
	{
		child = xmlNewTextChild (cur, NULL, (xmlChar *)"comments", NULL);
		xmlNewProp (child, (xmlChar *)"type", (xmlChar *)"disable");
	}
	if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dc->journal_backdate)))
	{
		GDate *date;
		gchar *date_string;
		
		xmlNewTextChild (cur, NULL, (xmlChar *)"backdated", NULL);
	
		date = g_date_new ();
		
		egg_datetime_get_as_gdate (EGG_DATETIME (dc->journal_date), date);
		/* FIXME: at the moment we don't support time */
		date_string = g_strdup_printf ("%i-%02i-%02i 00:00:00", date->year, date->month, date->day);
		xmlNewTextChild (cur, NULL, (xmlChar *)"time", (xmlChar *)date_string);
		g_free (date_string);
		g_date_free (date);
	}

	/* output the XML */
	xmlDocDumpFormatMemoryEnc (doc, &xmlbuf, &bufsize, "UTF-8", 1);
	output_data = g_strdup ((gchar *)xmlbuf);
	xmlFree (xmlbuf);
	xmlFreeDoc (doc);

	/* if 'filename' is not null, this was called from the "Save Draft" dialog.
	 * otherwise, it was called by the autosave function. */
	if (filename)
	{
		result = gnome_vfs_create (&handle, filename, GNOME_VFS_OPEN_WRITE, FALSE,
			GNOME_VFS_PERM_USER_READ | GNOME_VFS_PERM_USER_WRITE);
	}
	else
	{
		if (dc->config_directory)
		{
			gchar *file, *autosave;
			
			file = g_strdup_printf ("autosave_%s", dc->user->username);
			autosave = g_build_filename (dc->config_directory, file, NULL);
			result = gnome_vfs_create (&handle, autosave, GNOME_VFS_OPEN_WRITE, 
					FALSE, GNOME_VFS_PERM_USER_READ | GNOME_VFS_PERM_USER_WRITE);
			
			g_free (file);
			g_free (autosave);
		}
		else
			return FALSE;
	}
	
	if (result != GNOME_VFS_OK)
	{
		display_gnomevfs_error_dialog (dc, result);
		
		return FALSE;
	}
	
	result = gnome_vfs_seek (handle, GNOME_VFS_SEEK_START, 0);
	if (result != GNOME_VFS_OK)
	{
		display_gnomevfs_error_dialog (dc, result);
		
		return FALSE;
	}
	
	result = gnome_vfs_write (handle, output_data, strlen (output_data), &written);
	if (result != GNOME_VFS_OK)
	{
		display_gnomevfs_error_dialog (dc, result);
		
		return FALSE;
	}
	
	result = gnome_vfs_close (handle);
	if (result != GNOME_VFS_OK)
	{
		display_gnomevfs_error_dialog (dc, result);
		
		return FALSE;
	}
	
	if (filename)
	{
		dc->modified = FALSE;
		remove_autosave (dc);
	}
	
	g_free (output_data);
	
	return TRUE;
}

static gboolean
save_draft (DrivelClient *dc)
{
	GtkWidget *dialog;
	GtkFileFilter *filter;
	gboolean retval;
	
	debug ("save_draft()");

	/* FIXME: add something to override .drivel extension or something like that */
	dialog = gtk_file_chooser_dialog_new (_("Drivel - Save Draft"),
										  GTK_WINDOW (dc->current_window),
										  GTK_FILE_CHOOSER_ACTION_SAVE,
										  GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
										  GTK_STOCK_SAVE, GTK_RESPONSE_ACCEPT,
										  NULL);
	gtk_dialog_set_default_response (GTK_DIALOG (dialog), GTK_RESPONSE_ACCEPT);
	
	/* Add filters for all XML files all XML files which appear to be Drivel
	   Drafts */
	filter = gtk_file_filter_new ();
	gtk_file_filter_set_name (filter, _("All XML Files"));
	gtk_file_filter_add_mime_type (filter, "text/xml");
	gtk_file_chooser_add_filter (GTK_FILE_CHOOSER (dialog), filter);
	filter = gtk_file_filter_new ();
	gtk_file_filter_set_name (filter, _("Drafts"));
	gtk_file_filter_add_mime_type (filter, "application/x-drivel");
	gtk_file_chooser_add_filter (GTK_FILE_CHOOSER (dialog), filter);
	gtk_file_chooser_set_filter (GTK_FILE_CHOOSER (dialog), filter);
	
	if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT)
	{
		gchar *filename, *uri;
		
		filename = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (dialog));
		if (dc->draft_filename)
			g_free (dc->draft_filename);
		dc->draft_filename = filename;
		save_file (filename, dc);
		
		uri = gnome_vfs_get_uri_from_local_path (filename);
		egg_recent_model_add (dc->recent_model, uri);
		g_free (uri);
		
		retval = TRUE;
	}
	else
		retval = FALSE;
	
	gtk_widget_destroy (dialog);
	
	return retval;
}

gboolean
save_draft_cb (GtkWidget *widget, gpointer data)
{
	gboolean retval;
	DrivelClient *dc = (DrivelClient *) data;
	
	if (dc->draft_filename)
		retval = save_file (dc->draft_filename, dc);
	else
		retval = save_draft (dc);

	return retval;
}

static void
save_draft_as_cb (GtkWidget *menu, gpointer data)
{
	DrivelClient *dc = (DrivelClient *) data;
	
	save_draft (dc);
	
	return;
}

static gboolean
autosave_cb (gpointer data)
{
	DrivelClient *dc = (DrivelClient *) data;
	
	if (!dc->modified)
		return TRUE;
	
	save_file (NULL, dc);
	
	return TRUE;
}

static void
post_entry (DrivelClient *dc, DrivelRequestType mode)
{
	gchar *text, *mood, *mesg;
	gchar *music, *subject, *pic, *security, *key;
	GtkTextIter start, end;
	GtkTextBuffer *buffer;
	GtkTreeIter iter;
	gint i, group, allowmask, mood_id;
	gboolean autoformat, no_comments, backdate;
	
	debug ("post_entry()");
	
	allowmask = 0;
	
	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (dc->journal_text));
	gtk_text_buffer_get_bounds (buffer, &start,& end);
	text = gtk_text_buffer_get_text (buffer, &start, &end, FALSE);
	
	mood = gtk_editable_get_chars (GTK_EDITABLE (GTK_BIN (dc->journal_mood)->child), 0, -1);
	music = gtk_editable_get_chars (GTK_EDITABLE (GTK_BIN (dc->journal_music)->child), 0, -1);
	subject = gtk_editable_get_chars (GTK_EDITABLE (dc->journal_subject), 0, -1);
	if (gtk_combo_box_get_active_iter (GTK_COMBO_BOX (dc->journal_security), &iter))
	{
		gtk_tree_model_get (GTK_TREE_MODEL (dc->security_store), &iter, 
				STORE_SECURITY_NUM, &group, -1);
		switch (group)
		{
			case -10:
			{
				security = g_strdup ("public");
				break;
			}
			case -5:
			{
				security = g_strdup ("private");
				break;
			}
			default:
			{
				security = g_strdup ("usemask");
				allowmask = 1 << group;
				break;
			}
		}
	}
	else
		security = NULL;
	i = gtk_combo_box_get_active (GTK_COMBO_BOX (dc->journal_picture));
	key = g_strdup_printf ("pickw_%d", i);
	if (dc->picture_keywords)
		pic = g_hash_table_lookup (dc->picture_keywords, key);
	else
		pic = NULL;
	autoformat = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dc->journal_autoformat));
	no_comments = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dc->journal_comment));
	backdate = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dc->journal_backdate));
	
	/* get the current date/time, then adjust as needed */
	fill_time (dc);
	if (backdate)
	{
		egg_datetime_get_date (EGG_DATETIME (dc->journal_date),
				(GDateYear *)&(dc->time.year), (GDateMonth *)&(dc->time.month), 
				(GDateDay *)&(dc->time.day));
	}
	
	if (mood && (mesg = g_hash_table_lookup (dc->mood_icons, mood)))
		mood_id = (gint) g_ascii_strtod (mesg, NULL);
	else
		mood_id = 0;
	
	switch (mode)
	{
	case REQUEST_TYPE_POSTEVENT:
	{
		switch (dc->user->api)
		{
		case BLOG_API_LJ:
		{
			blog_lj_build_postevent_request (dc->user->username, 
							 dc->user->server, text, music, mood, mood_id, 
							 subject, security, allowmask, pic, dc->time.year, 
							 dc->time.month, dc->time.day, dc->time.hour, 
							 dc->time.minute, no_comments, autoformat, 
							 dc->active_journal, backdate);
			break;
		}
		case BLOG_API_MT:
		{
			blog_mt_build_postevent_request (dc->user->username,
							 dc->user->password, dc->user->server, 
							 dc->active_journal->id, FALSE, subject, text);
			break;
		}
		case BLOG_API_BLOGGER:
		{
			blog_blogger_build_postevent_request (
				dc->user->username, dc->user->password, 
				dc->user->server, dc->active_journal->id, TRUE, 
				text);
			break;
		}
		case BLOG_API_ADVOGATO:
		{
			blog_advogato_build_postevent_request (
				dc->user->cookie, dc->user->server, -1, subject, 
				text);
			break;
		}
		case BLOG_API_ATOM:
		{
			blog_atom_build_post_request (dc->user->username,
						      dc->user->password, dc->active_journal->uri_post, 
						      subject, text, NULL);
			break;
		}
		default:
		{
			g_warning ("post_entry: Unknown API");
		}
		}
		break;
	}
	case REQUEST_TYPE_EDITEVENT:
	{
		switch (dc->user->api)
		{
		case BLOG_API_LJ:
		{
			blog_lj_build_editevent_request (dc->user->username, 
							 dc->user->server, dc->journal_entry->postid, text, 
							 music, mood, mood_id, subject, security, allowmask, 
							 pic, dc->time.year, dc->time.month, dc->time.day, 
							 backdate, no_comments, autoformat, 
							 dc->active_journal);
			break;
		}
		case BLOG_API_BLOGGER:
		{
			blog_blogger_build_editevent_request (
				dc->user->username, dc->user->password, 
				dc->user->server, dc->journal_entry->postid, TRUE, 
				text);
			break;
		}
		case BLOG_API_ADVOGATO:
		{
			blog_advogato_build_postevent_request (
				dc->user->cookie, dc->user->server, 
				(guint) g_strtod (dc->journal_entry->postid, NULL), 
				subject, text);
			break;
		}
		case BLOG_API_ATOM:
		{
			blog_atom_build_post_request (dc->user->username,
						      dc->user->password, dc->active_journal->uri_post, 
						      subject, text, dc->journal_entry->link);
			break;
		}
		case BLOG_API_MT:
		{
			blog_mt_build_editevent_request (dc->user->username,
							 dc->user->password, dc->user->server, 
							 dc->journal_entry->postid, TRUE, subject, text);
			break;
		}
		default:
		{
			g_warning ("post_entry: Unknown API");
		}
		}
		break;
	}
	default:
	{
		g_warning ("post_entry: Unknown request type");
		break;
	}
	}
	
	g_free (text);
	g_free (mood);
	g_free (music);
	g_free (subject);
	g_free (security);
	g_free (key);
	
	return;
}

static void
delete_entry (DrivelClient *dc)
{
	if (dc->user->api == BLOG_API_LJ)
	{
		GtkTextIter start, end;
		GtkTextBuffer *buffer;
		
		gtk_source_buffer_begin_not_undoable_action (GTK_SOURCE_BUFFER (dc->buffer));
		
		buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (dc->journal_text));
		
		gtk_text_buffer_get_bounds (buffer, &start, &end);
		
		gtk_text_buffer_delete (buffer, &start, &end);
		
		post_entry (dc, REQUEST_TYPE_EDITEVENT);
	}
	else
	{
		switch (dc->user->api)
		{
		case BLOG_API_ATOM:
		{
			blog_atom_build_delete_request (dc->user->username,
							dc->user->password, dc->journal_entry->link);
			break;
		}
		case BLOG_API_BLOGGER:
		case BLOG_API_MT:
		{
			blog_blogger_build_deleteevent_request (
				dc->user->username, dc->user->password, 
				dc->user->server, dc->journal_entry->postid);
			break;
		}
		default: break;
		}
	}
	
	return;
}

/* display the default items for a blank journal entry and set the sensitivity
   of the widgets appropriately */

static void
journal_display_defaults (DrivelClient *dc)
{
	gchar *string;
	gint num;
	gboolean state;
	GtkTextIter start, end;
	GConfValue *value;

	debug ("journal_display_defaults()");
	
	journal_entry_free (dc->journal_entry);
	dc->journal_entry = journal_entry_new ();
	
	/* if the user saved a draft, clear it's filename so that she gets prompted
	   again. */
	if (dc->draft_filename)
	{
		g_free (dc->draft_filename);
		dc->draft_filename = NULL;
	}
	
	gtk_text_buffer_get_bounds (dc->buffer, &start, &end);
	
	gtk_text_buffer_delete (dc->buffer, &start, &end);
	
	gtk_entry_set_text (GTK_ENTRY (dc->journal_subject), "");

	string = get_default_text (dc->client, dc->gconf->default_mood, "");
	gtk_entry_set_text (GTK_ENTRY (GTK_BIN (dc->journal_mood)->child), string);
	g_free (string);

	string = get_default_text (dc->client, dc->gconf->default_music, "");
	gtk_entry_set_text (GTK_ENTRY (GTK_BIN (dc->journal_music)->child), string);
	g_free (string);
	
	gtk_combo_box_set_active (GTK_COMBO_BOX (dc->journal_picture),
			gconf_client_get_int (dc->client, dc->gconf->default_picture, NULL));

	string = gconf_client_get_string (dc->client, dc->gconf->default_security, NULL);
	num = gconf_client_get_int (dc->client, dc->gconf->default_security_mask, NULL);
	select_security_group (GTK_TREE_MODEL (dc->security_store),
			GTK_COMBO_BOX (dc->journal_security), string, num);
	g_free (string);
	
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dc->journal_comment),
			gconf_client_get_bool (dc->client, dc->gconf->default_comment, NULL));
	
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dc->journal_autoformat),
			gconf_client_get_bool (dc->client, dc->gconf->default_autoformat, NULL));
	
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dc->journal_backdate), FALSE);
	gtk_widget_set_sensitive (dc->journal_date, FALSE);
	
	gtk_combo_box_set_active (GTK_COMBO_BOX (dc->journal_category), 0);

	/* set menu sensitivity */
	gtk_widget_set_sensitive (dc->menu_post, FALSE);
	gtk_widget_set_sensitive (dc->menu_update, FALSE);
	gtk_widget_set_sensitive (dc->menu_delete, FALSE);
	gtk_widget_set_sensitive (dc->menu_undo, FALSE);
	gtk_widget_set_sensitive (dc->menu_redo, FALSE);
	gtk_widget_set_sensitive (dc->menu_cut, FALSE);
	gtk_widget_set_sensitive (dc->menu_copy, FALSE);
	gtk_widget_set_sensitive (dc->menu_last, 
			supported_by_api (MENU_LAST, dc->user->api));
	gtk_widget_set_sensitive (dc->menu_friends,
			supported_by_api (MENU_FRIENDS, dc->user->api));
	gtk_widget_set_sensitive (dc->menu_history,
			supported_by_api (MENU_HISTORY, dc->user->api));
	gtk_widget_set_sensitive (dc->menu_active,
			supported_by_api (MENU_ACTIVE_JOURNAL, dc->user->api));
	gtk_widget_set_sensitive (gtk_ui_manager_get_widget (
			dc->menus, "/MainMenu/ViewMenu/EntryOptions"),
			supported_by_api (MENU_VIEW_OPTIONS, dc->user->api));
			
	/* set the expander state */
	state = gconf_client_get_bool (dc->client, dc->gconf->expander_open, NULL);
	gtk_expander_set_expanded (GTK_EXPANDER (dc->journal_expander), state);
	gtk_toggle_action_set_active (GTK_TOGGLE_ACTION (dc->menu_view_options), state);
	
	/* enable/disable highlighting */
	gtk_toggle_action_set_active (GTK_TOGGLE_ACTION (dc->menu_view_misspelled_words),
			gconf_client_get_bool (dc->client, dc->gconf->spellcheck, NULL));
	value = gconf_client_get (dc->client, dc->gconf->highlight_syntax, NULL);
	if (value)
		state = gconf_value_get_bool (value);
	else
		state = TRUE;
	gtk_toggle_action_set_active (GTK_TOGGLE_ACTION (dc->menu_view_html_keywords), 
			state);
	
	/* set button sensitivity */
	gtk_widget_set_sensitive (dc->journal_picture, 
			supported_by_api (OPTION_PICTURE, dc->user->api));
	gtk_widget_set_sensitive (dc->journal_post, FALSE);
	
	gtk_widget_grab_focus (dc->journal_text);
	
	return;
}

void
journal_edit_entry (DrivelClient *dc, const gchar *itemid, const gchar *event, const gchar *security,
		const gchar *allowmask, const gchar *subject, const gchar *mood, const gchar *music, const gchar *picture,
		const gchar *eventtime, const gchar *comments, const gchar *autoformat, const gchar *link)
{
	gboolean bool_comments, bool_autoformat;
	gchar time [5], *pickw, *text;
	gint int_allowmask, i;
	
	debug ("journal_edit_entry()");
	
	time [4] = '\0';
	
	if (eventtime)
	{
		memcpy (time, eventtime, 4);
		dc->time.year = (gint) g_ascii_strtod (time, NULL);
		time [2] = '\0';
		memcpy (time, eventtime + 5, 2);
		dc->time.month = (gint) g_ascii_strtod (time, NULL);
		memcpy (time, eventtime + 8, 2);
		dc->time.day = (gint) g_ascii_strtod (time, NULL);
		memcpy (time, eventtime + 11, 2);
		dc->time.hour = (gint) g_ascii_strtod (time, NULL);
		memcpy (time, eventtime + 14, 2);
		dc->time.minute = (gint) g_ascii_strtod (time, NULL);
	}
	
	if (allowmask)
		int_allowmask = (gint)g_strtod (allowmask, NULL);
	else
		int_allowmask = 0;
	
	select_security_group (GTK_TREE_MODEL (dc->security_store), 
			GTK_COMBO_BOX (dc->journal_security), security, int_allowmask);
	
	if (comments)
		bool_comments = (gboolean) g_ascii_strtod (comments, NULL);
	else
		bool_comments = FALSE;
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dc->journal_comment),
			bool_comments);
	
	if (autoformat)
		bool_autoformat = (gboolean) g_ascii_strtod (autoformat, NULL);
	else
		bool_autoformat = FALSE;
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dc->journal_autoformat),
			bool_autoformat);

	if (picture)
	{
		for (i = 0; i < dc->pictures + 1; i++)
		{
			pickw = g_strdup_printf ("pickw_%d", i);
			text = g_hash_table_lookup (dc->picture_keywords, pickw);

			if (!strcmp (picture, text))
				gtk_combo_box_set_active (GTK_COMBO_BOX (dc->journal_picture), i);

			g_free (pickw);
		}
	}
	else
		gtk_combo_box_set_active (GTK_COMBO_BOX (dc->journal_picture), 0);
		
	if (!event)
		event = g_strdup ("");
	if (!subject)
		subject = g_strdup ("");
	if (!mood)
		mood = g_strdup ("");
	if (!music)
		music = g_strdup ("");
		
	gtk_text_buffer_set_text (dc->buffer, event, -1);
	
	gtk_entry_set_text (GTK_ENTRY (dc->journal_subject), subject);
	gtk_entry_set_text (GTK_ENTRY (GTK_BIN (dc->journal_mood)->child), mood);
	gtk_entry_set_text (GTK_ENTRY (GTK_BIN (dc->journal_music)->child), music);
	
	if (itemid)
		dc->journal_entry->postid = g_strdup (itemid);
	if (link)
		dc->journal_entry->link = g_strdup (link);
	
	gtk_widget_set_sensitive (dc->menu_post, FALSE);
	gtk_widget_set_sensitive (dc->menu_update, TRUE);
	gtk_widget_set_sensitive (dc->menu_delete, 
			supported_by_api (MENU_DELETE, dc->user->api));
	gtk_widget_hide (dc->journal_post);
	gtk_widget_set_sensitive (dc->edit_delete, 
			supported_by_api (MENU_DELETE, dc->user->api));
	gtk_widget_show (dc->edit_delete);
	gtk_widget_show (dc->edit_save);
	gtk_widget_show (dc->edit_cancel);
	
	gtk_widget_grab_default (dc->edit_save);
	gtk_widget_grab_focus (dc->journal_text);
	
	dc->modified = FALSE;

	gtk_source_buffer_end_not_undoable_action (GTK_SOURCE_BUFFER (dc->buffer));
	
	return;
}

void
journal_edit_entry_finished (DrivelClient *dc)
{
	debug ("journal_edit_entry_finished()");
	
	gtk_widget_set_sensitive (dc->menu_post, TRUE);
	gtk_widget_set_sensitive (dc->menu_update, FALSE);
	gtk_widget_set_sensitive (dc->menu_delete, FALSE);
	gtk_widget_hide (dc->edit_delete);
	gtk_widget_hide (dc->edit_save);
	gtk_widget_hide (dc->edit_cancel);
	gtk_widget_show (dc->journal_post);
	
	journal_display_defaults (dc);

	gtk_source_buffer_end_not_undoable_action (GTK_SOURCE_BUFFER (dc->buffer));
	
	return;
}

static void
proxy_changed_cb (GConfClient *client, guint id, GConfEntry *entry, gpointer data)
{
	GConfValue *value;
	gboolean state;
	DrivelClient *dc = (DrivelClient *) data;

	value = gconf_entry_get_value (entry);
	state = gconf_value_get_bool (value);
	
	g_mutex_lock (net_mutex);
	dc->proxy = state;
	g_mutex_unlock (net_mutex);
	
	return;
}

static void
proxy_user_changed_cb (GConfClient *client, guint id, GConfEntry *entry, gpointer data)
{
	GConfValue *value;
	const gchar *user;
	DrivelClient *dc = (DrivelClient *) data;

	value = gconf_entry_get_value (entry);
	user = gconf_value_get_string (value);
	
	g_mutex_lock (net_mutex);
	g_free (dc->proxy_user);
	dc->proxy_user = g_strdup (user);
	g_mutex_unlock (net_mutex);
	
	return;
}

static void
proxy_pass_changed_cb (GConfClient *client, guint id, GConfEntry *entry, gpointer data)
{
	GConfValue *value;
	const gchar *pass;
	DrivelClient *dc = (DrivelClient *) data;

	value = gconf_entry_get_value (entry);
	pass = gconf_value_get_string (value);
	
	g_mutex_lock (net_mutex);
	g_free (dc->proxy_pass);
	dc->proxy_pass = g_strdup (pass);
	g_mutex_unlock (net_mutex);
	
	return;
}
 
static void
proxy_url_changed_cb (GConfClient *client, guint id, GConfEntry *entry, gpointer data)
{
	GConfValue *value;
	const gchar *url;
	DrivelClient *dc = (DrivelClient *) data;

	value = gconf_entry_get_value (entry);
	url = gconf_value_get_string (value);
	
	g_mutex_lock (net_mutex);
	g_free (dc->proxy_url);
	dc->proxy_url = g_strdup (url);
	g_mutex_unlock (net_mutex);

	return;
}

static void
proxy_port_changed_cb (GConfClient *client, guint id, GConfEntry *entry, gpointer data)
{
	GConfValue *value;
	gint port;
	DrivelClient *dc = (DrivelClient *) data;

	value = gconf_entry_get_value (entry);
	port = gconf_value_get_int (value);
	
	g_mutex_lock (net_mutex);
	dc->proxy_port = port;
	g_mutex_unlock (net_mutex);
	
	return;
}

static void
tray_changed_cb (GConfClient *client, guint id, GConfEntry *entry, gpointer data)
{
	GConfValue *value;
	gboolean state;
	DrivelClient *dc = (DrivelClient *) data;

	value = gconf_entry_get_value (entry);
	state = gconf_value_get_bool (value);
	
	if (state && dc->journal_window)
		tray_turn_on (dc);
	else
		tray_turn_off (dc);
	
	return;
}

static void
expander_changed_cb (GConfClient *client, guint id, GConfEntry *entry, gpointer user_data)
{
	GConfValue *value;
	gboolean state;
	DrivelClient *dc = (DrivelClient *) user_data;
	
	/* if the journal hasn't been built yet, skip this */
	if (!dc->journal_window)
		return;
	
	value = gconf_entry_get_value (entry);
	state = gconf_value_get_bool (value);
	
	gtk_expander_set_expanded (GTK_EXPANDER (dc->journal_expander), state);
	gtk_toggle_action_set_active (GTK_TOGGLE_ACTION (dc->menu_view_options), state);
	
	return;
}

static void
highlight_syntax_changed_cb (GConfClient *client, guint id, GConfEntry *entry, gpointer user_data)
{
	GConfValue *value;
	gboolean state;
	DrivelClient *dc = (DrivelClient *) user_data;

	/* if the journal hasn't been built yet, skip this */
	if (!dc->journal_window)
		return;
	
	value = gconf_entry_get_value (entry);
	state = gconf_value_get_bool (value);

	gtk_source_buffer_set_highlight (GTK_SOURCE_BUFFER (dc->buffer), state);
	gtk_toggle_action_set_active (GTK_TOGGLE_ACTION (dc->menu_view_html_keywords), state);
	
	return;
}

#ifdef HAVE_GTKSPELL
static void
spell_language_select_menuitem (DrivelClient *dc, const gchar *lang)
{
	GtkComboBox *combo = GTK_COMBO_BOX (dc->pref_dictionary);
	GtkTreeModel *model;
	GtkTreeIter iter;
	gchar *tmp_lang;
	gint i = 0, found = -1;

	if (!combo)
		return;
	if (lang == NULL)
	{
		gtk_combo_box_set_active (combo, 0);
		return;
	}
	model = gtk_combo_box_get_model (combo);
	
	if (!gtk_tree_model_get_iter_first (model, &iter))
		return;
	
	do
	{
		gtk_tree_model_get (model, &iter, 0, &tmp_lang, -1);
		if (g_str_equal (tmp_lang, lang))
			found = i;
		g_free (tmp_lang);
		i++;
	} while (gtk_tree_model_iter_next (model, &iter) && found < 0);
	

	if (found >= 0)
		gtk_combo_box_set_active (combo, found);
	else
		g_warning ("Language %s from GConf isn't in the list of available languages\n", lang);
		
	return;
}
#endif /* HAVE_GTKSPELL */

static void
spell_language_changed_cb (GConfClient *client, guint id, GConfEntry *entry, gpointer user_data)
{
#ifdef HAVE_GTKSPELL
	DrivelClient *dc;
	GConfValue *value;
	GtkSpell *spell;
	const gchar *gconf_lang;
	gchar *lang;
	gboolean spellcheck_wanted;
	
	g_return_if_fail (user_data);
	
	dc = (DrivelClient *) user_data;
	value = gconf_entry_get_value (entry);
	gconf_lang = gconf_value_get_string (value);

	if (*gconf_lang == '\0' || gconf_lang == NULL)
		lang = NULL;
	else
		lang = g_strdup (gconf_lang);
	
	/* if the journal hasn't been built yet, skip this */
	if (dc->journal_window)
	{
		spellcheck_wanted = gconf_client_get_bool (dc->client, dc->gconf->spellcheck, NULL);
		spell = gtkspell_get_from_text_view (GTK_TEXT_VIEW (dc->journal_text));

		if (spellcheck_wanted)
		{
			if (spell && lang)
				/* Only if we have both spell and lang non-null we can use _set_language() */
				gtkspell_set_language (spell, lang, NULL);
			else 
			{
				/* We need to create a new spell widget if we want to use lang == NULL (use default lang)
				 * or if the spell isn't initialized */
				if (spell)
					gtkspell_detach (spell);
				spell = gtkspell_new_attach (GTK_TEXT_VIEW (dc->journal_text), lang, NULL);
			}
			gtkspell_recheck_all (spell);
			
		}
	}
	
	spell_language_select_menuitem ((DrivelClient *) user_data, lang);
	
	g_free (lang);
#endif /* HAVE_GTKSPELL */
	return;
}

static void
spellcheck_changed_cb (GConfClient *client, guint id, GConfEntry *entry, gpointer user_data)
{
#ifdef HAVE_GTKSPELL
	GConfValue *value;
	GtkSpell *spell;
	gboolean state;
	gchar *lang;
	DrivelClient *dc = (DrivelClient *) user_data;
	
	value = gconf_entry_get_value (entry);
	state = gconf_value_get_bool (value);
	
	/* if the preferences dialog exists, toggle the sensitivity of the 
	 * dictionary list */
	if (dc->pref_dictionary)
			gtk_widget_set_sensitive (dc->pref_dictionary_box, state);
	
	/* if the journal hasn't been built yet, skip this */
	if (!dc->journal_window)
		return;
	
	spell = gtkspell_get_from_text_view (GTK_TEXT_VIEW (dc->journal_text));
	lang = gconf_client_get_string (dc->client, dc->gconf->spell_language, NULL);
	
	if (state)
	{
		if (!spell)
			gtkspell_new_attach (GTK_TEXT_VIEW (dc->journal_text),
					     (lang == NULL || *lang == '\0') ? NULL : lang,
					     NULL);
	}
	else
	{
		if (spell)
			gtkspell_detach (spell);
	}
	
	gtk_toggle_action_set_active (GTK_TOGGLE_ACTION (dc->menu_view_misspelled_words), state);
	
#endif /* HAVE_GTKSPELL */
	
	return;
}

static void
mood_changed_cb (GConfClient *client, guint id, GConfEntry *entry, gpointer data)
{
	GConfValue *value;
	const gchar *string;
	DrivelClient *dc = (DrivelClient *) data;
	
	value = gconf_entry_get_value (entry);
	string = gconf_value_get_string (value);

	if (dc->journal_mood)
		gtk_entry_set_text (GTK_ENTRY (GTK_BIN (dc->journal_mood)->child),
				string);
	
	return;
}

static void
music_changed_cb (GConfClient *client, guint id, GConfEntry *entry, gpointer data)
{
	GConfValue *value;
	const gchar *string;
	DrivelClient *dc = (DrivelClient *) data;
	
	value = gconf_entry_get_value (entry);
	string = gconf_value_get_string (value);
	
	if (dc->journal_music)
		gtk_entry_set_text (GTK_ENTRY (GTK_BIN (dc->journal_music)->child), string);
	
	return;
}

static void
backdate_toggled_cb (GtkWidget *checkbox, gpointer data)
{
	gboolean active;
	DrivelClient *dc = (DrivelClient *)data;
	
	active = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (checkbox));
	gtk_widget_set_sensitive (dc->journal_date, active);
	
	return;
}

static void
picture_changed_cb (GConfClient *client, guint id, GConfEntry *entry, gpointer data)
{
	GConfValue *value;
	gint num;
	DrivelClient *dc = (DrivelClient *) data;
	
	value = gconf_entry_get_value (entry);
	num = gconf_value_get_int (value);
	
	if (dc->journal_picture)
		gtk_combo_box_set_active (GTK_COMBO_BOX (dc->journal_picture), num);
	
	return;
}

static void
security_changed_cb (GConfClient *client, guint id, GConfEntry *entry, gpointer data)
{
	gint mask;
	gchar *name;
	DrivelClient *dc = (DrivelClient *) data;
	
	if (dc->journal_security)
	{
		name = gconf_client_get_string (dc->client, dc->gconf->default_security, NULL);
		mask = gconf_client_get_int (dc->client, dc->gconf->default_security_mask, NULL);
		select_security_group (GTK_TREE_MODEL (dc->security_store),
				GTK_COMBO_BOX (dc->journal_security), name, mask);
	}
	
	return;
}

static void
comment_changed_cb (GConfClient *client, guint id, GConfEntry *entry, gpointer data)
{
	GConfValue *value;
	gboolean state;
	DrivelClient *dc = (DrivelClient *) data;
	
	value = gconf_entry_get_value (entry);
	state = gconf_value_get_bool (value);

	if (dc->journal_comment)
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dc->journal_comment),
				state);

	return;
}

static void
autoformat_changed_cb (GConfClient *client, guint id, GConfEntry *entry, gpointer data)
{
	GConfValue *value;
	gboolean state;
	DrivelClient *dc = (DrivelClient *) data;
	
	value = gconf_entry_get_value (entry);
	state = gconf_value_get_bool (value);

	if (dc->journal_autoformat)
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dc->journal_autoformat),
				state);

	return;
}

gboolean
delete_event_cb (GtkWidget *widget, GdkEventAny *event, gpointer data)
{
	DrivelClient *dc = (DrivelClient *) data;
	gboolean retval;

	retval = TRUE;
	
	if (display_save_dialog_close (dc))
	{
		journal_window_state_save (dc);
		gtk_main_quit ();
		retval = FALSE;
	}
	
	return retval;
}

void
exit_cb (GtkWidget *widget, gpointer data)
{
	gboolean cancel = FALSE;
	DrivelClient *dc = (DrivelClient *) data;

	if (dc->journal_window)
	{
		if (!display_save_dialog_close (dc))
			cancel = TRUE;
	}
	
	if (!cancel)
		gtk_main_quit ();
	
	return;
}

static void
undo_cb (GtkWidget *widget, gpointer user_data)
{
	DrivelClient *dc = (DrivelClient *) user_data;
	if (gtk_source_buffer_can_undo (GTK_SOURCE_BUFFER (dc->buffer)))
		gtk_source_buffer_undo (GTK_SOURCE_BUFFER (dc->buffer));
}

static void
redo_cb (GtkWidget *widget, gpointer user_data)
{
	DrivelClient *dc = (DrivelClient *) user_data;
	
	if (gtk_source_buffer_can_redo (GTK_SOURCE_BUFFER (dc->buffer)))
		gtk_source_buffer_redo (GTK_SOURCE_BUFFER (dc->buffer));
}

static void
undo_update_cb (GtkSourceBuffer *buffer, gboolean undoable, gpointer user_data)
{
	DrivelClient *dc = (DrivelClient *) user_data;

	gtk_widget_set_sensitive (dc->menu_undo, undoable);
}

static void
redo_update_cb (GtkSourceBuffer *buffer, gboolean redoable, gpointer user_data)
{
	DrivelClient *dc = (DrivelClient *) user_data;
	
	gtk_widget_set_sensitive (dc->menu_redo, redoable);
}

static void
logout_cb (GtkWidget *widget, gpointer data)
{
	DrivelClient *dc = (DrivelClient *) data;

	if (!display_save_dialog_close (dc))
		return;
	
#ifdef HAVE_GTKSPELL
	if (gconf_client_get_bool (dc->client, dc->gconf->spellcheck, NULL))
	{
		GtkSpell *spell;
		spell = gtkspell_get_from_text_view (GTK_TEXT_VIEW (dc->journal_text));
		if (spell)
			gtkspell_detach (spell);
	}
#endif /* HAVE_GTKSPELL */
	
	journal_window_state_save (dc);
	
	g_object_unref (dc->recent_model);
	
	gtk_widget_destroy (dc->journal_window);
	gtk_widget_show_all (dc->login_window);
	drivel_pop_current_window (dc);
	dc->journal_window = NULL;
	
	clear_recent_entries (dc->recent_entries);
	
	if (dc->picture_keywords)
	{
		hash_table_clear (dc->picture_keywords);
		dc->picture_keywords = NULL;
	}
	
	if (dc->journal_list)
	{
		g_slist_foreach (dc->journal_list, (GFunc) drivel_journal_free, NULL);
		g_slist_free (dc->journal_list);
		dc->journal_list = NULL;
	}
	dc->journals = 0;
	dc->active_journal = NULL;
	
	if (dc->menu_list)
	{
		g_slist_foreach (dc->menu_list, menu_list_free_item, NULL);
		g_slist_free (dc->menu_list);
		dc->menu_list = NULL;
	}
	
	g_source_remove (dc->tag_autosave);
	dc->time_since_checkfriends = 0;
	
	journal_entry_free (dc->journal_entry);
	dc->journal_entry = NULL;
	drivel_fill_journal_null (dc);
	
	/* clear the password if the user doesn't want it saved */
	if (!dc->user->save_password)
	{
		gtk_entry_set_text (GTK_ENTRY (dc->login_password), "");
		g_free (dc->user->password);
		dc->user->password = NULL;
	}
	
	tray_turn_off (dc);
	
	gtk_editable_select_region (GTK_EDITABLE (GTK_BIN (dc->login_name)->child), 0, -1);
	gtk_widget_grab_focus (GTK_BIN (dc->login_name)->child);

	return;
}

static void
text_changed_cb (GtkWidget *widget, gpointer data)
{
	gint text;
	DrivelClient *dc = (DrivelClient *) data;
	
	text = gtk_text_buffer_get_char_count (GTK_TEXT_BUFFER (widget));
	
	if (text)
	{
		dc->modified = TRUE;
		gtk_widget_set_sensitive (dc->journal_post, TRUE);
		gtk_widget_set_sensitive (dc->menu_post, TRUE &&
				supported_by_api (MENU_POST, dc->user->api));
	}
	else
	{
		dc->modified = FALSE;
		gtk_widget_set_sensitive (dc->journal_post, FALSE);
		gtk_widget_set_sensitive (dc->menu_post, FALSE);
	}
	
	return;
}

static void
insert_html (DrivelClient *dc, const gchar *open, const gchar *close)
{
	GtkTextIter start, end;
	GtkTextMark *mark_insert, *mark_selection;
	gboolean reverse = FALSE;

	mark_selection = gtk_text_buffer_get_selection_bound (dc->buffer);
	mark_insert = gtk_text_buffer_get_insert (dc->buffer);
	
	gtk_text_buffer_get_iter_at_mark (dc->buffer, &start, mark_selection);
	gtk_text_buffer_get_iter_at_mark (dc->buffer, &end, mark_insert);
	if (gtk_text_iter_compare (&start, &end) > 0)
	{
		gtk_text_buffer_get_iter_at_mark (dc->buffer, &start, mark_insert);
		reverse = TRUE;
	}
	gtk_text_buffer_insert (dc->buffer, &start, open, -1);

	if (!close)
		return;
		
	if (reverse)
		gtk_text_buffer_get_iter_at_mark (dc->buffer, &end, mark_selection);
	else
		gtk_text_buffer_get_iter_at_mark (dc->buffer, &end, mark_insert);
	gtk_text_buffer_insert (dc->buffer, &end, close, -1);
	
	if (reverse)
		gtk_text_buffer_get_iter_at_mark (dc->buffer, &end, mark_selection);
	else
		gtk_text_buffer_get_iter_at_mark (dc->buffer, &end, mark_insert);
	gtk_text_iter_backward_chars (&end, strlen (close));
	gtk_text_buffer_place_cursor (dc->buffer, &end);
	
	return;	
}

static void
skip_tag (DrivelClient *dc, gchar *tag)
{
	gchar *text, *search_text, *search_tag;
	GtkTextMark *mark_insert;
	GtkTextIter start, end;
	gint characters = 0, matched = 0, to_match;
	gunichar ch1, ch2;
	
	mark_insert = gtk_text_buffer_get_insert (dc->buffer);
	gtk_text_buffer_get_iter_at_mark (dc->buffer, &start, mark_insert);
	gtk_text_buffer_get_end_iter (dc->buffer, &end);
		
	text = gtk_text_buffer_get_text (dc->buffer, &start, &end, FALSE);
	search_text = text;
	search_tag = tag;
	ch2 = g_utf8_get_char (search_tag);
	to_match = g_utf8_strlen (tag, -1);
	
	while (*search_text)
	{
		characters++;
		ch1 = g_utf8_get_char (search_text);
		if (ch1 == ch2)
		{
			if (++matched == to_match)
				break;
			
			search_tag = g_utf8_next_char (search_tag);
			ch2 = g_utf8_get_char (search_tag);
		}
		else if (matched)
		{
			matched = 0;
			search_tag = tag;
			ch2 = g_utf8_get_char (search_tag);
		}
		
		search_text = g_utf8_next_char (search_text);
		ch1 = g_utf8_get_char (search_text);
	}
	
	gtk_text_iter_forward_chars (&start, characters);
	gtk_text_buffer_place_cursor (dc->buffer, &start);
	
	g_free (text);
	
	return;
}

static gboolean
inside_tag (DrivelClient *dc, const gchar *open, const gchar *close)
{
	GtkTextIter start, end;
	GtkTextMark *mark_insert;
	gchar *text, *location_close, *location_open;
	gboolean retval = FALSE;
	
	if (!close)
		return FALSE;
	
	if (gtk_text_buffer_get_selection_bounds (dc->buffer, &start, &end))
	{
		text = gtk_text_buffer_get_text (dc->buffer, &start, &end, FALSE);
		if (!strncmp (text, open, strlen (open)) &&
				!strncmp (text + strlen (text) - strlen (close), close, strlen (close)))
			retval = TRUE;
		g_free (text);
	}
	else
	{
		mark_insert = gtk_text_buffer_get_insert (dc->buffer);
		gtk_text_buffer_get_iter_at_mark (dc->buffer, &start, mark_insert);
		gtk_text_buffer_get_end_iter (dc->buffer, &end);
		
		text = gtk_text_buffer_get_text (dc->buffer, &start, &end, FALSE);
		
		location_close = strstr (text, close);
		location_open = strstr (text, open);
		if ((location_close && !location_open) ||
				(location_close && location_close < location_open))
			retval = TRUE;
		
		g_free (text);
	}
	
	return retval;
}

static void
handle_insert_html (DrivelClient *dc, gchar *open, gchar *close)
{
	if (inside_tag (dc, open, close))
		skip_tag (dc, close);
	else
		insert_html (dc, open, close);
	
	return;
}

static void
bold_cb (GtkWidget *menu, DrivelClient *dc)
{
	handle_insert_html (dc, "<strong>", "</strong>");
	
	return;
}

static void
italic_cb (GtkWidget *menu, DrivelClient *dc)
{
	handle_insert_html (dc, "<em>", "</em>");
	
	return;
}

static void
underline_cb (GtkWidget *menu, DrivelClient *dc)
{
	handle_insert_html (dc, "<u>", "</u>");
	
	return;
}

static void
strikethrough_cb (GtkWidget *menu, DrivelClient *dc)
{
	handle_insert_html (dc, "<strike>", "</strike>");
	
	return;
}

static void
superscript_cb (GtkWidget *menu, DrivelClient *dc)
{
	handle_insert_html (dc, "<sup>", "</sup>");
	
	return;
}

static void
subscript_cb (GtkWidget *menu, DrivelClient *dc)
{
	handle_insert_html (dc, "<sub>", "</sub>");
	
	return;
}

static void
list_cb (GtkWidget *menu, DrivelClient *dc)
{
	handle_insert_html (dc, "<ul>", "</ul>");
	
	return;
}

static void
listitem_cb (GtkWidget *menu, DrivelClient *dc)
{
	handle_insert_html (dc, "<li>", "</li>");
	
	return;
}

static void
blockquote_cb (GtkWidget *menu, DrivelClient *dc)
{
	handle_insert_html (dc, "<blockquote>", "</blockquote>");
	
	return;
}

static void
grey_edit_menu (DrivelClient *dc)
{
	GtkClipboard *clipboard;
	
	if (gtk_text_buffer_get_selection_bounds (dc->buffer, NULL, NULL))
	{
		gtk_widget_set_sensitive (dc->menu_cut, TRUE);
		gtk_widget_set_sensitive (dc->menu_copy, TRUE);
	}
	else
	{
		gtk_widget_set_sensitive (dc->menu_cut, FALSE);
		gtk_widget_set_sensitive (dc->menu_copy, FALSE);
	}
	
	clipboard = gtk_clipboard_get (GDK_NONE);
	if (gtk_clipboard_wait_is_text_available (clipboard))
		gtk_widget_set_sensitive (dc->menu_paste, TRUE);
	else
		gtk_widget_set_sensitive (dc->menu_paste, FALSE);
	
	return;
}

static gboolean
text_button_release_cb (GtkWidget *text, GdkEventButton *event, gpointer data)
{
	DrivelClient *dc = (DrivelClient *) data;
	
	grey_edit_menu (dc);
	
	return FALSE;
}

static gboolean
text_key_release_cb (GtkWidget *text, GdkEventKey *event, gpointer data)
{
	DrivelClient *dc = (DrivelClient *) data;
	
	grey_edit_menu (dc);
	
	return TRUE;
}

void
add_gconf_notifies (DrivelClient *dc)
{
	dc->id.mood_id = gconf_client_notify_add (dc->client, dc->gconf->default_mood, mood_changed_cb,
			dc, NULL, NULL);
	dc->id.music_id = gconf_client_notify_add (dc->client, dc->gconf->default_music, music_changed_cb,
			dc, NULL, NULL);
	dc->id.picture_id = gconf_client_notify_add (dc->client, dc->gconf->default_picture, picture_changed_cb,
			dc, NULL, NULL);
	dc->id.security_id = gconf_client_notify_add (dc->client, dc->gconf->default_security, security_changed_cb,
			dc, NULL, NULL);
	dc->id.security_mask_id = gconf_client_notify_add (dc->client, dc->gconf->default_security_mask, security_changed_cb,
			dc, NULL, NULL);
	dc->id.comment_id = gconf_client_notify_add (dc->client, dc->gconf->default_comment, comment_changed_cb,
			dc, NULL, NULL);
	dc->id.autoformat_id = gconf_client_notify_add (dc->client, dc->gconf->default_autoformat, autoformat_changed_cb,
			dc, NULL, NULL);
	dc->id.proxy_id = gconf_client_notify_add (dc->client, "/system/http_proxy/use_http_proxy", proxy_changed_cb,
			dc, NULL, NULL);
	dc->id.proxy_user_id = gconf_client_notify_add (dc->client, "/system/http_proxy/authentication_user", proxy_user_changed_cb,
			dc, NULL, NULL);
	dc->id.proxy_pass_id = gconf_client_notify_add (dc->client, "/system/http_proxy/authentication_password", proxy_pass_changed_cb,
			dc, NULL, NULL);
	dc->id.proxy_url_id = gconf_client_notify_add (dc->client, "/system/http_proxy/host", proxy_url_changed_cb,
			dc, NULL, NULL);
	dc->id.proxy_port_id = gconf_client_notify_add (dc->client, "/system/http_proxy/port", proxy_port_changed_cb,
			dc, NULL, NULL);
	dc->id.tray_id = gconf_client_notify_add (dc->client, dc->gconf->tray, tray_changed_cb,
			dc, NULL, NULL);
	dc->id.expander_open_id = gconf_client_notify_add (dc->client, dc->gconf->expander_open, expander_changed_cb,
			dc, NULL, NULL);
	dc->id.highlight_syntax_id = gconf_client_notify_add (dc->client, dc->gconf->highlight_syntax, highlight_syntax_changed_cb,
			dc, NULL, NULL);
	dc->id.spellcheck_id = gconf_client_notify_add (dc->client, dc->gconf->spellcheck, spellcheck_changed_cb,
			dc, NULL, NULL);
	dc->id.spell_language_id = gconf_client_notify_add (dc->client, dc->gconf->spell_language, spell_language_changed_cb,
			dc, NULL, NULL);
			
	return;
}

void
remove_gconf_notifies (GConfClient *client, DrivelIDs *id)
{
	gconf_client_notify_remove (client, id->mood_id);
	gconf_client_notify_remove (client, id->music_id);
	gconf_client_notify_remove (client, id->picture_id);
	gconf_client_notify_remove (client, id->security_id);
	gconf_client_notify_remove (client, id->security_mask_id);
	gconf_client_notify_remove (client, id->comment_id);
	gconf_client_notify_remove (client, id->autoformat_id);
	gconf_client_notify_remove (client, id->proxy_id);
	gconf_client_notify_remove (client, id->proxy_user_id);
	gconf_client_notify_remove (client, id->proxy_pass_id);
	gconf_client_notify_remove (client, id->proxy_url_id);
	gconf_client_notify_remove (client, id->proxy_port_id);
	gconf_client_notify_remove (client, id->expander_open_id);
	gconf_client_notify_remove (client, id->highlight_syntax_id);
	gconf_client_notify_remove (client, id->spellcheck_id);
	gconf_client_notify_remove (client, id->spell_language_id);
	
	return;
}

static GtkWidget *
get_focused_widget (DrivelClient *dc)
{
	GtkWidget *focused;
	
	if (gtk_widget_is_focus (dc->journal_subject))
		focused = dc->journal_subject;
	else if (gtk_widget_is_focus (GTK_BIN (dc->journal_mood)->child))
		focused = GTK_BIN (dc->journal_mood)->child;
	else if (gtk_widget_is_focus (dc->journal_music))
		focused = dc->journal_music;
	else
		focused = dc->journal_text;
	
	return focused;
}

static void
cut_cb (GtkWidget *menu, gpointer data)
{
	GtkWidget *editable;
	GtkClipboard *clipboard;
	DrivelClient *dc = (DrivelClient *) data;
	
	editable = get_focused_widget (dc);
	
	if (GTK_IS_EDITABLE (editable))
		gtk_editable_cut_clipboard (GTK_EDITABLE (editable));
	else
	{
		clipboard = gtk_clipboard_get (GDK_NONE);
		gtk_text_buffer_cut_clipboard (dc->buffer, clipboard, TRUE);
	}
	
	gtk_widget_set_sensitive (dc->menu_cut, FALSE);
	gtk_widget_set_sensitive (dc->menu_copy, FALSE);
		
	return;
}

static void
copy_cb (GtkWidget *menu, gpointer data)
{
	GtkWidget *editable;
	GtkClipboard *clipboard;
	DrivelClient *dc = (DrivelClient *) data;
	
	editable = get_focused_widget (dc);
	
	if (GTK_IS_EDITABLE (editable))
		gtk_editable_copy_clipboard (GTK_EDITABLE (editable));
	else
	{
		clipboard = gtk_clipboard_get (GDK_NONE);
		gtk_text_buffer_copy_clipboard (dc->buffer, clipboard);
	}
	
	return;
}

static void
paste_cb (GtkWidget *menu, gpointer data)
{
	GtkWidget *editable;
	GtkClipboard *clipboard;
	DrivelClient *dc = (DrivelClient *) data;
	
	editable = get_focused_widget (dc);
	
	if (GTK_IS_EDITABLE (editable))
		gtk_editable_paste_clipboard (GTK_EDITABLE (editable));
	else
	{
		clipboard = gtk_clipboard_get (GDK_NONE);
		gtk_text_buffer_paste_clipboard (dc->buffer, clipboard, NULL, TRUE);
	}
	
	return;
}

static void
clear_cb (GtkWidget *menu, gpointer data)
{
	DrivelClient *dc = (DrivelClient *) data;
	
	if (display_save_dialog_proceed (dc))
		journal_display_defaults (dc);
	
	return;
}

static void
select_all_cb (GtkWidget *menu, gpointer data)
{
	GtkWidget *editable;
	GtkTextIter start, end;
	DrivelClient *dc = (DrivelClient *) data;
	
	editable = get_focused_widget (dc);
	
	if (GTK_IS_EDITABLE (editable))
		gtk_editable_select_region (GTK_EDITABLE (editable), 0, -1);
	else
	{
		gtk_text_buffer_get_bounds (dc->buffer, &start, &end);
		gtk_text_buffer_move_mark_by_name (dc->buffer, "insert", &start);
		gtk_text_buffer_move_mark_by_name (dc->buffer, "selection_bound", &end);	
	}
	
	return;
}

static void
insert_link_cb (GtkWidget *menu, gpointer data)
{
	DrivelClient *dc = (DrivelClient *) data;

	display_insert_link_dialog (dc);
	
	return;
}

static void
insert_image_cb (GtkWidget *menu, gpointer data)
{
	DrivelClient *dc = (DrivelClient *) data;
	
	display_insert_image_dialog (dc);
	
	return;
}

static void
insert_poll_cb (GtkWidget *menu, gpointer data)
{
	DrivelClient *dc = (DrivelClient *) data;
	
	display_insert_poll_dialog (dc);	
	
	return;
}

static void
edit_entry_cb (GtkWidget *widget, gpointer data)
{
	DrivelClient *dc = (DrivelClient *) data;

	if (!display_save_dialog_proceed (dc))
		return;
	
	dc->edit_entry = TRUE;

	gtk_source_buffer_begin_not_undoable_action (GTK_SOURCE_BUFFER (dc->buffer));

	switch (dc->user->api)
	{
	case BLOG_API_ADVOGATO:
	{
		blog_advogato_build_getevents_request (dc->user->username,
						       dc->user->server);
		break;
	}
	case BLOG_API_ATOM:
	{
		blog_atom_build_getevents_request (dc->user->username,
						   dc->user->password, dc->active_journal->uri_feed, TRUE);
		break;
	}
	case BLOG_API_BLOGGER:
	{
		blog_blogger_build_getevents_request (dc->user->username,
						      dc->user->password, dc->user->server, 
						      dc->active_journal->id, 1);
		break;
	}
	case BLOG_API_LJ:
	{
		blog_lj_build_getevents_request (dc->user->username, 
						 dc->user->server, 0, FALSE, FALSE, "one", NULL, 0, 0, 0, 0, 
						 NULL, -1, dc->active_journal);
		break;
	}
	case BLOG_API_MT:
	{
		blog_mt_build_getevents_request (dc->user->username,
						 dc->user->password, dc->user->server, 
						 dc->active_journal->id, TRUE);
		break;
	}
	default: g_warning ("edit_entry_cb: unknown api"); break;
	}
	
	return;
}

/* Setup window title, etc., for the selected journal */
static void
setup_active_journal (DrivelClient *dc, DrivelJournal *dj)
{
	g_return_if_fail (dc);
	g_return_if_fail (dj);
	
	dc->active_journal = dj;
	
	/* If the account has multiple journals, remember this as the most recently
	 * used. */
	if (dj->name && dj->type == JOURNAL_TYPE_USER)
	{
		gconf_client_set_string (dc->client, dc->gconf->last_journal, 
				dj->name, NULL);
	}
	
	/* Set the window's title to match the journal name and description. */
	set_journal_title (dc->active_journal, GTK_WINDOW (dc->journal_window),
			dc->user->api);
	
	/* Filter out categories that are not part of the selected journal */
	gtk_tree_model_filter_refilter (
			GTK_TREE_MODEL_FILTER (dc->category_store_filtered));
	gtk_combo_box_set_active (GTK_COMBO_BOX (dc->journal_category), 0);
	
	/* Reload the Recent Entries menu */
	journal_refresh_recent_entries (dc);
	
	return;
}

static void
active_journal_cb (GtkRadioAction *action, GtkRadioAction *current, gpointer data)
{
	DrivelClient *dc;
	DrivelJournal *dj;
	gboolean active;
	
	dj = (DrivelJournal *) data;
	dc = g_object_get_data (G_OBJECT (action), "dc");
	active = gtk_toggle_action_get_active (GTK_TOGGLE_ACTION (action));
	
	if (active)
		setup_active_journal (dc, dj);
	
	return;
}

void
edit_preferences_cb (GtkWidget *widget, gpointer data)
{
	GSList *user_list;
	DrivelClient *dc = (DrivelClient *) data;

	/* if we're at the login window, take the username and selected api and use
	   it to fill in the gconf information */
	if (!dc->journal_window && dc->user->username && strcmp (dc->user->username, ""))
	{
		drivel_gconf_data_fill (dc->gconf, dc->user->username, dc->user->api, 
				dc->client, &dc->id, dc);

		gconf_client_set_string (dc->client, "/apps/drivel/global_settings/current_user", dc->user->username, NULL);

		user_list = gconf_client_get_list (dc->client, "/apps/drivel/global_settings/user_list",
				GCONF_VALUE_STRING, NULL);
		if (user_list != NULL && g_slist_find_custom (user_list, dc->user->username, string_compare))
		{
		}
		else
		{
			user_list = g_slist_prepend (user_list, g_strdup (dc->user->username));
			gconf_client_set_list (dc->client, "/apps/drivel/global_settings/user_list", GCONF_VALUE_STRING, user_list, NULL);
		}
	}
	/* if there is no username, throw a fit */
	else if (!dc->user->username || !strcmp (dc->user->username, ""))
	{
		display_error_dialog (dc, 
				_("Please select a journal account"),
				_("Preferences apply to each account separately, so you must "
				"select your account before you can edit its preferences."));
		
		return;
	}
	
	display_edit_preferences_dialog (dc);
	
	return;
}

static void
edit_friends_cb (GtkWidget *widget, gpointer data)
{
	DrivelClient *dc = (DrivelClient *) data;
	
	blog_lj_build_getfriends_request (dc->user->username, 
					  dc->user->server, TRUE, FALSE);
	
	return;
}

static void
edit_history_cb (GtkWidget *widget, gpointer data)
{
	DrivelClient *dc = (DrivelClient *) data;
	
	display_edit_history_dialog (dc);
	
	return;
}

static void
edit_security_cb (GtkWidget *widget, gpointer data)
{
	DrivelClient *dc = (DrivelClient *)data;
	
	blog_lj_build_getfriends_request (dc->user->username,
					  dc->user->server, FALSE, TRUE);

	return;
}

static void
view_more_options_cb (GtkWidget *w, gpointer data)
{
	gboolean state;
	DrivelClient *dc = (DrivelClient *)data;
	
	state = gtk_toggle_action_get_active (GTK_TOGGLE_ACTION (dc->menu_view_options));
	gconf_client_set_bool (dc->client, dc->gconf->expander_open, state, NULL);
	
	return;
}

static void
view_misspelled_words_cb (GtkWidget *w, gpointer data)
{
	gboolean state;
	DrivelClient *dc = (DrivelClient *)data;
	
	state = gtk_toggle_action_get_active (GTK_TOGGLE_ACTION (dc->menu_view_misspelled_words));
	gconf_client_set_bool (dc->client, dc->gconf->spellcheck, state, NULL);
	
	return;
}

static void
view_html_keywords_cb (GtkWidget *w, gpointer data)
{
	gboolean state;
	DrivelClient *dc = (DrivelClient *)data;
	
	state = gtk_toggle_action_get_active (GTK_TOGGLE_ACTION (dc->menu_view_html_keywords));
	gconf_client_set_bool (dc->client, dc->gconf->highlight_syntax, state, NULL);
	
	return;
}

static void
journal_post_cb (GtkWidget *w, gpointer data)
{
	DrivelClient *dc = (DrivelClient *) data;
	DrivelRequestType request;
	
	gtk_source_buffer_begin_not_undoable_action (GTK_SOURCE_BUFFER (dc->buffer));
	if (GTK_WIDGET_VISIBLE (dc->journal_post))
		request = REQUEST_TYPE_POSTEVENT;
	else
		request = REQUEST_TYPE_EDITEVENT;
	
	post_entry (dc, request);
	
	return;
}

static void
help_cb (GtkWidget *menu, gpointer data)
{
	gnome_help_display ("drivel.xml", NULL, NULL);
	
	return;
}

static void
faq_cb (GtkWidget *menu, gpointer data)
{
	gnome_help_display ("drivel.xml", "drivel-faq", NULL);
	
	return;
}

static void
expander_activate_cb (GtkWidget *expander, GParamSpec *param_spec, gpointer data)
{
	DrivelClient *dc = (DrivelClient *) data;

	gconf_client_set_bool (dc->client, dc->gconf->expander_open, 
			gtk_expander_get_expanded (GTK_EXPANDER (expander)), 
			NULL);

}

static void
journal_cancel_cb (GtkWidget *button, gpointer data)
{
	DrivelClient *dc = (DrivelClient *) data;

	gtk_source_buffer_begin_not_undoable_action (GTK_SOURCE_BUFFER (dc->buffer));
	journal_edit_entry_finished (dc);
	
	return;
}
	
static void
journal_delete_cb (GtkWidget *button, gpointer data)
{
	DrivelClient *dc = (DrivelClient*) data;
	
	delete_entry (dc);
	
	return;
}

void
journal_window_state_save (DrivelClient *dc)
{
	gboolean max;
	gint x, y, height, width;
	GdkWindowState window_state;
	
	debug ("journal_window_state_save()");
	
	/* save the window position and size */
	if (dc->journal_window)
	{
		gtk_window_get_position (GTK_WINDOW (dc->journal_window), &x, &y);
		gtk_window_get_size (GTK_WINDOW (dc->journal_window), &width, &height);
		window_state = gdk_window_get_state (dc->journal_window->window);
		max = (window_state & GDK_WINDOW_STATE_MAXIMIZED);
		if (!max)
		{
			gconf_client_set_int (dc->client, dc->gconf->entry_x, x, NULL);
			gconf_client_set_int (dc->client, dc->gconf->entry_y, y, NULL);
			gconf_client_set_int (dc->client, dc->gconf->entry_height, height, NULL);
			gconf_client_set_int (dc->client, dc->gconf->entry_width, width, NULL);
		}
		gconf_client_set_bool (dc->client, dc->gconf->entry_max, max, NULL);
	}
	
	return;
}

void
journal_window_state_restore (DrivelClient *dc)
{
	gboolean max;
	gint x, y, height, width;
	
	debug ("journal_window_state_restore()");
	
	if (dc->journal_window)
	{
		x = gconf_client_get_int (dc->client, dc->gconf->entry_x, NULL);
		y = gconf_client_get_int (dc->client, dc->gconf->entry_y, NULL);
		height = gconf_client_get_int (dc->client, dc->gconf->entry_height, NULL);
		width = gconf_client_get_int (dc->client, dc->gconf->entry_width, NULL);
		max = gconf_client_get_bool (dc->client, dc->gconf->entry_max, NULL);
		
		if (height < 5)
			height = 400;
		if (width < 5)
			width = 600;
		
		gtk_window_move (GTK_WINDOW (dc->journal_window), x, y);
		gtk_window_resize (GTK_WINDOW (dc->journal_window), width, height);
		if (max)
			gtk_window_maximize (GTK_WINDOW (dc->journal_window));
		
		gtk_window_present (GTK_WINDOW (dc->journal_window));
		if (dc->current_window != dc->journal_window)
			gtk_window_present (GTK_WINDOW (dc->current_window));
	}
	
	return;
}

/* Clears up the entry window following a successful post */

void
journal_finished_post (DrivelClient *dc)
{
	debug ("journal_finished_post()");
	
	journal_edit_entry_finished (dc);
	
	remove_autosave (dc);
	
	if (gconf_client_get_bool (dc->client, dc->gconf->min_post, NULL))
		gtk_window_iconify (GTK_WINDOW (dc->journal_window));
	recent_entry_refresh_cb(NULL,dc);
	
	return;
}

static gchar *
fix_mnemonic_string (const gchar *string)
{
	gchar *new_string;
	gint i, j, len, count;
	
	len = strlen (string);
	
	for (i = count = 0; i < len; i++)
		if (string [i] == '_')
			count++;
	
	new_string = g_new0 (gchar, len + count + 1);
	new_string [len + count] = '\0';
	
	for (i = j = 0; i < len; i++)
	{
		if (string [i] == '_')
			new_string [j++] = '_';
		new_string [j++] = string [i];
	}
	
	return new_string;
}

static gint
build_active_journal_menu (DrivelClient *dc)
{
	GtkActionGroup *action_group;
	GtkToggleAction *primary_journal;
	GSList *list, *group;
	gchar **ui_descriptions, *ui_description;
	gint i, last_active_journal;
	GError *error;
	
	debug ("build_active_journal_menu()");
	
	primary_journal = NULL;
	ui_descriptions = g_new0 (gchar *, dc->journals + 3);
	ui_descriptions[0] = g_strdup (
		"<ui>"
		"  <menubar name='MainMenu'>"
		"    <menu action='EditMenu'>"
		"      <menu action='ActiveJournal'>");
	
	action_group = gtk_action_group_new ("ActiveJournalActions");
	gtk_action_group_set_translation_domain (action_group, GETTEXT_PACKAGE);
	list = dc->journal_list;
	group = NULL;
	last_active_journal = 0;
	
	for (i = 0; i < dc->journals; i++)
	{
		GtkRadioAction *action;
		gchar *name, *label, *last_journal;
		DrivelJournal *dj;
		
		dj = list->data;
		name = g_strdup_printf ("ActiveJournalItem%d", i);
		label = fix_mnemonic_string (dj->name);
		debug (dj->name);
		
		action = gtk_radio_action_new (name, label, 
				N_("Select a journal to post to"), NULL, i);
		gtk_radio_action_set_group (action, group);
		group = gtk_radio_action_get_group (action);
		g_object_set_data (G_OBJECT (action), "dc", dc);
		g_signal_connect (G_OBJECT (action), "changed",
					G_CALLBACK (active_journal_cb), dj);
		gtk_action_group_add_action (action_group, GTK_ACTION (action));
		/* set the active user to the first journal */
		last_journal = gconf_client_get_string (dc->client, 
							dc->gconf->last_journal, NULL);
		if ((!i && !last_journal) || 
			(last_journal && !strcmp (last_journal, dj->name)))
		{
			last_active_journal = i;
			if (!i)
				dc->active_journal = dj;
		}
		
		ui_descriptions[i + 1] = g_strdup_printf ("<menuitem action='%s'/>", name);
		
		list = list->next;
		
		g_free (name);
		g_free (label);
		g_free (last_journal);
	}
	
	ui_descriptions[dc->journals + 1] = g_strdup (
		"      </menu>"
		"    </menu>"
		"  </menubar>"
		"</ui>");
	ui_descriptions[dc->journals + 2] = NULL;
	
	ui_description = g_strjoinv (NULL, ui_descriptions);
	gtk_ui_manager_insert_action_group (dc->menus, action_group, 0);
	
	error = NULL;
	if (!gtk_ui_manager_add_ui_from_string (dc->menus, ui_description, -1, &error))
	{
		g_message ("building active journal menu failed: %s", error->message);
		g_error_free (error);
	}
	
	g_strfreev (ui_descriptions);
	
	return last_active_journal;
}

static void
web_menu_cb (GtkWidget *menu, gpointer data)
{
	gchar *url = (gchar *) data;
	
	gnome_url_show (url, NULL);
	
	return;
}

static gboolean
music_combo_cb (GtkWidget *music, GdkEventCrossing *event, gpointer data)
{
	DrivelClient *dc = (DrivelClient *) data;
	GtkListStore *music_store;
	gint active;
	
	active = gtk_combo_box_get_active (GTK_COMBO_BOX (dc->journal_music));
	music_store = GTK_LIST_STORE (gtk_combo_box_get_model (GTK_COMBO_BOX (dc->journal_music)));
	query_music_players (music_store);
	gtk_combo_box_set_active (GTK_COMBO_BOX (dc->journal_music), active);
	if (!active)
		gtk_entry_set_text (GTK_ENTRY (GTK_BIN (dc->journal_music)->child), "");

	return FALSE;
}

static void
music_combo_changed_cb (GtkWidget *music, gpointer user_data)
{
	if (!gtk_combo_box_get_active (GTK_COMBO_BOX (music)))
		gtk_entry_set_text (GTK_ENTRY (GTK_BIN (music)->child), "");
}

static gchar *
create_mnemonic_string (const gchar *string, gchar **mnemonics)
{
	gint i, j, len;
	gchar *new_string, *fixed_string, needle [2];
	gboolean added = FALSE;
	
	needle [1] = '\0';
	
	fixed_string = fix_mnemonic_string (string);
	len = strlen (fixed_string);
	
	new_string = g_new0 (gchar, len + 2);
	j = 0;

	if (mnemonics [0][0] == '\0')
	{
		new_string [j++] = '_';
		mnemonics [0][0] = fixed_string [0];
		added = TRUE;
	}
	
	for (i = 0; i < len; i++)
	{
		if (!added)
		{
			needle [0] = fixed_string [i];

			if (!strstr (mnemonics [0], needle))
			{
				new_string [j++] = '_';
				added = TRUE;
				mnemonics [0][strlen (mnemonics [0])] = fixed_string [i];
				mnemonics [0][strlen (mnemonics [0]) + 1] = '\0';
			}
		}
		new_string [j++] = fixed_string [i];
	}

	return new_string;
}

static void
build_web_link_menu (DrivelClient *dc)
{
	GSList *list;
	LJMenuItem *menu;
	GtkActionGroup *action_group;
	gint i, j, k, *submenus, n_entries, n_separators, n_submenus, n_subitems;
	gchar *mnemonics, *ui_description, **ui_descriptions, **merge_descriptions;
	GError *error;
	
	debug ("build_web_link_menu()");
	
	n_entries = n_separators = n_submenus = n_subitems = 0;
	
	/* count the number of non-separator menu entries */
	for (list = dc->menu_list; list; list = list->next)
	{
		menu = list->data;
		if (strcmp (menu->label, "-"))
			n_entries++;
		else
			n_separators++;
		if (menu->sub_menu)
			n_submenus++;
		if (menu->menu_index)
			n_subitems++;
	}
	
	mnemonics = g_new0 (gchar, n_entries + 1);
	mnemonics [0] = '\0';
	
	ui_descriptions = g_new0 (gchar *, n_entries + n_separators + 3);
	ui_descriptions[0] = g_strdup (
		"<ui>"
		"  <menubar name='MainMenu'>"
		"    <menu action='WebMenu'>");
	
	/* submenus holds the index of the parent menu */
	submenus = g_new0 (gint, n_submenus);
	for (i = 0; i < n_submenus; i++)
		submenus[i] = 0;
	
	merge_descriptions = g_new0 (gchar *, n_subitems);
	
	action_group = gtk_action_group_new ("WebActions");
	gtk_action_group_set_translation_domain (action_group, GETTEXT_PACKAGE);

	for (i = 1, j = 1, k = 0, list = dc->menu_list; list; list = list->next)
	{
		menu = list->data;
		
		if (!strcmp (menu->label, "-"))
			ui_descriptions[i] = g_strdup_printf ("<separator name=\'sep%d\'/>", i);
		else
		{
			gchar *id, *label;
			GtkAction *action;
			
			id = g_strdup_printf ("WebItem%d", j);
			label = create_mnemonic_string (menu->label, &mnemonics);
			
			action = gtk_action_new (id, label, NULL, NULL);
			gtk_action_group_add_action (action_group, action);
			
			if (!menu->sub_menu)
			{
				g_signal_connect (G_OBJECT (action), "activate",
					G_CALLBACK (web_menu_cb), menu->url);
				
				if (menu->menu_index)
				{
					gchar *parent;
					
					parent = g_strdup_printf ("WebItem%d", 
							submenus[menu->menu_index]);
					ui_descriptions[i] = g_strdup ("");
					merge_descriptions[k] = g_strdup_printf (
							"<ui><menubar name='MainMenu'>"
							"  <menu action='WebMenu'>"
							"    <menu action='%s'>"
							"      <menuitem action='%s'/>"
							"    </menu>"
							"  </menu>"
							"</menubar></ui>",
							parent, id);
					
					g_free (parent);
					
					k++;
				}
				else
				{
					ui_descriptions[i] = g_strdup_printf (
							"<menuitem action=\'%s\'/>", id);
				}
			}
			else
			{
				submenus[menu->sub_menu] = j;
				ui_descriptions[i] = g_strdup_printf (
						"<menu action=\'%s\'></menu>", id);
			}
			
			g_free (id);
			g_free (label);
			
			j++;
		}
		i++;
	}

	ui_descriptions[n_entries + n_separators + 1] = g_strdup (
		"    </menu>"
		"  </menubar>"
		"</ui>");
	ui_descriptions[n_entries + n_separators + 2] = NULL;
	
	ui_description = g_strjoinv (NULL, ui_descriptions);

	gtk_ui_manager_insert_action_group (dc->menus, action_group, 0);
	
	error = NULL;
	if (!gtk_ui_manager_add_ui_from_string (dc->menus, ui_description, -1, &error))
	{
		g_message ("building web menu failed: %s", error->message);
		g_error_free (error);
	}
	for (i = 0; i < n_subitems; i++)
	{
		error = NULL;
		if (!gtk_ui_manager_add_ui_from_string (dc->menus, merge_descriptions[i], -1, &error))
		{
			g_message ("building web submenu %d failed: %s", i, error->message);
			g_error_free (error);
		}
	}
	
	g_strfreev (ui_descriptions);
	
	return;
}

void
journal_window_build (DrivelClient *dc)
{
	/*
	 * these are the menus
	 */
	static GtkActionEntry entries[] =
	{
		{ "JournalMenu", NULL, N_("_Journal") },
		{ "EditMenu", NULL, N_("_Edit") },
		{ "ViewMenu", NULL, N_("_View") },
		{ "FormatMenu", NULL, N_("_Format") },
		{ "WebMenu", NULL, N_("_Web Links"), },
		{ "HelpMenu", NULL, N_("_Help"), },
		{ "OpenDraft", GTK_STOCK_OPEN, N_("_Open Draft..."), "<control>O", N_("Open an unfinished journal entry"), G_CALLBACK (open_draft_cb) },
		{ "SaveDraft", GTK_STOCK_SAVE, N_("_Save Draft"), "<control>S", N_("Save the current entry without adding it to your journal"), G_CALLBACK (save_draft_cb) },
		{ "SaveDraftAs", GTK_STOCK_SAVE_AS, N_("Save Draft _as..."), NULL, N_("Save the current entry without adding it to your journal"), G_CALLBACK (save_draft_as_cb) },
		{ "RecentEntries", NULL, N_("_Recent Entries"), },
		{ "RecentDrafts", NULL, N_("Recent _Drafts") },
		{ "PostEntry", "drivel-post", N_("_Post Entry"), "<control>Return", N_("Post the current entry to your journal"), G_CALLBACK (journal_post_cb) },
		/* FIXME: make an icon for update-entry */
		{ "UpdateEntry", "drivel-update", N_("_Update Entry"), "<control>Return", N_("Update the selected entry in your journal"), G_CALLBACK (journal_post_cb) },
		{ "DeleteEntry", GTK_STOCK_DELETE, N_("_Delete Entry"), NULL, N_("Delete the selected entry from your journal."), G_CALLBACK (journal_delete_cb) },
		{ "LogOut", GTK_STOCK_GO_BACK, N_("_Log Out"), NULL, N_("Switch usernames"), G_CALLBACK (logout_cb) },
		{ "Quit", GTK_STOCK_QUIT, NULL, NULL, NULL, G_CALLBACK (exit_cb) },
		{ "Undo", GTK_STOCK_UNDO, NULL, "<control>Z", NULL, G_CALLBACK (undo_cb) },
		{ "Redo", GTK_STOCK_REDO, NULL, "<control>R", NULL, G_CALLBACK (redo_cb) },
		{ "Cut", GTK_STOCK_CUT, NULL, NULL, NULL, G_CALLBACK (cut_cb) },
		{ "Copy", GTK_STOCK_COPY, NULL, NULL, NULL, G_CALLBACK (copy_cb) },
		{ "Paste", GTK_STOCK_PASTE, NULL, NULL, NULL, G_CALLBACK (paste_cb) },
		{ "Clear", GTK_STOCK_CLEAR, N_("C_lear Entry"), "<control>T", NULL, G_CALLBACK (clear_cb) },
		{ "SelectAll", NULL, N_("Select _All"), "<control>A", NULL, G_CALLBACK (select_all_cb) },
		/* FIXME: make an icon for edit-last-entry */
		{ "LastEntry", GTK_STOCK_FIND_AND_REPLACE, N_("Last _Entry"), "<control>L", N_("Edit the last entry you posted"), G_CALLBACK (edit_entry_cb) },
		{ "Friends", NULL, N_("_Friends"), NULL, N_("Edit your friends list"), G_CALLBACK (edit_friends_cb) },
		{ "History", NULL, N_("_History"), NULL, N_("View or edit a previous entry"), G_CALLBACK (edit_history_cb) },
		{ "Security", NULL, N_("Security Groups"), NULL, N_("Edit specific groups of people able to read your entries"), G_CALLBACK (edit_security_cb) },
		{ "ActiveJournal", NULL, N_("Active _Journal"), },
		{ "Preferences", GTK_STOCK_PREFERENCES, NULL, NULL, NULL, G_CALLBACK (edit_preferences_cb) },
		{ "Bold", GTK_STOCK_BOLD, NULL, "<control>B", NULL, G_CALLBACK (bold_cb) },
		{ "Italic", GTK_STOCK_ITALIC, NULL, "<control>I", NULL, G_CALLBACK (italic_cb) },
		{ "Underline", GTK_STOCK_UNDERLINE, NULL, "<control>U", NULL, G_CALLBACK (underline_cb) },
		{ "Strike", GTK_STOCK_STRIKETHROUGH, NULL, NULL, NULL, G_CALLBACK (strikethrough_cb) },
		{ "Super", NULL, N_("Supe_rscript"), NULL, NULL, G_CALLBACK (superscript_cb) },
		{ "Sub", NULL, N_("Subs_cript"), NULL, NULL, G_CALLBACK (subscript_cb) },
		{ "List", NULL, N_("Lis_t"), NULL, NULL, G_CALLBACK (list_cb) },
		{ "ListItem", NULL, N_("List Ite_m"), NULL, NULL, G_CALLBACK (listitem_cb) },
		{ "Indent", NULL, N_("I_ndent"), NULL, NULL, G_CALLBACK (blockquote_cb) },
		{ "InsertLink", "drivel-insert-link", N_("Insert _Link..."), NULL, N_("Create a link to a web page, livejournal user, or a long journal entry"), G_CALLBACK (insert_link_cb) },
		{ "InsertImage", "drivel-insert-image", N_("Insert Ima_ge..."), NULL, N_("Insert an image into your journal entry"), G_CALLBACK (insert_image_cb) },
		{ "InsertPoll", NULL, N_("Insert _Poll..."), NULL, NULL, G_CALLBACK (insert_poll_cb) },
		{ "Contents", GTK_STOCK_HELP, N_("_Contents"), "F1", NULL, G_CALLBACK (help_cb) },
		{ "FAQ", NULL, N_("_Frequently Asked Questions"), NULL, NULL, G_CALLBACK (faq_cb) },
		{ "About", GNOME_STOCK_ABOUT, N_("_About"), NULL, NULL, G_CALLBACK (about_cb) }
	};
	static GtkToggleActionEntry toggle_entries[] =
	{
		{ "EntryOptions", NULL, N_("More Entry _Options"), NULL, NULL, G_CALLBACK (view_more_options_cb) },
		{ "HighlightSpelling", NULL, N_("Highlight _Spelling Errors"), "F7", NULL, G_CALLBACK (view_misspelled_words_cb) },
		{ "HighlightSyntax", NULL, N_("Highlight _HTML"), NULL, NULL, G_CALLBACK (view_html_keywords_cb) },
	};
	static const gchar *ui_description =
	{
		"<ui>"
		"  <menubar name='MainMenu'>"
		"    <menu action='JournalMenu'>"
		"      <menuitem action='OpenDraft'/>"
		"      <menuitem action='SaveDraft'/>"
		"      <menuitem action='SaveDraftAs'/>"
		"      <separator name='sep0.5'/>"
		"      <menu action='RecentEntries'/>"
		"      <menu action='RecentDrafts'/>"
		"      <separator name='sep1'/>"
		"      <menuitem action='PostEntry'/>"
		"      <menuitem action='UpdateEntry'/>"
		"      <menuitem action='DeleteEntry'/>"
		"      <separator name='sep1.2'/>"
		"      <menuitem action='LogOut'/>"
		"      <menuitem action='Quit'/>"
		"    </menu>"
		"    <menu action='EditMenu'>"
		"      <menuitem action='Undo'/>"
		"      <menuitem action='Redo'/>"
		"      <separator name='sep1.5'/>"
		"      <menuitem action='Cut'/>"
		"      <menuitem action='Copy'/>"
		"      <menuitem action='Paste'/>"
		"      <separator name='sep2'/>"
		"      <menuitem action='SelectAll'/>"
		"      <menuitem action='Clear'/>"
		"      <separator name='sep3'/>"
		"      <menuitem action='LastEntry'/>"
		"      <menuitem action='Friends'/>"
		"      <menuitem action='History'/>"
/*		"      <menuitem action='Security'/>" */
		"      <menu action='ActiveJournal'>"
		"      </menu>"
		"      <separator name='sep4'/>"
		"      <menuitem action='Preferences'/>"
		"    </menu>"
		"    <menu action='ViewMenu'>"
		"      <menuitem action='EntryOptions'/>"
		"      <separator name='sep4.5'/>"
		"      <menuitem action='HighlightSpelling'/>"
		"      <menuitem action='HighlightSyntax'/>"
		"    </menu>"
		"    <menu action='FormatMenu'>"
		"      <menuitem action='Bold'/>"
		"      <menuitem action='Italic'/>"
		"      <menuitem action='Underline'/>"
		"      <menuitem action='Strike'/>"
		"      <menuitem action='Super'/>"
		"      <menuitem action='Sub'/>"
		"      <separator name='sep5'/>"
		"      <menuitem action='List'/>"
		"      <menuitem action='ListItem'/>"
		"      <separator name='sep6'/>"
		"      <menuitem action='Indent'/>"
		"      <separator name='sep7'/>"
		"      <menuitem action='InsertLink'/>"
		"      <menuitem action='InsertImage'/>"
		"      <menuitem action='InsertPoll'/>"
		"    </menu>"
		"    <menu action='WebMenu'>"
		"    </menu>"
		"    <menu action='HelpMenu'>"
		"      <menuitem action='Contents'/>"
		"      <menuitem action='FAQ'/>"
		"      <menuitem action='About'/>"
		"    </menu>"
		"  </menubar>"
		"</ui>"
	};

	/*
	 * this is the new glade interface for the journal
	 * written by Davyd Madeley <davyd@ucc.asn.au>
	 */
	GladeXML *xml;
	GtkWidget *window, *subject, *security, *mood, *music, *comment, *autoformat, *picture;
	GtkWidget *text_area, *menubar, *backdate, *date;
	GtkWidget *post_button, *cancel_button, *delete_button, *save_button, *help_button;
	GtkWidget *save_draft_button, *widget, *options_lj, *options_mt;
	GtkListStore *picture_store, *mood_store, *music_store;
	GtkTreeIter iter;
	GtkCellRenderer *renderer;
	GtkTextBuffer *buffer;
	GSList *list;
	GtkActionGroup *action_group;
	GtkAccelGroup *accel_group;
	GtkAction *action;
	GError *error;
	GtkEntryCompletion *completion;
	GtkSourceLanguagesManager *language_manager;
	GtkSourceLanguage *language;
	GSList *language_path;
	GConfValue *value;
	gboolean state;
	EggRecentModel *recent_model;
	EggRecentViewUIManager *recent_view;
	GtkSizeGroup *col1, *row1, *row2;
	gchar *filename, *last_journal_path;
	gint last_active_journal;
	
	debug ("journal_window_build()");
	
	xml = load_glade_xml ("journal_editor");
	if (!xml)
		return;
	
	/* build size groups for the options pane */
	col1 = gtk_size_group_new (GTK_SIZE_GROUP_HORIZONTAL);
	row1 = gtk_size_group_new (GTK_SIZE_GROUP_VERTICAL);
	row2 = gtk_size_group_new (GTK_SIZE_GROUP_VERTICAL);
	
	/* grab and set the icon for the window */
	window = glade_xml_get_widget (xml, "journal_editor");
	filename = g_build_filename (DATADIR, "pixmaps", "drivel", "drivel-24.png", NULL);
	gtk_window_set_icon_from_file (GTK_WINDOW (window), filename, NULL);
	g_free (filename);
	
	/* subject entry */
	subject = glade_xml_get_widget (xml, "subject");
	
	/* the security dropdown, populate with entries and pictures */
	security = glade_xml_get_widget (xml, "security");
	gtk_combo_box_set_model (GTK_COMBO_BOX (security), 
			GTK_TREE_MODEL (dc->security_store));
	gtk_cell_layout_clear (GTK_CELL_LAYOUT (security));
	renderer = gtk_cell_renderer_pixbuf_new ();
	gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (security), renderer, FALSE);
	gtk_cell_layout_set_attributes (GTK_CELL_LAYOUT (security), renderer,
			"pixbuf", STORE_SECURITY_ICON,
			NULL);
	renderer = gtk_cell_renderer_text_new ();
	gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (security), renderer, TRUE);
	gtk_cell_layout_set_attributes (GTK_CELL_LAYOUT (security), renderer,
			"text", STORE_SECURITY_NAME,
			NULL);
	
	/* the text area */
	widget = glade_xml_get_widget (xml, "textview_scrolls");
	text_area = gtk_source_view_new ();
	gtk_container_add (GTK_CONTAINER (widget), text_area);
	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (text_area));
	gtk_text_view_set_wrap_mode (GTK_TEXT_VIEW (text_area), GTK_WRAP_WORD);
	gtk_source_buffer_set_check_brackets (GTK_SOURCE_BUFFER (buffer), FALSE);
	language_manager = gtk_source_languages_manager_new ();
	language_path = g_slist_append (NULL, g_strdup (DRIVEL_LANGUAGES_DIR));
	language_manager = g_object_new (GTK_TYPE_SOURCE_LANGUAGES_MANAGER,
			"lang-files-dirs", language_path, NULL);
	language = gtk_source_languages_manager_get_language_from_mime_type (language_manager, "text/html+livejournal");
	gtk_source_buffer_set_language (GTK_SOURCE_BUFFER (buffer), language);
	value = gconf_client_get (dc->client, dc->gconf->highlight_syntax, NULL);
	if (value)
		state = gconf_value_get_bool (value);
	else
		state = TRUE;
	gtk_source_buffer_set_highlight (GTK_SOURCE_BUFFER (buffer), state);
	gtk_source_view_set_smart_home_end (GTK_SOURCE_VIEW (text_area), FALSE);
#ifdef HAVE_GTKSPELL
	if (gconf_client_get_bool (dc->client, dc->gconf->spellcheck, NULL))
	{
	    	const gchar *spell_lang;
		spell_lang = gconf_client_get_string (dc->client, dc->gconf->spell_language, NULL);
		gtkspell_new_attach (GTK_TEXT_VIEW (text_area),
				     (spell_lang == NULL || *spell_lang == '\0') ? NULL : spell_lang,
				     NULL);
	}
#endif /* HAVE_GTKSPELL */
	
	/* the mood entry */
	widget = glade_xml_get_widget (xml, "mood_label");
	gtk_size_group_add_widget (col1, widget);
	mood_store = gtk_list_store_new (1, G_TYPE_STRING);
	dc->mood_list = g_slist_sort (dc->mood_list, string_compare);
	for (list = dc->mood_list; list != NULL; list = list->next)
	{
		gtk_list_store_append (mood_store, &iter);
		gtk_list_store_set (mood_store, &iter, 0, list->data, -1);
	}
	mood = glade_xml_get_widget (xml, "mood");
	gtk_combo_box_set_model (GTK_COMBO_BOX (mood), GTK_TREE_MODEL (mood_store));
	gtk_cell_layout_clear (GTK_CELL_LAYOUT (mood));
	renderer = gtk_cell_renderer_text_new ();
	gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (mood), renderer, TRUE);
	gtk_cell_layout_set_attributes (GTK_CELL_LAYOUT (mood), renderer,
			"text", 0,
			NULL);
	completion = gtk_entry_completion_new ();
	gtk_entry_set_completion (GTK_ENTRY (GTK_BIN (mood)->child), completion);
	gtk_entry_completion_set_model (completion, GTK_TREE_MODEL (mood_store));
	gtk_entry_completion_set_text_column (completion, 0);

	/* the music entry */
	widget = glade_xml_get_widget (xml, "music_label");
	gtk_size_group_add_widget (col1, widget);
	music_store = gtk_list_store_new (2, G_TYPE_STRING, GDK_TYPE_PIXBUF);
	music = glade_xml_get_widget (xml, "music");
	gtk_combo_box_set_model (GTK_COMBO_BOX (music), GTK_TREE_MODEL (music_store));
	gtk_cell_layout_clear (GTK_CELL_LAYOUT (music));
	renderer = gtk_cell_renderer_pixbuf_new ();
	gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (music), renderer, FALSE);
	gtk_cell_layout_set_attributes (GTK_CELL_LAYOUT (music), renderer,
			"pixbuf", 1,
			NULL);
	renderer = gtk_cell_renderer_text_new ();
	gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (music), renderer, TRUE);
	gtk_cell_layout_set_attributes (GTK_CELL_LAYOUT (music), renderer,
			"text", 0,
			NULL);
	
	/* no comments check */
	comment = glade_xml_get_widget (xml, "nocomments");
	gtk_size_group_add_widget (row1, comment);
	
	/* no autoformatting check */
	autoformat = glade_xml_get_widget (xml, "noautoformat");
	gtk_size_group_add_widget (row2, autoformat);
	
	/* date entry */
	backdate = glade_xml_get_widget (xml, "backdate");
	date = glade_xml_get_widget (xml, "date");
	egg_datetime_set_lazy (EGG_DATETIME (date), FALSE);
	widget = glade_xml_get_widget (xml, "options_date_box");
	gtk_size_group_add_widget (row1, widget);
	
	/* pictures dropdown */
	widget = glade_xml_get_widget (xml, "options_picture_box");
	gtk_size_group_add_widget (row2, widget);
	picture_store = gtk_list_store_new (2, G_TYPE_STRING, GDK_TYPE_PIXBUF);
	fill_picture_menu (dc, picture_store);
	picture = glade_xml_get_widget (xml, "picture");
	gtk_combo_box_set_model (GTK_COMBO_BOX (picture), GTK_TREE_MODEL (picture_store));
	gtk_cell_layout_clear (GTK_CELL_LAYOUT (picture));
	renderer = gtk_cell_renderer_pixbuf_new ();
	gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (picture), renderer, FALSE);
	gtk_cell_layout_set_attributes (GTK_CELL_LAYOUT (picture), renderer,
			"pixbuf", 1,
			NULL);
	renderer = gtk_cell_renderer_text_new ();
	gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (picture), renderer, TRUE);
	gtk_cell_layout_set_attributes (GTK_CELL_LAYOUT (picture), renderer,
			"text", 0,
			NULL);
	dc->picture_store = picture_store;
	
	/* category list */
	widget = glade_xml_get_widget (xml, "category");
	gtk_combo_box_set_model (GTK_COMBO_BOX (widget), 
			GTK_TREE_MODEL (dc->category_store_filtered));
	gtk_cell_layout_clear (GTK_CELL_LAYOUT (widget));
	renderer = gtk_cell_renderer_text_new ();
	gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (widget), renderer, TRUE);
	gtk_cell_layout_set_attributes (GTK_CELL_LAYOUT (widget), renderer,
			"text", 0,
			NULL);
	dc->journal_category = widget;
	widget = glade_xml_get_widget (xml, "category_label");
	gtk_label_set_mnemonic_widget(GTK_LABEL(widget),dc->journal_category);
	
	/* options expander */
	dc->journal_expander = glade_xml_get_widget (xml, "more_options");

	/* buttons */
	help_button = glade_xml_get_widget (xml, "help_button");
	delete_button = glade_xml_get_widget (xml, "delete_button");
	cancel_button = glade_xml_get_widget (xml, "cancel_button");
	save_draft_button = glade_xml_get_widget (xml, "save_draft_button");
	save_button = glade_xml_get_widget (xml, "save_button");
	post_button = glade_xml_get_widget (xml, "post_button");
	gtk_button_set_use_stock (GTK_BUTTON (post_button), TRUE);

	/* store things in the struct */
	dc->journal_text = text_area;
	dc->journal_window = window;
	dc->journal_subject = subject;
	dc->journal_mood = mood;
	dc->journal_music = music;
	dc->journal_security = security;
	dc->journal_picture = picture;
	dc->journal_comment = comment;
	dc->journal_autoformat = autoformat;
	dc->journal_backdate = backdate;
	dc->journal_date = date;
	dc->journal_post = post_button;
	dc->modified = FALSE;
	dc->buffer = buffer;
	dc->edit_delete = delete_button;
	dc->edit_cancel = cancel_button;
	dc->edit_save = save_button;
	
	drivel_push_current_window (dc, window);

	/* menubar */
	debug ("building menubar");
	action_group = gtk_action_group_new ("MenuActions");
	gtk_action_group_set_translation_domain (action_group, GETTEXT_PACKAGE);
	gtk_action_group_add_actions (action_group, entries, G_N_ELEMENTS (entries), dc);
	gtk_action_group_add_toggle_actions (action_group, toggle_entries, G_N_ELEMENTS (toggle_entries), dc);
	dc->menus = gtk_ui_manager_new ();
	gtk_ui_manager_insert_action_group (dc->menus, action_group, 0);
	accel_group = gtk_ui_manager_get_accel_group (dc->menus);
	gtk_window_add_accel_group (GTK_WINDOW (window), accel_group);

	error = NULL;
	if (!gtk_ui_manager_add_ui_from_string (dc->menus, ui_description, -1, &error))
	{
		g_message ("building menus failed: %s", error->message);
		g_error_free (error);
	}

	last_active_journal = build_active_journal_menu (dc);
	build_web_link_menu (dc);
	menubar = gtk_ui_manager_get_widget (dc->menus, "/MainMenu");
	gnome_app_set_menus (GNOME_APP (window), GTK_MENU_BAR (menubar));

	dc->menu_post = gtk_ui_manager_get_widget (dc->menus, "/MainMenu/JournalMenu/PostEntry");
	dc->menu_update = gtk_ui_manager_get_widget (dc->menus, "/MainMenu/JournalMenu/UpdateEntry");
	dc->menu_delete = gtk_ui_manager_get_widget (dc->menus, "/MainMenu/JournalMenu/DeleteEntry");
	dc->menu_undo = gtk_ui_manager_get_widget (dc->menus, "/MainMenu/EditMenu/Undo");
	dc->menu_redo = gtk_ui_manager_get_widget (dc->menus, "/MainMenu/EditMenu/Redo");
	dc->menu_cut = gtk_ui_manager_get_widget (dc->menus, "/MainMenu/EditMenu/Cut");
	dc->menu_copy = gtk_ui_manager_get_widget (dc->menus, "/MainMenu/EditMenu/Copy");
	dc->menu_paste = gtk_ui_manager_get_widget (dc->menus, "/MainMenu/EditMenu/Paste");
	dc->menu_last = gtk_ui_manager_get_widget (dc->menus, "/MainMenu/EditMenu/LastEntry");
	dc->menu_friends = gtk_ui_manager_get_widget (dc->menus, "/MainMenu/EditMenu/Friends");
	dc->menu_history = gtk_ui_manager_get_widget (dc->menus, "/MainMenu/EditMenu/History");
	dc->menu_security = gtk_ui_manager_get_widget (dc->menus, "/MainMenu/EditMenu/Security");
	dc->menu_active = gtk_ui_manager_get_widget (dc->menus, "/MainMenu/EditMenu/ActiveJournal");
	dc->menu_view_options = gtk_ui_manager_get_action (dc->menus, "/MainMenu/ViewMenu/EntryOptions");
	dc->menu_view_misspelled_words = gtk_ui_manager_get_action (dc->menus, "/MainMenu/ViewMenu/HighlightSpelling");
	dc->menu_view_html_keywords = gtk_ui_manager_get_action (dc->menus, "/MainMenu/ViewMenu/HighlightSyntax");
	
	/* setup the recent files menu */
	debug ("setting up recent files");
	recent_model = egg_recent_model_new (EGG_RECENT_MODEL_SORT_MRU);
	egg_recent_model_set_filter_mime_types (recent_model, 
			"application/x-drivel", NULL);
	egg_recent_model_set_filter_uri_schemes (recent_model, "file", NULL);
	egg_recent_model_set_limit (recent_model, 4);
	recent_view = egg_recent_view_uimanager_new (dc->menus, 
			"/MainMenu/JournalMenu/RecentDrafts", 
			G_CALLBACK (open_draft_recent_cb), dc);
	dc->recent_model = recent_model;
	dc->recent_view = recent_view;
	egg_recent_view_set_model (EGG_RECENT_VIEW (recent_view), recent_model);
	g_object_unref (G_OBJECT (recent_model));
	
	/* signals */
	debug ("connecting signals");
	g_signal_connect (G_OBJECT (window), "delete_event",
			G_CALLBACK (delete_event_cb), dc);
	g_signal_connect (G_OBJECT (buffer), "changed",
			G_CALLBACK (text_changed_cb), dc);
	g_signal_connect (G_OBJECT (text_area), "button_release_event",
			G_CALLBACK (text_button_release_cb), dc);
	g_signal_connect (G_OBJECT (post_button), "clicked",
			G_CALLBACK (journal_post_cb), dc);
	g_signal_connect (G_OBJECT (delete_button), "clicked",
			G_CALLBACK (journal_delete_cb), dc);
	g_signal_connect (G_OBJECT (cancel_button), "clicked",
			G_CALLBACK (journal_cancel_cb), dc);
	g_signal_connect (G_OBJECT (save_button), "clicked",
			G_CALLBACK (journal_post_cb), dc);
	g_signal_connect (G_OBJECT (save_draft_button), "clicked",
			G_CALLBACK (save_draft_cb), dc);
	g_signal_connect (G_OBJECT (dc->journal_expander), "notify::expanded",
			G_CALLBACK (expander_activate_cb), dc);
	widget = glade_xml_get_widget (xml, "options_music_eventbox");
	g_signal_connect (G_OBJECT (widget), "enter_notify_event",
			G_CALLBACK (music_combo_cb), dc);
	g_signal_connect (G_OBJECT (music), "changed",
			G_CALLBACK (music_combo_changed_cb), dc);
	g_signal_connect (G_OBJECT (backdate), "toggled",
			G_CALLBACK (backdate_toggled_cb), dc);
	g_signal_connect (G_OBJECT (buffer), "can-undo",
			G_CALLBACK (undo_update_cb), dc);
	g_signal_connect (G_OBJECT (buffer), "can-redo",
			G_CALLBACK (redo_update_cb), dc);

	dc->tag_autosave = g_timeout_add (5000, autosave_cb, dc);
	
	text_key_release_cb (NULL, NULL, dc);

	/* journal_display_defaults() will free dc->draft_filename, so if the user
	   specified a file on the command line, we need to save it */
	if (dc->draft_filename)
		filename = g_strdup (dc->draft_filename);
	else
		filename = NULL;
	journal_display_defaults (dc);
	dc->draft_filename = filename;
	
	/* load the autosave file (if available) or a file specified on the command line */
	open_file (dc->draft_filename, dc);
	
	/* turn on the tray icon */
	value = gconf_client_get (dc->client, dc->gconf->tray, NULL);
	
	if (value)
		state = gconf_value_get_bool (value);
	else
		state = TRUE;
	if (state)
		tray_turn_on (dc);
	
	/* select the last active journal */
	last_journal_path = g_strdup_printf ("/MainMenu/EditMenu/ActiveJournal/ActiveJournalItem%d", 
					     last_active_journal);
	action = gtk_ui_manager_get_action (dc->menus, last_journal_path);
	if (last_active_journal && action)
		gtk_toggle_action_set_active (GTK_TOGGLE_ACTION (action), TRUE);
	g_free (last_journal_path);

	/* hide the top-level window while we setup the size and position */
	widget = glade_xml_get_widget (xml, "main_vbox");
	gtk_widget_hide (window);
	gtk_widget_show_all (widget);

	/* this needs to be after show_all() */
	egg_datetime_set_display_mode (EGG_DATETIME (date), EGG_DATETIME_DISPLAY_DATE);
	
	/* this is hidden for the time being, as we don't have any help */
	gtk_widget_hide (help_button);
	gtk_widget_hide (delete_button);
	gtk_widget_hide (cancel_button);
	gtk_widget_hide (save_button);
	
	/* depending on the api, only show the appropriate controls */
	options_lj = glade_xml_get_widget (xml, "options_lj");
	options_mt = glade_xml_get_widget (xml, "options_mt");
	widget = glade_xml_get_widget (xml, "more_options");
	gtk_widget_hide (options_lj);
	gtk_widget_hide (options_mt);
	switch (dc->user->api)
	{
		case BLOG_API_LJ: gtk_widget_show (options_lj); break;
		case BLOG_API_MT: gtk_widget_show (options_mt); break;
		default: gtk_widget_hide (widget); break;
	}
	widget = glade_xml_get_widget (xml, "options_security_box");
	if (dc->user->api != BLOG_API_LJ)
		gtk_widget_hide (widget);
	widget = glade_xml_get_widget (xml, "options_subject_box");
	if (dc->user->api == BLOG_API_BLOGGER ||
		dc->user->api == BLOG_API_ADVOGATO)
		gtk_widget_hide (widget);
	
 	dc->statusbar = gnome_appbar_new (TRUE, TRUE, GNOME_PREFERENCES_NEVER);
 	gnome_app_set_statusbar (GNOME_APP (window), dc->statusbar);

	/* offline users can't post, online users can use the menu to save a draft */
	if (dc->user->api == BLOG_API_OFFLINE)
		gtk_widget_hide (post_button);
	else
		gtk_widget_hide (save_draft_button);
	
	set_journal_title (dc->active_journal, GTK_WINDOW (window), dc->user->api);
	journal_window_state_restore (dc);

	gtk_widget_show (window);
	
	gtk_window_present (GTK_WINDOW (window));
	
	if (gconf_client_get_bool (dc->client, dc->gconf->min_start, NULL))
		gtk_window_iconify (GTK_WINDOW (dc->journal_window));
	
	debug ("journal_window_build() is complete");
	
	return;
}
