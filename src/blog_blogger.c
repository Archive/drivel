/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * Copyright 2004-2005 Todd Kulesza
 *
 * Authors:
 * 		Todd Kulesza <todd@dropline.net>
 */

#define _XOPEN_SOURCE /* glibc2 needs this */

#include <sys/types.h>
#include <config.h>

#include <time.h>
#include <string.h>

#include "drivel.h"
#include "drivel_request.h"
#include "journal.h"
#include "network.h"
#include "xmlrpc.h"
#include "blog_blogger.h"

extern DrivelClient *dc;

/* Parse the server's response to our getUsersBlogs request */

static void
parse_login_request (SoupMessage *msg, gpointer data)
{
	SoupXmlrpcResponse *response;
	SoupXmlrpcValue *value;
	SoupXmlrpcValueArrayIterator *iter;

	g_return_if_fail (msg);

	debug ("blog_blogger_parse_login_request()");
	
	if (!(response = net_msg_get_response (SOUP_XMLRPC_MESSAGE (msg))))
		return;
	
	value = soup_xmlrpc_response_get_value (response);
	if (!value)
	{
		g_warning ("No value in reponse");
		return;
	}

	add_account_to_list (dc);
	dc->journals = 0;

	soup_xmlrpc_value_array_get_iterator (value, &iter);
	while (soup_xmlrpc_value_array_iterator_get_value (iter, &value) && value)
	{
		GHashTable *table;
		gchar *url, *id, *name;
		DrivelJournal *dj;

		soup_xmlrpc_value_get_struct (value, &table);

		value = g_hash_table_lookup (table, "url");
		soup_xmlrpc_value_get_string (value, &url);
		value = g_hash_table_lookup (table, "blogid");
		soup_xmlrpc_value_get_string (value, &id);
		value = g_hash_table_lookup (table, "blogName");
		soup_xmlrpc_value_get_string (value, &name);
		
		dj = drivel_journal_new ();
		dj->name = name;
		dj->uri_view = url;
		dj->id = id;
		dj->type = JOURNAL_TYPE_USER;
		dc->journal_list = g_slist_prepend (dc->journal_list, dj);

		g_hash_table_destroy (table);

		dc->journals++;
		iter = soup_xmlrpc_value_array_iterator_next (iter);

		blog_blogger_build_getevents_request (dc->user->username,
						      dc->user->password,
						      dc->user->server,
						      dj->id,
						      DRIVEL_N_RECENT_POSTS);
	}

	dc->journal_list = g_slist_sort (dc->journal_list, (GCompareFunc)sort_journals);

	gtk_widget_hide (dc->login_window);
	journal_window_build (dc);

	g_object_unref (response);

	return;
}

/* To 'login' to Blogger, we send the username and password and get the user's
 * blogid */

DrivelRequest*
blog_blogger_build_login_request (const gchar *username, const gchar *password,
				  const gchar *uri)
{
	SoupXmlrpcMessage *msg;

	msg = xmlrpc_start ("blogger.getUsersBlogs", uri);
	xmlrpc_add_string_param (msg, DRIVEL_APPKEY);
	xmlrpc_add_string_param (msg, username);
	xmlrpc_add_string_param (msg, password);
	xmlrpc_end (msg);
	
	g_signal_connect (G_OBJECT (msg), "finished",
			  G_CALLBACK (parse_login_request), NULL);

	net_enqueue_msg (SOUP_MESSAGE (msg));

	return NULL;
}

/* Parse the server's response to our post request */

static void
parse_postevent_request (SoupMessage *msg, gpointer data)
{
	SoupXmlrpcResponse *response;
	SoupXmlrpcValue *value;

	g_return_if_fail (msg);

	debug ("blog_blogger_parse_postevent_request()");

	if (!(response = net_msg_get_response (SOUP_XMLRPC_MESSAGE (msg))))
		return;
	
	value = soup_xmlrpc_response_get_value (response);
	if (!value)
	{
		g_warning ("No value in reponse");
		return;
	}

	/* FIXME: ping technorati */
	journal_finished_post (dc);

	g_object_unref (response);

	return;
}

/* Build an XML-RPC packet with the journal entry content */

DrivelRequest*
blog_blogger_build_postevent_request (const gchar *username, const gchar *password, 
				      const gchar *uri, const gchar *blogid, 
				      gboolean publish, const gchar *content)
{
	SoupXmlrpcMessage *msg;

	debug ("blog_blogger_build_postevent_request()");

	msg = xmlrpc_start ("blogger.newPost", uri);
	xmlrpc_add_string_param (msg, DRIVEL_APPKEY);
	xmlrpc_add_string_param (msg, blogid);
	xmlrpc_add_string_param (msg, username);
	xmlrpc_add_string_param (msg, password);
	xmlrpc_add_string_param (msg, content);
	xmlrpc_add_bool_param (msg, publish);
	xmlrpc_end (msg);
	
	g_signal_connect (G_OBJECT (msg), "finished",
			  G_CALLBACK (parse_postevent_request), NULL);

	net_enqueue_msg (SOUP_MESSAGE (msg));

	return NULL;
}

DrivelRequest*
blog_blogger_build_editevent_request (const gchar *username, const gchar *password, 
				      const gchar *uri, const gchar *postid, 
				      gboolean publish, const gchar *content)
{
       	SoupXmlrpcMessage *msg;

	msg = xmlrpc_start ("blogger.editPost", uri);
	xmlrpc_add_string_param (msg, DRIVEL_APPKEY);
	xmlrpc_add_string_param (msg, postid);
	xmlrpc_add_string_param (msg, username);
	xmlrpc_add_string_param (msg, password);
	xmlrpc_add_string_param (msg, content);
	xmlrpc_add_bool_param (msg, publish);
	xmlrpc_end (msg);
	
	g_signal_connect (G_OBJECT (msg), "finished",
			  G_CALLBACK (parse_postevent_request), NULL);

	net_enqueue_msg (SOUP_MESSAGE (msg));

	return NULL;
}

DrivelRequest*
blog_blogger_build_deleteevent_request (const gchar *username, const gchar *password, 
					const gchar *uri, const gchar *postid)
{
       	SoupXmlrpcMessage *msg;

	msg = xmlrpc_start ("blogger.deletePost", uri);
	xmlrpc_add_string_param (msg, DRIVEL_APPKEY);
	xmlrpc_add_string_param (msg, postid);
	xmlrpc_add_string_param (msg, username);
	xmlrpc_add_string_param (msg, password);
	xmlrpc_add_bool_param (msg, TRUE);
	xmlrpc_end (msg);
	
	g_signal_connect (G_OBJECT (msg), "finished",
			  G_CALLBACK (parse_postevent_request), NULL);

	net_enqueue_msg (SOUP_MESSAGE (msg));

	return NULL;
}

static void
parse_getevents_request (SoupMessage *msg, gpointer data)
{
	SoupXmlrpcResponse *response;
	SoupXmlrpcValue *value;
	SoupXmlrpcValueArrayIterator *iter;
	const gchar *blogid;
	gint counter = 0;
	gboolean last_entry = FALSE;

	g_return_if_fail (msg);

	debug ("blog_blogger_parse_getevents_request()");

	if (!(response = net_msg_get_response (SOUP_XMLRPC_MESSAGE (msg))))
		return;
	
	value = soup_xmlrpc_response_get_value (response);
	if (!value)
	{
		g_warning ("No value in reponse");
		return;
	}

	blogid = g_object_get_data (G_OBJECT (msg), "blogid");
	if (g_object_get_data (G_OBJECT (msg), "last_entry"))
		last_entry = TRUE;

	soup_xmlrpc_value_array_get_iterator (value, &iter);
	while (soup_xmlrpc_value_array_iterator_get_value (iter, &value) && 
	       value && counter < DRIVEL_N_RECENT_POSTS)
	{
		GHashTable *table;
		gchar *postid, *content;
		time_t time;

		soup_xmlrpc_value_get_struct (value, &table);

		value = g_hash_table_lookup (table, "postid");
		soup_xmlrpc_value_get_string (value, &postid);
		value = g_hash_table_lookup (table, "content");
		soup_xmlrpc_value_get_string (value, &content);
		value = g_hash_table_lookup (table, "dateCreated");
		soup_xmlrpc_value_get_datetime (value, &time);

		if (last_entry)
		{
			journal_edit_entry (dc, postid, content, NULL, NULL, NULL, NULL,
					    NULL, NULL, NULL, NULL, NULL, NULL);
			counter = DRIVEL_N_RECENT_POSTS;
		}
		else if (postid && content)
		{
			DrivelJournalEntry *entry = journal_entry_new ();
			entry->postid =  postid;
			entry->content = content;
			entry->userid = g_strdup (blogid);
			entry->date_posted = time;
			g_array_append_val (dc->recent_entries, entry);
		}

		g_hash_table_destroy (table);

		counter++;
		iter = soup_xmlrpc_value_array_iterator_next (iter);
	}

	if (!last_entry)
		journal_refresh_recent_entries (dc);

	g_object_unref (response);

	return;
}

/* Use blogger.getRecentPosts to revtrieve a history of recent entries */

DrivelRequest*
blog_blogger_build_getevents_request (const gchar *username, const gchar *password, 
				      const gchar *uri, const gchar *blogid, 
				      gint n_posts)
{
       	SoupXmlrpcMessage *msg;

	debug ("blog_blogger_build_getevents_request");
	g_print ("blogid: %s\n", blogid);
	msg = xmlrpc_start ("blogger.getRecentPosts", uri);
	xmlrpc_add_string_param (msg, DRIVEL_APPKEY);
	xmlrpc_add_string_param (msg, blogid);
	xmlrpc_add_string_param (msg, username);
	xmlrpc_add_string_param (msg, password);
	xmlrpc_add_int_param (msg, n_posts);
	xmlrpc_end (msg);
	
	g_signal_connect (G_OBJECT (msg), "finished",
			  G_CALLBACK (parse_getevents_request), NULL);

	g_object_set_data_full (G_OBJECT (msg), "blogid", g_strdup (blogid), g_free);
	if (n_posts == 1)
		g_object_set_data_full (G_OBJECT (msg), "last_entry", g_strdup ("true"), g_free);

	net_enqueue_msg (SOUP_MESSAGE (msg));

	return NULL;
}
