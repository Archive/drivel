/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * Copyright 2002-2004 Todd Kulesza, Davyd Madeley
 *
 * Authors:
 * 		Davyd Madeley <davyd@ucc.asn.au>
 */

#include <config.h>

#include <gnome.h>
#include <glade/glade.h>

#include "drivel.h"
#include "insert_poll_dialog.h"
#include "journal.h"

enum
{
	POLL_TYPE_COMBO,
	POLL_TYPE_RADIO,
	POLL_TYPE_CHECK,
	POLL_TYPE_ENTRY,
	POLL_TYPE_SCALE,
	POLL_TYPE_NONE,
	POLL_TYPE_ANSWER
};

typedef struct
{
	DrivelClient *dc;
	GtkTreeStore *poll_store;

	GtkWidget *dialog;
	
	GtkWidget *poll_name;
	GtkWidget *poll_voters;
	GtkWidget *poll_results;
	GtkWidget *poll_structure;
	
	GtkWidget *options_main;
	GtkWidget *options_textentry;
	GtkWidget *options_scale;
	GtkWidget *options_answer;

	GtkWidget *options_main_question;
	GtkWidget *options_main_type;
	GtkWidget *options_textentry_fieldsize;
	GtkWidget *options_textentry_maxlength;
	GtkWidget *options_scale_from;
	GtkWidget *options_scale_to;
	GtkWidget *options_scale_by;
	GtkWidget *options_answer_text;

	GtkWidget *add_question_button;
	GtkWidget *add_answer_button;
	GtkWidget *delete_item_button;
	GtkWidget *help_button;
	GtkWidget *cancel_button;
	GtkWidget *insert_button;
	GtkWidget *up_button;
	GtkWidget *down_button;
} DrivelPoll;

static void
set_dialog_mode (DrivelPoll *dp, gint mode);

static void
set_dialog_updown (DrivelPoll *dp, GtkTreeIter iter);

static GdkPixbuf
*get_type_pixbuf (gint type);

static void
move_up_cb (GtkWidget *button, gpointer user_data);

static void
move_down_cb (GtkWidget *button, gpointer user_data);

static void
add_question_cb (GtkWidget *button, gpointer user_data);

static void
add_answer_cb (GtkWidget *button, gpointer user_data);

static void
delete_item_cb (GtkWidget *button, gpointer user_data);

static void
cancel_cb (GtkWidget *button, gpointer user_data);

static void
destroy_cb (GtkWidget *widget, gpointer user_data);

static void
insert_cb (GtkWidget *button, gpointer user_data);

static void
selection_changed_cb (GtkTreeSelection *tree_selection, gpointer user_data);

static void
options_main_type_cb (GtkWidget *type, gpointer user_data);

static void
options_textentry_cb (GtkEditable *question, gpointer user_data);

static void
options_textentry_changed_cb (GtkEditable *entry, gpointer user_data);

static void
options_scale_changed_cb (GtkWidget *spinner, gpointer user_data);

void
display_insert_poll_dialog (DrivelClient *dc)
{
	GladeXML *xml;
	DrivelPoll *dp;
	GtkListStore *voters_store, *results_store, *type_store;
	GtkTreeIter iter;
	GtkCellRenderer *renderer;

	dp = g_new0 (DrivelPoll, 1);
	dp->dc = dc;
	
	xml = load_glade_xml ("poll_dialog");
	if (!xml)
		return;

	/* dialog */
	dp->dialog = glade_xml_get_widget (xml, "poll_dialog");
	gtk_window_set_transient_for (GTK_WINDOW (dp->dialog), GTK_WINDOW (dc->current_window));
	drivel_push_current_window (dc, dp->dialog);
	/* the name of the poll */
	dp->poll_name = glade_xml_get_widget (xml, "poll_name");

	/* voters */
	voters_store = gtk_list_store_new (1, G_TYPE_STRING);
	gtk_list_store_append (voters_store, &iter);
	gtk_list_store_set (voters_store, &iter, 0, _("All users"), -1);
	gtk_list_store_append (voters_store, &iter);
	gtk_list_store_set (voters_store, &iter, 0, _("Friends"), -1);
	dp->poll_voters = glade_xml_get_widget (xml, "poll_votors");
	gtk_combo_box_set_model (GTK_COMBO_BOX (dp->poll_voters), GTK_TREE_MODEL (voters_store));
	gtk_cell_layout_clear (GTK_CELL_LAYOUT (dp->poll_voters));
	renderer = gtk_cell_renderer_text_new ();
	gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (dp->poll_voters), renderer, TRUE);
	gtk_cell_layout_set_attributes (GTK_CELL_LAYOUT (dp->poll_voters), renderer,
			"text", 0,
			NULL);
	gtk_combo_box_set_active (GTK_COMBO_BOX (dp->poll_voters), 0);

	/* results viewable */
	results_store = gtk_list_store_new (1, G_TYPE_STRING);
	gtk_list_store_append (results_store, &iter);
	gtk_list_store_set (results_store, &iter, 0, _("All users"), -1);
	gtk_list_store_append (results_store, &iter);
	gtk_list_store_set (results_store, &iter, 0, _("Friends"), -1);
	gtk_list_store_append (results_store, &iter);
	gtk_list_store_set (results_store, &iter, 0, _("None"), -1);
	dp->poll_results = glade_xml_get_widget (xml, "poll_results");
	gtk_combo_box_set_model (GTK_COMBO_BOX (dp->poll_results), GTK_TREE_MODEL (results_store));
	gtk_cell_layout_clear (GTK_CELL_LAYOUT (dp->poll_results));
	renderer = gtk_cell_renderer_text_new ();
	gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (dp->poll_results), renderer, TRUE);
	gtk_cell_layout_set_attributes (GTK_CELL_LAYOUT (dp->poll_results), renderer,
			"text", 0,
			NULL);
	gtk_combo_box_set_active (GTK_COMBO_BOX (dp->poll_results), 0);

	/* poll structure */
	/* structure in the tree, enum-ed type, an image for that type, the question or answer, 3 ints */
	dp->poll_store = gtk_tree_store_new (6, G_TYPE_INT, GDK_TYPE_PIXBUF, G_TYPE_STRING, G_TYPE_INT, G_TYPE_INT, G_TYPE_INT);
	dp->poll_structure = glade_xml_get_widget (xml, "poll_structure");
	gtk_tree_view_set_model (GTK_TREE_VIEW (dp->poll_structure), GTK_TREE_MODEL (dp->poll_store));
	renderer = gtk_cell_renderer_pixbuf_new ();
	gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (dp->poll_structure), -1,
			_("Type"), renderer,
			"pixbuf", 1, NULL);
	renderer = gtk_cell_renderer_text_new ();
	gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (dp->poll_structure), -1,
			_("Text"), renderer,
			"text", 2, NULL);
	gtk_tree_view_set_expander_column (GTK_TREE_VIEW (dp->poll_structure),
			gtk_tree_view_get_column (GTK_TREE_VIEW (dp->poll_structure), 1));
	
	/* options tables & boxes*/
	dp->options_main = glade_xml_get_widget (xml, "poll_question_options_main_table");
	dp->options_textentry = glade_xml_get_widget (xml, "poll_question_options_textentry_table");
	dp->options_scale = glade_xml_get_widget (xml, "poll_question_options_scale_box");
	dp->options_answer = glade_xml_get_widget (xml, "poll_question_options_answer_table");

	/* stuff from the options table */
	dp->options_main_question = glade_xml_get_widget (xml, "poll_question_options_question");
	dp->options_main_type = glade_xml_get_widget (xml, "poll_question_options_type");
	dp->options_textentry_fieldsize = glade_xml_get_widget (xml, "poll_questions_options_textfieldsize");
	dp->options_textentry_maxlength = glade_xml_get_widget (xml, "poll_questions_options_maxtextlength");
	dp->options_scale_from = glade_xml_get_widget (xml, "poll_questions_options_scale_from");
	dp->options_scale_to = glade_xml_get_widget (xml, "poll_questions_options_scale_to");
	dp->options_scale_by = glade_xml_get_widget (xml, "poll_questions_options_scale_by");
	dp->options_answer_text = glade_xml_get_widget (xml, "poll_question_options_answer");
	/* fleshing out the type list */
	type_store = gtk_list_store_new (2, GDK_TYPE_PIXBUF, G_TYPE_STRING);
	gtk_list_store_append (type_store, &iter);
	gtk_list_store_set (type_store, &iter, 0, get_type_pixbuf (POLL_TYPE_COMBO), 1, _("Dropdown selection"), -1);
	gtk_list_store_append (type_store, &iter);
	gtk_list_store_set (type_store, &iter, 0, get_type_pixbuf (POLL_TYPE_RADIO), 1, _("Radio selection"), -1);
	gtk_list_store_append (type_store, &iter);
	gtk_list_store_set (type_store, &iter, 0, get_type_pixbuf (POLL_TYPE_CHECK), 1, _("Checkbox selection"), -1);
	gtk_list_store_append (type_store, &iter);
	gtk_list_store_set (type_store, &iter, 0, get_type_pixbuf (POLL_TYPE_ENTRY), 1, _("Text entry"), -1);
	gtk_list_store_append (type_store, &iter);
	gtk_list_store_set (type_store, &iter, 0, get_type_pixbuf (POLL_TYPE_SCALE), 1, _("Scale"), -1);
	gtk_combo_box_set_model (GTK_COMBO_BOX (dp->options_main_type), GTK_TREE_MODEL (type_store));
	gtk_cell_layout_clear (GTK_CELL_LAYOUT (dp->options_main_type));
	renderer = gtk_cell_renderer_pixbuf_new ();
	gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (dp->options_main_type), renderer, FALSE);
	gtk_cell_layout_set_attributes (GTK_CELL_LAYOUT (dp->options_main_type), renderer,
			"pixbuf", 0,
			NULL);
	renderer = gtk_cell_renderer_text_new ();
	gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (dp->options_main_type), renderer, TRUE);
	gtk_cell_layout_set_attributes (GTK_CELL_LAYOUT (dp->options_main_type), renderer,
			"text", 1,
			NULL);
	gtk_combo_box_set_active (GTK_COMBO_BOX (dp->options_main_type), POLL_TYPE_COMBO);

	/* buttons */
	dp->add_question_button = glade_xml_get_widget (xml, "poll_add_question_button");
	gtk_button_set_use_stock (GTK_BUTTON (dp->add_question_button), TRUE);
	dp->add_answer_button = glade_xml_get_widget (xml, "poll_add_answer_button");
	gtk_button_set_use_stock (GTK_BUTTON (dp->add_answer_button), TRUE);
	dp->delete_item_button = glade_xml_get_widget (xml, "poll_delete_item_button");
	dp->help_button = glade_xml_get_widget (xml, "poll_help_button");
	dp->cancel_button = glade_xml_get_widget (xml, "poll_cancel_button");
	dp->insert_button = glade_xml_get_widget (xml, "poll_insert_button");
	gtk_button_set_use_stock (GTK_BUTTON (dp->insert_button), TRUE);
	dp->up_button = glade_xml_get_widget (xml, "poll_up_button");
	dp->down_button = glade_xml_get_widget (xml, "poll_down_button");

	g_signal_connect (G_OBJECT (dp->add_question_button), "clicked",
			G_CALLBACK (add_question_cb), dp);
	g_signal_connect (G_OBJECT (dp->add_answer_button), "clicked",
			G_CALLBACK (add_answer_cb), dp);
	g_signal_connect (G_OBJECT (dp->delete_item_button), "clicked",
			G_CALLBACK (delete_item_cb), dp);
	g_signal_connect (G_OBJECT (dp->cancel_button), "clicked",
			G_CALLBACK (cancel_cb), dp);
	g_signal_connect (G_OBJECT (dp->insert_button), "clicked",
			G_CALLBACK (insert_cb), dp);
	g_signal_connect (G_OBJECT (dp->up_button), "clicked",
			G_CALLBACK (move_up_cb), dp);
	g_signal_connect (G_OBJECT (dp->down_button), "clicked",
			G_CALLBACK (move_down_cb), dp);
	g_signal_connect (G_OBJECT (dp->dialog), "destroy",
			G_CALLBACK (destroy_cb), dp);
	g_signal_connect (G_OBJECT (gtk_tree_view_get_selection (GTK_TREE_VIEW (dp->poll_structure))), "changed",
			G_CALLBACK (selection_changed_cb), dp);
	g_signal_connect (G_OBJECT (GTK_EDITABLE (dp->options_main_question)), "changed",
			G_CALLBACK (options_textentry_cb), dp);
	g_signal_connect (G_OBJECT (dp->options_main_type), "changed",
			G_CALLBACK (options_main_type_cb), dp);
	g_signal_connect (G_OBJECT (dp->options_textentry_fieldsize), "changed",
			G_CALLBACK (options_textentry_changed_cb), dp);
	g_signal_connect (G_OBJECT (dp->options_textentry_maxlength), "changed",
			G_CALLBACK (options_textentry_changed_cb), dp);
	g_signal_connect (G_OBJECT (dp->options_scale_from), "value-changed",
			G_CALLBACK (options_scale_changed_cb), dp);
	g_signal_connect (G_OBJECT (dp->options_scale_to), "value-changed",
			G_CALLBACK (options_scale_changed_cb), dp);
	g_signal_connect (G_OBJECT (dp->options_scale_by), "value-changed",
			G_CALLBACK (options_scale_changed_cb), dp);
	g_signal_connect (G_OBJECT (GTK_EDITABLE (dp->options_answer_text)), "changed",
			G_CALLBACK (options_textentry_cb), dp);
	
	gtk_widget_show_all (dp->dialog);
	
	/* set the initial defaults */
	set_dialog_mode (dp, POLL_TYPE_NONE);
	/* FIXME: no help yet */
	gtk_widget_hide (dp->help_button);
}

static void
set_dialog_mode (DrivelPoll *dp, gint mode)
{
	switch (mode) {
		case POLL_TYPE_NONE:
			gtk_widget_show (dp->options_main);
			gtk_widget_set_sensitive (dp->options_main, FALSE);
			gtk_widget_hide (dp->options_textentry);
			gtk_widget_hide (dp->options_scale);
			gtk_widget_hide (dp->options_answer);
			gtk_widget_set_sensitive (dp->add_question_button, TRUE);
			gtk_widget_set_sensitive (dp->add_answer_button, FALSE);
			gtk_widget_set_sensitive (dp->delete_item_button, FALSE);
			gtk_widget_set_sensitive (dp->up_button, FALSE);
			gtk_widget_set_sensitive (dp->down_button, FALSE);
			break;
		case POLL_TYPE_COMBO:
		case POLL_TYPE_RADIO:
		case POLL_TYPE_CHECK:
			gtk_widget_show (dp->options_main);
			gtk_widget_set_sensitive (dp->options_main, TRUE);
			gtk_widget_hide (dp->options_textentry);
			gtk_widget_hide (dp->options_scale);
			gtk_widget_hide (dp->options_answer);
			gtk_widget_set_sensitive (dp->add_question_button, TRUE);
			gtk_widget_set_sensitive (dp->add_answer_button, TRUE);
			gtk_widget_set_sensitive (dp->delete_item_button, TRUE);
			break;
		case POLL_TYPE_ANSWER:
			gtk_widget_hide (dp->options_main);
			gtk_widget_hide (dp->options_textentry);
			gtk_widget_hide (dp->options_scale);
			gtk_widget_show (dp->options_answer);
			gtk_widget_set_sensitive (dp->add_question_button, TRUE);
			gtk_widget_set_sensitive (dp->add_answer_button, TRUE);
			gtk_widget_set_sensitive (dp->delete_item_button, TRUE);
			break;
		case POLL_TYPE_ENTRY:
			gtk_widget_show (dp->options_main);
			gtk_widget_set_sensitive (dp->options_main, TRUE);
			gtk_widget_show (dp->options_textentry);
			gtk_widget_hide (dp->options_scale);
			gtk_widget_hide (dp->options_answer);
			gtk_widget_set_sensitive (dp->add_question_button, TRUE);
			gtk_widget_set_sensitive (dp->add_answer_button, FALSE);
			gtk_widget_set_sensitive (dp->delete_item_button, FALSE);
			break;
		case POLL_TYPE_SCALE:
			gtk_widget_show (dp->options_main);
			gtk_widget_set_sensitive (dp->options_main, TRUE);
			gtk_widget_hide (dp->options_textentry);
			gtk_widget_show (dp->options_scale);
			gtk_widget_hide (dp->options_answer);
			gtk_widget_set_sensitive (dp->add_question_button, TRUE);
			gtk_widget_set_sensitive (dp->add_answer_button, FALSE);
			gtk_widget_set_sensitive (dp->delete_item_button, FALSE);
			break;
	}
}

static void
set_dialog_updown (DrivelPoll *dp, GtkTreeIter iter)
{
	GtkTreePath *path;

	path = gtk_tree_model_get_path (GTK_TREE_MODEL (dp->poll_store), &iter);
	gtk_widget_set_sensitive (dp->up_button, gtk_tree_path_prev (path));
	gtk_tree_path_free (path);
	gtk_widget_set_sensitive (dp->down_button, gtk_tree_model_iter_next (GTK_TREE_MODEL (dp->poll_store), &iter));
}

static GdkPixbuf
*get_type_pixbuf (gint type)
{
	GdkPixbuf *pixbuf;
	gchar *pixmapfile;

	switch (type) {
		case POLL_TYPE_COMBO:
			pixmapfile = g_strdup (DATADIR G_DIR_SEPARATOR_S "pixmaps"
					G_DIR_SEPARATOR_S "drivel" G_DIR_SEPARATOR_S "combobox.xpm");
			break;
		case POLL_TYPE_RADIO:
			pixmapfile = g_strdup (DATADIR G_DIR_SEPARATOR_S "pixmaps"
					G_DIR_SEPARATOR_S "drivel" G_DIR_SEPARATOR_S "radiobutton.xpm");
			break;
		case POLL_TYPE_CHECK:
			pixmapfile = g_strdup (DATADIR G_DIR_SEPARATOR_S "pixmaps"
					G_DIR_SEPARATOR_S "drivel" G_DIR_SEPARATOR_S "checkbutton.xpm");
			break;
		case POLL_TYPE_ENTRY:
			pixmapfile = g_strdup (DATADIR G_DIR_SEPARATOR_S "pixmaps"
					G_DIR_SEPARATOR_S "drivel" G_DIR_SEPARATOR_S "entry.xpm");
			break;
		case POLL_TYPE_SCALE:
			pixmapfile = g_strdup (DATADIR G_DIR_SEPARATOR_S "pixmaps"
					G_DIR_SEPARATOR_S "drivel" G_DIR_SEPARATOR_S "hscale.xpm");
			break;
		case POLL_TYPE_NONE:
		case POLL_TYPE_ANSWER:
		default:
			pixmapfile = NULL;
			break;
	}

	if (pixmapfile)
		pixbuf = gdk_pixbuf_new_from_file (pixmapfile, NULL);
	else
		pixbuf = NULL;

	g_free (pixmapfile);
	return pixbuf;
}

static void
move_down_cb (GtkWidget *button, gpointer user_data)
{
	GtkTreeIter iter, nextiter;
	GtkTreeSelection *selection;
	DrivelPoll *dp = (DrivelPoll *) user_data;

	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (dp->poll_structure));
	if (gtk_tree_selection_get_selected (selection, NULL, &iter))
	{
		nextiter = iter;
		if (gtk_tree_model_iter_next (GTK_TREE_MODEL (dp->poll_store), &nextiter))
		{
			gtk_tree_store_move_after (dp->poll_store, &iter, &nextiter);
			gtk_tree_selection_select_iter (selection, &iter);
		}
	}
	selection_changed_cb (selection, dp);
}

static void
move_up_cb (GtkWidget *button, gpointer user_data)
{
	GtkTreeIter iter, previter;
	GtkTreePath *path;
	GtkTreeSelection *selection;
	DrivelPoll *dp = (DrivelPoll *) user_data;

	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (dp->poll_structure));
	if (gtk_tree_selection_get_selected (selection, NULL, &iter))
	{
		path = gtk_tree_model_get_path (GTK_TREE_MODEL (dp->poll_store), &iter);
		if (gtk_tree_path_prev (path) && gtk_tree_model_get_iter (GTK_TREE_MODEL (dp->poll_store), &previter, path))
		{
			gtk_tree_store_move_before (dp->poll_store, &iter, &previter);
			gtk_tree_selection_select_iter (selection, &iter);
		}
		gtk_tree_path_free (path);
	}
	selection_changed_cb (selection, dp);
}

static void
add_question_cb (GtkWidget *button, gpointer user_data)
{
	GtkTreeIter iter;
	DrivelPoll *dp = (DrivelPoll *) user_data;

	gtk_tree_store_insert (dp->poll_store, &iter, NULL, -1);
	gtk_tree_store_set (dp->poll_store, &iter,
			0, POLL_TYPE_COMBO,
			1, get_type_pixbuf (POLL_TYPE_COMBO),
			2, "Question Text",
			-1);
	gtk_tree_view_expand_all (GTK_TREE_VIEW (dp->poll_structure));
	gtk_tree_selection_select_iter (gtk_tree_view_get_selection (GTK_TREE_VIEW (dp->poll_structure)), &iter);
	gtk_widget_grab_focus (dp->options_main_question);
}

static void
add_answer_cb (GtkWidget *button, gpointer user_data)
{
	GtkTreeIter parent, parentparent, iter;
	DrivelPoll *dp = (DrivelPoll *) user_data;
	gint type;

	if (gtk_tree_selection_get_selected (gtk_tree_view_get_selection (GTK_TREE_VIEW (dp->poll_structure)), NULL, &parent))
	{
		gtk_tree_model_get (GTK_TREE_MODEL (dp->poll_store), &parent,
				0, &type,
				-1);
		if (type == POLL_TYPE_ANSWER) {
			/* this is an answer, we need the parent of this iter */
			if (gtk_tree_model_iter_parent (GTK_TREE_MODEL (dp->poll_store), &parentparent, &parent))
			{
				parent = parentparent;
			}
		}
		gtk_tree_store_insert (dp->poll_store, &iter, &parent, -1);
		gtk_tree_store_set (dp->poll_store, &iter,
				0, POLL_TYPE_ANSWER,
				1, get_type_pixbuf (POLL_TYPE_ANSWER),
				2, "Question Answer",
				-1);
	}
	gtk_tree_view_expand_all (GTK_TREE_VIEW (dp->poll_structure));
	gtk_tree_selection_select_iter (gtk_tree_view_get_selection (GTK_TREE_VIEW (dp->poll_structure)), &iter);
	gtk_widget_grab_focus (dp->options_answer_text);
}

static void
delete_item_cb (GtkWidget *button, gpointer user_data)
{
	GtkTreeIter iter;
	GtkTreeSelection *selection;
	DrivelPoll *dp = (DrivelPoll *) user_data;

	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (dp->poll_structure));
	if (gtk_tree_selection_get_selected (selection, NULL, &iter))
	{
		if (gtk_tree_store_remove (dp->poll_store, &iter))
			gtk_tree_selection_select_iter (selection, &iter);
	}
	selection_changed_cb (selection, dp);
}

static void
cancel_cb (GtkWidget *button, gpointer user_data)
{
	DrivelPoll *dp = (DrivelPoll *) user_data;
	gtk_widget_destroy (dp->dialog);
}


static void
destroy_cb (GtkWidget *widget, gpointer user_data)
{
	DrivelPoll *dp = (DrivelPoll *) user_data;
	drivel_pop_current_window (dp->dc);
	g_free (dp);
}

static void
insert_cb (GtkWidget *button, gpointer user_data)
{
	GtkTreeIter iter, parent;
	gint type, value1, value2, value3;
	gchar *text, *who, *whoview;
	GString *poll_text = NULL;
	DrivelPoll *dp = (DrivelPoll *) user_data;

	poll_text = g_string_new ("");
	
	switch (gtk_combo_box_get_active (GTK_COMBO_BOX (dp->poll_voters)))
	{
		case 0:
			who = g_strdup ("all");
			break;
		case 1:
			who = g_strdup ("friends");
			break;
		default:
			who = g_strdup ("");
	}

	switch (gtk_combo_box_get_active (GTK_COMBO_BOX (dp->poll_results)))
	{
		case 0:
			whoview = g_strdup ("all");
			break;
		case 1:
			whoview = g_strdup ("friends");
			break;
		case 2:
			whoview = g_strdup ("none");
			break;
		default:
			whoview = g_strdup ("");
	}

	g_string_append_printf (poll_text, "<lj-poll name=\"%s\" who=\"%s\" whoview=\"%s\">\n",
			gtk_entry_get_text (GTK_ENTRY (dp->poll_name)), who, whoview);
	g_free (who);
	g_free (whoview);

	while (gtk_tree_model_iter_children (GTK_TREE_MODEL (dp->poll_store), &parent, NULL))
	{
		gtk_tree_model_get (GTK_TREE_MODEL (dp->poll_store), &parent,
				0, &type,
				2, &text,
				3, &value1,
				4, &value2,
				5, &value3,
				-1);
		g_string_append (poll_text, "<lj-pq type=\"");
		switch (type)
		{
			case POLL_TYPE_COMBO:
				g_string_append (poll_text, "drop\">");
				break;
			case POLL_TYPE_RADIO:
				g_string_append (poll_text, "radio\">");
				break;
			case POLL_TYPE_CHECK:
				g_string_append (poll_text, "check\">");
				break;
			case POLL_TYPE_ENTRY:
				g_string_append_printf (poll_text, "text\" size=\"%i\" maxlength=\"%i\">", value1, value2);
				break;
			case POLL_TYPE_SCALE:
				g_string_append_printf (poll_text, "scale\" from=\"%i\" to=\"%i\" by=\"%i\">", value1, value2, value3);
				break;
		}
		g_string_append_printf (poll_text, "%s\n", text);
		if (type == POLL_TYPE_COMBO || type == POLL_TYPE_RADIO || type == POLL_TYPE_CHECK)
			while (gtk_tree_model_iter_children (GTK_TREE_MODEL (dp->poll_store), &iter, &parent))
			{
				gtk_tree_model_get (GTK_TREE_MODEL (dp->poll_store), &iter,
						0, &type,
						2, &text,
						-1);
				if (type == POLL_TYPE_ANSWER)
				{
					g_string_append_printf (poll_text, "<lj-pi>%s</lj-pi>\n", text);
				}
				gtk_tree_store_remove (dp->poll_store, &iter);
			}
		g_string_append (poll_text, "</lj-pq>\n");
		gtk_tree_store_remove (dp->poll_store, &parent);
	}
	g_string_append (poll_text, "</lj-poll>\n");

	gtk_text_buffer_delete_selection (dp->dc->buffer, FALSE, FALSE);
	gtk_text_buffer_insert_at_cursor (dp->dc->buffer, poll_text->str, poll_text->len);
	g_string_free (poll_text, TRUE);
	
	gtk_widget_destroy (dp->dialog);
}

static void
selection_changed_cb (GtkTreeSelection *tree_selection, gpointer user_data)
{
	GtkTreeIter iter;
	DrivelPoll *dp = (DrivelPoll *) user_data;
	gint type, value1, value2, value3;
	gchar *text;

	if (gtk_tree_selection_get_selected (tree_selection, NULL, &iter))
	{
		gtk_tree_model_get (GTK_TREE_MODEL (dp->poll_store), &iter,
				0, &type,
				2, &text,
				3, &value1,
				4, &value2,
				5, &value3,
				-1);
		set_dialog_mode (dp, type);
		set_dialog_updown (dp, iter);
		switch (type) {
			case POLL_TYPE_COMBO:
			case POLL_TYPE_RADIO:
			case POLL_TYPE_CHECK:
				gtk_entry_set_text (GTK_ENTRY (dp->options_main_question), text);
				gtk_combo_box_set_active (GTK_COMBO_BOX (dp->options_main_type), type);
				break;
			case POLL_TYPE_ANSWER:
				gtk_entry_set_text (GTK_ENTRY (dp->options_answer_text), text);
				break;
			case POLL_TYPE_ENTRY:
				gtk_entry_set_text (GTK_ENTRY (dp->options_main_question), text);
				if (value1)
					gtk_entry_set_text (GTK_ENTRY (dp->options_textentry_fieldsize), g_strdup_printf ("%i", value1));
				else
					gtk_entry_set_text (GTK_ENTRY (dp->options_textentry_fieldsize), g_strdup(""));
				if (value2)
					gtk_entry_set_text (GTK_ENTRY (dp->options_textentry_maxlength), g_strdup_printf ("%i", value2));
				else
					gtk_entry_set_text (GTK_ENTRY (dp->options_textentry_maxlength), g_strdup(""));
				gtk_combo_box_set_active (GTK_COMBO_BOX (dp->options_main_type), type);
				break;
			case POLL_TYPE_SCALE:
				gtk_entry_set_text (GTK_ENTRY (dp->options_main_question), text);
				if (!value1)
					value1 = 1;
				if (!value2)
					value2 = 20;
				if (!value3)
					value3 = 1;
				gtk_spin_button_set_value (GTK_SPIN_BUTTON (dp->options_scale_from), value1);
				gtk_spin_button_set_value (GTK_SPIN_BUTTON (dp->options_scale_to), value2);
				gtk_spin_button_set_value (GTK_SPIN_BUTTON (dp->options_scale_by), value3);
				gtk_combo_box_set_active (GTK_COMBO_BOX (dp->options_main_type), type);
				break;
		}
	}
	else
	{
		set_dialog_mode (dp, POLL_TYPE_NONE);
	}
}

static void
options_main_type_cb (GtkWidget *type, gpointer user_data)
{
	GtkTreeIter iter, parent;
	GtkTreeSelection *tree_selection;
	gint question_type;
	DrivelPoll *dp = (DrivelPoll *) user_data;

	tree_selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (dp->poll_structure));
	question_type = gtk_combo_box_get_active (GTK_COMBO_BOX (type));
	
	if (gtk_tree_selection_get_selected (tree_selection, NULL, &iter))
	{
		gtk_tree_store_set (dp->poll_store, &iter,
				0, question_type,
				1, get_type_pixbuf (question_type),
				-1);
	}
	parent = iter;
	if (question_type == POLL_TYPE_ENTRY || question_type == POLL_TYPE_SCALE)
	{
		while (gtk_tree_model_iter_children (GTK_TREE_MODEL (dp->poll_store), &iter, &parent))
		{
			gtk_tree_store_remove (dp->poll_store, &iter);		
		}
	}
	selection_changed_cb (tree_selection, user_data);
}

static void
options_textentry_cb (GtkEditable *question, gpointer user_data)
{
	GtkTreeIter iter;
	DrivelPoll *dp = (DrivelPoll *) user_data;

	if (gtk_tree_selection_get_selected (gtk_tree_view_get_selection (GTK_TREE_VIEW (dp->poll_structure)), NULL, &iter))
	{
		gtk_tree_store_set (dp->poll_store, &iter,
				2, gtk_entry_get_text (GTK_ENTRY (question)),
				-1);
	}
}

static void
options_textentry_changed_cb (GtkEditable *entry, gpointer user_data)
{
	GtkTreeIter iter;
	gint value1, value2;
	DrivelPoll *dp = (DrivelPoll *) user_data;

	value1 = g_strtod (gtk_entry_get_text (GTK_ENTRY (dp->options_textentry_fieldsize)), NULL);
	value2 = g_strtod (gtk_entry_get_text (GTK_ENTRY (dp->options_textentry_maxlength)), NULL);

	if (gtk_tree_selection_get_selected (gtk_tree_view_get_selection (GTK_TREE_VIEW (dp->poll_structure)), NULL, &iter))
	{
		gtk_tree_store_set (dp->poll_store, &iter,
				3, value1,
				4, value2,
				-1);
	}

	if (value1)
		gtk_entry_set_text (GTK_ENTRY (dp->options_textentry_fieldsize), g_strdup_printf("%i", value1));
	if (value2)
		gtk_entry_set_text (GTK_ENTRY (dp->options_textentry_maxlength), g_strdup_printf("%i", value2));
}

static void
options_scale_changed_cb (GtkWidget *spinner, gpointer user_data)
{
	GtkTreeIter iter;
	DrivelPoll *dp = (DrivelPoll *) user_data;

	if (gtk_tree_selection_get_selected (gtk_tree_view_get_selection (GTK_TREE_VIEW (dp->poll_structure)), NULL, &iter))
	{
		gtk_tree_store_set (dp->poll_store, &iter,
				3, gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (dp->options_scale_from)),
				4, gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (dp->options_scale_to)),
				5, gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (dp->options_scale_by)),
				-1);
	}
}
