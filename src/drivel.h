/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * Copyright 2002-2005 Todd Kulesza
 *
 * Authors:
 * 		Todd Kulesza <todd@dropline.net>
 */

#ifndef _DRIVEL_H_
#define _DRIVEL_H_

#include "drivel_request.h"
#include "egg-recent.h"
#include "utils.h"

#define _XOPEN_SOURCE /* glibc2 needs this */
#include <time.h>
#include <gtk/gtk.h>
#include <libgnomevfs/gnome-vfs.h>
#include <gconf/gconf-client.h>

#define DRIVEL_APPKEY "EB85AE01035D2303C7D70415B673CE9937BE4056DE691BF2"
#define DRIVEL_N_RECENT_POSTS 15

typedef struct _DrivelClient DrivelClient;
typedef struct _DrivelNet DrivelNet;
typedef struct _DrivelGConfData DrivelGConfData;
typedef struct _DrivelIDs DrivelIDs;
typedef struct _DrivelTime DrivelTime;
typedef struct _LJMenuItem LJMenuItem;
typedef struct _LJFriend LJFriend;
typedef struct _DrivelButtonVAList DrivelButtonVAList;
typedef struct _DrivelJournalEntry DrivelJournalEntry;
typedef struct _DrivelJournalProp DrivelJournalProp;

struct _LJFriend
{
	gchar *name;
	gchar *username;
	gchar *bg;
	gchar *fg;
	gint type;
	guint groupmask;
	gboolean friend;
	gboolean friend_of;
};

enum
{
	FRIEND_TYPE_USER,
	FRIEND_TYPE_COMMUNITY,
	FRIEND_TYPE_FEED
};

struct _LJMenuItem
{
	gchar *label;
	gchar *url;
	gushort menu_index;
	gushort item_index;
	gushort sub_menu;
};

struct _DrivelTime
{
	gint year;
	gint month;
	gint day;
	gint hour;
	gint minute;
};

struct _DrivelButtonVAList
{
	GtkWidget *button;
	GSList *entries;
	GSList *signals;
};

struct _DrivelIDs
{
	/* gconf ids */
	guint mood_id;
	guint music_id;
	guint picture_id;
	guint security_id;
	guint security_mask_id;
	guint comment_id;
	guint autoformat_id;
	guint proxy_id;
	guint proxy_url_id;
	guint proxy_port_id;
	guint proxy_user_id;
	guint proxy_pass_id;
	guint tray_id;
	guint expander_open_id;
	guint highlight_syntax_id;
	guint spellcheck_id;
	guint spell_language_id;
	guint last_journal_id;
	guint technorati_id;
	
	/* loop ids */
	guint loop_checkfriends_id;
};

struct _DrivelGConfData
{
	/* login window preferences */
	gchar *user;
	
	/* journal entry preferences */
	gchar *default_mood;
	gchar *default_music;
	gchar *default_picture;
	gchar *default_security;
	gchar *default_security_mask;
	gchar *default_comment;
	gchar *default_autoformat;
	gchar *expander_open;
	gchar *highlight_syntax;
	gchar *spellcheck;
	gchar *spell_language;
	gchar *last_journal;
	gchar *technorati;
	
	/* notification area preferences */
	gchar *tray;
	
	/* entry window position and size */
	gchar *entry_x;
	gchar *entry_y;
	gchar *entry_height;
	gchar *entry_width;
	gchar *entry_max;
	gchar *min_start;
	gchar *min_post;
};

struct _DrivelJournalEntry
{
	gchar *subject;
	gchar *content;
	time_t date_posted;
	gchar *security; /* lj security type */
	gchar *security_mask; /* lj security mask */
	gchar *postid; /* post_uri for atom, postid for others */
	gchar *userid; /* feed_uri for atom, userid for others */
	gchar *issued;
	gchar *link;
	GArray *properties; /* lj meta data */
};

struct _DrivelJournalProp
{
	gchar *id;
	gchar *name;
	gchar *value;
};

struct _DrivelNet
{
	DrivelBlogAPI api;
	gint fast_servers;
	GnomeVFSCancellation *cancel;
};

struct _DrivelClient
{
	/* windows */
	GtkWidget *login_window;
	GtkWidget *journal_window;
	GtkWidget *current_window;
	GtkWidget *friends_list_window;
	GtkWidget *edit_history_window;

	/* location of the configuration directory */
	gchar *config_directory;
	
	/* login window widgets */
	GtkWidget *login_name;
	GtkWidget *login_password;
	GtkWidget *save_password;
	GtkWidget *autologin;
	GtkWidget *login_button;
	GtkWidget *login_menu;
	GtkWidget *login_type;
	GtkWidget *login_server;
	GtkWidget *login_remove;
	GtkWidget *statusbar;
	GtkListStore *login_type_store;
	
	/* journal window widgets */
	GtkWidget *journal_subject;
	GtkWidget *journal_mood;
	GtkWidget *journal_music;
	GtkWidget *journal_text;
	GtkWidget *journal_security;
	GtkWidget *journal_expander;
	GtkWidget *journal_picture;
	GtkWidget *journal_comment;
	GtkWidget *journal_autoformat;
	GtkWidget *journal_backdate;
	GtkWidget *journal_date;
	GtkWidget *journal_category;
	GtkTextBuffer *buffer;
	GtkWidget *journal_post;
	GtkWidget *edit_delete;
	GtkWidget *edit_save;
	GtkWidget *edit_cancel;
	GtkUIManager *menus;
	GtkWidget *menu_post;
	GtkWidget *menu_update;
	GtkWidget *menu_delete;
	GtkWidget *menu_undo;
	GtkWidget *menu_redo;
	GtkWidget *menu_cut;
	GtkWidget *menu_copy;
	GtkWidget *menu_paste;
	GtkWidget *menu_last;
	GtkWidget *menu_friends;
	GtkWidget *menu_history;
	GtkWidget *menu_security;
	GtkWidget *menu_active;
	GtkAction *menu_view_options;
	GtkAction *menu_view_misspelled_words;
	GtkAction *menu_view_html_keywords;
	EggRecentModel *recent_model;
	EggRecentViewUIManager *recent_view;
	DrivelJournalEntry *journal_entry;
	
	/* friends dialog */
	GtkListStore *list_store;
	GtkWidget *friend_list;
	GtkWidget *friend_remove;
	GtkWidget *friend_view_journal;
	GtkWidget *friends_username;
	GtkWidget *friends_fg_colour;
	GtkWidget *friends_bg_colour;
	GtkWidget *friends_type_icon;
	GtkWidget *friends_type_name;
	GtkWidget *dialog_add_friend;
	GtkWidget *dialog_add_fg;
	GtkWidget *dialog_add_bg;
	GtkWidget *dialog_add_ok;
	
	/* history dialog */
	GtkListStore *history_store;
	GtkWidget *history_list;
	GtkWidget *history_calendar;
	GtkWidget *history_edit;
	
	/* insert link dialog */
	GtkWidget *link_text;
	GtkWidget *link_text_label;
	GtkWidget *link_url;
	GtkWidget *link_url_label;
	GtkWidget *link_url_example;
	GtkWidget *link_user;
	GtkWidget *link_user_label;
	GtkWidget *link_ok;
	DrivelButtonVAList *link_bval;
	
	/* insert image dialog */
	GtkWidget *image_url;
	GtkWidget *image_height;
	GtkWidget *image_width;
	GtkWidget *image_alt;
	GtkWidget *image_ok;
	DrivelButtonVAList *image_bval;
	
	/* network data */
	DrivelNet *net;
	guint tag_autosave;
	gint time_since_checkfriends;
	gchar *lastupdate;
	gboolean checking_friends;
	gboolean friends_update;

	/* preferences widgets */
	GtkWidget *pref_dictionary;
	GtkWidget *pref_dictionary_box;
	
	GConfClient *client;
	DrivelGConfData *gconf;
	
	/* user-specific settings */
	DrivelUser *user;
	GSList *user_list;
	gboolean use_fast_servers;
	GHashTable *picture_keywords;
	GHashTable *picture_filenames;
	gchar *default_picture_file;
	GHashTable *mood_icons;
	GSList *mood_list;
	gint pictures;
	gint moods;
	gint journals;
	GSList *journal_list;
	DrivelJournal *active_journal;
	gboolean modified;
	gboolean modified_autosave;
	gboolean proxy;
	gboolean proxy_auth;
	gchar *proxy_user;
	gchar *proxy_pass;
	gchar *proxy_url;
	gint proxy_port;
	GSList *menu_list;
	GSList *friends_list;
	gboolean edit_entry;
	GSList *window_list;
	GtkListStore *picture_store;
	GtkListStore *category_store;
	GtkTreeModel *category_store_filtered;
	GtkListStore *security_store;
	GtkListStore *security_store_filtered;
	gchar *draft_filename;
	DrivelTime time;
	DrivelIDs id;
	GArray *recent_entries;
};

void
display_error_dialog (DrivelClient *dc, const gchar *header, const gchar *mesg);

void
debug (const gchar *msg);

void
fill_time (DrivelClient *dc);

void
drivel_button_list_clear (DrivelButtonVAList *bval);

void
drivel_push_current_window (DrivelClient *dc, GtkWidget *window);

GtkWidget *
drivel_pop_current_window (DrivelClient *dc);

GtkWidget *
drivel_get_current_window (GSList *list);

void
drivel_gconf_data_fill (DrivelGConfData *data, const gchar *username, 
		DrivelBlogAPI api, GConfClient *client, DrivelIDs *id, DrivelClient *dc);

void
drivel_fill_journal_null (DrivelClient *dc);

gboolean
validate_username (const gchar *username);

gchar *
get_default_text (GConfClient *client, const gchar *key, const gchar *standard_text);

void
list_free_item (gpointer data, gpointer user_data);

void
menu_list_free_item (gpointer data, gpointer user_data);

void
friends_list_free_item (gpointer data, gpointer user_data);

gint
compare_usernames (gconstpointer a, gconstpointer b);

void
fill_security_menu (DrivelClient *dc, GtkListStore *security_store);

void
fill_picture_menu (DrivelClient *dc, GtkListStore *picture_store);

void
add_account_to_list (DrivelClient *dc);

gint
sort_journals (DrivelJournal *a, DrivelJournal *b);

void
clear_recent_entries (GArray *recent);

#endif
