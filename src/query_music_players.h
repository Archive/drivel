/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * Copyright 2002-2004 Todd Kulesza, Davyd Madeley
 *
 * Authors:
 * 		Davyd Madeley <davyd@ucc.asn.au>
 */

#ifndef _QUERY_MUSIC_PLAYERS_H_
#define _QUERY_MUSIC_PLAYERS_H_

#include <gtk/gtk.h>

void
query_music_players (GtkListStore *playing);

#endif
