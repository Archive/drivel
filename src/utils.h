/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * Copyright 2002-2004 Todd Kulesza
 *
 * Authors:
 * 		Todd Kulesza <todd@dropline.net>
 */

#ifndef _UTILS_H_
#define _UTILS_H_

#include <glib.h>
#include <gtk/gtk.h>
#include <glade/glade.h>

#include "drivel_request.h"

typedef struct _DrivelUser DrivelUser;
typedef struct _DrivelJournal DrivelJournal;

struct _DrivelUser
{
	gchar *username;
	gchar *password;
	DrivelBlogAPI api;
	gchar *server;
	gchar *rsd;
	gchar *cookie;
	gboolean save_password;
	gboolean autologin; /* automatically login this account when Drivel starts */
	gboolean lastuser; /* this was the last account to log in */
};

struct _DrivelJournal
{
	gchar *name;
	gchar *description;
	gchar *uri_view;
	gchar *uri_post;
	gchar *uri_feed;
	gchar *id;
	guint type;
};

enum
{
	JOURNAL_TYPE_USER,
	JOURNAL_TYPE_COMMUNITY
};

enum
{
	STORE_SECURITY_ICON,
	STORE_SECURITY_NAME,
	STORE_SECURITY_NUM,
	STORE_SECURITY_ORDER,
	STORE_SECURITY_PUBLIC,
	STORE_SECURITY_N
};

enum
{
	STORE_CATEGORY_NAME,
	STORE_CATEGORY_ID,
	STORE_CATEGORY_BLOG,
	STORE_CATEGORY_N
};

void
hash_table_item_free (gpointer data);

void
hash_table_clear (GHashTable *table);

gint
string_compare (gconstpointer a, gconstpointer b);

GladeXML*
load_glade_xml (const gchar *root);

gboolean
picture_exists (const gchar *config_dir, const gchar *pic_file);

GSList*
load_user_list (const gchar *config_dir);

void
save_user_list (GSList *list, const gchar *config_dir);

GSList*
find_in_user_list (GSList *list, DrivelUser *du);

DrivelUser*
drivel_user_new (void);

void
drivel_user_free (DrivelUser *du);

DrivelJournal*
drivel_journal_new (void);

void
drivel_journal_free (DrivelJournal *dj);

gchar*
build_nonce (void);

gchar*
get_w3dtf_timestamp (void);

gchar*
base64_encode (const gchar *text);

gchar*
sha1_hash (const gchar *text);

gchar*
md5_hash (const gchar *text);

void
store_security_append (GtkListStore *store, const gchar *icon, 
		const gchar *name, gint num, gint order, gboolean public);

void
select_security_group (GtkTreeModel *model, GtkComboBox *combo, 
		const gchar *name, gint mask);

gchar*
title_from_content (const gchar *content);

#endif /* _UTILS_H_ */
