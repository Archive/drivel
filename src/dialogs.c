/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * Copyright 2002-2004 Todd Kulesza
 *
 * Authors:
 * 		Todd Kulesza <todd@dropline.net>
 */

#include <config.h>

#include <stdarg.h> /* variable-length functions */
#include <gnome.h>
#include <gdk-pixbuf/gdk-pixdata.h>
#include <glade/glade.h>
#include <string.h>
#include <gtksourceview/gtksourceview.h>

#include "drivel_request.h"
#include "blog_lj.h"
#include "drivel.h"
#include "journal.h"
#include "network.h"
#include "dialogs.h"

typedef struct _DrivelSpellLanguage DrivelSpellLanguage;

/* realname, the name that aspell understands, i.e. en_US, en_GB etc.
   label, the name displayed to the user, fallbacks to realname */
struct _DrivelSpellLanguage
{
	gchar *realname;
	gchar *label;
};

void
history_list_edit_cb (GtkWidget *button, gpointer data);

static void
grey_button_cb (GtkWidget *entry, gpointer data)
{
	GtkWidget *w;
	gchar *text;
	GSList *current;
	gboolean state = TRUE;
	DrivelButtonVAList *bval = (DrivelButtonVAList *) data;
	
	for (current = bval->entries; current; current = current->next)
	{
		w = GTK_WIDGET (current->data);
		
		if (!GTK_WIDGET_IS_SENSITIVE (w))
			continue;
		
		text = gtk_editable_get_chars (GTK_EDITABLE (w), 0, -1);
	
		if (!text || !strncmp (text, "", strlen (text)))
			state = FALSE;
	
		g_free (text);
	}
	
	gtk_widget_set_sensitive (bval->button, state);
	
	return;
}

static DrivelButtonVAList *
dialog_grey_button_on_invalid (GtkWidget *button, GtkWidget *entry, ...)
{
	va_list *args;
	gint signal;
	GtkWidget *w;
	DrivelButtonVAList *bval;
	
	args = g_new0 (va_list, 1);
	
	va_start (*args, entry);

	bval = g_new0 (DrivelButtonVAList, 1);
	bval->button = button;
	bval->entries = NULL;
	bval->signals = NULL;
	
	for (w = entry; w; w = va_arg (*args, GtkWidget *))
	{
		bval->entries = g_slist_prepend (bval->entries, w);
		signal = g_signal_connect (G_OBJECT (w), "changed",
				G_CALLBACK (grey_button_cb), bval);
		bval->signals = g_slist_prepend (bval->signals,
				GINT_TO_POINTER (signal));
	}
	
	va_end (*args);
	
	return bval;
}

void
update_history_marks (DrivelClient *dc, gint8 days [])
{
	gint i;
	
	gtk_calendar_freeze (GTK_CALENDAR (dc->history_calendar));
	
	gtk_calendar_clear_marks (GTK_CALENDAR (dc->history_calendar));
	
	for (i = 0; i < 31; i++)
		if (days [i])
			gtk_calendar_mark_day (GTK_CALENDAR (dc->history_calendar), i + 1);
	
	gtk_calendar_thaw (GTK_CALENDAR (dc->history_calendar));
	
	return;
}

static void
mark_history_list (DrivelClient *dc)
{
	blog_lj_build_getdaycounts_request (dc->user->username, 
					    dc->user->server, dc->active_journal);
	
	return;
}

static void
mark_history_list_cb (GtkWidget *calendar, gpointer data)
{
	DrivelClient *dc = (DrivelClient *) data;
	
	mark_history_list (dc);
	
	return;
}

static void
dialog_close_cb (GtkWidget *widget, gint arg, gpointer data)
{
	DrivelClient *dc = (DrivelClient *) data;
	
	if (arg == GTK_RESPONSE_CLOSE || arg == GTK_RESPONSE_DELETE_EVENT)
	{
		drivel_pop_current_window (dc);
			
		gtk_widget_destroy (widget);
	}
	
	return;
}

static void
history_response_cb (GtkWidget *widget, gint arg, gpointer data)
{
	DrivelClient *dc = (DrivelClient *) data;

	if (arg == GTK_RESPONSE_OK)
	{
		history_list_edit_cb (NULL, data);
		gtk_window_present (GTK_WINDOW (dc->journal_window));
	}
	else {
		drivel_pop_current_window (dc);
		gtk_widget_destroy (widget);
		dc->edit_history_window = NULL;
	}
	return;
}

static void
preferences_close_cb (GtkWidget *widget, gint arg, gpointer data)
{
	DrivelClient *dc = (DrivelClient *) data;
	
	if (arg == GTK_RESPONSE_CLOSE || arg == GTK_RESPONSE_DELETE_EVENT)
	{
		gtk_widget_destroy (widget);
		drivel_pop_current_window (dc);
	}
	
	return;
}

void
update_friends_list (DrivelClient *dc)
{
	GSList *list_item;
	LJFriend *friend;
	GdkPixbuf *user, *community, *feed, *link, *type;
	GdkPixbuf *leftarrow, *rightarrow, *doublearrow;
	GtkTreeIter iter;
	
	gtk_list_store_clear (dc->list_store);
	
	user = gdk_pixbuf_new_from_file (DATADIR G_DIR_SEPARATOR_S "pixmaps" 
		G_DIR_SEPARATOR_S "drivel" G_DIR_SEPARATOR_S "user.png",
		NULL);
	community = gdk_pixbuf_new_from_file (DATADIR G_DIR_SEPARATOR_S "pixmaps" 
		G_DIR_SEPARATOR_S "drivel" G_DIR_SEPARATOR_S "community.png",
		NULL);

	feed = gdk_pixbuf_new_from_file (DATADIR G_DIR_SEPARATOR_S "pixmaps"
		G_DIR_SEPARATOR_S "drivel" G_DIR_SEPARATOR_S "feed.png",
		NULL);
	
	leftarrow = gdk_pixbuf_new_from_file (DATADIR G_DIR_SEPARATOR_S "pixmaps" 
		G_DIR_SEPARATOR_S "drivel" G_DIR_SEPARATOR_S "leftarrow.png",
		NULL);
	rightarrow = gdk_pixbuf_new_from_file (DATADIR G_DIR_SEPARATOR_S "pixmaps" 
		G_DIR_SEPARATOR_S "drivel" G_DIR_SEPARATOR_S "rightarrow.png",
		NULL);
	doublearrow = gdk_pixbuf_new_from_file (DATADIR G_DIR_SEPARATOR_S "pixmaps" 
		G_DIR_SEPARATOR_S "drivel" G_DIR_SEPARATOR_S "doublearrow.png",
		NULL);
	
	for (list_item = dc->friends_list; list_item; list_item = list_item->next)
	{
		friend = list_item->data;
		if (friend->type == FRIEND_TYPE_COMMUNITY)
			type = community;
		else if (friend->type == FRIEND_TYPE_FEED)
			type = feed;
		else
			type = user;
		
		if (friend->friend && !friend->friend_of)
			link = rightarrow;
		else if (!friend->friend && friend->friend_of)
			link = leftarrow;
		else
			link = doublearrow;
		
		gtk_list_store_append (dc->list_store, &iter);
		gtk_list_store_set (dc->list_store, &iter,
				TYPE_COL, type,
				LINK_COL, link,
				USERNAME_COL, friend->username,
				NAME_COL, friend->name,
				FRIEND_COL, friend->friend,
				FRIENDOF_COL, friend->friend_of,
				FG_COL, friend->fg,
				BG_COL, friend->bg,
				TYPE_INT_COL, friend->type,
				-1);
	}
	
	gtk_widget_set_sensitive (dc->friend_remove, FALSE);
	
	return;
}

static void
update_history_list_cb (GtkWidget *widget, gpointer data)
{
	guint day, month, year;
	DrivelClient *dc = (DrivelClient *) data;
	
	dc->edit_entry = FALSE;
	/* setting the calendar false will stop it's urge to drag */
	gtk_widget_set_sensitive (dc->history_calendar, FALSE);
	
	gtk_calendar_get_date (GTK_CALENDAR (dc->history_calendar),
			&year, &month, &day);
	month++; /* the months are numbered starting with 0 */
	
	blog_lj_build_getevents_request (dc->user->username, dc->user->server,
					 50, TRUE, TRUE, "day", NULL, year, month, day, 0, NULL, 0, 
					 dc->active_journal);
	
	return;
}

void
update_history_list (DrivelClient *dc, gchar **itemid, gchar **event, gchar **eventtime, gint count)
{
	GtkTreeIter iter;
	GtkTreeSelection *selection;
	gint i, len;
	gchar *time;
	
	gtk_list_store_clear (dc->history_store);
	
	for (i = 0; i < count; i++)
	{
		len = strlen (eventtime [i]);
		time = g_new0 (gchar, len + 1);
		memcpy (time, eventtime [i] + 10, len - 13);
		
		gtk_list_store_append (dc->history_store, &iter);
		gtk_list_store_set (dc->history_store, &iter,
				HISTORY_TIME_COL, time,
				HISTORY_SUBJECT_COL, event [i],
				HISTORY_ITEMID_COL, itemid [i],
				-1);

		g_free (time);
	}
	
	if (i == 0)
	{
		
		selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (dc->history_list));
		gtk_tree_selection_set_mode (selection, GTK_SELECTION_NONE);
		
		gtk_list_store_append (dc->history_store, &iter);
		gtk_list_store_set (dc->history_store, &iter,
				HISTORY_TIME_COL, "",
				HISTORY_SUBJECT_COL, _("[No journal entries]"),
				-1);
	}
	else
	{
		selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (dc->history_list));
		gtk_tree_selection_set_mode (selection, GTK_SELECTION_SINGLE);
	}

	gtk_widget_set_sensitive (dc->history_calendar, TRUE);
	
	return;
}

#ifdef HAVE_GTKSPELL

static GSList *
get_available_spell_languages (void)
{
	GSList *rv = NULL;
	gchar *prgm, *cmd;
	gchar *prgm_err, *prgm_out;
	gint exit_status, i;
	GError *err = NULL;
	DrivelSpellLanguage *lang;
	gchar **lang_arr;
	
	if ((prgm = g_find_program_in_path ("aspell")) == NULL)
		return NULL;
	cmd = g_strdup_printf ("%s dump dicts", prgm);
	g_spawn_command_line_sync (cmd, &prgm_out, &prgm_err, &exit_status, &err);
	g_free (cmd);
	g_free (prgm);
	if (err)
	{
		g_warning ("Failed to get language list: %s", err->message);
		g_error_free (err);
		return NULL;
	}
	if (exit_status != 0)
	{
		g_warning ("Failed to get language list, program output was: %s", prgm_err);
		g_free (prgm_out);
		g_free (prgm_err);
		return NULL;
	}
	lang_arr = g_strsplit (prgm_out, "\n", -1);
	
	i = 0;
	while (lang_arr[i])
	{
		g_strstrip (lang_arr[i]);
		if (*(lang_arr[i]) != '\0')
		{
			lang = g_new0 (DrivelSpellLanguage, 1);
			/* For now, set realname == label */
			lang->realname = g_strdup (lang_arr[i]);
			lang->label = g_strdup (lang_arr[i]);
			rv = g_slist_insert_sorted (rv, lang, (GCompareFunc) g_strcasecmp);
		}
		i++;
	}
	g_strfreev (lang_arr);

	lang = g_new0 (DrivelSpellLanguage, 1);
	/* Context: Spell check dictionary */
	lang->label = g_strdup (_("System default"));
	lang->realname = g_strdup ("");
	rv = g_slist_prepend (rv, lang);
	
	return rv;
}
#endif /* HAVE_GTKSPELL */

static void
add_friend_dialog_cb (GtkWidget *widget, gint arg, gpointer data)
{
	gchar *friend, *fg, *bg;
	GdkColor color;
	DrivelClient *dc = (DrivelClient *) data;
	
	if (arg == GTK_RESPONSE_CANCEL)
		gtk_widget_destroy (widget);
	else if (arg == GTK_RESPONSE_OK)
	{
		friend = gtk_editable_get_chars (GTK_EDITABLE (dc->dialog_add_friend),
				0, -1);
		gtk_color_button_get_color (GTK_COLOR_BUTTON (dc->dialog_add_fg),
				&color);
		fg = g_strdup_printf ("#%02X%02X%02X", 
				color.red / 256, color.green / 256, color.blue / 256);
		gtk_color_button_get_color (GTK_COLOR_BUTTON (dc->dialog_add_bg),
				&color);
		bg = g_strdup_printf ("#%02X%02X%02X", 
				color.red / 256, color.green / 256, color.blue / 256);
		
		if (!friend || !strcmp (friend, ""))
		{
			display_error_dialog (dc, _("Missing username"), 
					_("Please enter a username to add."));
			
			return;
		}
		
		blog_lj_build_editfriends_request (dc->user->username, 
						   dc->user->server, friend, FALSE, TRUE, fg, bg);
		
		g_free (friend);
		g_free (fg);
		g_free (bg);

		gtk_widget_destroy (widget);
	}
	
	return;
}

static void
edit_friend_colour_cb (GtkWidget *widget, gpointer data)
{
	GdkColor color;
	gchar *friend, *fg, *bg;
	DrivelClient *dc = (DrivelClient *) data;
	
	friend = gtk_editable_get_chars (GTK_EDITABLE (dc->friends_username),
			0, -1);
	gtk_color_button_get_color (GTK_COLOR_BUTTON (dc->friends_fg_colour),
			&color);
	fg = g_strdup_printf ("#%02X%02X%02X", 
			color.red / 256, color.green / 256, color.blue / 256);
	gtk_color_button_get_color (GTK_COLOR_BUTTON (dc->friends_bg_colour),
			&color);
	bg = g_strdup_printf ("#%02X%02X%02X", 
			color.red / 256, color.green / 256, color.blue / 256);
	
	blog_lj_build_editfriends_request (dc->user->username, 
					   dc->user->server, friend, FALSE, FALSE, fg, bg);

	g_free (friend);
	g_free (fg);
	g_free (bg);

	return;
}

/* FIXME: figure out how this function is supposed to work */
static gint
sort_type_func (GtkTreeModel *model, GtkTreeIter *a, GtkTreeIter *b, gpointer data)
{
	gboolean a_type, b_type;
	gint retval;
	
	gtk_tree_model_get (model, a, TYPE_INT_COL, &a_type, -1);
	gtk_tree_model_get (model, b, TYPE_INT_COL, &b_type, -1);

	if (a_type < b_type)
		retval = 1;
	else
		retval = 0;
	
	return retval;
}

static void
tray_cb (GtkWidget *button, gpointer data)
{
	gboolean state;
	DrivelClient *dc = (DrivelClient *) data;
	
	state = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (button));
	
	gconf_client_set_bool (dc->client, dc->gconf->tray, state, NULL);
	
	return;
}

static void
technorati_cb (GtkWidget *button, gpointer data)
{
	gboolean state;
	DrivelClient *dc = (DrivelClient *) data;
	
	state = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (button));
	
	gconf_client_set_bool (dc->client, dc->gconf->technorati, state, NULL);
	
	return;
}

static void
min_start_cb (GtkWidget *button, gpointer data)
{
	gboolean state;
	DrivelClient *dc = (DrivelClient *) data;
	
	state = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (button));
	
	gconf_client_set_bool (dc->client, dc->gconf->min_start, state, NULL);
	
	return;
}

static void
min_post_cb (GtkWidget *button, gpointer data)
{
	gboolean state;
	DrivelClient *dc = (DrivelClient *) data;
	
	state = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (button));
	
	gconf_client_set_bool (dc->client, dc->gconf->min_post, state, NULL);
	
	return;
}

static void
highlight_syntax_cb (GtkWidget *button, gpointer user_data)
{
	gboolean state;
	DrivelClient *dc = (DrivelClient *) user_data;

	state = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (button));

	gconf_client_set_bool (dc->client, dc->gconf->highlight_syntax, state, NULL);

	return;
}

#ifdef HAVE_GTKSPELL
static void
spellcheck_cb (GtkWidget *button, gpointer user_data)
{
	gboolean state;
	DrivelClient *dc = (DrivelClient *) user_data;

	state = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (button));

	gconf_client_set_bool (dc->client, dc->gconf->spellcheck, state, NULL);

	return;
}

static void
spellcheck_language_cb (GtkComboBox *combobox, gpointer user_data)
{
	gchar *language;
	DrivelClient *dc = (DrivelClient *) user_data;
	GtkTreeModel *model;
	GtkTreeIter iter;
	gint sel;

	model = gtk_combo_box_get_model (combobox);
	sel = gtk_combo_box_get_active (combobox);

	if (gtk_tree_model_iter_nth_child (model, &iter, NULL, sel))
	{
		gtk_tree_model_get (model, &iter, 0, &language, -1);
		gconf_client_set_string (dc->client, dc->gconf->spell_language, language, NULL);
		g_free (language);
	}

	return;
}
#endif /* HAVE_GTKSPELL */

static void
picture_cb (GtkWidget *menu, gpointer data)
{
	gint number;
	DrivelClient *dc = (DrivelClient *) data;

	number = gtk_combo_box_get_active (GTK_COMBO_BOX (menu));

	gconf_client_set_int (dc->client, dc->gconf->default_picture, number, NULL);
	
	return;
}

static void
security_cb (GtkWidget *menu, gpointer data)
{
	gint num;
	gchar *name, *group;
	GtkTreeIter iter;
	DrivelClient *dc = (DrivelClient *) data;

	gtk_combo_box_get_active_iter (GTK_COMBO_BOX (menu), &iter);
	gtk_tree_model_get (gtk_combo_box_get_model (GTK_COMBO_BOX (menu)), &iter,
			STORE_SECURITY_NAME, &name, STORE_SECURITY_NUM, &num, -1);

	if (name && !strcmp (name, _("Public")))
	{
		group = g_strdup ("public");
		num = 0;
	}
	else if (name && !strcmp (name, _("Private")))
	{
		group = g_strdup ("private");
		num = 0;
	}
	else
	{
		group = g_strdup ("usemask");
		num = 1 << num;
	}
	
	gconf_client_set_string (dc->client, dc->gconf->default_security, group, NULL);
	gconf_client_set_int (dc->client, dc->gconf->default_security_mask, num, NULL);
	
	g_free (name);
	g_free (group);
	
	return;
}

static void
comments_cb (GtkWidget *button, gpointer data)
{
	gboolean state;
	DrivelClient *dc = (DrivelClient *) data;
	
	state = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (button));
	
	gconf_client_set_bool (dc->client, dc->gconf->default_comment, state, NULL);
	
	return;
}

static void
autoformat_cb (GtkWidget *button, gpointer data)
{
	gboolean state;
	DrivelClient *dc = (DrivelClient *) data;
	
	state = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (button));
	
	gconf_client_set_bool (dc->client, dc->gconf->default_autoformat, state, NULL);
	
	return;
}

static void
history_list_changed (GtkTreeSelection *selection, gpointer data)
{
	GtkTreeModel *model;
	GtkTreeIter iter;
	gchar *subject;
	gboolean state = FALSE;
	DrivelClient *dc = (DrivelClient *) data;
	
	if (gtk_tree_selection_get_selected (selection, &model, &iter))
	{
		gtk_tree_model_get (model, &iter, HISTORY_SUBJECT_COL, &subject, -1);
		
		if (strcmp (subject, _("[No journal entries]")))
			state = TRUE;
	}
	
	gtk_widget_set_sensitive (dc->history_edit, state);
	
	return;
}

void
history_list_edit_cb (GtkWidget *button, gpointer data)
{
	GtkTreeModel *model;
	GtkTreeIter iter;
	GtkTreeSelection *selection;
	gchar *itemid_string;
	gint itemid;
	DrivelClient *dc = (DrivelClient *) data;
	
	dc->edit_entry = TRUE;
	
	gtk_source_buffer_begin_not_undoable_action (GTK_SOURCE_BUFFER (dc->buffer));
	
	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (dc->history_list));
	if (gtk_tree_selection_get_selected (selection, &model, &iter))
	{
		gtk_tree_model_get (model, &iter, HISTORY_ITEMID_COL, &itemid_string, -1);
		itemid = g_strtod (itemid_string, NULL);

		blog_lj_build_getevents_request (dc->user->username, 
						 dc->user->server, 0, FALSE, FALSE, "one", NULL, 0, 0, 0, 0, 
						 NULL, itemid, dc->active_journal);
	}
	
	return;
}

static void
history_list_row_activated_cb (GtkTreeView *view, GtkTreePath *path, GtkTreeViewColumn *column, gpointer data)
{
	GtkTreeSelection *selection;
	DrivelClient *dc = (DrivelClient *) data;
	
	selection = gtk_tree_view_get_selection (view);
	if (gtk_tree_selection_get_mode (selection) != GTK_SELECTION_NONE)
		history_response_cb (dc->edit_history_window, GTK_RESPONSE_OK, dc);
	
	return;
}

static void
friends_list_changed (GtkTreeSelection *selection, gpointer data)
{
	GtkTreeModel *model;
	GtkTreeIter iter;
	gboolean friend;
	DrivelClient *dc = (DrivelClient *) data;
	
	if (gtk_tree_selection_get_selected (selection, &model, &iter))
	{
		gchar *friend_text, *fg, *bg, *type;
		GdkPixbuf *type_icon;
		gint type_int;
		GdkColor gdk_colour;
		gchar hex[5];

		friend_text = NULL;
		hex[0] = '0';
		hex[1] = 'X';
		hex[4] = '\0';

		gtk_tree_model_get (model, &iter, FRIEND_COL, &friend, -1);

		/* These should only be available if we have this user listed
		   as a friend. */
		gtk_widget_set_sensitive (dc->friend_remove, friend);
		gtk_widget_set_sensitive (dc->friends_fg_colour, friend);
		gtk_widget_set_sensitive (dc->friends_bg_colour, friend);

		gtk_tree_model_get (model, &iter,
				USERNAME_COL, &friend_text,
				FG_COL, &fg,
				BG_COL, &bg,
				TYPE_COL, &type_icon,
				TYPE_INT_COL, &type_int,
				-1);

		/* This should be sensitive if a username is available */
		if (friend_text)
		  gtk_widget_set_sensitive (dc->friend_view_journal, TRUE);
		else
		  gtk_widget_set_sensitive (dc->friend_view_journal, FALSE);


		if (type_int == FRIEND_TYPE_COMMUNITY)
			type = g_strdup (_("Community"));
		else if (type_int == FRIEND_TYPE_FEED)
			type = g_strdup (_("Syndicated Feed"));
		else
			type = g_strdup (_("User"));
		
		gtk_entry_set_text (GTK_ENTRY (dc->friends_username), friend_text);
		gtk_image_set_from_pixbuf (GTK_IMAGE (dc->friends_type_icon), type_icon);
		gtk_label_set_text (GTK_LABEL (dc->friends_type_name), type);

		g_free (type);

		gdk_colour.pixel = 0;
		g_snprintf( hex + 2, 3, "%s", fg + 1);
		gdk_colour.red = 256 * (guint) g_ascii_strtod (hex, NULL);
		g_snprintf (hex + 2, 3, "%s", fg + 3);
		gdk_colour.green  = 256 * (guint) g_ascii_strtod (hex, NULL);
		g_snprintf (hex + 2, 3, "%s", fg + 5);
		gdk_colour.blue = 256 * (guint) g_ascii_strtod (hex, NULL);
		gtk_color_button_set_color (GTK_COLOR_BUTTON (dc->friends_fg_colour), &gdk_colour);
		
		gdk_colour.pixel = 0;
		g_snprintf( hex + 2, 3, "%s", bg + 1);
		gdk_colour.red = 256 * (guint) g_ascii_strtod (hex, NULL);
		g_snprintf (hex + 2, 3, "%s", bg + 3);
		gdk_colour.green  = 256 * (guint) g_ascii_strtod (hex, NULL);
		g_snprintf (hex + 2, 3, "%s", bg + 5);
		gdk_colour.blue = 256 * (guint) g_ascii_strtod (hex, NULL);
		gtk_color_button_set_color (GTK_COLOR_BUTTON (dc->friends_bg_colour), &gdk_colour);
	}
	
	return;
}

static void
friends_list_add_cb (GtkWidget *button, gpointer data)
{
	GtkWidget *label;
	GtkWidget *entry;
	GtkWidget *color;
	GtkWidget *hbox, *hbox2, *vbox;
	GtkTreeSelection *selection;
	GtkTreeModel *model;
	GtkTreeIter iter;
	GdkColor gdk_color;
	gboolean friend;
	gchar *friend_name;
	static GtkWidget *dialog = NULL;
	DrivelClient *dc = (DrivelClient *) data;
	
	if (dialog)
	{
		gtk_window_present (GTK_WINDOW (dialog));
		
		return;
	}
	
	dialog = gtk_dialog_new_with_buttons (
			_("Add Friend"),
			GTK_WINDOW (dc->friends_list_window),
			GTK_DIALOG_DESTROY_WITH_PARENT,
			NULL);
	gtk_dialog_add_button (GTK_DIALOG (dialog),
			GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL);
	dc->dialog_add_ok = gtk_dialog_add_button (GTK_DIALOG (dialog),
			GTK_STOCK_ADD, GTK_RESPONSE_OK);
	gtk_widget_set_sensitive (dc->dialog_add_ok, FALSE);
	gtk_dialog_set_default_response (GTK_DIALOG (dialog),
			GTK_RESPONSE_OK);
	
	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (dc->friend_list));
	if (gtk_tree_selection_get_selected (selection, &model, &iter))
	{
		gtk_tree_model_get (model, &iter, FRIEND_COL, &friend,
				USERNAME_COL, &friend_name, -1);
	}
	else
		friend_name = g_strdup ("");
	
	vbox = gtk_vbox_new (FALSE, 12);
	gtk_container_set_border_width (GTK_CONTAINER (vbox), 12);
	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->vbox), vbox, TRUE, TRUE, 0);
	
	hbox = gtk_hbox_new (FALSE, 6);
	gtk_box_pack_start (GTK_BOX (vbox), hbox, TRUE, TRUE, 0);
	
	label = gtk_label_new_with_mnemonic (_("_Friend's username:"));
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	
	entry = gtk_entry_new ();
	gtk_entry_set_activates_default (GTK_ENTRY (entry), TRUE);
	gtk_label_set_mnemonic_widget (GTK_LABEL (label), entry);
	dc->dialog_add_friend = entry;
	dc->link_bval = dialog_grey_button_on_invalid (dc->dialog_add_ok, entry, NULL);
	gtk_box_pack_start (GTK_BOX (hbox), entry, TRUE, TRUE, 0);
	
	if (!friend)
		gtk_entry_set_text (GTK_ENTRY (entry), friend_name);
	
	hbox = gtk_hbox_new (FALSE, 12);
	gtk_box_pack_start (GTK_BOX (vbox), hbox, TRUE, TRUE, 0);
	
	hbox2 = gtk_hbox_new (FALSE, 6);
	gtk_box_pack_start (GTK_BOX (hbox), hbox2, TRUE, FALSE, 0);
	
	label = gtk_label_new_with_mnemonic (_("_Text color:"));
	gtk_box_pack_start (GTK_BOX (hbox2), label, FALSE, FALSE, 0);
	
	gdk_color.pixel = 0;
	gdk_color.red = 0;
	gdk_color.green = 0;
	gdk_color.blue = 0;
	color = gtk_color_button_new_with_color (&gdk_color);
	gtk_label_set_mnemonic_widget (GTK_LABEL (label), color);
	dc->dialog_add_fg = color;
	gtk_box_pack_start (GTK_BOX (hbox2), color, FALSE, FALSE, 0);
	
	hbox2 = gtk_hbox_new (FALSE, 6);
	gtk_box_pack_start (GTK_BOX (hbox), hbox2, TRUE, FALSE, 0);
	
	label = gtk_label_new_with_mnemonic (_("_Background color:"));
	gtk_box_pack_start (GTK_BOX (hbox2), label, FALSE, FALSE, 0);
	
	gdk_color.pixel = 0;
	gdk_color.red = 65535;
	gdk_color.green = 65535;
	gdk_color.blue = 65535;
	color = gtk_color_button_new_with_color (&gdk_color);
	gtk_label_set_mnemonic_widget (GTK_LABEL (label), color);
	dc->dialog_add_bg = color;
	gtk_box_pack_start (GTK_BOX (hbox2), color, FALSE, FALSE, 0);
	
	g_signal_connect (G_OBJECT (dialog), "destroy",
			G_CALLBACK (gtk_widget_destroyed), &dialog);
	g_signal_connect (G_OBJECT (dialog), "response",
			G_CALLBACK (add_friend_dialog_cb), dc);
			
	gtk_widget_show_all (dialog);
	
	GTK_WIDGET_SET_FLAGS (dialog, GTK_CAN_FOCUS);
	gtk_widget_grab_focus (dialog);
	GTK_WIDGET_SET_FLAGS (entry, GTK_CAN_FOCUS);
	gtk_widget_grab_focus (entry);
	
	g_free (friend_name);
	
	return;
}

static void
friends_list_row_activated_cb (GtkTreeView *view, GtkTreePath *path, GtkTreeViewColumn *column, gpointer data)
{
	GtkTreeSelection *selection;
	GtkTreeModel *model;
	GtkTreeIter iter;
	gboolean friend;
	
	selection = gtk_tree_view_get_selection (view);
	gtk_tree_selection_get_selected (selection, &model, &iter);
	gtk_tree_model_get (model, &iter, FRIEND_COL, &friend, -1);
	
	if (!friend)
		friends_list_add_cb (NULL, data);
	
	return;
}

static void
friends_list_remove_cb (GtkWidget *widget, gpointer data)
{
	gchar *friend;
	GtkTreeSelection *selection;
	GtkTreeModel *model;
	GtkTreeIter iter;
	DrivelClient *dc = (DrivelClient *) data;
	
	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (dc->friend_list));
	gtk_tree_selection_get_selected (selection, &model, &iter);
	gtk_tree_model_get (model, &iter, USERNAME_COL, &friend, -1);

	blog_lj_build_editfriends_request (dc->user->username, 
					   dc->user->server, friend, TRUE, FALSE, NULL, NULL);
	
	g_free (friend);
	
	return;
}

static void
friends_list_view_journal_cb (GtkWidget *widget, gpointer data)
{
	gchar *friend;
	gchar url[255];
	GtkTreeSelection *selection;
	GtkTreeModel *model;
	GtkTreeIter iter;
	DrivelClient *dc = (DrivelClient *) data;
	
	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (dc->friend_list));
	gtk_tree_selection_get_selected (selection, &model, &iter);
	gtk_tree_model_get (model, &iter, USERNAME_COL, &friend, -1);

	/* XXX */
	g_snprintf (url, 255, "%s/users/%s", dc->user->server, friend);
	gnome_url_show (url, NULL);

	g_free (friend);

	return;
}

static void
set_link_text_cb (GtkWidget *entry, gpointer data)
{
	gchar *text;
	DrivelClient *dc = (DrivelClient *) data;
	
	text = gtk_editable_get_chars (GTK_EDITABLE (entry), 0, -1);
	gtk_entry_set_text (GTK_ENTRY (dc->link_text), text);
	
	g_free (text);
	
	return;
}

static void
link_response_cb (GtkWidget *widget, gint arg, gpointer data)
{
	gchar *text, *link, *insert;
	GtkTextIter start, end;
	GtkTextMark *mark1, *mark2;
	DrivelClient *dc = (DrivelClient *) data;
	
	if (arg == GTK_RESPONSE_CANCEL)
		gtk_widget_destroy (widget);
	
	if (arg == GTK_RESPONSE_OK)
	{
		text = gtk_editable_get_chars (GTK_EDITABLE (dc->link_text), 0, -1);
		
		if (GTK_WIDGET_SENSITIVE (dc->link_url))
		{
			link = gtk_editable_get_chars (GTK_EDITABLE (dc->link_url), 0, -1);
			/* Check for a protocol in there somewhere... */
			if (strstr(link,"://"))
				insert = g_strdup_printf (
					"<a href=\"%s\">%s</a>",
					link, text);
			else 
				insert = g_strdup_printf (
					"<a href=\"http://%s\">%s</a>",
					link, text);
		}
		else if (GTK_WIDGET_SENSITIVE (dc->link_user))
		{
			link = gtk_editable_get_chars (GTK_EDITABLE (dc->link_user), 0, -1);
			insert = g_strdup_printf (
					"<lj user=\"%s\">", link);
		}
		else
		{
			link = NULL;
			if (text [0] == '\0') /* text is empty */
				insert = g_strdup ("<lj-cut>");
			else
				insert = g_strdup_printf ("<lj-cut text=\"%s\">", text);
		}
		
		gtk_widget_destroy (widget);
	
		mark1 = gtk_text_buffer_get_mark (dc->buffer, "link_start");
		mark2 = gtk_text_buffer_get_mark (dc->buffer, "link_end");
		if (mark1 && mark2)
		{
			gtk_text_buffer_get_iter_at_mark (dc->buffer, &start, mark1);
			gtk_text_buffer_get_iter_at_mark (dc->buffer, &end, mark2);
			gtk_text_buffer_delete (dc->buffer, &start, &end);
			gtk_text_buffer_delete_mark (dc->buffer, mark1);
			gtk_text_buffer_delete_mark (dc->buffer, mark2);
		}
		gtk_text_buffer_insert_at_cursor (dc->buffer, insert, -1);
		
		g_free (text);
		g_free (link);
		g_free (insert);
	}
	
	return;
}

static void
image_response_cb (GtkWidget *widget, gint arg, gpointer data)
{
	gchar *url, *alt, *height_text, *width_text, *image_text;
	gint height, width;
	DrivelClient *dc = (DrivelClient *) data;
	
	if (arg == GTK_RESPONSE_CANCEL)
		gtk_widget_destroy (widget);
	
	if (arg == GTK_RESPONSE_OK)
	{
		gchar *protocol, *prefix;
		
		url = gtk_editable_get_chars (GTK_EDITABLE (dc->image_url), 0, -1);
		alt = gtk_editable_get_chars (GTK_EDITABLE (dc->image_alt), 0, -1);
		
		protocol = strstr (url, "://");
		if (protocol)
		{
			gchar *tmp;
			
			prefix = g_strdup (url);
			prefix[strlen (url) - strlen (protocol)] = '\0';
			tmp = g_strdup (protocol + 3);
			g_free (url);
			url = tmp;
		}
		else
			prefix = g_strdup ("http");
		
		height = gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (dc->image_height));
		width = gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (dc->image_width));
		
		if (height)
			height_text = g_strdup_printf ("height=%d ", height);
		else
			height_text = g_strdup ("");
		
		if (width)
			width_text = g_strdup_printf ("width=%d ", width);
		else
			width_text = g_strdup ("");
		
		image_text = g_strdup_printf (
				"<img src=\"%s://%s\" %s%salt=\"%s\" />",
				prefix, url, height_text, width_text, alt);
		
		gtk_text_buffer_insert_at_cursor (dc->buffer, image_text, -1);
		
		g_free (image_text);
		g_free (url);
		g_free (alt);
		g_free (height_text);
		g_free (width_text);
		g_free (prefix);

		gtk_widget_destroy (widget);
	}
	
	return;
}

static void
link_url_cb (GtkWidget *button, gpointer data)
{
	DrivelClient *dc = (DrivelClient *) data;
	
	gtk_widget_set_sensitive (dc->link_text, TRUE);
	gtk_widget_set_sensitive (dc->link_url, TRUE);
	gtk_widget_set_sensitive (dc->link_url_label, TRUE);
	gtk_widget_set_sensitive (dc->link_url_example, TRUE);
	gtk_widget_set_sensitive (dc->link_user, FALSE);
	gtk_widget_set_sensitive (dc->link_user_label, FALSE);
	
	drivel_button_list_clear (dc->link_bval);
	dc->link_bval = dialog_grey_button_on_invalid (
			dc->link_ok, dc->link_text, dc->link_url, NULL);
	
	g_signal_emit_by_name (dc->link_text, "changed");
	
	gtk_widget_grab_focus (dc->link_url);
	
	return;
}

static void
link_user_cb (GtkWidget *button, gpointer data)
{
	gchar *text;
	DrivelClient *dc = (DrivelClient *) data;
	
	gtk_widget_set_sensitive (dc->link_text, FALSE);
	gtk_widget_set_sensitive (dc->link_url, FALSE);
	gtk_widget_set_sensitive (dc->link_url_label, FALSE);
	gtk_widget_set_sensitive (dc->link_url_example, FALSE);
	gtk_widget_set_sensitive (dc->link_user, TRUE);
	gtk_widget_set_sensitive (dc->link_user_label, TRUE);
	
	drivel_button_list_clear (dc->link_bval);
	dc->link_bval = dialog_grey_button_on_invalid (
			dc->link_ok, dc->link_user, NULL);
	
	text = gtk_editable_get_chars (GTK_EDITABLE (dc->link_user), 0, -1);
	gtk_entry_set_text (GTK_ENTRY (dc->link_text), text);
	g_free (text);

	g_signal_emit_by_name (dc->link_user, "changed");
	
	gtk_widget_grab_focus (dc->link_user);
	
	return;
}

static void
link_cut_cb (GtkWidget *button, gpointer data)
{
	DrivelClient *dc = (DrivelClient *) data;
	
	gtk_widget_set_sensitive (dc->link_text, TRUE);
	gtk_widget_set_sensitive (dc->link_url, FALSE);
	gtk_widget_set_sensitive (dc->link_url_label, FALSE);
	gtk_widget_set_sensitive (dc->link_url_example, FALSE);
	gtk_widget_set_sensitive (dc->link_user, FALSE);
	gtk_widget_set_sensitive (dc->link_user_label, FALSE);
	gtk_widget_set_sensitive (dc->link_ok, TRUE);
	
	drivel_button_list_clear (dc->link_bval);
	dc->link_bval = NULL;
	
	g_signal_emit_by_name (dc->link_text, "changed");
	
	gtk_widget_grab_focus (dc->link_text);
	
	return;
}

static gchar*
get_selected_text (GtkTextBuffer *buffer)
{
	gchar *selection;
	GtkTextIter start, end;
	
	if (gtk_text_buffer_get_selection_bounds (buffer, &start, &end))
	{
		selection = gtk_text_buffer_get_text (buffer, &start, &end, FALSE);
		gtk_text_buffer_create_mark (buffer, "link_start", &start, FALSE);
		gtk_text_buffer_create_mark (buffer, "link_end", &end, TRUE);
	}
	else
		selection = NULL;
	
	return selection;
}

/****************************/
/*    GLOBAL FUNCTIONS      */
/****************************/

void
display_gnomevfs_error_dialog (DrivelClient *dc, GnomeVFSResult result)
{
	display_error_dialog (dc, _("Could not perform the file operation"),
			gnome_vfs_result_to_string (result));
	
	return;
}

void
display_open_error_dialog (DrivelClient *dc, const char *filename)
{
	gchar *msg;
	
	msg = g_strdup_printf (
			"The file \"%s\" does not appear to be a valid draft.",
			filename);
	display_error_dialog (dc, _("Could not open the selected file"), msg);
	g_free (msg);

	return;
}

static gboolean
parse_save_response (DrivelClient *dc, gint response)
{
	gboolean cancel = TRUE;
	
	switch (response)
	{
		case GTK_RESPONSE_ACCEPT:
		{
			if (save_draft_cb (NULL, dc))
				cancel = FALSE;
			break;
		}
		case GTK_RESPONSE_REJECT:
		{
			cancel = FALSE;
			break;
		}
		default: break;
	}
	
	if (!cancel)
	{
		remove_autosave (dc);
		dc->modified = FALSE;
	}
	
	return cancel;
}

gboolean
display_save_dialog_proceed (DrivelClient *dc)
{
	GtkWidget *dialog;
	gint response;
	gchar *msg;
	gboolean cancel;
	
	if (!dc || !dc->journal_window || !dc->modified)
		return TRUE;
	
	msg = g_strdup_printf ("<span weight=\"bold\" size=\"larger\">%s</span>\n\n%s",
			_("Save changes to this journal entry before proceeding?"),
			_("If you don't save, changes to this entry will be discarded."));
	dialog = gtk_message_dialog_new_with_markup (
			GTK_WINDOW (dc->current_window),
			GTK_DIALOG_DESTROY_WITH_PARENT,
			GTK_MESSAGE_WARNING,
			GTK_BUTTONS_NONE,
			msg);	
	gtk_dialog_add_buttons (GTK_DIALOG (dialog),
			"drivel-dont-save", GTK_RESPONSE_REJECT, 
			GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
			"drivel-save-draft", GTK_RESPONSE_ACCEPT, 
			NULL);
	gtk_dialog_set_default_response (GTK_DIALOG (dialog), GTK_RESPONSE_ACCEPT);
	g_free (msg);
	
	response = gtk_dialog_run (GTK_DIALOG (dialog));
	cancel = parse_save_response (dc, response);
	
	gtk_widget_destroy (dialog);
	
	return (!cancel);
}

gboolean
display_save_dialog_close (DrivelClient *dc)
{
	GtkWidget *dialog;
	gint response;
	gchar *msg;
	gboolean cancel;
	
	if (!dc || !dc->journal_window || !dc->modified)
		return TRUE;
	
	msg = g_strdup_printf ("<span weight=\"bold\" size=\"larger\">%s</span>\n\n%s",
			_("Save changes to this journal entry before closing?"),
			_("If you don't save, changes to this entry will be discarded."));
	dialog = gtk_message_dialog_new_with_markup (
			GTK_WINDOW (dc->current_window),
			GTK_DIALOG_DESTROY_WITH_PARENT,
			GTK_MESSAGE_WARNING,
			GTK_BUTTONS_NONE,
			msg);	
	gtk_dialog_add_buttons (GTK_DIALOG (dialog),
			"drivel-dont-save", GTK_RESPONSE_REJECT, 
			GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
			"drivel-save-draft", GTK_RESPONSE_ACCEPT, 
			NULL);
	gtk_dialog_set_default_response (GTK_DIALOG (dialog), GTK_RESPONSE_ACCEPT);
	g_free (msg);
	
	response = gtk_dialog_run (GTK_DIALOG (dialog));
	cancel = parse_save_response (dc, response);
	
	gtk_widget_destroy (dialog);
	
	return (!cancel);
}

void
display_edit_history_dialog (DrivelClient *dc)
{
	GladeXML *xml;
	GtkTreeSelection *selection;
	GtkCellRenderer *renderer;
	GtkTreeViewColumn *column;
	static GtkWidget *dialog = NULL;
	
	if (dialog)
	{
		gtk_window_present (GTK_WINDOW (dialog));
		
		return;
	}
	if (dc->edit_history_window)
	{
		gtk_window_present(GTK_WINDOW (dc->edit_history_window));

		return;
	}
	xml = load_glade_xml ("history_dialog");
	if (!xml)
		return;
	
	dialog = glade_xml_get_widget (xml, "history_dialog");
	gtk_dialog_set_default_response (GTK_DIALOG (dialog), GTK_RESPONSE_OK);
	gtk_window_set_transient_for (GTK_WINDOW (dialog), GTK_WINDOW (dc->current_window));
	dc->history_edit = glade_xml_get_widget (xml, "history_edit_button");
	gtk_button_set_use_stock (GTK_BUTTON (dc->history_edit), TRUE);
	dc->edit_history_window = dialog;
	
	drivel_push_current_window (dc, dialog);
	
	dc->history_calendar = glade_xml_get_widget (xml, "history_calendar");
	g_signal_connect (G_OBJECT (dc->history_calendar), "day-selected",
			G_CALLBACK (update_history_list_cb), dc);
	g_signal_connect (G_OBJECT (dc->history_calendar), "month-changed",
			G_CALLBACK (mark_history_list_cb), dc);

	dc->history_store = gtk_list_store_new (
			HISTORY_N_COLUMNS,
			G_TYPE_STRING,
			G_TYPE_STRING,
			G_TYPE_STRING);

	dc->history_list = glade_xml_get_widget (xml, "history_list");
	gtk_tree_view_set_model (GTK_TREE_VIEW (dc->history_list), GTK_TREE_MODEL (dc->history_store));
	
	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (dc->history_list));
	gtk_tree_selection_set_mode (selection, GTK_SELECTION_SINGLE);

	g_signal_connect (G_OBJECT (selection), "changed",
			G_CALLBACK (history_list_changed), dc);
	g_signal_connect (G_OBJECT (dc->history_list), "row_activated",
			G_CALLBACK (history_list_row_activated_cb), dc);

	renderer = gtk_cell_renderer_text_new ();
	column = gtk_tree_view_column_new_with_attributes (
			_("Time"),
			renderer,
			"text", HISTORY_TIME_COL,
			NULL);
	gtk_tree_view_append_column (GTK_TREE_VIEW (dc->history_list), column);
	
	renderer = gtk_cell_renderer_text_new ();
	column = gtk_tree_view_column_new_with_attributes (
			_("Journal Entry"),
			renderer,
			"text", HISTORY_SUBJECT_COL,
			NULL);
	gtk_tree_view_append_column (GTK_TREE_VIEW (dc->history_list), column);
	
	g_signal_connect (G_OBJECT (dialog), "destroy",
			G_CALLBACK (gtk_widget_destroyed), &dialog);
	g_signal_connect (G_OBJECT (dialog), "response",
			G_CALLBACK (history_response_cb), dc);
	
	gtk_widget_set_sensitive (dc->history_edit, FALSE);

	gtk_widget_show_all (dialog);
	/* FIXME: do some help */
	gtk_widget_hide (glade_xml_get_widget (xml, "history_help_button"));
	
	mark_history_list (dc);
	update_history_list_cb (NULL, dc);
	
	gtk_widget_grab_focus (dc->history_calendar);
	
	return;
}

void
display_edit_friends_dialog (DrivelClient *dc)
{
	GladeXML *xml;
	GtkCellRenderer *renderer;
	GtkTreeViewColumn *column, *default_col;
	GtkTreeSelection *selection;
	GtkWidget *button;
	static GtkWidget *dialog = NULL;
	
	if (dialog)
	{
		gtk_window_present (GTK_WINDOW (dialog));
		
		update_friends_list (dc);
		
		return;
	}

	xml = load_glade_xml ("friends_dialog");
	if (!xml)
		return;
	
	dialog = glade_xml_get_widget (xml, "friends_dialog");
	gtk_window_set_transient_for (GTK_WINDOW (dialog), GTK_WINDOW (dc->current_window));
	dc->friends_list_window = dialog;
	
	drivel_push_current_window (dc, dialog);
	
	dc->list_store = gtk_list_store_new (N_COLUMNS,
			GDK_TYPE_PIXBUF,
			GDK_TYPE_PIXBUF,
			G_TYPE_STRING,
			G_TYPE_STRING,
			G_TYPE_BOOLEAN,
			G_TYPE_BOOLEAN,
			G_TYPE_STRING,
			G_TYPE_STRING,
			G_TYPE_INT);
	
	dc->friend_list = glade_xml_get_widget (xml, "friends_list");
	gtk_tree_view_set_model (GTK_TREE_VIEW (dc->friend_list), GTK_TREE_MODEL (dc->list_store));
	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (dc->friend_list));
	gtk_tree_selection_set_mode (selection, GTK_SELECTION_SINGLE);
	g_signal_connect (G_OBJECT (selection), "changed",
			G_CALLBACK (friends_list_changed), dc);
	g_signal_connect (G_OBJECT (dc->friend_list), "row_activated",
			G_CALLBACK (friends_list_row_activated_cb), dc);
	
	renderer = gtk_cell_renderer_pixbuf_new ();
	column = gtk_tree_view_column_new_with_attributes (
			_("Type"),
			renderer,
			"pixbuf", TYPE_COL,
			NULL);
	gtk_tree_view_append_column (GTK_TREE_VIEW (dc->friend_list), column);
	
	renderer = gtk_cell_renderer_pixbuf_new ();
	column = gtk_tree_view_column_new_with_attributes (
			_("Link"),
			renderer,
			"pixbuf", LINK_COL,
			NULL);
	gtk_tree_view_append_column (GTK_TREE_VIEW (dc->friend_list), column);
	
	renderer = gtk_cell_renderer_text_new ();
	column = gtk_tree_view_column_new_with_attributes (
			_("Username"),
			renderer,
			"text", USERNAME_COL,
			NULL);
	gtk_tree_view_column_set_sort_column_id (column, SORT_USERNAME);
	default_col = column;
	gtk_tree_view_append_column (GTK_TREE_VIEW (dc->friend_list), column);
	
	renderer = gtk_cell_renderer_text_new ();
	column = gtk_tree_view_column_new_with_attributes (
			_("Name"),
			renderer,
			"text", NAME_COL,
			NULL);
	gtk_tree_view_column_set_sort_column_id (column, SORT_REALNAME);
	gtk_tree_view_append_column (GTK_TREE_VIEW (dc->friend_list), column);
	
	gtk_tree_sortable_set_sort_func (GTK_TREE_SORTABLE (dc->list_store),
			SORT_TYPE, sort_type_func, NULL, NULL);
	
	button = glade_xml_get_widget (xml, "friends_add_button");
	g_signal_connect (G_OBJECT (button), "clicked",
			G_CALLBACK (friends_list_add_cb), dc);
	gtk_button_set_use_stock (GTK_BUTTON (button), TRUE);
	
	button = glade_xml_get_widget (xml, "friends_remove_button");
	gtk_widget_set_sensitive (button, FALSE);
	g_signal_connect (G_OBJECT (button), "clicked",
			G_CALLBACK (friends_list_remove_cb), dc);
	dc->friend_remove = button;
	
	button = glade_xml_get_widget (xml, "friends_view_journal_button");
	gtk_widget_set_sensitive (button, FALSE);
	g_signal_connect (G_OBJECT (button), "clicked",
			G_CALLBACK (friends_list_view_journal_cb), dc);
	dc->friend_view_journal = button;

	g_signal_connect (G_OBJECT (dialog), "destroy",
			G_CALLBACK (gtk_widget_destroyed), &dialog);
	g_signal_connect (G_OBJECT (dialog), "response",
			G_CALLBACK (dialog_close_cb), dc);

	dc->friends_username = glade_xml_get_widget (xml, "friends_username");
	dc->friends_fg_colour = glade_xml_get_widget (xml, "friends_fg_colour");
	dc->friends_bg_colour = glade_xml_get_widget (xml, "friends_bg_colour");
	dc->friends_type_icon = glade_xml_get_widget (xml, "friends_type_icon");
	dc->friends_type_name = glade_xml_get_widget (xml, "friends_type_name");

	g_signal_connect (G_OBJECT (dc->friends_fg_colour), "color-set",
			G_CALLBACK (edit_friend_colour_cb), dc);
	g_signal_connect (G_OBJECT (dc->friends_bg_colour), "color-set",
			G_CALLBACK (edit_friend_colour_cb), dc);
	
	update_friends_list (dc);
	
	gtk_widget_show_all (dialog);
	/* FIXME: we don't have help */
	gtk_widget_hide (glade_xml_get_widget (xml, "friends_help_button"));

	gtk_tree_view_column_clicked (default_col);

	return;
}

void
display_insert_link_dialog (DrivelClient *dc)
{
	GladeXML *xml;
	GtkWidget *label;
	GtkWidget *entry;
	GtkWidget *radio_button;
	GtkSizeGroup *sizegroup;
	gchar *link_text;
	static GtkWidget *dialog = NULL;
	
	if (dialog)
	{
		gtk_window_present (GTK_WINDOW (dialog));
		
		return;
	}

	xml = load_glade_xml ("insert_link_dialog");
	if (!xml)
		return;

	sizegroup = gtk_size_group_new (GTK_SIZE_GROUP_BOTH);
	
	dialog = glade_xml_get_widget (xml, "insert_link_dialog");
	gtk_window_set_transient_for (GTK_WINDOW (dialog), GTK_WINDOW (dc->current_window));
	dc->link_ok = glade_xml_get_widget (xml, "insert_insert_button");
	gtk_dialog_set_default_response (GTK_DIALOG (dialog), GTK_RESPONSE_OK);
	gtk_widget_set_sensitive (dc->link_ok, FALSE);

	label = glade_xml_get_widget (xml, "insert_link_text_label");
	dc->link_text_label = label;
	
	/* if there is selected text in the journal entry, use it as the link
	   text. */
	link_text = get_selected_text (dc->buffer);
	
	entry = glade_xml_get_widget (xml, "insert_link_text");
	if (link_text)
		gtk_entry_set_text (GTK_ENTRY (entry), link_text);
	dc->link_text = entry;
	
	g_free (link_text);
	
	radio_button = glade_xml_get_widget (xml, "insert_radio");
	g_signal_connect (G_OBJECT (radio_button), "clicked",
			G_CALLBACK (link_url_cb), dc);
	
	label = glade_xml_get_widget (xml, "insert_url_label");
	gtk_size_group_add_widget (sizegroup, label);
	dc->link_url_label = label;
	
	label = glade_xml_get_widget (xml, "insert_url_example");
	dc->link_url_example = label;
	
	entry = glade_xml_get_widget (xml, "insert_url");
	dc->link_url = entry;
	dc->link_bval = dialog_grey_button_on_invalid (dc->link_ok, entry, dc->link_text, NULL);
	
	radio_button = glade_xml_get_widget (xml, "insert_radio_2");
	g_signal_connect (G_OBJECT (radio_button), "clicked",
			G_CALLBACK (link_user_cb), dc);
	
	label = glade_xml_get_widget (xml, "insert_username_label");
	gtk_size_group_add_widget (sizegroup, label);
	gtk_widget_set_sensitive (label, FALSE);
	dc->link_user_label = label;
	
	entry = glade_xml_get_widget (xml, "insert_username");
	dc->link_user = entry;
	gtk_widget_set_sensitive (entry, FALSE);
	g_signal_connect (G_OBJECT (entry), "changed",
			G_CALLBACK (set_link_text_cb), dc);
	
	radio_button = glade_xml_get_widget (xml, "insert_radio_3");
	g_signal_connect (G_OBJECT (radio_button), "clicked",
			G_CALLBACK (link_cut_cb), dc);
	
	g_signal_connect (G_OBJECT (dialog), "destroy",
			G_CALLBACK (gtk_widget_destroyed), &dialog);
	g_signal_connect (G_OBJECT (dialog), "response",
			G_CALLBACK (link_response_cb), dc);
	
	gtk_widget_show_all (dialog);
	
	GTK_WIDGET_SET_FLAGS (dc->link_text, GTK_CAN_FOCUS);
	gtk_widget_grab_focus (dc->link_text);
	
	return;
}

void
display_insert_image_dialog (DrivelClient *dc)
{
	GtkWidget *label;
	GtkSizeGroup *sizegroup;
	GladeXML *xml;
	static GtkWidget *dialog = NULL;
	
	if (dialog)
	{
		gtk_window_present (GTK_WINDOW (dialog));
		
		return;
	}
	
	xml = load_glade_xml ("insert_image_dialog");
	if (!xml)
		return;

	sizegroup = gtk_size_group_new (GTK_SIZE_GROUP_BOTH);
	
	dialog = glade_xml_get_widget (xml, "insert_image_dialog");
	gtk_window_set_transient_for (GTK_WINDOW (dialog), GTK_WINDOW (dc->current_window));
	dc->image_ok = glade_xml_get_widget (xml, "insert_image_button");
	gtk_dialog_set_default_response (GTK_DIALOG (dialog), GTK_RESPONSE_OK);
	gtk_widget_set_sensitive (dc->image_ok, FALSE);
	
	label = glade_xml_get_widget (xml, "insert_image_loc_label");
	gtk_size_group_add_widget (sizegroup, label);
	
	dc->image_url = glade_xml_get_widget (xml, "insert_image_loc");
	dc->image_bval = dialog_grey_button_on_invalid (dc->image_ok, dc->image_url,
			NULL);
	
	label = glade_xml_get_widget (xml, "insert_image_attr_height_label");
	gtk_size_group_add_widget (sizegroup, label);
	
	dc->image_height = glade_xml_get_widget (xml, "insert_image_attr_height");
	
	label = glade_xml_get_widget (xml, "insert_image_attr_width_label");
	gtk_size_group_add_widget (sizegroup, label);
	
	dc->image_width = glade_xml_get_widget (xml, "insert_image_attr_width");
	
	label = glade_xml_get_widget (xml, "insert_image_attr_desc_label");
	gtk_size_group_add_widget (sizegroup, label);
	
	dc->image_alt = glade_xml_get_widget (xml, "insert_image_attr_desc");
	
	g_signal_connect (G_OBJECT (dialog), "destroy",
			G_CALLBACK (gtk_widget_destroyed), &dialog);
	g_signal_connect (G_OBJECT (dialog), "response",
			G_CALLBACK (image_response_cb), dc);
	
	gtk_widget_show_all (dialog);
	
	return;
}

static void
security_group_selection_changed_cb (GtkTreeSelection *selection, gpointer data)
{
	
	return;
}

void
display_security_dialog (DrivelClient *dc)
{
	GtkCellRenderer *renderer;
	GtkTreeViewColumn *column;
	GtkTreeSelection *selection;
	GtkWidget *widget;
	GladeXML *xml;
	static GtkWidget *dialog = NULL;
	
	if (dialog)
	{
		gtk_window_present (GTK_WINDOW (dialog));
		
		return;
	}
	
	xml = load_glade_xml ("security_dialog");
	if (!xml)
		return;
	
	dialog = glade_xml_get_widget (xml, "security_dialog");
	gtk_window_set_transient_for (GTK_WINDOW (dialog), GTK_WINDOW (dc->current_window));
	
	/* Friends Groups list*/
	widget = glade_xml_get_widget (xml, "security_list");
	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (widget));
	g_signal_connect (G_OBJECT (selection), "changed",
			G_CALLBACK (security_group_selection_changed_cb), dc);
	gtk_tree_view_set_model (GTK_TREE_VIEW (widget), 
			GTK_TREE_MODEL (dc->security_store_filtered));
	renderer = gtk_cell_renderer_text_new ();
	column = gtk_tree_view_column_new_with_attributes (_("Available"),
			renderer, "text", STORE_SECURITY_NAME, NULL);
	gtk_tree_view_append_column (GTK_TREE_VIEW (widget), column);
	
	/* Available users list */
	widget = glade_xml_get_widget (xml, "security_details_friends_available");
	renderer = gtk_cell_renderer_text_new ();
	column = gtk_tree_view_column_new_with_attributes (_("Available"),
			renderer, "text", STORE_SECURITY_NAME, NULL);
	gtk_tree_view_append_column (GTK_TREE_VIEW (widget), column);
	
	/* In-Group users list */
	widget = glade_xml_get_widget (xml, "security_details_friends_in_group");
	renderer = gtk_cell_renderer_text_new ();
	column = gtk_tree_view_column_new_with_attributes (_("In Group"),
			renderer, "text", STORE_SECURITY_NAME, NULL);
	gtk_tree_view_append_column (GTK_TREE_VIEW (widget), column);
	
	g_signal_connect (G_OBJECT (dialog), "destroy",
			G_CALLBACK (gtk_widget_destroyed), &dialog);
	g_signal_connect (G_OBJECT (dialog), "response",
			G_CALLBACK (gtk_widget_destroy), dialog);
	
	gtk_widget_show_all (dialog);
	
	return;
}

static void
free_store (GtkWidget *widget, GObject *store)
{
	g_object_unref (store);
	
	return;
}

static void
nullify_pointer (GtkWidget *widget, gpointer *user_data)
{
	*user_data = NULL;

	return;
}

void
display_edit_preferences_dialog (DrivelClient *dc)
{
	GladeXML *xml;
	static GtkWidget *dialog = NULL;
	GtkWidget *widget, *spellcheck;
	GtkListStore *picture_store;
	GtkCellRenderer *renderer;
	gboolean state;
	gchar *string;
	gint num;
#ifdef HAVE_GTKSPELL
	gint sel = -1;
	GtkTreeIter iter;
	GtkListStore *language_store;
	GSList *list;
#endif /* HAVE_GTKSPELL */
	GConfValue *value;

	if (dialog)
	{
		gtk_window_present (GTK_WINDOW (dialog));
		return;
	}

	xml = load_glade_xml ("preferences_dialog");
	if (!xml)
		return;

	/* dialog box */
	dialog = glade_xml_get_widget (xml, "preferences_dialog");
	gtk_window_set_transient_for (GTK_WINDOW (dialog), GTK_WINDOW (dc->current_window));
	gtk_dialog_set_default_response (GTK_DIALOG (dialog), GTK_RESPONSE_CLOSE);
	drivel_push_current_window (dc, dialog);
	
	/* tray icon */
	widget = glade_xml_get_widget (xml, "pref_general_notification");
	value = gconf_client_get (dc->client, dc->gconf->tray, NULL);
	if (value)
		state = gconf_value_get_bool (value);
	else
		state = TRUE;
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (widget), state);
	g_signal_connect (G_OBJECT (widget), "toggled",
			G_CALLBACK (tray_cb), dc);

	/* technorati */
	widget = glade_xml_get_widget (xml, "pref_general_technorati");
	value = gconf_client_get (dc->client, dc->gconf->technorati, NULL);
	if (value)
		state = gconf_value_get_bool (value);
	else
		state = FALSE;
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (widget), state);
	g_signal_connect (G_OBJECT (widget), "toggled",
			G_CALLBACK (technorati_cb), dc);
	
	/* minimise after logging in */
	widget = glade_xml_get_widget (xml, "pref_general_entry_minimizelogin");
	state = gconf_client_get_bool (dc->client, dc->gconf->min_start, NULL);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (widget), state);
	g_signal_connect (G_OBJECT (widget), "toggled",
			G_CALLBACK (min_start_cb), dc);

	/* minimise after posting */
	widget = glade_xml_get_widget (xml, "pref_general_entry_minimizepost");
	state = gconf_client_get_bool (dc->client, dc->gconf->min_post, NULL);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (widget), state);
	g_signal_connect (G_OBJECT (widget), "toggled",
			G_CALLBACK (min_post_cb), dc);

	/* syntax highlighting */
	widget = glade_xml_get_widget (xml, "pref_general_entry_highlightsyntax");
	value = gconf_client_get (dc->client, dc->gconf->highlight_syntax, NULL);
	if (value)
		state = gconf_value_get_bool (value);
	else
		state = TRUE;
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (widget), state);
	g_signal_connect (G_OBJECT (widget), "toggled",
			G_CALLBACK (highlight_syntax_cb), dc);

	/* generic spellcheck widgets */
	
	widget = glade_xml_get_widget (xml, "pref_general_entry_spellcheck");
	spellcheck = widget;
	widget = glade_xml_get_widget (xml, "prefs_general_entry_dictionary_box");
	dc->pref_dictionary_box = widget;
	widget = glade_xml_get_widget (xml, "prefs_general_entry_dictionary");
	dc->pref_dictionary = widget;
	widget = glade_xml_get_widget (xml, "prefs_general_entry_dictionary_label");
	gtk_label_set_mnemonic_widget(GTK_LABEL(widget),dc->pref_dictionary);

#ifdef HAVE_GTKSPELL
	
	/* spellcheck */
	
	value = gconf_client_get (dc->client, dc->gconf->spellcheck, NULL);
	if (value)
		state = gconf_value_get_bool (value);
	else
		state = FALSE;
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (spellcheck), state);
	gtk_widget_set_sensitive (dc->pref_dictionary_box, state);
	g_signal_connect (G_OBJECT (spellcheck), "toggled",
			G_CALLBACK (spellcheck_cb), dc);

	/* spellcheck dictionaries */

	string = get_default_text (dc->client, dc->gconf->spell_language, "");
	
	language_store = gtk_list_store_new (2, G_TYPE_STRING, G_TYPE_STRING);
	list = get_available_spell_languages ();
	if (list)
	{
		DrivelSpellLanguage *lang_struct;
		gint i;
		GSList *head = list;
		
		for (i = 0, list = head; list != NULL; list = g_slist_next (list), i++)
		{
			lang_struct = list->data;
			gtk_list_store_append (language_store, &iter);
			gtk_list_store_set (language_store, &iter,
					    0, lang_struct->realname,
					    1, lang_struct->label,
					    -1);
			if (g_str_equal (lang_struct->realname, string))
				sel = i;
			g_free (lang_struct->realname);
			g_free (lang_struct->label);
		}
		
		g_slist_free (head);
	}
	gtk_combo_box_set_model (GTK_COMBO_BOX (widget), GTK_TREE_MODEL (language_store));
 	gtk_cell_layout_clear (GTK_CELL_LAYOUT (widget));
	renderer = gtk_cell_renderer_text_new ();
	gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (widget), renderer, TRUE);
	gtk_cell_layout_set_attributes (GTK_CELL_LAYOUT (widget), renderer,
					"text", 1,
					NULL);
	if (sel < 0)
	{
		if (strcmp (string, ""))
			g_warning ("Language \"%s\" from GConf isn't in the list of available languages\n", string);
		sel = 0;
	}
	
	gtk_combo_box_set_active (GTK_COMBO_BOX (widget), sel);
	g_signal_connect (G_OBJECT (widget), "changed",
			  G_CALLBACK (spellcheck_language_cb), dc);
	g_signal_connect (G_OBJECT (dialog), "destroy",
			  G_CALLBACK (free_store), G_OBJECT (language_store));
#endif /* HAVE_GTKSPELL */
	g_signal_connect (G_OBJECT (dialog), "destroy",
			  G_CALLBACK (nullify_pointer), &(dc->pref_dictionary));
	
	/* picture */
	picture_store = gtk_list_store_new (2, G_TYPE_STRING, GDK_TYPE_PIXBUF);
	fill_picture_menu (dc, picture_store);
	widget = glade_xml_get_widget (xml, "pref_defaults_picture");
	gtk_combo_box_set_model (GTK_COMBO_BOX (widget), GTK_TREE_MODEL (picture_store));
	gtk_cell_layout_clear (GTK_CELL_LAYOUT (widget));
	if (!dc->picture_keywords)
		gtk_widget_set_sensitive (widget, FALSE);
	gtk_combo_box_set_active (GTK_COMBO_BOX (widget),
			gconf_client_get_int (dc->client, dc->gconf->default_picture, NULL));
	renderer = gtk_cell_renderer_pixbuf_new ();
	gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (widget), renderer, FALSE);
	gtk_cell_layout_set_attributes (GTK_CELL_LAYOUT (widget), renderer,
			"pixbuf", 0,
			NULL);
	renderer = gtk_cell_renderer_text_new ();
	gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (widget), renderer, TRUE);
	gtk_cell_layout_set_attributes (GTK_CELL_LAYOUT (widget), renderer,
			"text", 1,
			NULL);
	g_signal_connect (G_OBJECT (widget), "changed",
			G_CALLBACK (picture_cb), dc);
	g_signal_connect (G_OBJECT (dialog), "destroy",
			G_CALLBACK (free_store), G_OBJECT (picture_store));
	
	/* security */
	widget = glade_xml_get_widget (xml, "pref_defaults_security");
	gtk_combo_box_set_model (GTK_COMBO_BOX (widget), 
			GTK_TREE_MODEL (dc->security_store));
	gtk_cell_layout_clear (GTK_CELL_LAYOUT (widget));
	renderer = gtk_cell_renderer_pixbuf_new ();
	gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (widget), renderer, FALSE);
	gtk_cell_layout_set_attributes (GTK_CELL_LAYOUT (widget), renderer,
			"pixbuf", STORE_SECURITY_ICON,
			NULL);
	renderer = gtk_cell_renderer_text_new ();
	gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (widget), renderer, TRUE);
	gtk_cell_layout_set_attributes (GTK_CELL_LAYOUT (widget), renderer,
			"text", STORE_SECURITY_NAME,
			NULL);
	if (dc->journal_security)
	{
		string = gconf_client_get_string (dc->client, dc->gconf->default_security, NULL);
		num = gconf_client_get_int (dc->client, dc->gconf->default_security_mask, NULL);
		select_security_group (GTK_TREE_MODEL (dc->security_store), 
				GTK_COMBO_BOX (widget), string, num);
		g_signal_connect (G_OBJECT (widget), "changed",
				G_CALLBACK (security_cb), dc);
		g_free (string);
	}
	else
		gtk_widget_set_sensitive (widget, FALSE);

	/* comments */
	widget = glade_xml_get_widget (xml, "pref_defaults_nocomments");
	state = gconf_client_get_bool (dc->client, dc->gconf->default_comment, NULL);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (widget), state);
	g_signal_connect (G_OBJECT (widget), "toggled",
			G_CALLBACK (comments_cb), dc);

	/* autoformat */
	widget = glade_xml_get_widget (xml, "pref_defaults_noautoformat");
	state = gconf_client_get_bool (dc->client, dc->gconf->default_autoformat, NULL);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (widget), state);
	g_signal_connect (G_OBJECT (widget), "toggled",
			G_CALLBACK (autoformat_cb), dc);

	g_signal_connect_swapped (G_OBJECT (dialog), "destroy",
			G_CALLBACK (g_object_unref), G_OBJECT (xml));
	g_signal_connect (G_OBJECT (dialog), "destroy",
			G_CALLBACK (gtk_widget_destroyed), &dialog);
	g_signal_connect (G_OBJECT (dialog), "response",
			G_CALLBACK (preferences_close_cb), dc);

	/* The Entry Defaults tab (index of 1) only applies to LiveJournal */
	if (dc->user->api != BLOG_API_LJ)
	{
		widget = glade_xml_get_widget (xml, "pref_notebook");
		gtk_notebook_remove_page (GTK_NOTEBOOK (widget), 1);
	}
	
	gtk_widget_show_all (dialog);
	
	
	
#ifndef HAVE_GTKSPELL
	gtk_widget_hide (spellcheck);
	gtk_widget_hide (dc->pref_dictionary_box);
#endif
	
	return;
}
