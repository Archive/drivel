/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * Copyright 2002-2005 Todd Kulesza
 *
 * Authors:
 * 		Todd Kulesza <todd@dropline.net>
 */

#include <config.h>

#define __USE_XOPEN
#include <time.h>
#include <string.h>
#include <gtk/gtk.h>
#include <libgnomevfs/gnome-vfs-ops.h>
#include <libxml/parser.h>

#include "drivel.h"
#include "md5.h"
#include "utils.h"

static gboolean
return_true (gpointer key, gpointer value, gpointer data)
{
	return TRUE;
}

void
hash_table_item_free (gpointer data)
{
	g_free (data);
	data = NULL;
	
	return;
}

void
hash_table_clear (GHashTable *table)
{
	g_hash_table_foreach_remove (table, return_true, NULL);
	
	return;
}

gint
string_compare (gconstpointer a, gconstpointer b)
{
	gint retval;
	
	if (strlen (a) < strlen (b))
		retval = strncmp (a, b, strlen (b));
	else 
		retval = strncmp (a, b, strlen (a));

	return retval;
}

/* gchar* */
/* unescape_text (const gchar *text) */
/* { */
/* 	gchar *delimited, *unescaped; */
	
/* 	if (!text) */
/* 		return NULL; */
	
/* 	delimited = g_strdup (text); */
/* 	g_strdelimit (delimited, "+", ' '); */
/* 	unescaped = curl_unescape (delimited, 0); */

/* 	g_free (delimited); */

/* 	return unescaped; */
/* } */

GladeXML*
load_glade_xml (const gchar *root)
{
	gchar *path;
	GladeXML *xml = NULL;
	
	if (g_file_test (DRIVEL_GLADE_FILE, G_FILE_TEST_EXISTS))
		xml = glade_xml_new (DRIVEL_GLADE_FILE, root, GETTEXT_PACKAGE);
	else
	{
		path = g_build_filename ("src", DRIVEL_GLADE_FILE, NULL);
		if (g_file_test (path, G_FILE_TEST_EXISTS))
			xml = glade_xml_new (path, root, GETTEXT_PACKAGE);
		else
		{
			g_free (path);
			path = g_build_filename (DRIVEL_GLADE_DIR, DRIVEL_GLADE_FILE, NULL);
			xml = glade_xml_new (path, root, GETTEXT_PACKAGE);
		}
		g_free (path);
	}
	
	if (xml)
		glade_xml_signal_autoconnect (xml);
	
	return xml;
}

gboolean
picture_exists (const gchar *config_dir, const gchar *pic_file)
{
	gboolean exists;
	gchar *path;
	GnomeVFSFileInfo info;
	
	path = g_build_filename (config_dir, "pictures", pic_file, NULL);
	if (gnome_vfs_get_file_info (path, &info, GNOME_VFS_FILE_INFO_DEFAULT) == GNOME_VFS_OK)
		exists = TRUE;
	else
		exists = FALSE;
	
	g_free (path);
	
	return exists;
}

GSList*
load_user_list (const gchar *config_dir)
{
	DrivelUser *du;
	GSList *list;
	xmlDocPtr doc;
	xmlNodePtr cur, child, grandchild;
	GnomeVFSHandle *handle;
	GnomeVFSFileSize bytes_read;
	GnomeVFSResult result;
	gchar *filename, *value, input_data [4096];
	GString *string;
	
	list = NULL;
	string = g_string_new ("");
	filename = g_build_filename (config_dir, "account_list", NULL);
	
	/* read the document from the disk */
	result = gnome_vfs_open (&handle, filename, GNOME_VFS_OPEN_READ);
	if (result == GNOME_VFS_OK)
	{
		result = gnome_vfs_seek (handle, GNOME_VFS_SEEK_START, 0);
		if (result == GNOME_VFS_OK)
		{
			do
			{
				result = gnome_vfs_read (handle, input_data, 4096, &bytes_read);
				if (result != GNOME_VFS_OK)
					break;
				g_string_append_len (string, input_data, bytes_read);
			}
			while (bytes_read == 4096); /* if we read an entire block, reiterate */
			result = gnome_vfs_close (handle);
		}
	}
	if (result != GNOME_VFS_OK)
	{
		g_warning ("Could not open user list");
		g_free (filename);
		g_string_free (string, TRUE);
		return NULL;
	}
	
	/* load the document */
	doc = xmlReadMemory (string->str, string->len, NULL, NULL, XML_PARSE_NOBLANKS);
	g_string_free (string, TRUE);
	
	if (!doc)
	{
		g_warning ("Could not open user list");
		return NULL;
	}

	cur = xmlDocGetRootElement (doc);
	
	if (!cur || strcmp ((gchar *)cur->name, "account_list"))
	{
		g_warning ("User list is not valid");
		return NULL;
	}
	
	/* parse the xml */
	child = cur->xmlChildrenNode;
	while (child)
	{
		if (!strcmp ((gchar *)child->name, "user_account"))
		{
			du = drivel_user_new ();
			
			grandchild = child->xmlChildrenNode;
			while (grandchild)
			{
				if (!strcmp ((gchar *)grandchild->name, "username"))
					du->username = (gchar *)xmlNodeListGetString (doc, 
							grandchild->xmlChildrenNode, 1);
				if (!strcmp ((gchar *)grandchild->name, "password"))
					du->password = (gchar *)xmlNodeListGetString (doc, 
							grandchild->xmlChildrenNode, 1);
				if (!strcmp ((gchar *)grandchild->name, "api"))
				{
					value = (gchar *)xmlNodeListGetString (doc, 
							grandchild->xmlChildrenNode, 1);
					du->api = (gint) g_strtod (value, NULL);
					g_free (value);
				}
				if (!strcmp ((gchar *)grandchild->name, "server"))
					du->server = (gchar *)xmlNodeListGetString (doc, 
							grandchild->xmlChildrenNode, 1);
				
				grandchild = grandchild->next;
			}
			
			value = (gchar *)xmlGetProp (child, (xmlChar *)"save_password");
			if (value)
			{
				du->save_password = (gint) g_strtod (value, NULL);
				g_free (value);
			}
			else
				du->save_password = 0;
			value = (gchar *)xmlGetProp (child, (xmlChar *)"autologin");
			if (value)
			{
				du->autologin = (gint) g_strtod (value, NULL);
				g_free (value);
			}
			else
				du->autologin = 0;
			value = (gchar *)xmlGetProp (child, (xmlChar *)"lastuser");
			if (value)
			{
				du->lastuser = (gint) g_strtod (value, NULL);
				g_free (value);
			}
			else
				du->lastuser = 0;
			
			list = g_slist_append (list, du);
		}
		
		child = child->next;
	}
	
	xmlFreeDoc (doc);
	
	return list;
}

void
save_user_list (GSList *list, const gchar *config_dir)
{
	DrivelUser *du;
	GSList *item;
	xmlDocPtr doc;
	xmlNodePtr cur, child;
	gchar *filename, *value;
	
	/* set up the document */
	doc = xmlNewDoc ((xmlChar *)"1.0");
	doc->children = xmlNewDocNode (doc, NULL, (xmlChar *)"account_list", NULL);
	cur = xmlDocGetRootElement (doc);
	
	/* add the user accounts to the xml document */
	for (item = list; item; item = item->next)
	{
		du = item->data;
		child = xmlNewChild (cur, NULL, (xmlChar *)"user_account", NULL);
		xmlNewTextChild (child, NULL, (xmlChar *)"username", (xmlChar *)du->username);
		xmlNewTextChild (child, NULL, (xmlChar *)"password", (xmlChar *)du->password);
		value = g_strdup_printf ("%d", du->api);
		xmlNewTextChild (child, NULL, (xmlChar *)"api", (xmlChar *)value);
		g_free (value);
		xmlNewTextChild (child, NULL, (xmlChar *)"server", (xmlChar *)du->server);
		value = g_strdup_printf ("%d", du->save_password);
		xmlNewProp (child, (xmlChar *)"save_password", (xmlChar *)value);
		g_free (value);
		value = g_strdup_printf ("%d", du->autologin);
		xmlNewProp (child, (xmlChar *)"autologin", (xmlChar *)value);
		g_free (value);
		value = g_strdup_printf ("%d", du->lastuser);
		xmlNewProp (child, (xmlChar *)"lastuser", (xmlChar *)value);
		g_free (value);
	}
	
	/* output the XML */
	filename = g_build_filename (config_dir, "account_list", NULL);
	xmlSaveFormatFile (filename, doc, 1);
	xmlFreeDoc (doc);
	g_free (filename);
	
	return;
}

GSList*
find_in_user_list (GSList *list, DrivelUser *du)
{
	GSList *item;
	DrivelUser *du_new;
	
	for (item = list; item; item = item->next)
	{
		du_new = item->data;
		
		/* if the username and api match, we consider the accounts to be the same */
		if (	!strcmp (du_new->username, du->username) &&
				du_new->api == du->api)
		{
			break;
		}
	}
	
	return item;
}

DrivelUser *
drivel_user_new (void)
{
	DrivelUser *du;
	
	du = g_new0 (DrivelUser, 1);
	
	du->username = NULL;
	du->password = NULL;
	du->api = BLOG_API_UNKNOWN;
	du->server = NULL;
	du->rsd = NULL;
	du->cookie = NULL;
	du->save_password = FALSE;
	du->autologin = FALSE;
	du->lastuser = FALSE;
	
	return du;
}

void
drivel_user_free (DrivelUser *du)
{
	g_return_if_fail (du);
	
	g_free (du->username);
	g_free (du->password);
	g_free (du->server);
	g_free (du->rsd);
	g_free (du->cookie);
	g_free (du);
	
	return;
}

DrivelJournal*
drivel_journal_new (void)
{
	DrivelJournal *dj;
	
	dj = g_new0 (DrivelJournal, 1);
	dj->name = NULL;
	dj->description = NULL;
	dj->uri_view = NULL;
	dj->uri_post = NULL;
	dj->uri_feed = NULL;
	dj->id = NULL;
	dj->type = JOURNAL_TYPE_USER;
	
	return dj;
}

void
drivel_journal_free (DrivelJournal *dj)
{
	g_return_if_fail (dj);
	
	g_free (dj->name);
	g_free (dj->description);
	g_free (dj->uri_view);
	g_free (dj->uri_post);
	g_free (dj->uri_feed);
	g_free (dj->id);
	g_free (dj);
	
	return;
}

gchar*
get_w3dtf_timestamp (void)
{
	struct tm *tm;
	time_t t;
	gchar *timestamp;
	
	t = time (NULL);
	tm = gmtime (&t);
	timestamp = g_new0 (gchar, 32);
	strftime (timestamp, 32, "%Y-%m-%dT%H:%M:%S%z", tm);
	
	return timestamp;
}

/* Encode a string of characters in base64.
 * Based on the algorithm published in 2001 by Bob Trower of Trantor Standard 
 * Systems Inc.
 */

gchar*
base64_encode (const gchar *text)
{
	gchar *base64;
	gint i, j, len_text, blocks, remainder;
	const gchar values[] = 
			"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
	
	g_return_val_if_fail (text, NULL);

	len_text = strlen (text);
	blocks = len_text / 3;
	remainder = len_text % 3;
	if (remainder)
		blocks++;
	base64 = g_new0 (gchar, (blocks * 4) + 1);
	
	for (i = j = 0; (i + 3) < len_text; i += 3)
	{
		base64[j++] = values[ (text[i] >> 2) & 0x3f ];
		base64[j++] = values[ ((text[i] & 0x03) << 4) | ((text[i + 1] >> 4) & 0x0f) ];
		base64[j++] = values[ ((text[i + 1] & 0x0f) << 2) | ((text[i + 2] >> 6) & 0x03) ];
		base64[j++] = values[ text[i + 2] & 0x3f ];
	}
	if (remainder)
	{
		base64[j++] = values[ (text[i] >> 2) & 0x3f ];
		base64[j++] = values[ ((text[i] & 0x03) << 4) | ((text[i + 1] >> 4) & 0x0f) ];
		base64[j++] = remainder > 1 ? values[ ((text[i + 1] & 0x0f) << 2) | ((text[i + 2] >> 6) & 0x03)] : '=';
		base64[j++] = remainder > 2 ? values[ text[i + 2] & 0x3f ] : '=';
	}
	base64[j] = '\0';
	
	return base64;
}

gchar*
md5_hash (const gchar *text)
{
	gint i;
	guchar *md5_text;
	md5_state_t state;
	md5_byte_t digest[16];
	
	g_return_val_if_fail (text, NULL);
	
	md5_text = g_new0 (guchar, (16 * 2 + 1));
	
	/* md5-hash the text */
	md5_init (&state);
	md5_append (&state, (guchar *)text, strlen (text));
	md5_finish (&state, digest);

	/* put the hash in lower-case hex */
	for (i = 0; i < 16; i++)
		g_snprintf ((gchar *)md5_text + i * 2, 3, "%02x", digest[i]);
	md5_text [16 * 2] = '\0';
	
	return (gchar *)md5_text;
}

void
store_security_append (GtkListStore *store, const gchar *icon, 
		const gchar *name, gint num, gint order, gboolean public)
{
	GdkPixbuf *pixbuf;
	GtkTreeIter iter;
	gchar *icon_path;
	
	if (icon)
	{
		icon_path = g_build_filename (DATADIR, "pixmaps", "drivel", icon, NULL);
		pixbuf = gdk_pixbuf_new_from_file (icon_path, NULL);
	}
	else
		pixbuf = NULL;
	
	gtk_list_store_append (store, &iter);
	
	gtk_list_store_set (store, &iter, 
			STORE_SECURITY_ICON, pixbuf, 
			STORE_SECURITY_NAME, name,
			STORE_SECURITY_NUM, num,
			STORE_SECURITY_ORDER, order, 
			STORE_SECURITY_PUBLIC, public,
			-1);
	
	return;
}

/* Return TRUE if 'name' is found and set 'iter' to the matching row */
static gboolean
find_security_group_by_name (GtkTreeModel *model, GtkTreeIter *iter, 
		const gchar *name)
{
	gboolean retval, valid;
	
	retval = FALSE;
	valid = gtk_tree_model_get_iter_first (model, iter);
	while (valid)
	{
		gchar *group_name;
		
		gtk_tree_model_get (model, iter, STORE_SECURITY_NAME, &group_name, -1);
		if (!strcmp (name, group_name))
		{
			retval = TRUE;
			valid = FALSE;
		}
		else
			valid = gtk_tree_model_iter_next (model, iter);
		
		g_free (group_name);
	}
		
	return retval;
}

/* Return TRUE if 'mask' is found and set 'iter' to the matching row */
static gboolean
find_security_group_by_mask (GtkTreeModel *model, GtkTreeIter *iter, gint mask)
{
	gboolean retval, valid;
	
	retval = FALSE;
	valid = gtk_tree_model_get_iter_first (model, iter);
	while (valid)
	{
		guint group_mask;
		
		gtk_tree_model_get (model, iter, STORE_SECURITY_NUM, &group_mask, -1);
		if (mask == (1 << group_mask))
		{
			retval = TRUE;
			valid = FALSE;
		}
		else
			valid = gtk_tree_model_iter_next (model, iter);
	}
	
	return retval;
}

void
select_security_group (GtkTreeModel *model, GtkComboBox *combo, 
		const gchar *name, gint mask)
{
	gboolean valid;
	GtkTreeIter iter;
	
	valid = FALSE;
	
	/* If security is Private or Allowmask, try to load the correct row from the
	   GtkListStore.  If it can't be found, set the default to Public. */
	
	/* load the "private" security group */
	if (name && !strcmp ("private", name))
		valid = find_security_group_by_name (model,	&iter, _("Private"));
	/* load the correct custom security group */
	else if (name)
		valid = find_security_group_by_mask (model,	&iter, mask);
	/* load the "public" security group */
	if (!valid)
		valid = find_security_group_by_name (model,	&iter, _("Public"));
	
	if (valid)
		gtk_combo_box_set_active_iter (combo, &iter);
	
	return;
}

/* Build a title from the content of a journal entry. */
/* Return the first line that isn't an HTML tag. */
gchar*
title_from_content (const gchar *content)
{
	gchar *title, **lines;
	gint i;
	
	if (!content)
		return (g_strdup (_("Untitled")));
	
	title = NULL;
	i = 0;
	lines = g_strsplit (content, "\n", -1);
	
	while (lines[i] && !title)
	{
		gchar *stripped = g_strstrip (lines[i]);
		
		/* If there is text inbetween two tags (i.e. <b>Hello world!</b>),
		 * strip out the tags and use the text as the title. */
		if (stripped)
		{
			gboolean inside_tag = FALSE;
			gint i;
			
			for (i = 0; i < strlen (stripped); i++)
			{
				if (stripped[i] == '>')
				{
					stripped[i] = ' ';
					inside_tag = FALSE;
				}
				else if (stripped[i] == '<')
					inside_tag = TRUE;
				
				if (inside_tag)
					stripped[i] = ' ';
			}
			
			stripped = g_strstrip (stripped);
			if (stripped)
			{
				gchar *tmp = g_strndup (stripped, 37);
				if (strlen (stripped) > 37)
					title = g_strdup_printf ("%s...", tmp);
				else
					title = g_strdup (tmp);
				g_free (tmp);
			}
		} 

		i++;
	}		
	
	if (!title)
		title = g_strdup (_("Untitled"));
	
	g_strfreev (lines);
	
	return title;
}
