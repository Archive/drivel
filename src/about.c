/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * Copyright 2002-2003 Todd Kulesza
 *
 * Authors:
 * 		Todd Kulesza <todd@dropline.net>
 */

#include <config.h>

#include <gnome.h>
#include <gdk-pixbuf/gdk-pixdata.h>
#include <string.h>

#include "about.h"

void
about_show (DrivelClient *dc)
{
	GdkPixbuf *logo;
	static GtkWidget *dialog = NULL;
	static const gchar *main_authors[] = 	
	{
		"Todd Kulesza <todd@dropline.net>",
		"Davyd Madeley <davyd@ucc.asn.au>",
		"",
		NULL
	};
	static const gchar *help_authors[] =
	{
		"Grahame Bowland <grahame@angrygoats.net>",
		"Geoff King <0glk@qlink.queensu.ca>",
		"Jess Little <jackymac@gmail.com>",
		"Evan Martin <evan@livejournal.com>",
		"Margot Pearce <cupidelock@gmail.com>",
		"Ari Pollak <compwiz@aripollak.com>",
		"Isak Savo <iso01001@student.mdh.se>",
		"",
		NULL
	};
	static const gchar *documentors[] = 
	{
		"Todd Kulesza <todd@dropline.net>",
		NULL
	};
	/*
	 * Translators should localize the following string
	 * which will give them credit in the About box.
	 * E.g. "Fulano de Tal <fulano@detal.com>" */
	gchar *path, *translators = g_strdup (_("translator-credits"));
	gchar **authors;
	GString *s;
	gint i;

	if (dialog)
	{
		gtk_window_present (GTK_WINDOW (dialog));
		return;
	}
	/* This is necessary, since the N_(string)-macro doesn't actually do anything,
	   it's only used as an indicator for xgettext */
	s = g_string_new ("");
	for (i = 0; main_authors[i] != NULL; i++){
	  s = g_string_append (s, main_authors[i]);
	  s = g_string_append_c (s, '\n');
	}
	g_string_append_printf (s, "%s\n", _("With help from:"));
	for (i = 0; help_authors[i] != NULL; i++) {
	    s = g_string_append (s, help_authors[i]);
	    s = g_string_append_c (s, '\n');
	};
	g_string_append (s, _("And many others--thank you, all!"));
	authors = g_strsplit (s->str, "\n", -1);

	path = g_build_filename (DATADIR, "pixmaps", "drivel-48.png", NULL);
	logo = gdk_pixbuf_new_from_file (path, NULL);
	g_free (path);
	dialog = gnome_about_new ("Drivel", VERSION,
				"Copyright \xc2\xa9 2002-2005 Todd Kulesza",
				_("A journal client for the GNOME desktop."),
				(const gchar **) authors,
				documentors,
				(strcmp (translators, "translator-credits") ? translators : NULL),
				logo);
	gtk_window_set_transient_for (GTK_WINDOW (dialog), GTK_WINDOW (dc->current_window));
	
	g_object_add_weak_pointer (G_OBJECT (dialog), (void **)&dialog);
	
	gtk_widget_show_all (dialog);
	
	g_free (translators);
	g_string_free (s, TRUE);
	g_strfreev (authors);
	
	return;
}
