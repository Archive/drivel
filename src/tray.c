/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * Copyright 2003-2004 Todd Kulesza
 *
 * Authors:
 * 		Todd Kulesza <todd@dropline.net>
 */

#include <config.h>

#include <gnome.h>

#include "eggtrayicon.h"
#include "drivel_request.h"
#include "blog_lj.h"
#include "network.h"
#include "journal.h"
#include "tray.h"
#include "drivel.h"

#define CHECKFRIENDS_INTERVAL	30 /* in seconds */

EggTrayIcon *tray_icon = NULL;
GtkWidget *menu = NULL;
guint checkid = 0;

/* This is called by a timer to check a user's LiveJournal Friends Page */

static gboolean
checkfriends (gpointer data)
{
	DrivelClient *dc = (DrivelClient *) data;
	
	if (dc->checking_friends || dc->friends_update)
		return TRUE;
	else
		dc->checking_friends = TRUE;
	
	if (dc->time_since_checkfriends < 1)
	{
		blog_lj_build_checkfriends_request (dc->user->username, 
						    dc->user->server, dc->lastupdate);
	}
	else
	{
		dc->time_since_checkfriends -= CHECKFRIENDS_INTERVAL;
		dc->checking_friends = FALSE;
	}
	
	return TRUE;
}

/* Update the GConf key */

static void
menu_remove_cb (GtkMenuItem *item, DrivelClient *dc)
{
	gconf_client_set_bool (dc->client, dc->gconf->tray, FALSE, NULL);
	
	return;
}

/* Once the user views their Friends Page, clear the counters and hide the icon */

static void
viewed_friends (DrivelClient *dc)
{
	dc->friends_update = FALSE;
	g_free (dc->lastupdate);
	dc->lastupdate = g_strdup ("");
	
	tray_display (FALSE);
	
	return;
}

/* Display the user's Friends Page in their default web browser */

static void
view_friends (DrivelClient *dc)
{
	gchar *url;
	
	url = g_strdup_printf ("http://www.livejournal.com/~%s/friends/", 
			dc->user->username);
	gnome_url_show (url, NULL);
	viewed_friends (dc);
	
	g_free (url);
	
	return;
}

/* Right-click menu callback for View Friends */

static void
view_friends_cb (GtkMenuItem *item, DrivelClient *dc)
{
	view_friends (dc);
	
	return;
}

/* Build the right-click menu */

static void
create_menu (DrivelClient *dc)
{
	GtkWidget *menuitem;
	
	menu = gtk_menu_new ();
	
	menuitem = gtk_menu_item_new_with_mnemonic (_("_View Friends Page"));
	g_signal_connect (G_OBJECT (menuitem), "activate",
		G_CALLBACK (view_friends_cb), dc);
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
	menuitem = gtk_separator_menu_item_new ();
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
	menuitem = gtk_menu_item_new_with_mnemonic (_("_Remove From Tray"));
	g_signal_connect (G_OBJECT (menuitem), "activate",
		G_CALLBACK (menu_remove_cb), dc);
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
	
	gtk_widget_show_all (menu);
	
	return;
}

/* Handle button-press events */

static void
clicked (GtkWidget *widget, GdkEventButton *event, gpointer data)
{
	DrivelClient *dc = (DrivelClient *) data;
	
	if (event->type != GDK_BUTTON_PRESS)
		return;
	
	switch (event->button) {
		case 1:
		{
			view_friends (dc);
			break;
		}
		case 2: break;
		case 3:
		{
			gtk_menu_popup (GTK_MENU (menu), NULL, NULL, NULL, dc, 
					event->button, event->time);
			break;
		}
	}
	
	return;
}

/* Build the tray widget and associated image */

static void
tray_create (DrivelClient *dc)
{
	GtkWidget *box, *icon;
	GdkPixbuf *pixbuf;
	
	if (tray_icon)
		return;
	
	create_menu (dc);
	
	tray_icon = egg_tray_icon_new ("Drivel");
	box = gtk_event_box_new ();
	icon = gtk_image_new ();
	pixbuf = gdk_pixbuf_new_from_file (DATADIR G_DIR_SEPARATOR_S "pixmaps" 
			G_DIR_SEPARATOR_S "drivel" G_DIR_SEPARATOR_S 
			"tray_livejournal.png", NULL);
	gtk_image_set_from_pixbuf (GTK_IMAGE (icon), pixbuf);
	
	gtk_container_add (GTK_CONTAINER (box), icon);
	gtk_container_add (GTK_CONTAINER (tray_icon), box);
	gtk_widget_show_all (GTK_WIDGET (box));
	gtk_widget_hide (GTK_WIDGET (tray_icon)); /* don't display the icon yet */
	
	g_signal_connect (G_OBJECT (box), "button-press-event", 
		G_CALLBACK (clicked), dc);
	
	return;
}

/* Destroy the tray widget and associated image */

static void
tray_destroy (void)
{
	if (!tray_icon)
		return;
	
	gtk_widget_destroy (GTK_WIDGET (tray_icon));
	tray_icon = NULL;
	
	return;
}

/* Turn on the LiveJournal Friends Page check */

void
tray_turn_on (DrivelClient *dc)
{
	tray_create (dc);
	
	if (dc->user->api == BLOG_API_LJ)
		checkid = g_timeout_add (1000 * CHECKFRIENDS_INTERVAL, checkfriends, dc);
	
	return;
}

/* Turn off the LiveJournal Friends Page and remove the tray icon */

void
tray_turn_off (DrivelClient *dc)
{
	if (checkid)
	{
		g_source_remove (checkid);
		checkid = 0;
	}
	
	tray_destroy ();
	
	return;
}

/* Display the tray icon */

void
tray_display (gboolean visible)
{
	if (!tray_icon)
	{
		g_warning ("You're calling tray_display() before tray_turn_on().  Bad!");
		return;
	}
	
	if (visible)
		gtk_widget_show (GTK_WIDGET (tray_icon));
	else
		gtk_widget_hide (GTK_WIDGET (tray_icon));
	
	return;
}
