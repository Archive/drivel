/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * Copyright 2002-2004 Todd Kulesza
 *
 * Authors:
 * 		Todd Kulesza <todd@dropline.net>
 */

#ifndef _XMLRPC_H_
#define _XMLRPC_H_

#include <libsoup/soup.h>
#include <libsoup/soup-xmlrpc-message.h>

SoupXmlrpcMessage*
xmlrpc_start (const gchar *name, const gchar *uri);

SoupXmlrpcMessage*
xmlrpc_start_with_struct (const gchar *name, const gchar *uri);

void
xmlrpc_add_string_param (SoupXmlrpcMessage *msg, const gchar *value);

void
xmlrpc_add_string_member (SoupXmlrpcMessage *msg, const gchar *name, const gchar *value);

void
xmlrpc_add_int_param (SoupXmlrpcMessage *msg, const gint value);

void
xmlrpc_add_int_member (SoupXmlrpcMessage *msg, const gchar *name, const gint value);

void
xmlrpc_add_bool_param (SoupXmlrpcMessage *msg, const gboolean value);

void
xmlrpc_add_bool_member (SoupXmlrpcMessage *msg, const gchar *name, const gboolean value);

void
xmlrpc_start_array_param (SoupXmlrpcMessage *msg);

void
xmlrpc_end_array_param (SoupXmlrpcMessage *msg);

void
xmlrpc_start_array_member (SoupXmlrpcMessage *msg, const gchar *name);

void
xmlrpc_end_array_member (SoupXmlrpcMessage *msg);

void
xmlrpc_start_struct_param (SoupXmlrpcMessage *msg);

void
xmlrpc_end_struct_param (SoupXmlrpcMessage *msg);

void
xmlrpc_start_struct_member (SoupXmlrpcMessage *msg, const gchar *name);

void
xmlrpc_end_struct_member (SoupXmlrpcMessage *msg);

void
xmlrpc_end (SoupXmlrpcMessage *msg);

void
xmlrpc_end_with_struct (SoupXmlrpcMessage *msg);

#endif /* _XMLRPC_H_ */
