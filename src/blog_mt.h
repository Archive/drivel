/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * Copyright 2004 Todd Kulesza
 *
 * Authors:
 * 		Todd Kulesza <todd@dropline.net>
 */
 
#ifndef _BLOG_MT_H_
#define _BLOG_MT_H_

#include "drivel_request.h"
#include "drivel.h"

DrivelRequest*
blog_mt_build_login_request (const gchar *url, const gchar *username, 
		const gchar *password);

DrivelRequest*
blog_mt_build_postevent_request (const gchar *username, const gchar *password,
		const gchar *uri, const gchar *blogid, gboolean publish, 
		const gchar *title, const gchar *content);

DrivelRequest*
blog_mt_build_editevent_request (const gchar *username, const gchar *password,
		const gchar *uri, const gchar *postid, gboolean publish,
		const gchar *title, const gchar *content);

DrivelRequest*
blog_mt_build_getcategories_request (const gchar *username, 
		const gchar *password, const gchar *uri, const gchar *journal_name,
		const gchar *blogid);

DrivelRequest*
blog_mwl_build_getcategories_request (const gchar *username, 
				      const gchar *password, const gchar *uri, const gchar *journal_name,
				      const gchar *blogid);

DrivelRequest*
blog_mt_build_setpostcategories_request (const gchar *username, 
		const gchar *password, const gchar *uri, const gchar *postid, 
		const gchar *category);

DrivelRequest*
blog_mt_build_publish_request (const gchar *username, const gchar *password,
		const gchar *uri, const gchar *postid);

DrivelRequest*
blog_mt_build_getevents_request (const gchar *username, const gchar *password,
		const gchar *uri, const gchar *blogid, gboolean last_entry);

DrivelRequest*
blog_mt_build_getpostcategories_request (const gchar *username,
		const gchar *password, const gchar *uri, const gchar *postid);

#endif /* _BLOG_MT_H_ */
