/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * Copyright 2005 Todd Kulesza
 *
 * Authors:
 * 		Todd Kulesza <todd@dropline.net>
 */

#include <config.h>

#include <string.h>
#include <libxml/HTMLparser.h>

#include "atom.h"
#include "journal.h"
#include "network.h"
#include "blog_atom.h"

typedef struct _AtomLink AtomLink;

typedef enum
{
	ATOM_LINK_POST,
	ATOM_LINK_FEED
} AtomLinkType;

struct _AtomLink
{
	gchar *uri;
	gchar *title;
	AtomLinkType type;
};

#define CLIENT_LOGIN_URI "https://www.google.com/accounts/ClientLogin"

extern DrivelClient *dc;
gchar *auth_token = NULL;

static void
parse_clientlogin_request (SoupMessage *msg, SoupMessage *target_msg)
{
	gchar *body, **lines;
	gint i;

	g_return_if_fail (msg);
	g_return_if_fail (target_msg);

	debug ("atom parse_clientlogin_request()");

	/* Clear the old auth token */
	if (auth_token)
	{
		g_free (auth_token);
		auth_token = NULL;
	}

	body = g_malloc (msg->response.length + 1);
	memcpy (body, msg->response.body, msg->response.length);
	body[msg->response.length] = '\0';

	lines = g_strsplit (body, "\n", -1);
	for (i = 0; lines[i]; i++)
	{
		if (!strncmp (lines[i], "Auth=", strlen ("Auth=")))
		{
			auth_token = g_strdup_printf ("GoogleLogin auth=%s", lines[i] + strlen ("Auth="));
		}
	}

	if (auth_token)
	{
		soup_message_add_header (target_msg->request_headers, 
					 g_strdup ("Authorization"),
					 g_strdup (auth_token));
		net_enqueue_msg (target_msg);
	}
	else
		g_warning ("Failed to get Google authentication token");
					 
	g_strfreev (lines);
	g_free (body);

	return;
}

static void
send_clientlogin_request (const gchar *username, const gchar *password, 
			  SoupMessage *target_msg)
{
	SoupMessage *msg;
	gchar *post_data, *email, *pw;
	
	email = soup_uri_encode (username, NULL);
	pw = soup_uri_encode (password, NULL);
	post_data = g_strdup_printf ("accountType=HOSTED_OR_GOOGLE&"
				     "Email=%s&"
				     "Passwd=%s&"
				     "service=blogger&"
				     "source=GNOME-Drivel-%s",
				     email, pw, VERSION);
	
	msg = soup_message_new (SOUP_METHOD_POST, CLIENT_LOGIN_URI);
	soup_message_set_request (msg, "application/x-www-form-urlencoded",
				  SOUP_BUFFER_SYSTEM_OWNED,
				  post_data,
				  strlen (post_data) + 1);
				   
	g_signal_connect (G_OBJECT (msg), "finished",
			  G_CALLBACK (parse_clientlogin_request), target_msg);

	net_enqueue_msg (msg);
}

/* Return a list of AtomEntries */

static GSList*
retrieve_atom_entries (const gchar *xml)
{
	xmlDocPtr doc;
	xmlNodePtr node;
	GSList *entries = NULL;
	
	/* Parse the XML into a tree */
	doc = xmlReadMemory (xml, strlen (xml), NULL, NULL, 
			XML_PARSE_NOERROR | XML_PARSE_NOBLANKS);
	if (!doc)
		return NULL;
	
	node = xmlDocGetRootElement (doc);
	if (!node || xmlStrcasecmp (node->name, (xmlChar *)"feed"))
	{
		g_warning ("retrieve_atom_entries: XML is not a <feed>");
		xmlFreeDoc (doc);
		return NULL;
	}
	
	/* Drop inside the <feed> element and look for <entry> elements */
	node = node->xmlChildrenNode;
	while (node)
	{
		if (!xmlStrcasecmp (node->name, (xmlChar *)"entry"))
		{
			AtomEntry *entry;
			entry = atom_entry_parse (doc, node->xmlChildrenNode);
			if (entry)
				entries = g_slist_prepend (entries, entry);
		}
		
		node = node->next;
	}
	
	/* Reverse the list so that it is sorted newest->oldest */
	entries = g_slist_reverse (entries);
	
	return entries;
}

/* Return a list of AtomLinks */

static GSList*
retrieve_atom_links (xmlNodePtr node)
{
	GSList *links = NULL;
	
	/* Drop inside the <feed> element */
	node = node->xmlChildrenNode;
	while (node)
	{
		/* Search out the <entry> elements */
		if (!xmlStrcmp (node->name, (xmlChar *)"entry"))
		{
			gchar *title = NULL, *feed_uri = NULL, *post_uri = NULL;
			xmlNodePtr entry = node->xmlChildrenNode;

			while (entry)
			{
				/* Find the title and appropriate links */
				if (!xmlStrcmp (entry->name, (xmlChar *)"title"))
				{
					title = (gchar*)xmlNodeGetContent (entry);
				}
				else if (!xmlStrcmp (entry->name, (xmlChar *)"link"))
				{
					gchar *type;

					type = (gchar *)xmlGetProp (entry, (xmlChar *)"type");
					if (type && !xmlStrcmp ((xmlChar *)type, (xmlChar *)"application/atom+xml"))
					{
						gchar *rel, *uri;

						uri = (gchar *)xmlGetProp (entry, (xmlChar *)"href");
						rel = (gchar *)xmlGetProp (entry, (xmlChar *)"rel");

						if (uri && rel)
						{
							gchar *needle;
							
							/* Check for links that end in 'post' or 'feed' */
							if ((needle = g_strrstr (rel, "post")) && 
							    ((glong)needle == (glong)rel + (strlen (rel) - 4)))
							{
								post_uri = g_strdup (uri);
							}
							else if ((needle = g_strrstr (rel, "feed")) &&
								 ((glong)needle == (glong)rel + (strlen (rel) - 4)))
							{
								feed_uri = g_strdup (uri);
							}
						}
						g_free (uri);
						g_free (rel);
					}
					g_free (type);
				}
				entry = entry->next;
			}
			if (title && feed_uri && post_uri)
			{
				AtomLink *link;

				link = g_new0 (AtomLink, 1);
				link->type = ATOM_LINK_FEED;
				link->title = g_strdup (title);
				link->uri = g_strdup (feed_uri);
				links = g_slist_prepend (links, link);

				link = g_new0 (AtomLink, 1);
				link->type = ATOM_LINK_POST;
				link->title = g_strdup (title);
				link->uri = g_strdup (post_uri);
				links = g_slist_prepend (links, link);
			}
			
			g_free (title);
			g_free (feed_uri);
			g_free (post_uri);
		}
		node = node->next;
	}
	
	return links;
}

/* Parse the XML journal list and return a list of AtomLinks */

static GSList*
parse_xml_journal_list (const gchar *xml)
{
	xmlDocPtr doc;
	xmlNodePtr node;
	GSList *links = NULL;
	
	g_return_val_if_fail (xml, NULL);

	/* Parse the XML into a tree */
	doc = xmlReadMemory (xml, strlen (xml), NULL, NULL, XML_PARSE_NOERROR);
	if (!doc)
		return NULL;

	node = xmlDocGetRootElement (doc);
	if (!node || xmlStrcasecmp (node->name, (xmlChar *)"feed"))
	{
		g_warning ("parse_xml_journal_list: XML is not a <feed>");
		xmlFreeDoc (doc);
		return NULL;
	}

	/* Build a list of Atom links */
	links = retrieve_atom_links (node);

	xmlFreeDoc (doc);
	
	return links;
}

/* Parse the response for the <link> tag holding the Atom interface URL */

static void
parse_login_request (SoupMessage *msg, gpointer data)
{
	GSList *links, *current;
	gchar *body;

	debug ("atom parse_login_request");

	if (msg->response.body)
	{
		body = g_malloc (msg->response.length + 1);
		memcpy (body, msg->response.body, msg->response.length);
		body[msg->response.length] = '\0';
		
		links = parse_xml_journal_list (body);
		if (!links)
		{
			display_error_dialog (dc, _("Server error"),
					      _("The server did not return a valid Atom response."));
			return;
		}
		
		dc->journals = 0;
		for (current = links; current; current = current->next)
		{
			GSList *journal;
			AtomLink *link;
			DrivelJournal *dj;
			gboolean exists = FALSE;
			
			link = current->data;
			for (journal = dc->journal_list; journal && !exists; journal = journal->next)
			{
				dj = journal->data;
				if (!strcmp (dj->name, link->title))
				{
					switch (link->type)
					{
					case ATOM_LINK_POST: dj->uri_post = g_strdup (link->uri); break;
					case ATOM_LINK_FEED: dj->uri_feed = g_strdup (link->uri); break;
					}
					exists = TRUE;
				}
			}
			
			if (!exists)
			{
				dj = drivel_journal_new ();
				dj->name = link->title;
				dj->uri_post = link->uri;
				dj->type = JOURNAL_TYPE_USER;
				dc->journal_list = g_slist_prepend (dc->journal_list, dj);
				dc->journals++;
			}
			else
			{
				g_free (link->title);
				g_free (link->uri);
				g_free (link);
			}
		}
		
		for (current = dc->journal_list; current; current = current->next)
		{
			DrivelRequest *dr;
			DrivelJournal *dj = current->data;
			
			/* get the recent entries for this journal */
			dr = blog_atom_build_getevents_request (dc->user->username,
								dc->user->password, dj->uri_feed, FALSE);

		}
	
		add_account_to_list (dc);
		
		gtk_widget_hide (dc->login_window);
		journal_window_build (dc);
	}
	else
	{
		g_print ("warning: no response body\n");
	}
	
	return;
}

/* Send a GET request to the blog's index page */

DrivelRequest*
blog_atom_build_login_request (const gchar *username, const gchar *password,
			       const gchar *uri)
{
	SoupMessage *msg;

	g_return_val_if_fail (username, NULL);
	g_return_val_if_fail (password, NULL);
	g_return_val_if_fail (uri, NULL);

	debug ("blog_atom_build_login_request()");

	msg = soup_message_new (SOUP_METHOD_GET, uri);
	
	g_signal_connect (G_OBJECT (msg), "finished",
			  G_CALLBACK (parse_login_request), NULL);
	
	send_clientlogin_request (dc->user->username, dc->user->password, msg);

	return NULL;
}

static void
parse_post_request (SoupMessage *msg, gpointer data)
{
	g_return_if_fail (msg);

	debug ("atom parse_post_request()");
	
	if (msg->status_code < 200 || msg->status_code >= 300)
	{
		gchar *body;
		
		if (msg->response.length)
		{
			body = g_malloc (msg->response.length + 1);
			memcpy (body, msg->response.body, msg->response.length);
			body[msg->response.length] = '\0';
		}
		else
		{
			body = g_strdup (_("Unknown error"));
		}

		display_error_dialog (dc, _("Error Posting Entry"), body);

		g_free (body);
	}
	else
	{
		journal_finished_post (dc);
	}
	
	return;
}

/* Build a request with all of the post entry information */
/* Pass link=NULL for a new post or link='entry_uri' to update an existing
 * post. */
DrivelRequest*
blog_atom_build_post_request (const gchar *username, const gchar *password,
			      const gchar *uri, const gchar *subject, 
			      const gchar *content, const gchar *link)
{
	SoupMessage *msg;
	AtomEntry *entry;
	gchar *packet;

	g_return_val_if_fail (username, NULL);
	g_return_val_if_fail (password, NULL);
	g_return_val_if_fail (uri, NULL);
	
	entry = atom_entry_new ();
	if (subject)
		entry->title = g_strdup (subject);
	else
		entry->title = g_strdup ("");
	if (content)
		entry->content = g_strdup (content);
	else
		entry->content = g_strdup ("");
	entry->issued = get_w3dtf_timestamp ();
	
	if (link)
	{
		msg = soup_message_new (SOUP_METHOD_PUT, link);
	}
	else
	{
		msg = soup_message_new (SOUP_METHOD_POST, uri);
	}
	
	packet = atom_build_packet (entry);
	
	soup_message_set_request (msg, "application/atom+xml",
				  SOUP_BUFFER_SYSTEM_OWNED,
				  packet,
				  strlen (packet));

	soup_message_add_header (msg->request_headers, 
				 g_strdup ("Authorization"),
				 g_strdup (auth_token));
	
	g_signal_connect (G_OBJECT (msg), "finished",
			  G_CALLBACK (parse_post_request), NULL);

	net_enqueue_msg (msg);
	
	return NULL;
}

DrivelRequest*
blog_atom_build_delete_request (const gchar *username, const gchar *password,
				const gchar *uri)
{
	SoupMessage *msg;

	g_return_val_if_fail (uri, NULL);
	
	debug ("blog_atom_build_delete_request()\n");
	
	msg = soup_message_new (SOUP_METHOD_DELETE, uri);
	
	soup_message_add_header (msg->request_headers, 
				 g_strdup ("Authorization"),
				 g_strdup (auth_token));
	
	g_signal_connect (G_OBJECT (msg), "finished",
			  G_CALLBACK (parse_post_request), NULL);

	net_enqueue_msg (msg);

	return NULL;
}

static void
parse_getevents_request (SoupMessage *msg, gpointer data)
{
	GSList *entries, *current;
	gchar *body;
	
	g_return_if_fail (msg);
	
	debug ("atom parse_getevents_request");
	
	body = g_malloc (msg->response.length + 1);
	memcpy (body, msg->response.body, msg->response.length);
	body[msg->response.length] = '\0';

	entries = retrieve_atom_entries (body);
	if (entries)
	{
		if (g_object_get_data (G_OBJECT (msg), "last_entry"))
		{
			AtomEntry *entry = entries->data;

			journal_edit_entry (dc, entry->id, entry->content, NULL,
					    NULL, entry->title,	NULL, NULL, NULL,
					    NULL, NULL, NULL, entry->link);
		}
		else
		{
			gchar *feed_uri;

			feed_uri = soup_uri_to_string (soup_message_get_uri (msg), FALSE);
			for (current = entries; current; current = current->next)
			{
				DrivelJournalEntry *jentry = journal_entry_new ();
				AtomEntry *aentry = current->data;

				jentry->content = g_strdup (aentry->content);
				jentry->subject = g_strdup (aentry->title);
 				jentry->userid = g_strdup (feed_uri);
				jentry->postid = g_strdup (aentry->id);
				jentry->link = g_strdup (aentry->link);
				g_array_append_val (dc->recent_entries, jentry);
			}

			journal_refresh_recent_entries (dc);
			g_free (feed_uri);
		}
	}
	
	return;
}

DrivelRequest*
blog_atom_build_getevents_request (const gchar *username, const gchar *password,
				   const gchar *uri, gboolean last_entry)
{
	SoupMessage *msg;

	debug ("blog_atom_build_getevents_request_request()\n");

	g_return_val_if_fail (uri, NULL);

	msg = soup_message_new (SOUP_METHOD_GET, uri);

	soup_message_add_header (msg->request_headers, 
				 g_strdup ("Authorization"),
				 g_strdup (auth_token));

	if (last_entry)
	{
		g_object_set_data_full (G_OBJECT (msg), "last_entry", g_strdup ("true"), g_free);
	}

	g_signal_connect (G_OBJECT (msg), "finished",
			  G_CALLBACK (parse_getevents_request), NULL);

	net_enqueue_msg (msg);

	return NULL;
}
