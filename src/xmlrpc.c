/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * Copyright 2002-2004 Todd Kulesza
 *
 * Authors:
 * 		Todd Kulesza <todd@dropline.net>
 */

#include <config.h>

#include <stdarg.h> /* variable-length functions */
#include <string.h>
#include <time.h>
#include <libxml/parser.h>
#include <glib.h>
#include <libsoup/soup.h>
#include <libsoup/soup-xmlrpc-message.h>

#include "xmlrpc.h"

SoupXmlrpcMessage*
xmlrpc_start (const gchar *name, const gchar *uri)
{
	SoupXmlrpcMessage *msg;

	msg = soup_xmlrpc_message_new (uri);
	soup_xmlrpc_message_start_call (msg, name);

	return (msg);
}

SoupXmlrpcMessage*
xmlrpc_start_with_struct (const gchar *name, const gchar *uri)
{
	SoupXmlrpcMessage *msg;

	msg = soup_xmlrpc_message_new (uri);
	soup_xmlrpc_message_start_call (msg, name);
	soup_xmlrpc_message_start_param (msg);
	soup_xmlrpc_message_start_struct (msg);

	return (msg);
}

void
xmlrpc_add_string_param (SoupXmlrpcMessage *msg, const gchar *value)
{
	soup_xmlrpc_message_start_param (msg);
	soup_xmlrpc_message_write_string (msg, value);
	soup_xmlrpc_message_end_param (msg);

	return;
}

void
xmlrpc_add_string_member (SoupXmlrpcMessage *msg, const gchar *name, const gchar *value)
{
	soup_xmlrpc_message_start_member (msg, name);
	soup_xmlrpc_message_write_string (msg, value);
	soup_xmlrpc_message_end_member (msg);

	return;
}

void
xmlrpc_add_int_param (SoupXmlrpcMessage *msg, const gint value)
{
	soup_xmlrpc_message_start_param (msg);
	soup_xmlrpc_message_write_int (msg, value);
	soup_xmlrpc_message_end_param (msg);

	return;
}

void
xmlrpc_add_int_member (SoupXmlrpcMessage *msg, const gchar *name, const gint value)
{
	soup_xmlrpc_message_start_member (msg, name);
	soup_xmlrpc_message_write_int (msg, value);
	soup_xmlrpc_message_end_member (msg);

	return;
}

void
xmlrpc_add_bool_param (SoupXmlrpcMessage *msg, const gboolean value)
{
	soup_xmlrpc_message_start_param (msg);
	soup_xmlrpc_message_write_boolean (msg, value);
	soup_xmlrpc_message_end_param (msg);

	return;
}

void
xmlrpc_add_bool_member (SoupXmlrpcMessage *msg, const gchar *name, const gboolean value)
{
	soup_xmlrpc_message_start_member (msg, name);
	soup_xmlrpc_message_write_boolean (msg, value);
	soup_xmlrpc_message_end_member (msg);

	return;
}

void
xmlrpc_start_array_param (SoupXmlrpcMessage *msg)
{
	soup_xmlrpc_message_start_param (msg);
	soup_xmlrpc_message_start_array (msg);

	return;
}

void
xmlrpc_end_array_param (SoupXmlrpcMessage *msg)
{
	soup_xmlrpc_message_end_array (msg);
	soup_xmlrpc_message_end_param (msg);
	
	return;
}

void
xmlrpc_start_array_member (SoupXmlrpcMessage *msg, const gchar *name)
{
	soup_xmlrpc_message_start_member (msg, name);
	soup_xmlrpc_message_start_array (msg);

	return;
}

void
xmlrpc_end_array_member (SoupXmlrpcMessage *msg)
{
	soup_xmlrpc_message_end_array (msg);
	soup_xmlrpc_message_end_member (msg);

	return;
}

void
xmlrpc_start_struct_param (SoupXmlrpcMessage *msg)
{
	soup_xmlrpc_message_start_param (msg);
	soup_xmlrpc_message_start_struct (msg);

	return;
}

void
xmlrpc_end_struct_param (SoupXmlrpcMessage *msg)
{
	soup_xmlrpc_message_end_param (msg);
	soup_xmlrpc_message_end_member (msg);

	return;
}

void
xmlrpc_start_struct_member (SoupXmlrpcMessage *msg, const gchar *name)
{
	soup_xmlrpc_message_start_member (msg, name);
	soup_xmlrpc_message_start_struct (msg);

	return;
}

void
xmlrpc_end_struct_member (SoupXmlrpcMessage *msg)
{
	soup_xmlrpc_message_end_struct (msg);
	soup_xmlrpc_message_end_member (msg);

	return;
}

void
xmlrpc_end (SoupXmlrpcMessage *msg)
{
	soup_xmlrpc_message_end_call (msg);
	soup_xmlrpc_message_persist (msg);

	return;
}

void
xmlrpc_end_with_struct (SoupXmlrpcMessage *msg)
{
	soup_xmlrpc_message_end_struct (msg);
	soup_xmlrpc_message_end_param (msg);
	soup_xmlrpc_message_end_call (msg);
	soup_xmlrpc_message_persist (msg);

	return;
}
