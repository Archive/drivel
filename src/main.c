/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * Copyright 2002-2005 Todd Kulesza
 *
 * Authors:
 * 		Todd Kulesza <todd@dropline.net>
 */

#include <config.h>

#include <math.h>
#include <popt.h>
#include <gnome.h>
#include <gdk-pixbuf/gdk-pixdata.h>
#include <libgnomevfs/gnome-vfs.h>
#include <gconf/gconf.h>
#include <time.h>
#include <string.h>
#include <libxml/parser.h>

#include "drivel.h"
#include "login.h"
#include "network.h"
#include "journal.h"

/* size for picture icons */
#define ICON_SIZE 20

GMutex *net_mutex;
gboolean verbose;
DrivelClient *dc;

void
display_error_dialog (DrivelClient *dc, const gchar *header, const gchar *mesg)
{
	GtkWidget *dialog;
	gchar *error_header, *error_mesg, *error_string;
	
	if (header)
		error_header = g_strdup (header);
	else
		error_header = g_strdup (_("Error"));
	
	if (mesg)
		error_mesg = g_strdup (mesg);
	else
		error_mesg = g_strdup (
				_("Oh bother, there's a server error.  Please try again later."));
	
	error_string = g_strdup_printf ("<span weight=\"bold\" size=\"larger\">"
			"%s</span>\n\n%s", error_header, error_mesg);
	
	if (dc->current_window)
	{
		dialog = gtk_message_dialog_new_with_markup (
				GTK_WINDOW (dc->current_window),
				GTK_DIALOG_DESTROY_WITH_PARENT,
				GTK_MESSAGE_ERROR,
				GTK_BUTTONS_CLOSE,
				error_string);
		
		gtk_dialog_run (GTK_DIALOG (dialog));
		
		gtk_widget_destroy (dialog);
	}
	else
		g_warning ("%s:\n%s", error_header, error_mesg);
	
	g_free (error_header);
	g_free (error_mesg);
	g_free (error_string);
	
	return;
}

/* If the user request verbose output, display a debugging message */

void
debug (const gchar *msg)
{
	if (verbose)
		g_print ("debug: %s\n", msg);
	
	return;
}

/* Add the current username (from the Login window) to the list of user accounts */

void
add_account_to_list (DrivelClient *dc)
{
	DrivelUser *du;
	GSList *du_existing, *item;

	debug ("add_account_to_list()");
	
	/* copy the current user into du */
	du = drivel_user_new ();
	du->username = g_strdup (dc->user->username);
	du->password = g_strdup (dc->user->password);
	du->api = dc->user->api;
	du->server = g_strdup (dc->user->server);
	du->save_password = dc->user->save_password;
	du->autologin = dc->user->autologin;
	du->lastuser = TRUE;
	
	/* update the user list with the most recent login information */
	du_existing = find_in_user_list (dc->user_list, du);
	if (du_existing)
	{
		drivel_user_free (du_existing->data);
		dc->user_list = g_slist_delete_link (dc->user_list, du_existing);
	}
	/* if the user doesn't want to save her password, remove it before saving
	   the list to disk */
	if (!du->save_password)
	{
		g_free (du->password);
		du->password = g_strdup ("");
	}
	/* logging in as one user turns off autologin on any other users */
	for (item = dc->user_list; item; item = item->next)
	{
		DrivelUser *du;
		du = item->data;
		du->autologin = FALSE;
		du->lastuser = FALSE;
	}
	dc->user_list = g_slist_prepend (dc->user_list, du);
	save_user_list (dc->user_list, dc->config_directory);
	user_list_changed (dc);
	drivel_gconf_data_fill (dc->gconf, du->username, du->api, dc->client,
			&dc->id, dc);
	
	return;
}

/* Sort the journal list alphabetically with single-user journals listed first */
gint
sort_journals (DrivelJournal *a, DrivelJournal *b)
{
	gint retval;
	
	retval = a->type - b->type;
	if (!retval)
		retval = strcmp (a->name, b->name);

	return retval;
}

/* Clear any elements in the recent_entries array */
void
clear_recent_entries (GArray *recent)
{
	while (recent->len)
	{
		DrivelJournalEntry *entry;
		
		entry = g_array_index (recent, DrivelJournalEntry*, 0);
		g_array_remove_index (recent, 0);
		journal_entry_free (entry);
	}
	
	return;
}

void
fill_time (DrivelClient *dc)
{
    time_t exact_time;
    struct tm *local_time;
    
    /* we need the year, month, day, hour, and minute in order to post */
    exact_time = time (NULL);
    local_time = localtime (&exact_time);
    
    dc->time.year = (local_time->tm_year + 1900);
    dc->time.month = (local_time->tm_mon + 1);
    dc->time.day = local_time->tm_mday;
    dc->time.hour = local_time->tm_hour;
    dc->time.minute = local_time->tm_min;
    
    return;
}

void
drivel_push_current_window (DrivelClient *dc, GtkWidget *window)
{
	dc->window_list = g_slist_prepend (dc->window_list, window);
	dc->current_window = window;
	
	return;
}

GtkWidget *
drivel_pop_current_window (DrivelClient *dc)
{
	GtkWidget *window;
	
	window = dc->window_list->data;
	dc->window_list = g_slist_remove (dc->window_list, window);
	dc->current_window = dc->window_list->data;
	
	return window;
}

GtkWidget *
drivel_get_current_window (GSList *list)
{
	GtkWidget *window;
	
	window = list->data;
	
	return window;
}

gboolean
validate_username (const gchar *username)
{
	gchar *temp, *p;
	gunichar c;
	gboolean retval = TRUE;

	if (!g_utf8_validate (username, -1, NULL))
		return FALSE;
	
	temp = g_strdup (username);
	p = temp;
	
	while (*p)
	{
		c = g_utf8_get_char_validated (p, -1);

		if (!g_unichar_isalnum (c) && c != '_' && c!= '-' && c!='@' && c!='.')
		{
			retval = FALSE;
			break;
		}
		
		p = g_utf8_next_char (p);
	}
	
	g_free (temp);
	
	return retval;
}

void
drivel_button_list_clear (DrivelButtonVAList *bval)
{
	GSList *item, *entries;
	gulong signal;
	GtkWidget *entry;
	
	if (!bval)
		return;
	
	entries = bval->entries;
	for (item = bval->signals; item; item = item->next)
	{
		signal = GPOINTER_TO_INT (item->data);
		entry = entries->data;
		
		g_signal_handler_disconnect (entry, signal);
		entries = entries->next;
	}
	
	g_free (bval);
	bval = NULL;
	
	return;
}

gchar *
get_default_text (GConfClient *client, const gchar *key, const gchar *standard_text)
{
	gchar *string;
	
	string = gconf_client_get_string (client, key, NULL);
	
	if (!string)
		string = g_strdup (standard_text);
	
	return string;
}

void
menu_list_free_item (gpointer data, gpointer user_data)
{
	LJMenuItem *menu_item = (LJMenuItem *) data;
	
	g_free (menu_item->label);
	g_free (menu_item->url);
	
	return;
}

void
friends_list_free_item (gpointer data, gpointer user_data)
{
	LJFriend *friend = (LJFriend *) data;
	
	g_free (friend->username);
	g_free (friend->name);
	g_free (friend->bg);
	g_free (friend->fg);
	
	return;
}

gint
compare_usernames (gconstpointer a, gconstpointer b)
{
	const LJFriend *friend = a;
	const gchar *username = b;
	
	g_return_val_if_fail (a, -1);
	g_return_val_if_fail (b, 1);

	return (strncmp (friend->username, username, strlen (friend->username)));
}

/* 'security_store' must already be a valid GtkListStore */
void
fill_security_menu (DrivelClient *dc, GtkListStore *security_store)
{
	gtk_list_store_clear (security_store);
	
	store_security_append (security_store, "public.png", _("Public"), 
			-10, -10, TRUE);
	store_security_append (security_store, "protected.png", _("Friends Only"), 
			0, -5, TRUE);
	store_security_append (security_store, "private.png", _("Private"), 
			-5, 256, TRUE);
	
	return;
}

/* 'picture_store' must already be a valid GtkListStore */
void
fill_picture_menu (DrivelClient *dc, GtkListStore *picture_store)
{
	gint i;
	gchar *pickw;
	const gchar *text;
	GdkPixbuf *pixbuf, *pixbuf_small;
	GtkTreeIter iter;
	
	debug ("fill_picture_menu()");
	
	gtk_list_store_clear (picture_store);
	
	if (!dc->picture_keywords || !dc->picture_filenames)
		return;
	
	for (i = 0; i < dc->pictures + 1; i++)
	{
		gchar *pickf;

		pickw = g_strdup_printf ("pickw_%d", i);
		text = g_hash_table_lookup (dc->picture_keywords, pickw);
		if (i > 0)
		{
			const gchar *file;
			file = g_hash_table_lookup (dc->picture_filenames, pickw);
			pickf = g_build_filename (dc->config_directory, "pictures", file, NULL);
		} else
		{
			pickf = dc->default_picture_file;
		}
		if (pickf)
		{
			GError *error = NULL;
			pixbuf = gdk_pixbuf_new_from_file (pickf, &error);
			if (error)
				g_warning ("Couldn't load %s: %s\n", pickf, error->message);
			if (pixbuf && !error)
			{
				gdouble ratio;
				gint width, height;
				
				/* scale the icon while preserving it's dimension ratio */
				width = gdk_pixbuf_get_width (pixbuf);
				height = gdk_pixbuf_get_height (pixbuf);
				if (width > ICON_SIZE || height > ICON_SIZE)
				{
					if (width < height)
					{
						ratio = (gdouble)width / height;
						width = (gint)ceil (ratio * ICON_SIZE);
						height = ICON_SIZE;
					}
					else
					{
						ratio = (gdouble)height / width;
						width = ICON_SIZE;
						height = (gint)ceil (ratio * ICON_SIZE);
					}
				}
				
				pixbuf_small = gdk_pixbuf_scale_simple (pixbuf, width, height, 
						GDK_INTERP_BILINEAR);
				g_object_unref (G_OBJECT (pixbuf));
			}
			else
				pixbuf_small = NULL;
			if (error)
				g_error_free (error);
		} else
			pixbuf_small = NULL;
		gtk_list_store_append (picture_store, &iter);
		gtk_list_store_set (picture_store, &iter, 1, pixbuf_small, 0, text, -1);
		
		g_free (pickw);
	}
	
	return;
}

static void
load_stored_moods (GHashTable *mood_icons, gint moods, GConfClient *client)
{
	gchar *gconf_key, *key, *value;
	gint i, offset;

	offset = 0;

	for (i = 1; i < moods + 1; i++)
	{		
		gconf_key = g_strdup_printf ("/apps/drivel/moods/mood_%d", i + offset);
		value = g_strdup_printf ("%d", i + offset);
		key = gconf_client_get_string (client, gconf_key, NULL);
		
		/* skip non-existant moods */
		if (!key)
		{
			offset++;
			
			g_free (gconf_key);
			g_free (value);
			
			continue;
		}

		g_hash_table_insert (mood_icons, key, value);
		
		g_free (gconf_key);
	}
	
	return;
}

static void
gconf_data_free (DrivelGConfData *data, GConfClient *client, DrivelIDs *id)
{
	if (id)
		remove_gconf_notifies (client, id);

	g_free (data->user);
	
	g_free (data->default_mood);
	g_free (data->default_music);
	g_free (data->default_picture);
	g_free (data->default_security);
	g_free (data->default_security_mask);
	g_free (data->default_comment);
	g_free (data->default_autoformat);
	
	g_free (data->tray);
	
	g_free (data->entry_x);
	g_free (data->entry_y);
	g_free (data->entry_width);
	g_free (data->entry_height);
	g_free (data->entry_max);
	
	g_free (data->min_start);
	g_free (data->min_post);
	g_free (data->highlight_syntax);
	g_free (data->spellcheck);
	g_free (data->expander_open);
	g_free (data->spell_language);
	g_free (data->last_journal);
	g_free (data->technorati);
	
	return;
}

void
drivel_gconf_data_fill (DrivelGConfData *data, const gchar *username, 
		DrivelBlogAPI api, GConfClient *client, DrivelIDs *id, DrivelClient *dc)
{
	gchar *base;
	gchar *uid;
	
	g_return_if_fail (username);
	
	if (data->user)
		gconf_data_free (data, client, id);
	
	/* the uid is the username and api concated together */
	uid = g_strdup_printf ("%s_%d", username, api);
	
	base = g_strdup_printf ("/apps/drivel/users/%s", uid);
	data->user = base;
	
	data->default_mood = g_strdup_printf ("%s/default_mood", base);
	data->default_music = g_strdup_printf ("%s/default_music", base);
	data->default_picture = g_strdup_printf ("%s/default_picture", base);
	data->default_security = g_strdup_printf ("%s/default_security_name", base);
	data->default_security_mask = g_strdup_printf ("%s/default_security_mask", base);
	data->default_comment = g_strdup_printf ("%s/default_comment", base);
	data->default_autoformat = g_strdup_printf ("%s/default_autoformat", base);
	
	data->tray = g_strdup_printf ("%s/tray", base);
	
	data->entry_x = g_strdup_printf ("%s/entry_x", base);
	data->entry_y = g_strdup_printf ("%s/entry_y", base);
	data->entry_height = g_strdup_printf ("%s/entry_height", base);
	data->entry_width = g_strdup_printf ("%s/entry_width", base);
	data->entry_max = g_strdup_printf ("%s/entry_max", base);
	
	data->min_start = g_strdup_printf ("%s/min_start", base);
	data->min_post = g_strdup_printf ("%s/min_post", base);
	data->highlight_syntax = g_strdup_printf ("%s/highlight_syntax", base);
	data->spellcheck = g_strdup_printf ("%s/spellcheck", base);
	data->spell_language = g_strdup_printf ("%s/spell_language", base);
	data->last_journal = g_strdup_printf ("%s/last_journal", base);
	data->technorati = g_strdup_printf ("%s/technorati", base);
	
	data->expander_open = g_strdup_printf ("%s/expander_open", base);
	
	gconf_client_add_dir (client, base, GCONF_CLIENT_PRELOAD_NONE, NULL);

	add_gconf_notifies (dc);
	
	return;
}

static void
drivel_register_icons (void)
{
	GtkIconFactory *factory;
	GtkIconSet *icon;
	GdkPixbuf *pixbuf;
	gchar *path;
	GtkStockItem items[] =
	{
		{
			"drivel-post",
			_("_Post"),
			0,
			0,
			NULL
		},
		{
			"drivel-update",
			_("U_pdate"),
			0,
			0,
			NULL
		},
		{
			"drivel-add-friend",
			_("_Add..."),
			0,
			0,
			NULL
		},
		{
			"drivel-add-question",
			_("Add _Question"),
			0,
			0,
			NULL
		},
		{
			"drivel-add-answer",
			_("Add _Answer"),
			0,
			0,
			NULL
		},
		{
			"drivel-insert-button",
			_("_Insert"),
			0,
			0,
			NULL
		},
		{
			"drivel-edit",
			_("_Edit..."),
			0,
			0,
			NULL
		},
		{
			"drivel-login",
			_("_Log In"),
			0,
			0,
			NULL
		},
		{
			"drivel-save-draft",
			_("_Save Draft"),
			0,
			0,
			NULL
		},
		{
			"drivel-save-draft-btn",
			_("Save _Draft"),
			0,
			0,
			NULL
		},
		{
			"drivel-dont-save",
			_("_Don't Save"),
			0,
			0,
			NULL
		},
		{
			"drivel-insert-link",
			_("Insert Link..."),
			0,
			0,
			NULL
		},
		{
			"drivel-insert-image",
			_("Insert Image..."),
			0,
			0,
			NULL
		},
	};
	
	factory = gtk_icon_factory_new ();
	
	/* create our own icons */
	path = g_build_filename (DATADIR, "pixmaps", "drivel", "drivel-24.png", NULL);
	pixbuf = gdk_pixbuf_new_from_file (path, NULL);
	g_free (path);
	icon = gtk_icon_set_new_from_pixbuf (pixbuf);
	gtk_icon_factory_add (factory, "drivel-post", icon);
	
	path = g_build_filename (DATADIR, "pixmaps", "drivel", "insert_image.png", NULL);
	pixbuf = gdk_pixbuf_new_from_file (path, NULL);
	g_free (path);
	icon = gtk_icon_set_new_from_pixbuf (pixbuf);
	gtk_icon_factory_add (factory, "drivel-insert-image", icon);
	
	path = g_build_filename (DATADIR, "pixmaps", "drivel", "insert_object.png", NULL);
	pixbuf = gdk_pixbuf_new_from_file (path, NULL);
	g_free (path);
	icon = gtk_icon_set_new_from_pixbuf (pixbuf);
	gtk_icon_factory_add (factory, "drivel-insert-link", icon);
	
	/* adjust stock icons */
	icon = gtk_icon_factory_lookup_default (GTK_STOCK_ADD);
	gtk_icon_factory_add (factory, "drivel-add-friend", icon);

	icon = gtk_icon_factory_lookup_default (GTK_STOCK_ADD);
	gtk_icon_factory_add (factory, "drivel-add-question", icon);

	icon = gtk_icon_factory_lookup_default (GTK_STOCK_ADD);
	gtk_icon_factory_add (factory, "drivel-add-answer", icon);

	icon = gtk_icon_factory_lookup_default (GTK_STOCK_GO_FORWARD);
	gtk_icon_factory_add (factory, "drivel-insert-button", icon);
	
	icon = gtk_icon_factory_lookup_default (GTK_STOCK_PROPERTIES);
	gtk_icon_factory_add (factory, "drivel-edit", icon);
	
	icon = gtk_icon_factory_lookup_default (GTK_STOCK_GO_FORWARD);
	gtk_icon_factory_add (factory, "drivel-login", icon);
	
	icon = gtk_icon_factory_lookup_default (GTK_STOCK_SAVE);
	gtk_icon_factory_add (factory, "drivel-save-draft", icon);
	
	icon = gtk_icon_factory_lookup_default (GTK_STOCK_SAVE);
	gtk_icon_factory_add (factory, "drivel-save-draft-btn", icon);
	
	icon = gtk_icon_factory_lookup_default (GTK_STOCK_NO);
	gtk_icon_factory_add (factory, "drivel-dont-save", icon);
	
	icon = gtk_icon_factory_lookup_default (GTK_STOCK_SAVE);
	gtk_icon_factory_add (factory, "drivel-update", icon);
	
	gtk_icon_factory_add_default (factory);
	gtk_stock_add (items, G_N_ELEMENTS (items));
	
	return;
}

void
drivel_fill_journal_null (DrivelClient *dc)
{
	dc->journal_mood = NULL;
	dc->journal_music = NULL;
	dc->journal_picture = NULL;
	dc->journal_security = NULL;
	dc->journal_comment = NULL;
	dc->journal_autoformat = NULL;
}

static gchar *
init_config_directory (void)
{
	const gchar *home;
	gchar *directory, *pictures;
	GnomeVFSFileInfo info;

	home = g_get_home_dir ();
	if (!home)
		return NULL;
	
	/* try and create the directory, if necessary. */
	directory = g_build_filename (home, ".gnome2", "drivel.d", NULL);
	if (gnome_vfs_get_file_info (directory, &info, 
			GNOME_VFS_FILE_INFO_FORCE_FAST_MIME_TYPE) != GNOME_VFS_OK)
	{
		if (gnome_vfs_make_directory (directory, 0700) != GNOME_VFS_OK)
		{
			g_free(directory);
			return NULL;
		}
	}
	pictures = g_build_filename (directory, "pictures", NULL);
	if (gnome_vfs_get_file_info (pictures, &info, 
			GNOME_VFS_FILE_INFO_FORCE_FAST_MIME_TYPE) != GNOME_VFS_OK)
	{
		if (gnome_vfs_make_directory (pictures, 0700) != GNOME_VFS_OK)
		{
			g_free(directory);
			g_free(pictures);
			return NULL;
		}
	}
	g_free(pictures);
	
	return directory;
}

static gint
security_cmp (GtkTreeModel *model, GtkTreeIter *a, GtkTreeIter *b, gpointer data)
{
	gint group1, group2;
	
	gtk_tree_model_get (model, a, STORE_SECURITY_ORDER, &group1, -1);
	gtk_tree_model_get (model, b, STORE_SECURITY_ORDER, &group2, -1);
	
	return group1 - group2;
}

static gint
category_cmp (GtkTreeModel *model, GtkTreeIter *a, GtkTreeIter *b, gpointer data)
{
	gchar *cat1, *cat2;
	gint retval;
	
	gtk_tree_model_get (model, a, STORE_CATEGORY_NAME, &cat1, -1);
	gtk_tree_model_get (model, b, STORE_CATEGORY_NAME, &cat2, -1);
	
	if (!strcmp (_("None"), cat1))
		retval = -1;
	else if (!strcmp (_("None"), cat2))
		retval = 1;
	else
		retval = strcmp (cat1, cat2);
	
	return retval;
}

static gboolean
is_category_visible (GtkTreeModel *model, GtkTreeIter *iter, gpointer data)
{
	DrivelClient *dc = (DrivelClient *)data;
	gchar *category_name = NULL, *journal_name = NULL;
	gboolean retval = FALSE;
	
	if (dc->active_journal)
		journal_name = dc->active_journal->name;
	
	gtk_tree_model_get (model, iter, STORE_CATEGORY_BLOG, &category_name, -1);
	if (category_name && journal_name && 
		(!strcmp (category_name, journal_name) || !strcmp (category_name, "")))
		retval = TRUE;
	
	return retval;
}

static void
drivel_init (void)
{
	dc->client = gconf_client_get_default ();
	dc->gconf = g_new0 (DrivelGConfData, 1);
	dc->mood_icons = g_hash_table_new_full (g_str_hash, g_str_equal,
			hash_table_item_free, hash_table_item_free);
	dc->mood_list = gconf_client_get_list (dc->client, "/apps/drivel/moods/mood_list",
			GCONF_VALUE_STRING, NULL);
	dc->moods = gconf_client_get_int (dc->client, "/apps/drivel/moods/moods",
			NULL);
	dc->net = g_new0 (DrivelNet, 1);
	/* FIXME: this needs to be done a per-account basis */
	dc->net->api = BLOG_API_LJ;
	dc->net->fast_servers = 0;
	dc->net->cancel = gnome_vfs_cancellation_new ();
	dc->gconf->user = NULL;
	dc->user = drivel_user_new ();
	dc->picture_keywords = NULL;
	dc->picture_filenames = NULL;
	dc->use_fast_servers = TRUE;
	dc->proxy = gconf_client_get_bool (dc->client, 
			"/system/http_proxy/use_http_proxy", NULL);
	dc->proxy_auth = gconf_client_get_bool (dc->client, 
			"/system/http_proxy/use_authentication", NULL);
	dc->proxy_url = gconf_client_get_string (dc->client, 
			"/system/http_proxy/host", NULL);
	dc->proxy_port = gconf_client_get_int (dc->client, 
			"/system/http_proxy/port", NULL);
	dc->proxy_user = gconf_client_get_string (dc->client,
			"/system/http_proxy/authentication_user", NULL);
	dc->proxy_pass = gconf_client_get_string (dc->client,
			"/system/http_proxy/authentication_password", NULL);
	dc->journal_entry = NULL;
	dc->journals = 0;
	dc->journal_list = NULL;
	dc->active_journal = NULL;
	dc->menu_list = NULL;
	dc->friends_list = NULL;
	dc->edit_entry = FALSE;
	dc->window_list = NULL;
	dc->time_since_checkfriends = 0;
	dc->checking_friends = FALSE;
	dc->friends_update = FALSE;
	dc->lastupdate = g_strdup ("");
	dc->picture_store = NULL; /* FIXME: allocate this here */
	dc->security_store = gtk_list_store_new (STORE_SECURITY_N,
			GDK_TYPE_PIXBUF, G_TYPE_STRING, G_TYPE_INT, G_TYPE_INT, 
			G_TYPE_BOOLEAN);
	gtk_tree_sortable_set_sort_func (
			GTK_TREE_SORTABLE (dc->security_store), STORE_SECURITY_ORDER, 
			security_cmp, NULL, NULL);
	gtk_tree_sortable_set_sort_column_id (GTK_TREE_SORTABLE (dc->security_store),
			STORE_SECURITY_ORDER, GTK_SORT_ASCENDING);
	dc->security_store_filtered = gtk_list_store_new (STORE_SECURITY_N,
			GDK_TYPE_PIXBUF, G_TYPE_STRING, G_TYPE_INT, G_TYPE_INT, 
			G_TYPE_BOOLEAN);
	gtk_tree_sortable_set_sort_func (
			GTK_TREE_SORTABLE (dc->security_store), STORE_SECURITY_ORDER, 
			security_cmp, NULL, NULL);
	gtk_tree_sortable_set_sort_column_id (GTK_TREE_SORTABLE (dc->security_store),
			STORE_SECURITY_ORDER, GTK_SORT_ASCENDING);
	dc->category_store = gtk_list_store_new (STORE_CATEGORY_N, 
			G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING);
	gtk_tree_sortable_set_sort_func (
			GTK_TREE_SORTABLE (dc->category_store), STORE_CATEGORY_NAME, 
			category_cmp, NULL, NULL);
	gtk_tree_sortable_set_sort_column_id (GTK_TREE_SORTABLE (dc->category_store),
			STORE_CATEGORY_NAME, GTK_SORT_ASCENDING);
	dc->category_store_filtered = gtk_tree_model_filter_new (
			GTK_TREE_MODEL (dc->category_store), NULL);
	gtk_tree_model_filter_set_visible_func (
			GTK_TREE_MODEL_FILTER (dc->category_store_filtered),
			is_category_visible, dc, NULL);
	dc->draft_filename = NULL;
	dc->recent_entries = g_array_new (FALSE, TRUE, sizeof (DrivelJournalEntry *));
	
	drivel_fill_journal_null (dc);
	
	if (!dc->moods)
		dc->mood_list = g_slist_prepend (dc->mood_list, "");
	
	load_stored_moods (dc->mood_icons, dc->moods, dc->client);
	
	gconf_client_add_dir (dc->client, "/apps/drivel/global_settings", GCONF_CLIENT_PRELOAD_NONE, NULL);

	dc->config_directory = init_config_directory();

	dc->edit_history_window = NULL;
	
	return;
}

gint
main (gint argc, gchar **argv)
{
	GnomeProgram *program;
	GError *error;
	struct poptOption options[] = 
	{
		{ "verbose", 'v', POPT_ARG_NONE, &verbose, 0, "Display debugging output", "" },
		{ NULL }
	};

#ifdef ENABLE_NLS	
	bindtextdomain (GETTEXT_PACKAGE, GNOMELOCALEDIR);
	bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
	textdomain (GETTEXT_PACKAGE);
#endif /* ENABLE_NLS */
	xmlInitParser ();
	
	error = NULL;
	if (gconf_init (argc, argv, &error) == FALSE)
	{
		g_assert (error != NULL);
		g_message (_("GConf init failed: %s"), error->message);
		g_error_free (error);
		exit (EXIT_FAILURE);
	}
	
	if (gnome_vfs_init () == FALSE)
		g_error (_("Could not initialize GnomeVFS!\n"));
	
	program = gnome_program_init (PACKAGE, VERSION, 
			LIBGNOMEUI_MODULE, argc, argv,
			GNOME_PARAM_POPT_TABLE, options,
			GNOME_PARAM_HUMAN_READABLE_NAME, _("Drivel Journal Editor"),
			GNOME_PARAM_APP_DATADIR, DATADIR,
			NULL);
	
	net_mutex = g_mutex_new ();

	/* Build and initialize the DrivelClient */
	dc = g_new0 (DrivelClient, 1);
	drivel_init ();
	
	/* Check if the user passed a filename as an argument */
	if ((argc > 1) && (argv[argc - 1][0] != '-'))
		dc->draft_filename = g_strdup (argv[argc - 1]);
	
	drivel_register_icons ();
	net_start_session ();
	login_window_build (dc);
	
	gtk_main ();
	
	return 0;
}
