/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * Copyright 2002-2005 Todd Kulesza
 *
 * Authors:
 * 		Todd Kulesza <todd@dropline.net>
 */

#include <config.h>

#include <gnome.h>
#include <string.h>
#include <glib.h>
#include <libgnomevfs/gnome-vfs-cancellation.h>
#include <libsoup/soup.h>
#include <libsoup/soup-soap-message.h>

#include "blog_advogato.h"
#include "blog_atom.h"
#include "blog_blogger.h"
#include "blog_lj.h"
#include "blog_mt.h"
#include "drivel.h"
#include "drivel_request.h"
#include "msg_queue.h"
#include "login.h"
#include "journal.h"
#include "xmlrpc.h"
#include "network.h"

#define USER_AGENT "GNOME-Drivel/" VERSION

typedef struct _LJChallenge LJChallenge;

extern GMutex *net_mutex;
extern gboolean verbose;
extern DrivelClient *dc;
static SoupSession *session = NULL;

/* Update the network progress dialog to explain to the user what is happening */

static void
update_progress_msg (GAsyncQueue *queue, DrivelRequestType type)
{
	MsgInfo *info;
	gchar *msg, *header;
	
	switch (type)
	{
		case REQUEST_TYPE_LOGIN:
		{
			msg = g_strdup (_("Retrieving user information"));
			break;
		}
		case REQUEST_TYPE_GETPICTURE:
		{
			msg = g_strdup (_("Downloading user pictures"));
			break;
		}
		case REQUEST_TYPE_POSTEVENT:
		{
			msg = g_strdup (_("Posting journal entry"));
			break;
		}
		case REQUEST_TYPE_EDITEVENT:
		{
			msg = g_strdup (_("Updating journal entry"));
			break;
		}
		case REQUEST_TYPE_GETEVENTS:
		{
			msg = g_strdup (_("Retrieving journal entries"));
			break;
		}
		case REQUEST_TYPE_GETDAYCOUNTS:
		{
			msg = g_strdup (_("Retrieving journal history"));
			break;
		}
		case REQUEST_TYPE_EDITFRIENDS:
		{
			msg = g_strdup (_("Updating Friends list"));
			break;
		}
		case REQUEST_TYPE_GETFRIENDS:
		{
			msg = g_strdup (_("Retrieving Friends list"));
			break;
		}
		case REQUEST_TYPE_GETCATEGORIES:
		case REQUEST_TYPE_GETPOSTCATEGORIES:
		{
			msg = g_strdup (_("Retrieving categories"));
			break;
		}
		case REQUEST_TYPE_SETPOSTCATEGORIES:
		{
			msg = g_strdup (_("Setting categories"));
			break;
		}
		case REQUEST_TYPE_PUBLISH:
		{
			msg = g_strdup (_("Publishing journal entry"));
			break;
		}
		case REQUEST_TYPE_DELETEEVENT:
		{
			msg = g_strdup (_("Deleting journal entry"));
			break;
		}
		case REQUEST_TYPE_PING:
		{
			msg = g_strdup (_("Notifying Technorati"));
			break;
		}
		case REQUEST_TYPE_GETFRIENDGROUPS:
		{
			msg = g_strdup (_("Retrieving security groups"));
			break;
		}
		case REQUEST_TYPE_SETFRIENDGROUPS:
		{
			msg = g_strdup (_("Updating security groups"));
			break;
		}
		default:
		{
			msg = g_strdup (_("We're doing something, but I'm not sure what"));
			break;
		}
	}
	
	info = msg_info_new ();
	info->type = MSG_TYPE_UPDATE_PROGRESS_LABEL;
	header = g_strdup (_("Sending / Receiving"));
	info->msg = g_strdup_printf ("<span weight=\"bold\" size=\"larger\">"
			"%s</span>\n\n"
			"%s...", header, msg);
	g_async_queue_push (queue, info);
	
	g_free (header);
	g_free (msg);
	
	return;
}

static void
authenticate_cb (SoupSession *session, SoupMessage *msg, gchar *auth_type,
		 gchar *auth_realm, gchar **username, gchar **password,
		 gpointer user_data)
{
	debug ("setting username and password for HTTP_BASIC auth");

	*username = g_strdup (dc->user->username);
	*password = g_strdup (dc->user->password);

	return;
}

/* If the user has setup a proxy, return a SoupUri to it */
static SoupUri*
get_proxy_uri (DrivelClient *dc)
{
	SoupUri *proxy_uri = NULL;

	if (dc->proxy && dc->proxy_url)
	{
		gchar *text_uri;

		debug ("Setting up proxy server:");
		if (!dc->proxy_port)
			dc->proxy_port = 8080;
		
		text_uri = g_strdup_printf ("%s:%d", dc->proxy_url, dc->proxy_port);
		debug (text_uri);
		proxy_uri = soup_uri_new (text_uri);
		g_free (text_uri);
	}

	return proxy_uri;
}

static gboolean
clear_progress (gpointer data)
{
	gnome_appbar_set_progress_percentage (GNOME_APPBAR (dc->statusbar), 0.0);

	return FALSE;
}

static void
wrote_headers_cb (SoupMessage *msg, gpointer data)
{
	gnome_appbar_set_progress_percentage (GNOME_APPBAR (dc->statusbar), 0.05);
}

static void
got_headers_cb (SoupMessage *msg, gpointer data)
{
	gnome_appbar_set_progress_percentage (GNOME_APPBAR (dc->statusbar), 0.05);
}

static void
wrote_chunk_cb (SoupMessage *msg, gpointer data)
{
	static gint content_length = 0;
	static gint bytes_transferred = 0;

	if (bytes_transferred >= content_length)
	{
		content_length = g_strtod (soup_message_get_header (msg->request_headers, 
								    "Content-Length"), NULL);
		bytes_transferred = msg->request.length;
	}
	else
	{
		bytes_transferred += msg->request.length;
	}

	gnome_appbar_set_progress_percentage (GNOME_APPBAR (dc->statusbar), 
					      bytes_transferred / content_length);
	g_timeout_add (3000, clear_progress, NULL);

	return;
}

static void
got_chunk_cb (SoupMessage *msg, gpointer data)
{
	static gint content_length = 0;
	static gint bytes_transferred = 0;
	static guint timeout = 0;

	if (bytes_transferred >= content_length)
	{
		content_length = g_strtod (soup_message_get_header (msg->response_headers, 
								    "Content-Length"), NULL);
		bytes_transferred = msg->response.length;
	}
	else
	{
		bytes_transferred += msg->response.length;
	}

	gnome_appbar_set_progress_percentage (GNOME_APPBAR (dc->statusbar), 
					      bytes_transferred / content_length);
	g_timeout_add (3000, clear_progress, NULL);

	return;
}

/* Add a SoupMessage to the queue for processing */
void
net_enqueue_msg (SoupMessage *msg)
{
	g_return_if_fail (msg);

	/* Add the User-Agent header to every request */
	soup_message_add_header (msg->request_headers, "User-Agent", USER_AGENT);

	/* Add the ljfastserver cookie if we have a paid account */
	if (dc->net->fast_servers)
		soup_message_add_header (msg->request_headers, "Cookie", "ljfastserver=1");
	
	g_signal_connect (G_OBJECT (msg), "wrote-headers",
			  G_CALLBACK (wrote_headers_cb), NULL);
	g_signal_connect (G_OBJECT (msg), "got-headers",
			  G_CALLBACK (got_headers_cb), NULL);
	g_signal_connect (G_OBJECT (msg), "wrote-chunk",
			  G_CALLBACK (wrote_chunk_cb), NULL);
	g_signal_connect (G_OBJECT (msg), "got-chunk",
			  G_CALLBACK (got_chunk_cb), NULL);

	/* Queue up the message */
	soup_session_queue_message (session, msg, NULL, NULL);
	
	return;
}

void
net_requeue_msg (SoupMessage *msg)
{
	g_return_if_fail (msg);

	soup_session_requeue_message (session, msg);

	return;
}

/* Check for an error response */
SoupXmlrpcResponse*
net_msg_get_response (SoupXmlrpcMessage *msg)
{
	SoupXmlrpcResponse *response = NULL;

	if (!SOUP_STATUS_IS_SUCCESSFUL (SOUP_MESSAGE (msg)->status_code))
	{
		display_error_dialog (dc, _("Server error"), _("Network connection failed"));
	}
	else
	{
		if (!(response = soup_xmlrpc_message_parse_response (msg)))
		{
			display_error_dialog (dc, _("Server error"), _("Could not understand server response"));
		}
		else
		{
			/* If the response is a fault, display the error */
			if (soup_xmlrpc_response_is_fault (response))
			{
				SoupXmlrpcValue *value;
				GHashTable *table;
				gchar *error;

				value = soup_xmlrpc_response_get_value (response);
				soup_xmlrpc_value_get_struct (value, &table);
				value = g_hash_table_lookup (table, "faultString");
				soup_xmlrpc_value_get_string (value, &error);
				display_error_dialog (dc, _("Server error"), error);

				g_free (error);
				g_hash_table_destroy (table);
				g_object_unref (response);
				response = NULL;
			}
		}
	}

	return response;
}

/* Initialize the global SoupSession */
gint
net_start_session (void)
{
	SoupUri *proxy;

	g_return_val_if_fail (!session, -1);

	proxy = get_proxy_uri (dc);
	session = soup_session_async_new_with_options (SOUP_SESSION_PROXY_URI,
						       proxy,
						       NULL);
/* 	g_signal_connect (G_OBJECT (session), "authenticate", */
/* 			  G_CALLBACK (authenticate_cb), NULL); */

	return 0;
}

/* send a ping to technorati */
/* void */
/* net_ping_technorati (DrivelClient *dc) */
/* { */
/* 	DrivelRequest *dr; */
/* 	gchar *packet; */
/* 	const gchar *name; */
	
/* 	debug ("net_ping_technorati ()"); */
	
/* 	if (!gconf_client_get_bool (dc->client, dc->gconf->technorati, NULL)) */
/* 		return; */
	
/* 	/\* Use the LiveJournal description as the Technorati title for LJ users *\/ */
/* 	if ((dc->user->api == BLOG_API_LJ) &&  */
/* 		(dc->active_journal->type == JOURNAL_TYPE_USER)) */
/* 		name = dc->active_journal->description; */
/* 	else */
/* 		name = dc->active_journal->name; */
	
/* 	packet = xmlrpc_build_packet ("weblogUpdates.ping",  */
/* 			XMLRPC_TYPE_STRING, name, */
/* 			XMLRPC_TYPE_STRING, dc->active_journal->uri_view, */
/* 			-1); */
	
/* 	dr = drivel_request_new_with_items (REQUEST_TYPE_PING, */
/* 			REQUEST_PROTOCOL_POST,  */
/* 			BLOG_API_GENERIC,  */
/* 			"http://rpc.technorati.com/rpc/ping", */
/* 			g_strdup ("xml"), packet, */
/* 			NULL); */
	
/* /\* 	net_enqueue_request (dc, dr); *\/ */
	
/* 	return; */
/* } */
