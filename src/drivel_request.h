/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * Copyright 2004 Todd Kulesza
 *
 * Authors:
 * 		Todd Kulesza <todd@dropline.net>
 *		Davyd Madeley <davyd@ucc.asn.au>
 */

#ifndef _DRIVEL_REQUEST_H_
#define _DRIVEL_REQUEST_H_

#include <config.h>
#include <glib.h>
#include <libsoup/soup.h>

typedef enum
{
	REQUEST_TYPE_NONE,
	REQUEST_TYPE_LOGIN,
	REQUEST_TYPE_GETPICTURE,
	REQUEST_TYPE_CHECKFRIENDS,
	REQUEST_TYPE_POSTEVENT,
	REQUEST_TYPE_EDITEVENT,
	REQUEST_TYPE_GETEVENTS,
	REQUEST_TYPE_GETDAYCOUNTS,
	REQUEST_TYPE_EDITFRIENDS,
	REQUEST_TYPE_GETFRIENDS,
	REQUEST_TYPE_GETCATEGORIES,
	REQUEST_TYPE_PUBLISH,
	REQUEST_TYPE_DELETEEVENT,
	REQUEST_TYPE_GETPOSTCATEGORIES,
	REQUEST_TYPE_SETPOSTCATEGORIES,
	REQUEST_TYPE_PING,
	REQUEST_TYPE_GETFRIENDGROUPS,
	REQUEST_TYPE_SETFRIENDGROUPS
} DrivelRequestType;

typedef enum
{
	REQUEST_PROTOCOL_NONE,
	REQUEST_PROTOCOL_POST,
	REQUEST_PROTOCOL_GET,
	REQUEST_PROTOCOL_PUT,
	REQUEST_PROTOCOL_DELETE,
} DrivelRequestProtocol;

typedef enum
{
	BLOG_API_UNKNOWN,
	BLOG_API_LJ,
	BLOG_API_MT,
	BLOG_API_BLOGGER,
	BLOG_API_ADVOGATO,
	BLOG_API_ATOM,
	BLOG_API_OFFLINE,
	BLOG_API_GENERIC
} DrivelBlogAPI;

typedef struct _DrivelRequestData DrivelRequestData;
typedef struct _DrivelRequestItem DrivelRequestItem;
typedef struct _DrivelRequest DrivelRequest;

struct _DrivelRequestData
{
	gchar *data;
	guint len;
};

struct _DrivelRequestItem
{
	gchar *key;
	gchar *value;
};

struct _DrivelRequest
{
	DrivelRequestType type;
	DrivelRequestProtocol protocol;
	DrivelBlogAPI api;
	DrivelRequestData *data;
	GSList *items;
	GSList *current;
	GHashTable *request_values;
	gchar *uri;
	SoupMessage *msg;
};

/* Create an empty DrivelRequest */
DrivelRequest*
drivel_request_new (void);

/* Create a DrivelRequest with a type and multiple items, terminated by NULL */
DrivelRequest*
drivel_request_new_with_items (DrivelRequestType type, 
		DrivelRequestProtocol protocol, DrivelBlogAPI api, const gchar *uri, ...);

/* Create a DrivelRequest with a type and SoupMessage */
DrivelRequest*
drivel_request_new_with_msg (DrivelRequestType type, 
		DrivelRequestProtocol protocol, DrivelBlogAPI api, const gchar *uri, SoupMessage *msg);

/* Add multiple items to a DrivelRequest.  Terminate the list with NULL. */
void
drivel_request_add_items (DrivelRequest *dr, ...);

/* Remove multiple items by their key names.  Terminate the list with NULL. */
void
drivel_request_remove_items (DrivelRequest *dr, ...);

void
drivel_request_set_msg (DrivelRequest *dr, SoupMessage *msg);

SoupMessage*
drivel_request_get_msg (DrivelRequest *dr);

/* Set the DrivelRequestType */
void
drivel_request_set_type (DrivelRequest *dr, DrivelRequestType type);

/* Get the DrivelRequestType */
DrivelRequestType
drivel_request_get_type (DrivelRequest *dr);

/* Set the DrivelRequestProtocol */
void
drivel_request_set_protocol (DrivelRequest *dr, DrivelRequestProtocol protocol);

/* Get the DrivelRequestProtocol */
DrivelRequestProtocol
drivel_request_get_protocol (DrivelRequest *dr);

/* Set the journal API */
void
drivel_request_set_api (DrivelRequest *dr, DrivelBlogAPI api);

/* Get the journal API */
DrivelBlogAPI
drivel_request_get_api (DrivelRequest *dr);

/* Set the returned network data */
void
drivel_request_set_data (DrivelRequest *dr, DrivelRequestData *data);

/* Get a pointer to the returned network data */
DrivelRequestData*
drivel_request_get_data (DrivelRequest *dr);

/* Insert a key/value pair into the request_values hash table */
void
drivel_request_value_insert (DrivelRequest *dr, const gchar *key, const gchar *value);

/* Lookup a value given a key in the request_values hash table */
const gchar*
drivel_request_value_lookup (DrivelRequest *dr, const gchar *key);

/* Clear the request_values hash table */
void
drivel_request_clear_values (DrivelRequest *dr);

/* Replace the existing hash table with a user-supplied table */
void
drivel_request_set_values (DrivelRequest *dr, GHashTable *table);

/* Set the DrivelRequest to the first item, returns FALSE if no items are
   available. */
gboolean
drivel_request_start (DrivelRequest *dr);

/* Iterate to the next item in the list, returns FALSE if no items are
   available. */
gboolean
drivel_request_next (DrivelRequest *dr);

/* Returns the current item in the list, or NULL if no items are available. */
DrivelRequestItem*
drivel_request_get_current_item (DrivelRequest *dr);

/* Looks for 'key' in 'items', returns 'value' if found */
const gchar*
drivel_request_item_lookup (DrivelRequest *dr, const gchar *key);

/* Set the URI to connect to */
void
drivel_request_set_uri (DrivelRequest *dr, const gchar *uri);

/* Get the URI of the server */
const gchar*
drivel_request_get_uri (DrivelRequest *dr);

/* Frees the memory used by the object and the list. */
void
drivel_request_free (DrivelRequest *dr);

#endif /* _DRIVEL_REQUEST_H_ */
