/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * Copyright 2004 Todd Kulesza
 *
 * Authors:
 * 		Todd Kulesza <todd@dropline.net>
 */

#include <config.h>

#include "drivel.h"
#include "journal.h"
#include "blog_offline.h"

/* fill in the values of 'dc' with stub data */

void
blog_offline_fake_login (DrivelClient *dc)
{
	DrivelJournal *dj;
	
	/* fill generic account info with NULL or "Offline" */
	dc->journal_list = NULL;
	dc->journals = 0;
	dj = drivel_journal_new ();
	dj->description = g_strdup (_("Offline"));
	dc->active_journal = dj;
	g_free (dc->user->username);
	dc->user->username = g_strdup ("offline");
	g_free (dc->user->password);
	dc->user->password = NULL;
	g_free (dc->user->server);
	dc->user->server = NULL;
	dc->user->api = BLOG_API_OFFLINE;
	
	/* fill in the gconf path names */
	drivel_gconf_data_fill (dc->gconf, "offline", BLOG_API_OFFLINE, dc->client,
			&dc->id, dc);
	
	/* clear or create the hash tables for picture filenames and keywords */
	if (dc->picture_keywords != NULL)
		hash_table_clear (dc->picture_keywords);
	else
		dc->picture_keywords = g_hash_table_new_full (g_str_hash, g_str_equal,
				hash_table_item_free, hash_table_item_free);
	if (dc->picture_filenames != NULL)
		hash_table_clear (dc->picture_filenames);
	else
		dc->picture_filenames = g_hash_table_new_full (g_str_hash, g_str_equal,
				hash_table_item_free, hash_table_item_free);
	dc->default_picture_file = NULL;
	
	gtk_widget_hide (dc->login_window);
	journal_window_build (dc);
	
	return;
}
