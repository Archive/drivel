/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * Copyright 2002 Todd Kulesza
 *
 * Authors:
 * 		Todd Kulesza <todd@dropline.net>
 */

#ifndef _JOURNAL_H_
#define _JOURNAL_H_

#include "drivel.h"
     
void
journal_window_build (DrivelClient *dc);

void
jounal_edit_preferences (DrivelClient *dc);

void
journal_edit_entry (DrivelClient *dc, const gchar *itemid, const gchar *event, 
		const gchar *security, const gchar *allowmask, const gchar *subject, 
		const gchar *mood, const gchar *music, const gchar *picture,
		const gchar *eventtime, const gchar *comments, const gchar *autoformat, 
		const gchar *link);

void
journal_edit_entry_finished (DrivelClient *dc);

void
journal_edit_friends (DrivelClient *dc);

gboolean
save_draft_cb (GtkWidget *widget, gpointer data);

void
edit_preferences_cb (GtkWidget *widget, gpointer data);

void
add_gconf_notifies (DrivelClient *dc);

void
remove_gconf_notifies (GConfClient *client, DrivelIDs *id);

gboolean
delete_event_cb (GtkWidget *widget, GdkEventAny *event, gpointer data);

void
exit_cb (GtkWidget *widget, gpointer data);

void
remove_autosave (DrivelClient *dc);

void
journal_window_state_save (DrivelClient *dc);

void
journal_window_state_restore (DrivelClient *dc);

/* Clears up the entry window following a successful post */
void
journal_finished_post (DrivelClient *dc);

DrivelJournalProp*
journal_prop_new (void);

void
journal_prop_free (DrivelJournalProp *prop);

DrivelJournalEntry*
journal_entry_new (void);

void
journal_entry_free (DrivelJournalEntry *entry);

/* Refresh the Recent Entries menu */
void
journal_refresh_recent_entries (DrivelClient *dc);

#endif
