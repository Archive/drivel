/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * Copyright 2004 Todd Kulesza
 *
 * Authors:
 * 		Todd Kulesza <todd@dropline.net>
 */

#ifndef _BLOG_LJ_H_
#define _BLOG_LJ_H_

#include "drivel_request.h"
#include "drivel.h"

DrivelRequest*
blog_lj_build_login_request (const gchar *username, const gchar *uri, 
			     const gchar *version, gint n_moods);

DrivelRequest*
blog_lj_build_getpicture_request (const gchar *url, gchar *filename);


DrivelRequest*
blog_lj_build_checkfriends_request (const gchar *username, const gchar *uri,
				    const gchar* lastupdate);

void
blog_lj_parse_checkfriends_request (DrivelClient *dc, DrivelRequest *dr);

DrivelRequest*
blog_lj_build_postevent_request (const gchar *username, const gchar *uri,
				 const gchar *event, const gchar *music, const gchar *mood,
				 gint moodid, const gchar *subject, const gchar *security, gint mask,
				 const gchar *picture, gint year, gint month, gint day, gint hour,
				 gint minute, gint nocomments, gint preformatted, 
				 const DrivelJournal *dj, gint backdate);
		
DrivelRequest*
blog_lj_build_editevent_request (const gchar *username, const gchar *uri, 
				 const gchar *itemid, const gchar *event, const gchar *music, 
				 const gchar *mood, gint moodid, const gchar *subject, 
				 const gchar *security, gint mask, const gchar *picture, gint year, 
				 gint month, gint day, gboolean newdate, gint nocomments, 
				 gint preformatted, const DrivelJournal *dj);

DrivelRequest*
blog_lj_build_getevents_request (const gchar *username, const gchar *uri,
				 gint truncate, gboolean prefersubject, gboolean noprops, 
				 const gchar *selecttype, const gchar *lastsync, gint year, gint month, 
				 gint day, gint howmany, const gchar *beforedate, gint itemid, 
				 const DrivelJournal *dj);

DrivelRequest*
blog_lj_build_getdaycounts_request (const gchar *username, const gchar *uri,
				    const DrivelJournal *dj);

DrivelRequest*
blog_lj_build_editfriends_request (const gchar *username, const gchar *uri,
				   const gchar *friend, gboolean delete, gboolean add, const gchar *fg, 
				   const gchar *bg);

DrivelRequest*
blog_lj_build_getfriends_request (const gchar *username, const gchar *uri,
				  gboolean friendsof, gboolean groups);

#endif /* _BLOG_LJ_H_ */
