/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * Copyright 2005 Todd Kulesza
 *
 * Authors:
 * 		Todd Kulesza <todd@dropline.net>
 */

#include <config.h>

#include <string.h>

#include "drivel.h"
#include "drivel_request.h"
#include "network.h"
#include "journal.h"
#include "xmlrpc.h"
#include "blog_advogato.h"

extern DrivelClient *dc;

/* Convert line-breaks to HTML breaks (<br>) */
static gchar*
convert_linebreaks_to_html (const gchar *content)
{
	gchar **split, *converted;
	
	g_return_val_if_fail (content, NULL);
	
	split = g_strsplit (content, "\n", -1);
	if (split)
	{
		converted = g_strjoinv ("<br />", split);
		g_strfreev (split);
	}
	else
		converted = g_strdup (content);
	
	
	return converted;
}

/* Convert HTML breaks to line-breaks */
static gchar*
convert_linebreaks_from_html (const gchar *content)
{
	gchar **split, *converted, *temp;
	gchar *breaks[] = { "<br>", "<BR>", "<br />", "<BR />" };
	gint i;
	
	g_return_val_if_fail (content, NULL);
	
	temp = g_strdup (content);
	
	for (i = 0; i < 4; i++)
	{
		split = g_strsplit (temp, breaks[i], -1);
		if (split)
		{
			converted = g_strjoinv ("\n", split);
			g_strfreev (split);
		}
		else
			converted = g_strdup (content);
		
		g_free (temp);
		temp = converted;
	}
	
	return converted;
}

static void
parse_getevents_get_request (SoupMessage *msg, gpointer data)
{
	SoupXmlrpcResponse *response;
	SoupXmlrpcValue *value;
	gchar *html, *entry, *itemid_string;
	gint itemid;

	if (!(response = net_msg_get_response (SOUP_XMLRPC_MESSAGE (msg))))
		return;
	
	value = soup_xmlrpc_response_get_value (response);
	if (!value)
	{
		g_warning ("No value in reponse");
		return;
	}

	soup_xmlrpc_value_get_string (value, &html);
	entry = convert_linebreaks_from_html (html);
	itemid = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (msg), "itemid"));
	itemid_string = g_strdup_printf ("%d", itemid);

	journal_edit_entry (dc, itemid_string, entry, NULL, NULL, NULL, NULL, NULL, 
			    NULL, NULL, NULL, NULL, NULL);
	
	g_free (entry);
	g_free (html);
	g_free (itemid_string);
	g_object_unref (response);

	return;
}

/* Get the most recent journal entry */
static DrivelRequest*
build_getevents_get_request (const gchar *username, const gchar *uri, 
			     gint itemid)
{
	SoupXmlrpcMessage *msg;

	msg = xmlrpc_start ("diary.get", uri);
	xmlrpc_add_string_param (msg, username);
	xmlrpc_add_int_param (msg, itemid);
	xmlrpc_end (msg);
	
	g_signal_connect (G_OBJECT (msg), "finished",
			  G_CALLBACK (parse_getevents_get_request), NULL);
	g_object_set_data (G_OBJECT (msg), "itemid",
			   GINT_TO_POINTER (itemid));

	net_enqueue_msg (SOUP_MESSAGE (msg));

	return NULL;
}

static void
parse_getevents_len_request (SoupMessage *msg, gpointer data)
{
	SoupXmlrpcResponse *response;
	SoupXmlrpcValue *value;
	glong len;

	if (!(response = net_msg_get_response (SOUP_XMLRPC_MESSAGE (msg))))
		return;
	
	value = soup_xmlrpc_response_get_value (response);
	if (!value)
	{
		g_warning ("No value in reponse");
		return;
	}

	soup_xmlrpc_value_get_int (value, &len);

	build_getevents_get_request (g_object_get_data (G_OBJECT (msg), "username"),
				     g_object_get_data (G_OBJECT (msg), "uri"),
				     len - 1);

	g_object_unref (response);

	return;
}

/* Get the number of journal entries */
static DrivelRequest*
build_getevents_len_request (const gchar *username, const gchar *uri)
{
	SoupXmlrpcMessage *msg;

	msg = xmlrpc_start ("diary.len", uri);
	xmlrpc_add_string_param (msg, username);
	xmlrpc_end (msg);
	
	g_signal_connect (G_OBJECT (msg), "finished",
			  G_CALLBACK (parse_getevents_len_request), NULL);
	g_object_set_data_full (G_OBJECT (msg), "username", g_strdup (username), g_free);
	g_object_set_data_full (G_OBJECT (msg), "uri", g_strdup (uri), g_free);

	net_enqueue_msg (SOUP_MESSAGE (msg));

	return NULL;
}

static void
parse_login_request (SoupMessage *msg, gpointer data)
{
	SoupXmlrpcResponse *response;
	SoupXmlrpcValue *value;
	gchar *cookie;
	DrivelJournal *dj;

	if (!(response = net_msg_get_response (SOUP_XMLRPC_MESSAGE (msg))))
		return;
	
	value = soup_xmlrpc_response_get_value (response);
	if (!value)
	{
		g_warning ("No value in reponse");
		return;
	}

	soup_xmlrpc_value_get_string (value, &cookie);

	add_account_to_list (dc);
	dc->user->cookie = cookie; /* Don't free this */

	/* Advogato only supports a single journal per username */
	dj = drivel_journal_new ();
	dj->name = g_strdup (dc->user->username);
	dj->description = g_strdup (dc->user->username);
	dj->type = JOURNAL_TYPE_USER;
	dc->journal_list = g_slist_prepend (dc->journal_list, dj);
	dc->journals = 1;
	
	gtk_widget_hide (dc->login_window);
	journal_window_build (dc);

	g_object_unref (response);

	return;
}

DrivelRequest*
blog_advogato_build_login_request (const gchar *username, const gchar *password,
				   const gchar *uri)
{
	SoupXmlrpcMessage *msg;

	msg = xmlrpc_start ("authenticate", uri);
	xmlrpc_add_string_param (msg, username);
	xmlrpc_add_string_param (msg, password);
	xmlrpc_end (msg);
	
	g_signal_connect (G_OBJECT (msg), "finished",
			  G_CALLBACK (parse_login_request), NULL);

	net_enqueue_msg (SOUP_MESSAGE (msg));

	return NULL;
}

void
parse_postevent_request (SoupMessage *msg, gpointer data)
{
	SoupXmlrpcResponse *response;

	if (!(response = net_msg_get_response (SOUP_XMLRPC_MESSAGE (msg))))
		return;

	/* FIXME: ping technorati */
	journal_finished_post (dc);

	g_object_unref (response);

	return;
}

DrivelRequest*
blog_advogato_build_postevent_request (const gchar *cookie, const gchar *uri, 
				       gint index, const gchar *title, const gchar *content)
{
	gchar *linebreak_content;
	SoupXmlrpcMessage *msg;

	g_return_val_if_fail (cookie, NULL);
	
	linebreak_content = convert_linebreaks_to_html (content);
	
	msg = xmlrpc_start ("diary.set", uri);
	xmlrpc_add_string_param (msg, cookie);
	xmlrpc_add_int_param (msg, index);
	xmlrpc_add_string_param (msg, linebreak_content);
	xmlrpc_end (msg);
	
	g_signal_connect (G_OBJECT (msg), "finished",
			  G_CALLBACK (parse_postevent_request), NULL);

	net_enqueue_msg (SOUP_MESSAGE (msg));
	
	g_free (linebreak_content);
	
	return NULL;
}

/* Get the most recent entry */
DrivelRequest*
blog_advogato_build_getevents_request (const gchar *username, const gchar *uri)
{
	DrivelRequest *dr;
	
	g_return_val_if_fail (username, NULL);
	
	dr = build_getevents_len_request (username, uri);
	
	return dr;
}
