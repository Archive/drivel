/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * Copyright 2002-2003 Todd Kulesza
 *
 * Authors:
 * 		Todd Kulesza <todd@dropline.net>
 */

#ifndef _DIALOGS_H_
#define _DIALOGS_H_

#include <gtk/gtk.h>
#include <stdio.h>
#include <stdarg.h>

#include "drivel.h"

enum
{
	TYPE_COL,
	LINK_COL,
	USERNAME_COL,
	NAME_COL,
	FRIEND_COL,
	FRIENDOF_COL,
	FG_COL,
	BG_COL,
	TYPE_INT_COL,
	N_COLUMNS
};

enum
{
	SORT_TYPE,
	SORT_LINK,
	SORT_USERNAME,
	SORT_REALNAME
};

enum
{
	HISTORY_TIME_COL,
	HISTORY_SUBJECT_COL,
	HISTORY_ITEMID_COL,
	HISTORY_N_COLUMNS
};

void
update_friends_list (DrivelClient *dc);

void
update_history_list (DrivelClient *dc, gchar **itemid, gchar **event, gchar **eventtime, gint count);

void
update_history_marks (DrivelClient *dc, gint8 days []);

void
display_gnomevfs_error_dialog (DrivelClient *dc, GnomeVFSResult result);

void
display_open_error_dialog (DrivelClient *dc, const char *filename);

gboolean
display_save_dialog_proceed (DrivelClient *dc);

gboolean
display_save_dialog_close (DrivelClient *dc);

void
display_edit_history_dialog (DrivelClient *dc);

void
display_edit_friends_dialog (DrivelClient *dc);

void
display_insert_link_dialog (DrivelClient *dc);

void
display_insert_image_dialog (DrivelClient *dc);

void
display_insert_poll_dialog (DrivelClient *dc);

void
display_security_dialog (DrivelClient *dc);

void
display_edit_preferences_dialog (DrivelClient *dc);

#endif
