/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * Copyright 2002 Todd Kulesza
 *
 * Authors:
 * 		Todd Kulesza <todd@dropline.net>
 */

#ifndef _NETWORK_H_
#define _NETWORK_H_

#include <config.h>

#include <libsoup/soup-xmlrpc-message.h>

#include "drivel_request.h"
#include "drivel.h"

/* Add a SoupMessage to the queue for processing */
void
net_enqueue_msg (SoupMessage *msg);

void
net_requeue_msg (SoupMessage *msg);

/* Check for an error response */
SoupXmlrpcResponse*
net_msg_get_response (SoupXmlrpcMessage *msg);

/* start the network session */
gint
net_start_session (void);

/* send a ping to technorati */
void
net_ping_technorati (DrivelClient *dc);

#endif /* _NETWORK_H_ */
