/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * Copyright 2004 Todd Kulesza
 *
 * Authors:
 * 		Todd Kulesza <todd@dropline.net>
 */

#include <config.h>

#include <string.h>
#include <glib.h>
#include <libsoup/soup.h>
#include <libsoup/soup-xmlrpc-message.h>

#include "drivel_request.h"
#include "drivel.h"
#include "journal.h"
#include "dialogs.h"
#include "login.h"
#include "network.h"
#include "tray.h"
#include "utils.h"
#include "xmlrpc.h"
#include "blog_lj.h"

extern DrivelClient *dc;

static gchar*
build_challenge_response (const gchar *challenge, const gchar *password)
{
	gchar *response, *pwhash, *hash;

	pwhash = md5_hash (password);
	response = g_strdup_printf ("%s%s", challenge, pwhash);
	hash = md5_hash (response);
	g_free (response);
	g_free (pwhash);
	
	return (hash);
}

static void
parse_getchallenge_request (SoupMessage *msg, SoupMessage *target_msg)
{
	SoupXmlrpcResponse *response;
	SoupXmlrpcValue *value;
	GHashTable *table;
	gchar *challenge, *challenge_response;

	g_return_if_fail (msg);
	g_return_if_fail (target_msg);

	debug ("livejournal parse_getchallenge_request()");

	if (!(response = net_msg_get_response (SOUP_XMLRPC_MESSAGE (msg))))
		return;
	
	value = soup_xmlrpc_response_get_value (response);
	if (!value)
	{
		g_warning ("No value in reponse");
		return;
	}

	soup_xmlrpc_value_get_struct (value, &table);
	value = g_hash_table_lookup (table, "challenge");
	soup_xmlrpc_value_get_string (value, &challenge);

	challenge_response = build_challenge_response (challenge,
						       dc->user->password);
	xmlrpc_add_string_member (SOUP_XMLRPC_MESSAGE (target_msg), 
				  "auth_method",
				  "challenge");
	xmlrpc_add_string_member (SOUP_XMLRPC_MESSAGE (target_msg),
				  "auth_challenge",
				  challenge);
	xmlrpc_add_string_member (SOUP_XMLRPC_MESSAGE (target_msg),
				  "auth_response",
				  challenge_response);
	xmlrpc_end_with_struct (SOUP_XMLRPC_MESSAGE (target_msg));

	net_enqueue_msg (target_msg);

	g_free (challenge);
	g_free (challenge_response);
	g_hash_table_destroy (table);
	g_object_unref (response);

	return;
}

static void
send_getchallenge_request (const gchar *uri, SoupXmlrpcMessage *target_msg)
{
	SoupXmlrpcMessage *msg;

	msg = xmlrpc_start ("LJ.XMLRPC.getchallenge", uri);
	xmlrpc_end (msg);

	g_signal_connect (G_OBJECT (msg), "finished",
			  G_CALLBACK (parse_getchallenge_request), target_msg);

	net_enqueue_msg (SOUP_MESSAGE (msg));

	return;
}

static void
parse_friendgroup_data (SoupXmlrpcValue *value, GtkListStore *store)
{
	SoupXmlrpcValueArrayIterator *iter;
	
	/* fetch the user's security groups */

	soup_xmlrpc_value_array_get_iterator (value, &iter);
	while (soup_xmlrpc_value_array_iterator_get_value (iter, &value) && value)
	{
		GHashTable *table;
		gchar *name;
		glong id, sortorder, public;
		
		soup_xmlrpc_value_get_struct (value, &table);

		value = g_hash_table_lookup (table, "name");
		soup_xmlrpc_value_get_string (value, &name);
		value = g_hash_table_lookup (table, "id");
		soup_xmlrpc_value_get_int (value, &id);
		value = g_hash_table_lookup (table, "sortorder");
		soup_xmlrpc_value_get_int (value, &sortorder);
		value = g_hash_table_lookup (table, "public");
		soup_xmlrpc_value_get_int (value, &public);

		store_security_append (store, "protected.png", name, id, 
				       sortorder, (gboolean) public);

		g_hash_table_destroy (table);
		iter = soup_xmlrpc_value_array_iterator_next (iter);
	}

	return;
}

static void
parse_menus (SoupXmlrpcValue *value, gint level)
{
	SoupXmlrpcValueArrayIterator *iter;
	gint j = 1;

	soup_xmlrpc_value_array_get_iterator (value, &iter);
	while (soup_xmlrpc_value_array_iterator_get_value (iter, &value) && value)
	{
		GHashTable *table;
		gchar *text, *url;
		LJMenuItem *menu_item;

		soup_xmlrpc_value_get_struct (value, &table);
		value = g_hash_table_lookup (table, "text");
		soup_xmlrpc_value_get_string (value, &text);
		value = g_hash_table_lookup (table, "url");
		soup_xmlrpc_value_get_string (value, &url);
			
		menu_item = g_new0 (LJMenuItem, 1);
		menu_item->label = text;
		menu_item->url = url;
		menu_item->menu_index = level;
		menu_item->item_index = j;

		/* if this is a submenu, recurse into it */
		value = g_hash_table_lookup (table, "sub");
		if (value)
		{
			menu_item->sub_menu = TRUE;
			parse_menus (value, level + 1);
		}
		else
			menu_item->sub_menu = FALSE;
		
		/* FIXME: the menu list isn't working */
/*		dc->menu_list = g_slist_append (dc->menu_list, menu_item); */

		g_hash_table_destroy (table);
		j++;
		iter = soup_xmlrpc_value_array_iterator_next (iter);
	}

	return;
}

static void
parse_login_request (SoupMessage *msg, gpointer data)
{
	SoupXmlrpcResponse *response;
	SoupXmlrpcValue *value;
	GHashTable *table;
	DrivelJournal *dj = NULL;

	g_return_if_fail (msg);

	debug ("livejournal parse_login_request()");
	
	if (!(response = net_msg_get_response (SOUP_XMLRPC_MESSAGE (msg))))
		return;
	
	value = soup_xmlrpc_response_get_value (response);
	if (!value)
	{
		g_warning ("No value in reponse");
		return;
	}

	add_account_to_list (dc);

	/* clear out the list of security groups and add the defaults */
	fill_security_menu (dc, dc->security_store);

	soup_xmlrpc_value_get_struct (value, &table);

	/* if lj sent us a message, display it to the user */
	value = g_hash_table_lookup (table, "message");
	if (value)
	{
		GtkWidget *dialog;
		gchar *message;

		soup_xmlrpc_value_get_string (value, &message);

		dialog = gtk_message_dialog_new (
			GTK_WINDOW (dc->current_window),
			GTK_DIALOG_DESTROY_WITH_PARENT,
			GTK_MESSAGE_INFO,
			GTK_BUTTONS_OK,
			message);
		
		gtk_dialog_run (GTK_DIALOG (dialog));
		
		g_free (message);
		gtk_widget_destroy (dialog);
	}

	/* are we able to use the fast servers? */
	dc->use_fast_servers = FALSE;
	value = g_hash_table_lookup (table, "fastserver");
	if (value)
	{
		glong fastserver;

		soup_xmlrpc_value_get_int (value, &fastserver);
		if (fastserver)
			dc->use_fast_servers = TRUE;
	}

	/* clear or create the hash tables for picture filenames and keywords */
	if (dc->picture_keywords != NULL)
		hash_table_clear (dc->picture_keywords);
	else
		dc->picture_keywords = g_hash_table_new_full (g_str_hash, g_str_equal,
							      hash_table_item_free, 
							      hash_table_item_free);
	if (dc->picture_filenames != NULL)
		hash_table_clear (dc->picture_filenames);
	else
		dc->picture_filenames = g_hash_table_new_full (g_str_hash, g_str_equal,
							       hash_table_item_free, 
							       hash_table_item_free);
	dc->default_picture_file = NULL;
	g_hash_table_insert (dc->picture_keywords, g_strdup ("pickw_0"),
			     g_strdup_printf ("[%s]", _("default")));

	/* fill the picture keywords hash table */
	dc->pictures = 0;
	value = g_hash_table_lookup (table, "pickws");
	if (value)
	{
		SoupXmlrpcValueArrayIterator *iter;
		gint i = 1;

		soup_xmlrpc_value_array_get_iterator (value, &iter);
		while (soup_xmlrpc_value_array_iterator_get_value (iter, &value) && value)
		{
			gchar *kw, *localkey;

			soup_xmlrpc_value_get_string (value, &kw);
			localkey = g_strdup_printf ("pickw_%d", i);
			g_hash_table_insert (dc->picture_keywords, localkey, kw);

			i++;
			iter = soup_xmlrpc_value_array_iterator_next (iter);
		}

		dc->pictures = i - 1;
	}

	/* fill the picture filenames hash table */
	value = g_hash_table_lookup (table, "pickwurls");
	if (value)
	{
		SoupXmlrpcValueArrayIterator *iter;
		gint i = 1;

		soup_xmlrpc_value_array_get_iterator (value, &iter);
		while (soup_xmlrpc_value_array_iterator_get_value (iter, &value) && value)
		{
			gchar *url, *localkey, *user_id, *no_user_id, *pic_id, *pic_file;

			soup_xmlrpc_value_get_string (value, &url);
			localkey = g_strdup_printf ("pickw_%d", i);
			user_id = g_path_get_basename (url);
			no_user_id = g_path_get_dirname (url);
			pic_id = g_path_get_basename (no_user_id);
			pic_file = g_strdup_printf ("%s_%s", user_id, pic_id);
			g_free (no_user_id);
			g_free (user_id);
			g_free (pic_id);

			g_hash_table_insert (dc->picture_filenames, localkey, pic_file);

			if (!picture_exists (dc->config_directory, pic_file))
				blog_lj_build_getpicture_request (url, pic_file);

			i++;
			iter = soup_xmlrpc_value_array_iterator_next (iter);
		}
	}

	value = g_hash_table_lookup (table, "defaultpicurl");
	if (value)
	{
		gchar *url, *user_id, *no_user_id, *pic_id, *pic_file;
		
		soup_xmlrpc_value_get_string (value, &url);
		user_id = g_path_get_basename (url);
		no_user_id = g_path_get_dirname (url);
		pic_id = g_path_get_basename (no_user_id);
		pic_file = g_strdup_printf ("%s_%s", user_id, pic_id);
		dc->default_picture_file = g_build_filename (dc->config_directory,
							     "pictures", pic_file, NULL);

		if (!picture_exists (dc->config_directory, pic_file))
			blog_lj_build_getpicture_request (url, pic_file);

		g_free (no_user_id);
		g_free (user_id);
		g_free (pic_id);
	}

	/* get any new moods */
	value = g_hash_table_lookup (table, "moods");
	if (value)
	{
		SoupXmlrpcValueArrayIterator *iter;

		soup_xmlrpc_value_array_get_iterator (value, &iter);
		while (soup_xmlrpc_value_array_iterator_get_value (iter, &value) && value)
		{
			GHashTable *table;
			gchar *label, *key;
			glong id;

			soup_xmlrpc_value_get_struct (value, &table);

			value = g_hash_table_lookup (table, "id");
			soup_xmlrpc_value_get_int (value, &id);
			value = g_hash_table_lookup (table, "name");
			soup_xmlrpc_value_get_string (value, &label);
			key = g_strdup_printf ("/apps/drivel/moods/mood_%ld", id);
			gconf_client_set_string (dc->client, key, label, NULL);
			g_hash_table_insert (dc->mood_icons, key, value);

			dc->mood_list = g_slist_append (dc->mood_list, label);
			dc->moods++;
			
			g_hash_table_destroy (table);
			iter = soup_xmlrpc_value_array_iterator_next (iter);
		}

		dc->mood_list = g_slist_sort (dc->mood_list, string_compare);
		
		gconf_client_set_int (dc->client, "/apps/drivel/moods/moods", dc->moods, NULL);
		gconf_client_set_list (dc->client, "/apps/drivel/moods/mood_list", 
				       GCONF_VALUE_STRING, dc->mood_list, NULL);
	}

	/* fetch the user's security groups */
	value = g_hash_table_lookup (table, "friendgroups");
	parse_friendgroup_data (value, dc->security_store);

	/* # of journals this user has access to */
	dc->journals = 0;
	value = g_hash_table_lookup (table, "usejournals");
	if (value)
	{
		SoupXmlrpcValueArrayIterator *iter;

		soup_xmlrpc_value_array_get_iterator (value, &iter);
		while (soup_xmlrpc_value_array_iterator_get_value (iter, &value) && value)
		{
			gchar *name;

			soup_xmlrpc_value_get_string (value, &name);
			
			if (strncmp (name, dc->user->username, strlen (name)))
			{
				dj = drivel_journal_new ();
				dj->name = g_strdup (name);
				dj->uri_view = g_strdup_printf ("%s/users/%s",
								dc->user->server, name);
				dj->type = JOURNAL_TYPE_COMMUNITY;
				dc->journal_list = g_slist_prepend (dc->journal_list, dj);
				dc->journals++;
			}

			g_free (name);
			iter = soup_xmlrpc_value_array_iterator_next (iter);
		}
	}

	/* get the user's journal name */
	value = g_hash_table_lookup (table, "fullname");
	if (value)
	{
		gchar *fullname;

		soup_xmlrpc_value_get_string (value, &fullname);

		dj = drivel_journal_new ();
		dj->description = fullname;
		dj->name = g_strdup (dc->user->username);
		dj->uri_view = g_strdup_printf ("%s/users/%s",
						dc->user->server, 
						dc->user->username);

		dc->journal_list = g_slist_prepend (dc->journal_list, dj);
		dc->journals++;
		
		/* set the default active journal to this, as it's the primary account */
		dc->active_journal = dj;
	}
 	dc->journal_list = g_slist_sort (dc->journal_list, (GCompareFunc)sort_journals);

	/* fetch recent entries for our journal */
	blog_lj_build_getevents_request (dc->user->username,
					 dc->user->server, 0, 
					 FALSE, FALSE, "lastn", 
					 NULL, 0, 0, 0,
					 DRIVEL_N_RECENT_POSTS, 
					 NULL, 0, dj);

	/* create the web links menu */
	value = g_hash_table_lookup (table, "menus");
	if (value)
		parse_menus (value, 0);

 	gtk_widget_hide (dc->login_window);
 	journal_window_build (dc);

	g_hash_table_destroy (table);
	g_object_unref (response);

 	return;
}

DrivelRequest*
blog_lj_build_login_request (const gchar *username, const gchar *uri, 
			     const gchar *version, gint n_moods)
{
	SoupXmlrpcMessage *msg;

	debug ("livejournal build_login_request()");

	msg = xmlrpc_start_with_struct ("LJ.XMLRPC.login", uri);
	xmlrpc_add_string_member (msg, "username", username);
	xmlrpc_add_int_member (msg, "ver", 1);
	xmlrpc_add_string_member (msg, "clientversion", version);
	xmlrpc_add_int_member (msg, "getmoods", n_moods);
	xmlrpc_add_int_member (msg, "getmenus", 1);
	xmlrpc_add_int_member (msg, "getpickws", 1);
	xmlrpc_add_int_member (msg, "getpickwurls", 1);

	g_signal_connect (G_OBJECT (msg), "finished",
			  G_CALLBACK (parse_login_request), NULL);

	send_getchallenge_request (uri, msg);

	return NULL;
}

static void
parse_getpicture_request (SoupMessage *msg, gpointer data)
{
	gchar *path;
	GnomeVFSHandle *handle;
	GnomeVFSResult result;
	gchar *filename = data;

	g_return_if_fail (msg);

	debug ("livejournal parse_getpicture_request()");

	if (!dc->config_directory)
	{
		g_warning ("You do not have a valid config directory.");
		return;
	}

	path = g_build_filename (dc->config_directory, "pictures", 
				 filename, NULL);
	result = gnome_vfs_create (&handle, path, GNOME_VFS_OPEN_WRITE, FALSE,
				   GNOME_VFS_PERM_USER_READ | GNOME_VFS_PERM_USER_WRITE);
	g_free (path);
	g_free (filename);

	if (result != GNOME_VFS_OK)
	{
		g_warning ("%s", gnome_vfs_result_to_string (result));
		return;
	}
	
	gnome_vfs_write (handle, msg->response.body, msg->response.length, NULL);
	gnome_vfs_close (handle);
	
	fill_picture_menu (dc, dc->picture_store);
	gtk_combo_box_set_active (GTK_COMBO_BOX (dc->journal_picture),
				  gconf_client_get_int (dc->client, dc->gconf->default_picture, NULL));

	return;
}

DrivelRequest*
blog_lj_build_getpicture_request (const gchar *url, gchar *filename)
{
	SoupMessage *msg;

	debug ("livejournal build_getpicture_request");

	msg = soup_message_new (SOUP_METHOD_GET, url);

	g_signal_connect (G_OBJECT (msg), "finished",
			  G_CALLBACK (parse_getpicture_request), filename);

	net_enqueue_msg (msg);

	return NULL;
}

DrivelRequest*
blog_lj_build_checkfriends_request (const gchar *username, const gchar *uri,
		const gchar* lastupdate)
{
	debug ("livejournal build_checkfriends_request()");
/* 	DrivelRequest *dr; */
	
/* 	dr = drivel_request_new_with_items (REQUEST_TYPE_CHECKFRIENDS,  */
/* 			REQUEST_PROTOCOL_POST,  */
/* 			BLOG_API_LJ, */
/* 			uri, */
/* 			g_strdup ("mode"), g_strdup ("checkfriends"), */
/* 			g_strdup ("user"), curl_escape (username, 0), */
/* 			g_strdup ("ver"), g_strdup ("1"), */
/* 			g_strdup ("lastupdate"), curl_escape (lastupdate, 0), */
/* 			NULL); */
	
/* 	return dr; */
	return NULL;
}

void
blog_lj_parse_checkfriends_request (DrivelClient *dc, DrivelRequest *dr)
{
	const gchar *mesg;
	
	debug ("livejournal parse_checkfriends_request()");

	dc->checking_friends = FALSE;
	
	mesg = drivel_request_value_lookup (dr, "success");
	if (!mesg || !strcmp (mesg, "FAIL"))
		return;
	
	mesg = drivel_request_value_lookup (dr, "lastupdate");
	if (mesg)
	{
		g_free (dc->lastupdate);
		dc->lastupdate = g_strdup (mesg);
	}
	
	mesg = drivel_request_value_lookup (dr, "new");
	if (mesg)
	{
		if ((gint) g_strtod (mesg, NULL))
		{
			dc->friends_update = TRUE;
			tray_display (TRUE);
		}
		else
			dc->friends_update = FALSE;
	}
	
	mesg = drivel_request_value_lookup (dr, "interval");
	if (mesg)
		dc->time_since_checkfriends = (gint) g_strtod (mesg, NULL);
	
	return;
}

static void
parse_postevent_request (SoupMessage *msg, gpointer data)
{
	SoupXmlrpcResponse *response;

	g_return_if_fail (msg);

	debug ("livejournal parse_postevent_request()");

	if (!(response = net_msg_get_response (SOUP_XMLRPC_MESSAGE (msg))))
		return;

	/* FIXME: ping technorati */
	journal_finished_post (dc);

	g_object_unref (response);

	return;
}

DrivelRequest*
blog_lj_build_postevent_request (const gchar *username, const gchar *uri,
				 const gchar *event, const gchar *music, const gchar *mood,
				 gint moodid, const gchar *subject, const gchar *security, gint mask,
				 const gchar *picture, gint year, gint month, gint day, gint hour,
				 gint minute, gint nocomments, gint preformatted, 
				 const DrivelJournal *dj, gint backdate)
{
	SoupXmlrpcMessage *msg;

	debug ("livejournal build_postevent_request()");

	msg = xmlrpc_start_with_struct ("LJ.XMLRPC.postevent", uri);
	xmlrpc_add_string_member (msg, "username", username);
	xmlrpc_add_int_member (msg, "ver", 1);
	xmlrpc_add_string_member (msg, "event", event);
	xmlrpc_add_string_member (msg, "lineendings", "unix");
	xmlrpc_add_string_member (msg, "subject", subject);
	xmlrpc_add_string_member (msg, "security", security);
	xmlrpc_add_int_member (msg, "allowmask", mask);
	xmlrpc_start_array_member (msg, "props");
	xmlrpc_start_struct_param (msg);
	xmlrpc_add_string_member (msg, "current_music", music);
	xmlrpc_add_string_member (msg, "current_mood", mood);
	xmlrpc_add_int_member (msg, "current_moodid", moodid);
	xmlrpc_add_bool_member (msg, "opt_preformatted", (gboolean)preformatted);
	xmlrpc_add_bool_member (msg, "opt_nocomments", (gboolean)nocomments);
	xmlrpc_add_string_member (msg, "picture_keyword", picture);
	xmlrpc_add_bool_member (msg, "opt_backdated", (gboolean)backdate);
	xmlrpc_end_struct_param (msg);
	xmlrpc_end_array_member (msg);
	xmlrpc_add_int_member (msg, "year", year);
	xmlrpc_add_int_member (msg, "mon", month);
	xmlrpc_add_int_member (msg, "day", day);
	xmlrpc_add_int_member (msg, "hour", hour);
	xmlrpc_add_int_member (msg, "min", minute);
	if (dj && dj->type == JOURNAL_TYPE_COMMUNITY)
		xmlrpc_add_string_member (msg, "usejournal", dj->name);

	g_signal_connect (G_OBJECT (msg), "finished",
			  G_CALLBACK (parse_postevent_request), NULL);

	send_getchallenge_request (uri, msg);

	return NULL;
}

DrivelRequest*
blog_lj_build_editevent_request (const gchar *username, const gchar *uri,
				 const gchar *itemid, const gchar *event, const gchar *music, 
				 const gchar *mood, gint moodid, const gchar *subject, 
				 const gchar *security, gint mask, const gchar *picture, gint year, 
				 gint month, gint day, gboolean newdate, gint nocomments, 
				 gint preformatted, const DrivelJournal *dj)
{
	SoupXmlrpcMessage *msg;
	glong itemid_int;

	debug ("livejournal build_editevent_request()");

	itemid_int = (glong) g_ascii_strtod (itemid, NULL);
	msg = xmlrpc_start_with_struct ("LJ.XMLRPC.editevent", uri);
	xmlrpc_add_string_member (msg, "username", username);
	xmlrpc_add_int_member (msg, "ver", 1);
	xmlrpc_add_int_member (msg, "itemid", itemid_int);
	xmlrpc_add_string_member (msg, "event", event);
	xmlrpc_add_string_member (msg, "lineendings", "unix");
	xmlrpc_add_string_member (msg, "subject", subject);
	xmlrpc_add_string_member (msg, "security", security);
	xmlrpc_add_int_member (msg, "allowmask", mask);
	xmlrpc_start_struct_member (msg, "props");
	xmlrpc_add_string_member (msg, "current_music", music);
	xmlrpc_add_string_member (msg, "current_mood", mood);
	xmlrpc_add_int_member (msg, "current_moodid", moodid);
	xmlrpc_add_bool_member (msg, "opt_preformatted", (gboolean)preformatted);
	xmlrpc_add_bool_member (msg, "opt_nocomments", (gboolean)nocomments);
	xmlrpc_add_string_member (msg, "picture_keyword", picture);
	xmlrpc_end_struct_member (msg);
	if (newdate)
	{
		xmlrpc_add_int_member (msg, "year", year);
		xmlrpc_add_int_member (msg, "mon", month);
		xmlrpc_add_int_member (msg, "day", day);
	}
	if (dj && dj->type == JOURNAL_TYPE_COMMUNITY)
		xmlrpc_add_string_member (msg, "usejournal", dj->name);

	g_signal_connect (G_OBJECT (msg), "finished",
			  G_CALLBACK (parse_postevent_request), NULL);

	send_getchallenge_request (uri, msg);

	return NULL;
}

static void
parse_getevents_request (SoupMessage *msg, gpointer data)
{
	SoupXmlrpcResponse *response;
	SoupXmlrpcValue *value;
	GHashTable *table;

	g_return_if_fail (msg);

	debug ("livejournal parse_getevents_request()");

	if (!(response = net_msg_get_response (SOUP_XMLRPC_MESSAGE (msg))))
		return;

	value = soup_xmlrpc_response_get_value (response);
	soup_xmlrpc_value_get_struct (value, &table);

	value = g_hash_table_lookup (table, "events");

	if (value)
	{
		SoupXmlrpcValueArrayIterator *iter;
		SoupXmlrpcValue *array_value;
		gint events_count, i;
		gchar **itemid;
		gchar **event, **security, **allowmask, **subject;
		gchar **eventtime, **comments = NULL, **autoformat = NULL;
		gchar **mood = NULL, **music = NULL, **picture = NULL;

		/* Count the number of entries */
		events_count = 0;
		soup_xmlrpc_value_array_get_iterator (value, &iter);
		while (soup_xmlrpc_value_array_iterator_get_value (iter, &array_value) && array_value)
		{
			events_count++;
			iter = soup_xmlrpc_value_array_iterator_next (iter);
		}

		itemid = g_new (gchar *, events_count + 1);
		event = g_new (gchar *, events_count + 1);
		security = g_new (gchar *, events_count + 1);
		allowmask = g_new (gchar *, events_count + 1);
		subject = g_new (gchar *, events_count + 1);
		eventtime = g_new (gchar *, events_count + 1);
		picture = g_new (gchar *, events_count + 1);
		mood = g_new (gchar *, events_count + 1);
		music = g_new (gchar *, events_count + 1);
		comments = g_new (gchar *, events_count + 1);
		autoformat = g_new (gchar *, events_count + 1);

		for (i = events_count; i > -1; i--)
		{
			itemid [i] = NULL;
			event [i] = NULL;
			security [i] = NULL;
			allowmask [i] = NULL;
			subject [i] = NULL;
			eventtime [i] = NULL;
			picture [i] = NULL;
			mood [i] = NULL;
			music [i] = NULL;
			comments [i] = NULL;
			autoformat [i] = NULL;
		}
		i++;

		soup_xmlrpc_value_array_get_iterator (value, &iter);
		while (soup_xmlrpc_value_array_iterator_get_value (iter, &value) && value)
		{
			GHashTable *table;
			glong val;

			soup_xmlrpc_value_get_struct (value, &table);

			if ((value = g_hash_table_lookup (table, "itemid")))
			{
				soup_xmlrpc_value_get_int (value, &val);
				itemid[i] = g_strdup_printf ("%ld", val);
			}
			if ((value = g_hash_table_lookup (table, "eventtime")))
				soup_xmlrpc_value_get_string (value, &eventtime[i]);
			if ((value = g_hash_table_lookup (table, "security")))
				soup_xmlrpc_value_get_string (value, &security[i]);
			if ((value = g_hash_table_lookup (table, "allowmask")))
			{
				soup_xmlrpc_value_get_int (value, &val);
				allowmask[i] = g_strdup_printf ("%ld", val);
			}
			if ((value = g_hash_table_lookup (table, "subject")))
			{
				soup_xmlrpc_value_get_string (value, &subject[i]);
			}
			if ((value = g_hash_table_lookup (table, "event")))
			{
				soup_xmlrpc_value_get_string (value, &event[i]);
			}
			if ((value = g_hash_table_lookup (table, "props")))
			{
				GHashTable *table;
				gboolean val;

				soup_xmlrpc_value_get_struct (value, &table);
				if ((value = g_hash_table_lookup (table, "picture_keyword")))
					soup_xmlrpc_value_get_string (value, &picture[i]);
				if ((value = g_hash_table_lookup (table, "current_mood")))
					soup_xmlrpc_value_get_string (value, &mood[i]);
				if ((value = g_hash_table_lookup (table, "current_music")))
					soup_xmlrpc_value_get_string (value, &music[i]);
				if ((value = g_hash_table_lookup (table, "opt_nocomments")))
				{
					soup_xmlrpc_value_get_boolean (value, &val);
					comments[i] = g_strdup_printf ("%d", (gint)val);
				}
				if ((value = g_hash_table_lookup (table, "opt_preformatted")))
				{
					soup_xmlrpc_value_get_boolean (value, &val);
					autoformat[i] = g_strdup_printf ("%d", (gint)val);
				}

				g_hash_table_destroy (table);
			}

			g_hash_table_destroy (table);
			i++;
			iter = soup_xmlrpc_value_array_iterator_next (iter);
		}

		if (g_object_get_data (G_OBJECT (msg), "lastn"))
		{
			for (i = 0; i < events_count; i++)
			{
				DrivelJournalProp *prop ;
				DrivelJournalEntry *entry = journal_entry_new ();
				
				entry->content = g_strdup (event[i]);
				if (subject[i])
					entry->subject = g_strdup (subject[i]);
				entry->userid = g_strdup (g_object_get_data (G_OBJECT (msg), "user"));
				entry->postid = g_strdup (itemid[i]);
				entry->security = g_strdup (security[i]);
				entry->security_mask = g_strdup (allowmask[i]);
				if (picture[i])
				{
					prop = journal_prop_new ();
					prop->name = g_strdup ("picture");
					prop->value = g_strdup (picture[i]);
					g_array_append_val (entry->properties, prop);
				}
				if (music[i])
				{
					prop = journal_prop_new ();
					prop->name = g_strdup ("music");
					prop->value = g_strdup (music[i]);
					g_array_append_val (entry->properties, prop);
				}
				if (mood[i])
				{
					prop = journal_prop_new ();
					prop->name = g_strdup ("mood");
					prop->value = g_strdup (mood[i]);
					g_array_append_val (entry->properties, prop);
				}
				if (comments[i])
				{
					prop = journal_prop_new ();
					prop->name = g_strdup ("comments");
					prop->value = g_strdup (comments[i]);
					g_array_append_val (entry->properties, prop);
				}
				if (autoformat[i])
				{
					prop = journal_prop_new ();
					prop->name = g_strdup ("autoformat");
					prop->value = g_strdup (autoformat[i]);
					g_array_append_val (entry->properties, prop);
				}
				prop = journal_prop_new ();
				prop->name = g_strdup ("eventtime");
				prop->value = g_strdup (eventtime[i]);
				g_array_append_val (entry->properties, prop);
				
				g_array_append_val (dc->recent_entries, entry);
			}
			
			journal_refresh_recent_entries (dc);
		}
		else if (dc->edit_entry)
		{
			journal_edit_entry (dc, itemid [0], event [0], security [0],
					    allowmask [0], subject [0],	mood [0], music [0], picture [0],
					    eventtime [0], comments [0], autoformat [0], NULL);
		}
		else
		{
			update_history_list (dc, itemid, event, eventtime, events_count);
		}
		
		g_strfreev (itemid);
		g_strfreev (event);
		g_strfreev (security);
		g_strfreev (allowmask);
		g_strfreev (subject);
		g_strfreev (eventtime);
		g_strfreev (picture);
		g_strfreev (mood);
		g_strfreev (music);
		g_strfreev (comments);
		g_strfreev (autoformat);
	}

	g_hash_table_destroy (table);
	g_object_unref (response);

	return;
}

DrivelRequest*
blog_lj_build_getevents_request (const gchar *username, const gchar *uri,
				 gint truncate, gboolean prefersubject, gboolean noprops, 
				 const gchar *selecttype, const gchar *lastsync, gint year, gint month, 
				 gint day, gint howmany, const gchar *beforedate, gint itemid, 
				 const DrivelJournal *dj)
{
	SoupXmlrpcMessage *msg;

	debug ("livejournal build_getevents_request()");

	msg = xmlrpc_start_with_struct ("LJ.XMLRPC.getevents", uri);
	xmlrpc_add_string_member (msg, "username", username);
	xmlrpc_add_int_member (msg, "ver", 1);
	xmlrpc_add_string_member (msg, "lineendings", "unix");
	xmlrpc_add_int_member (msg, "truncate", truncate);
	xmlrpc_add_int_member (msg, "prefersubject", (gint)prefersubject);
	xmlrpc_add_int_member (msg, "noprops", (gint)noprops);
	xmlrpc_add_string_member (msg, "selecttype", selecttype);

	if (!strncmp (selecttype, "day", strlen ("day")))
	{
		xmlrpc_add_int_member (msg, "year", year);
		xmlrpc_add_int_member (msg, "month", month);
		xmlrpc_add_int_member (msg, "day", day + 1);
	}
	else if (!strncmp (selecttype, "lastn", strlen ("lastn")))
	{
		xmlrpc_add_int_member (msg, "howmany", howmany);
		if (beforedate)
		{
			xmlrpc_add_string_member (msg, "beforedate", beforedate);
		}
		g_object_set_data_full (G_OBJECT (msg), "lastn", g_strdup ("true"), g_free);
	}
	else if (!strncmp (selecttype, "one", strlen ("one")))
	{
		xmlrpc_add_int_member (msg, "itemid", itemid);
	}
	else if (!strncmp (selecttype, "syncitems", strlen ("syncitems")))
	{
		xmlrpc_add_string_member (msg, "lasysync", lastsync);
	}

	if (dj && dj->type == JOURNAL_TYPE_COMMUNITY)
		xmlrpc_add_string_member (msg, "usejournal", dj->name);
	
	g_object_set_data_full (G_OBJECT (msg), "user", g_strdup (username), g_free);

	g_signal_connect (G_OBJECT (msg), "finished",
			  G_CALLBACK (parse_getevents_request), NULL);

	send_getchallenge_request (uri, msg);

	return NULL;
}

static void
parse_getdaycounts_request (SoupMessage *msg, gpointer data)
{
	SoupXmlrpcResponse *response;
	SoupXmlrpcValue *value;
	GHashTable *table;

	g_return_if_fail (msg);

	debug ("livejournal parse_getdaycounts_request()");

	if (!(response = net_msg_get_response (SOUP_XMLRPC_MESSAGE (msg))))
		return;

	value = soup_xmlrpc_response_get_value (response);
	soup_xmlrpc_value_get_struct (value, &table);

	value = g_hash_table_lookup (table, "daycounts");
	
	if (value)
	{
		SoupXmlrpcValueArrayIterator *iter;
		guint year, month, day, i;
		gchar range[8];
		gint8 days [31];

		for (i = 0; i < 31; i++)
			days[i] = 0;
		gtk_calendar_get_date (GTK_CALENDAR (dc->history_calendar),
				       &year, &month, &day);
		g_snprintf (range, 8, "%04d-%02d", year, month + 1);
		
		soup_xmlrpc_value_array_get_iterator (value, &iter);

		while (soup_xmlrpc_value_array_iterator_get_value (iter, &value) && value)
		{
			GHashTable *table;
			glong count;
			gchar *date = NULL;

			soup_xmlrpc_value_get_struct (value, &table);

			if ((value = g_hash_table_lookup (table, "date")))
				soup_xmlrpc_value_get_string (value, &date);
			if ((value = g_hash_table_lookup (table, "count")))
				soup_xmlrpc_value_get_int (value, &count);
			
			if (!strncmp (range, date, 7))
			{
				sscanf (date, "%d-%d-%d", &year, &month, &day);
				days[day] = count;
			}

			g_free (date);
			g_hash_table_destroy (table);

			iter = soup_xmlrpc_value_array_iterator_next (iter);
		}

		update_history_marks (dc, days);
	}

	g_hash_table_destroy (table);
	g_object_unref (response);

	return;
}

DrivelRequest*
blog_lj_build_getdaycounts_request (const gchar *username, const gchar *uri,
				    const DrivelJournal *dj)
{
	SoupXmlrpcMessage *msg;

	debug ("livejournal build_getdaycounts_request()");

	msg = xmlrpc_start_with_struct ("LJ.XMLRPC.getdaycounts", uri);
	xmlrpc_add_string_member (msg, "username", username);
	xmlrpc_add_int_member (msg, "ver", 1);
	if (dj && dj->type == JOURNAL_TYPE_COMMUNITY)
		xmlrpc_add_string_member (msg, "usejournal", dj->name);
	
	g_signal_connect (G_OBJECT (msg), "finished",
			  G_CALLBACK (parse_getdaycounts_request), NULL);

	send_getchallenge_request (uri, msg);

	return NULL;
}

static void
parse_editfriends_request (SoupMessage *msg, gpointer data)
{
	SoupXmlrpcResponse *response;

	g_return_if_fail (msg);

	debug ("livejournal parse_editfriends_request()");

	if (!(response = net_msg_get_response (SOUP_XMLRPC_MESSAGE (msg))))
		return;

	/* fetch recent entries for our journal */
	blog_lj_build_getfriends_request (dc->user->username,
					  dc->user->server,
					  TRUE, FALSE);

	g_object_unref (response);
	
	return;
}

DrivelRequest*
blog_lj_build_editfriends_request (const gchar *username, const gchar *uri,
				   const gchar *friend, gboolean delete, 
				   gboolean new, const gchar *fg, const gchar *bg)
{
	SoupXmlrpcMessage *msg;

	debug ("livejournal build_editfriends_request()");

	msg = xmlrpc_start_with_struct ("LJ.XMLRPC.editfriends", uri);
	xmlrpc_add_string_member (msg, "username", username);
	xmlrpc_add_int_member (msg, "ver", 1);
	if (delete || !new)
	{
		xmlrpc_start_array_member (msg, "delete");
		soup_xmlrpc_message_write_string (msg, friend);
		xmlrpc_end_array_member (msg);
	}
	if (!delete)
	{
		xmlrpc_start_array_member (msg, "add");
		soup_xmlrpc_message_start_struct (msg);
		xmlrpc_add_string_member (msg, "username", friend);
		if (fg)
			xmlrpc_add_string_member (msg, "fgcolor", fg);
		if (bg)
			xmlrpc_add_string_member (msg, "bgcolor", bg);
		soup_xmlrpc_message_end_struct (msg);
		xmlrpc_end_array_member (msg);
	}
	
	g_signal_connect (G_OBJECT (msg), "finished",
			  G_CALLBACK (parse_editfriends_request), NULL);

	send_getchallenge_request (uri, msg);

	return NULL;
}

static void
parse_getfriends_request (SoupMessage *msg, gpointer data)
{
	SoupXmlrpcResponse *response;
	SoupXmlrpcValue *value;
	GHashTable *table;

	g_return_if_fail (msg);

	debug ("livejournal parse_getfriends_request()");

	if (dc->friends_list)
	{
		g_slist_foreach (dc->friends_list, friends_list_free_item, NULL);
		g_slist_free (dc->friends_list);
		dc->friends_list = NULL;
	}

	if (!(response = net_msg_get_response (SOUP_XMLRPC_MESSAGE (msg))))
		return;

	value = soup_xmlrpc_response_get_value (response);
	soup_xmlrpc_value_get_struct (value, &table);

	value = g_hash_table_lookup (table, "friends");
	if (value)
	{
		SoupXmlrpcValueArrayIterator *iter;
		
		soup_xmlrpc_value_array_get_iterator (value, &iter);
		while (soup_xmlrpc_value_array_iterator_get_value (iter, &value) && value)
		{
			LJFriend *friend;
			GHashTable *table;
			gchar *type;

			friend = g_new0 (LJFriend, 1);
		
			soup_xmlrpc_value_get_struct (value, &table);
			
			value = g_hash_table_lookup (table, "username");
			soup_xmlrpc_value_get_string (value, &friend->username);
			value = g_hash_table_lookup (table, "fullname");
			soup_xmlrpc_value_get_string (value, &friend->name);
			value = g_hash_table_lookup (table, "fgcolor");
			soup_xmlrpc_value_get_string (value, &friend->fg);
			value = g_hash_table_lookup (table, "bgcolor");
			soup_xmlrpc_value_get_string (value, &friend->bg);
			value = g_hash_table_lookup (table, "groupmask");
			if (value)
			{
				glong mask;
				soup_xmlrpc_value_get_int (value, &mask);
				friend->groupmask = mask;
			}
			else
				friend->groupmask = 0;
			value = g_hash_table_lookup (table, "type");
			friend->type = FRIEND_TYPE_USER;
			if (value)
			{
				soup_xmlrpc_value_get_string (value, &type);
				if (!strcmp (type, "community"))
					friend->type = FRIEND_TYPE_COMMUNITY;
				else if (!strcmp (type, "syndicated"))
					friend->type = FRIEND_TYPE_FEED;
			}
			friend->friend = TRUE;
			friend->friend_of = FALSE;

			g_hash_table_destroy (table);
			dc->friends_list = g_slist_prepend (dc->friends_list, friend);
			iter = soup_xmlrpc_value_array_iterator_next (iter);
		}
	}

	value = g_hash_table_lookup (table, "friendofs");
	if (value)
	{
		SoupXmlrpcValueArrayIterator *iter;

		soup_xmlrpc_value_array_get_iterator (value, &iter);
		while (soup_xmlrpc_value_array_iterator_get_value (iter, &value) && value)
		{
			LJFriend *friend;
			GHashTable *table;
			GSList *list_item;
			gchar *type, *username;

			soup_xmlrpc_value_get_struct (value, &table);

			value = g_hash_table_lookup (table, "username");
			soup_xmlrpc_value_get_string (value, &username);

			list_item = g_slist_find_custom (dc->friends_list, username, compare_usernames);
			if (list_item)
			{
				friend = list_item->data;
				friend->friend_of = TRUE;
				g_free (username);
			}
			else
			{
				friend = g_new0 (LJFriend, 1);
				friend->username = username;

				value = g_hash_table_lookup (table, "fullname");
				soup_xmlrpc_value_get_string (value, &friend->name);
				value = g_hash_table_lookup (table, "fgcolor");
				soup_xmlrpc_value_get_string (value, &friend->fg);
				value = g_hash_table_lookup (table, "bgcolor");
				soup_xmlrpc_value_get_string (value, &friend->bg);
				value = g_hash_table_lookup (table, "groupmask");
				if (value)
				{
					glong mask;
					soup_xmlrpc_value_get_int (value, &mask);
					friend->groupmask = mask;
				}
				else
					friend->groupmask = 0;
				value = g_hash_table_lookup (table, "type");
				friend->type = FRIEND_TYPE_USER;
				if (value)
				{
					soup_xmlrpc_value_get_string (value, &type);
					if (!strcmp (type, "community"))
						friend->type = FRIEND_TYPE_COMMUNITY;
					else if (!strcmp (type, "syndicated"))
						friend->type = FRIEND_TYPE_FEED;
				}
				friend->friend = FALSE;
				friend->friend_of = TRUE;

				dc->friends_list = g_slist_prepend (dc->friends_list, friend);
			}

			g_hash_table_destroy (table);
			iter = soup_xmlrpc_value_array_iterator_next (iter);
		}
	}

	g_hash_table_destroy (table);
	g_object_unref (response);

	/* FIXME: what's up with the security dialog? */
	if (FALSE)
	{
/* 		/\* fetch the user's security groups *\/ */
/* 		gtk_list_store_clear (dc->security_store_filtered); */
/* /\*		parse_friendgroup_data (dr, dc->security_store_filtered);*\/ */
/* 		display_security_dialog (dc); */
	}
	else
		display_edit_friends_dialog (dc);

	return;
}

DrivelRequest*
blog_lj_build_getfriends_request (const gchar *username, const gchar *uri, 
				  gboolean friendof, gboolean groups)
{
	SoupXmlrpcMessage *msg;

	debug ("livejournal build_getfriends_request()");

	msg = xmlrpc_start_with_struct ("LJ.XMLRPC.getfriends", uri);
	xmlrpc_add_string_member (msg, "username", username);
	xmlrpc_add_int_member (msg, "ver", 1);
	xmlrpc_add_int_member (msg, "includefriendof", (gint)friendof);
	xmlrpc_add_int_member (msg, "includegroups", (gint)groups);
	
	g_signal_connect (G_OBJECT (msg), "finished",
			  G_CALLBACK (parse_getfriends_request), NULL);

	send_getchallenge_request (uri, msg);

	return NULL;
}
