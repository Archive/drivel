/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * Copyright 2002 Todd Kulesza
 *
 * Authors:
 * 		Todd Kulesza <todd@dropline.net>
 */

#ifndef _LOGIN_H_
#define _LOGIN_H_

#include <gnome.h>

#include "drivel.h"

void
about_cb(GtkWidget *widget, gpointer data);

void
login_window_build (DrivelClient *dc);

void
set_user_login_prefs (DrivelClient *dc);

DrivelUser*
get_drivel_user (DrivelClient *dc);

void
user_list_changed (DrivelClient *dc);

#endif
