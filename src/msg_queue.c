/*
 *  Copyright 2004 Todd Kulesza <todd@dropline.net>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <config.h>

#include "ephy-spinner.h"
#include "blog_advogato.h"
#include "blog_atom.h"
#include "blog_blogger.h"
#include "blog_lj.h"
#include "blog_mt.h"
#include "drivel.h"
#include "msg_queue.h"

#include <gtk/gtk.h>
#include <glade/glade.h>
#include <libgnomevfs/gnome-vfs-cancellation.h>

/* Cancel the transaction if the user clicks "Cancel" or closes the progress
   dialog. */

static void
progress_response (GtkWidget *w, gint response, GnomeVFSCancellation *cancel)
{
	gnome_vfs_cancellation_cancel (cancel);
	
	return;
}

/* Stop the delete_event from closing the window--we'll do it ourselves after
   cancelling the network operation. */

static gboolean
stop_delete_event_cb (GtkWidget *w, GdkEvent *e, gpointer data)
{
	return TRUE;
}

/* Build a progress dialog that can be used for any network transactions */

static GtkWidget*
build_progress_dialog (GtkWidget *parent, const gchar *msg, GnomeVFSCancellation *cancel)
{
	GtkWidget *dialog;
	GtkWidget *spinner, *label, *progress;
	GladeXML *xml;
	
	xml = load_glade_xml ("progress_dialog");
	if (!xml)
		return NULL;
	
	dialog = glade_xml_get_widget (xml, "progress_dialog");
	gtk_window_set_transient_for (GTK_WINDOW (dialog), GTK_WINDOW (parent));
	
	spinner = glade_xml_get_widget (xml, "progress_spinner");
	ephy_spinner_start (EPHY_SPINNER (spinner));
	
	label = glade_xml_get_widget (xml, "progress_label");
	gtk_label_set_markup (GTK_LABEL (label), msg);
	
	progress = glade_xml_get_widget (xml, "progress_bar");

	/* add 'label' and 'progress' data types to 'dialog' for use by msg-queue.c */
	g_object_set_data (G_OBJECT (dialog), "label", label);
	g_object_set_data (G_OBJECT (dialog), "progress", progress);
	
	g_signal_connect (G_OBJECT (dialog), "response",
			G_CALLBACK (progress_response), cancel);
	g_signal_connect (G_OBJECT (dialog), "delete_event",
			G_CALLBACK (stop_delete_event_cb), NULL);
	
	gtk_widget_show_all (dialog);
	
	return dialog;
}
	
static void
update_progress_bar (GtkWidget *dialog, gdouble percent)
{
	GtkWidget *progress;
	
	progress = g_object_get_data (G_OBJECT (dialog), "progress");
	
	gtk_progress_bar_set_fraction (GTK_PROGRESS_BAR (progress), percent);
	
	return;
}

static void
update_progress_label (GtkWidget *dialog, const gchar *msg)
{
	GtkWidget *label;
	
	label = g_object_get_data (G_OBJECT (dialog), "label");
	
	gtk_label_set_markup (GTK_LABEL (label), msg);
	
	return;
}

/* This function is called via a timeout loop and processes any events waiting
 * in 'queue'.
 */

static gboolean
queue_loop (GAsyncQueue *queue)
{
	MsgInfo *info;
	static gpointer active_widget = NULL;
	static gint dialog_count = 0, dialogs_processed = 0;
	
	do
	{
		info = g_async_queue_try_pop (queue);
		if (info)
		{
			switch (info->type)
			{
				case MSG_TYPE_SET_WIDGET:
				{
					g_assert (info->widget);
					g_assert (GTK_IS_WIDGET (info->widget));
					active_widget = info->widget;
					break;
				}
				case MSG_TYPE_ERROR:
				{
					g_assert (info->dc);
					display_error_dialog ((DrivelClient *)info->dc, 
							info->header, info->msg);
					break;
				}
				case MSG_TYPE_UPDATE_PROGRESS_PERCENT:
				{
					if (active_widget && GTK_IS_WIDGET (active_widget))
					{
						gdouble progress;
						
						progress = (info->progress + (gdouble)dialogs_processed) /
							(gdouble)(dialog_count + dialogs_processed);
						
						update_progress_bar (GTK_WIDGET (active_widget), progress);
					}
					break;
				}
				case MSG_TYPE_UPDATE_PROGRESS_LABEL:
				{
					if (!active_widget || !GTK_IS_WIDGET (active_widget))
						break;
					update_progress_label (GTK_WIDGET (active_widget), info->msg);
					break;
				}
				case MSG_TYPE_BUILD_PROGRESS:
				{
					if (!dialog_count)
					{
						DrivelClient *dc;
						g_assert (info->dc);
						dc = (DrivelClient *)info->dc;
						active_widget = build_progress_dialog (dc->current_window,
								info->msg, dc->net->cancel);
					}
					dialog_count++;
					break;
				}
				case MSG_TYPE_REPARENT_DIALOG:
				{
					DrivelClient *dc;
					g_assert (info->dc);
					dc = (DrivelClient *)info->dc;
					if (dc->current_window)
					{
						gtk_window_set_transient_for (
								GTK_WINDOW (active_widget),
								GTK_WINDOW (dc->current_window));
					}
					break;
				}
				case MSG_TYPE_DONE:
				{
					if (!active_widget || !GTK_IS_WIDGET (active_widget))
						break;
					dialog_count--;
					if (!dialog_count)
					{
						gtk_widget_destroy (GTK_WIDGET (active_widget));
						active_widget = NULL;
						dialogs_processed = 0;
					}
					else
						dialogs_processed++;
					
					break;
				}
				default: break;
			}
			msg_info_free (info);
		}
	} while (info);
	
	return TRUE;
}

/* Create a new GAsyncQueue and setup a timeout function to check the
 * queue for new events.
 */

GAsyncQueue*
msg_queue_setup (void)
{
	GAsyncQueue *queue;
	
	queue = g_async_queue_new ();
	
	g_timeout_add (333, (GSourceFunc) queue_loop, queue);
	
	return queue;
}

/* Allocate memory for a new MsgInfo structure. */

MsgInfo*
msg_info_new (void)
{
	MsgInfo *info;
	
	info = g_new0 (MsgInfo, 1);
	info->widget = NULL;
	info->msg = NULL;
	info->header = NULL;
	info->progress = 0.0;
	info->dr = NULL;
	info->dc = NULL;
	
	return info;
}

/* Free the memory used by a MsgInfo structure. */

void
msg_info_free (MsgInfo *info)
{
	if (info->dr)
		drivel_request_free ((DrivelRequest *)info->dr);
	g_free (info->msg);
	g_free (info->header);
	g_free (info);
	
	return;
}
