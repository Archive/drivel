Drivel 2.0.3 ("Regret Reminds me Anyway")
=========================================

  * Improvements:
  - Added a View Journal button to the LiveJournal Friends dialog
  - Refresh Recent Entries menu after posting or updating an entry
  - Use D-BUS to detect current music

  * Fixes:
  - Correctly fetch LiveJournal attributes
  - Compilation fixes for OpenBSD and GCC 2.95
  - Expand username compatibility
  - Fix several double mnemonics
  - Improve XML-RPC compatibility
  - Support HTTP redirections
  - Resolve problem opening draft entries
  - Numerous fixes for Atom/Blogger 2.0

  * Translations:
  - Added Nepali translation (Pawan Chitrakar)
  - Added Lithuanian translation (Žygimantas Beručka)
  - Added Catalan translation (Gil Forcada)
  - Added Vietnamese translation (Clytie Siddall)
  - Added Finnish translation (Ilkka Tuohela)
  - Added French translation (Jeff Coquery)
  - Added Russian translation (Valek Filippov)
  - Updated Czech translation (Miloslav Trmac)
  - Updated German translation (Frank Arnold)
  - Updated Dutch translation (Vincent van Adrighem)
  - Updated Simplified Chinese translation (Funda Wang)
  - Updated Bulgarian translation (Yavor Doganov)
  - Updated Swedish translation (Daniel Nylander)
  - Updated Spanish translation (Francisco Javier F. Serrador)

Drivel 2.0.2 ("The Lost Release")
=================================

  * Improvements:
  ?

  * Fixes:
  ?

  * Translations:
  ?

Drivel 2.0.1 ("Tea and Health")
===============================

  * Improvements:
  - Add a FAQ section to the documentation (Todd)
  
  * Fixes:
  - Syntax fix for .desktop file (Jeremy)
  - Allow usernames with more than fifteen characters (Jess)
  - Fix default HTML highlighting state (Todd)
  - Allow usernames with hyphens (Todd)
  - Numerous UTF-8 fixes for Blogger, MovableType, and Atom (Todd)
  - Fix placement of Refresh button in Recent Entries menu (Todd)
  - Handle HTML in Blogger titles gracefully (Todd)
  
  * Translations:
  - Updated Spanish translation (Francisco Javier F. Serrador)
  - Updated Czech translation (Miloslav Trmac).

Drivel 2.0 ("Psychoactive Kitty")
=================================

  * Improvements since 1.2:
  - Support for Atom, Blogger, MovableType, and Advogato weblogs
  - Offline mode
  - Fetch current music from Beep Media Player
  - Shortcut key (F7) to enabled/disable spellcheck
  - MIME support for draft entries
  - Allow selection of spellchecking language
  - Recent Entries menu to edit the previous fifteen posts
  - Support LiveJournal security groups
  - Ability to ping Technorati after journal updates
  
Drivel 1.3.91 ("Whoop it up")
=============================

  * Improvements:
  - Clarify the Blogger API wording (Todd).
  - New artwork (Todd).
  - Updated documentation (Todd).
  
  * Fixes:
  - Handle IPv6 proxy servers correctly (Todd).
  - Respect the GConf key for proxy authentication (Todd).
  
  * Translations:
  - Updated Czech translation (Miloslav Trmac).
  - Updated Canadian English translation (Adam Weinberger).
  - Updated Swedish translation (Christian Rose).
  - Added Bulgarian translation (Yavor Doganov).
  - Updated German translation (Frank Arnold).
  - Updated Spanish tranlation (Francisco Javier F. Serrador).
  
Drivel 1.3.90 (The "I'm out!" release)
=====================================

  * Improvements:
  - Support fetching, editing, and deleting recent MovableType entries (Todd).
  - Build a Recent Entries menu for MovableType (Todd).
  - Handle MovableType categories from all journals (Todd).
  - Alphabetize list of journal types and categories (Todd).
  - Option to ping Technorati after posting (Todd).
  - New splash screen (Margot).
  - Support LiveJournal security groups (Todd).
  
  * Fixes:
  - Shortcut key fixes (Todd).
 
  * Translations:
  - Updated Czech translation (Miloslav Trmac).
  - Added Greek translation (Kostas Papadimas).
  - Updated Spanish translation (Francisco Javier F. Serrador).
  - Updated Canadian English translation (Adam Weinberger).
  - Updated German translation (Frank Arnold).

Drivel 1.3.4 (The "Strawberry Shortcake!" release)
==================================================

  * Improvements:
  - Build a Recent Entries menu for Atom, Blogger, and LiveJournal journals
    (Todd).
  - Sync with latest libegg code (Todd).
  - Support deleting entries for Atom and Blogger journals (todd).
  
  * Fixes:
  - Display categories for all MovableType journals, not just the default 
    (Todd).
  - Desensitize buttons and menu items that don't apply to the current journal
    (Todd).
  - XML-RPC compatibility fixes (Todd).
  - Remove support for Blogger titles, it was broken to begin with and caused
    issues when editing old entries (Todd).
  - Fix the inconsistent progress bar (Todd).
  
Drivel 1.3.3 (The "Trust me, the horsies won't mind" release)
=============================================================

  * Improvements:
  - Support XHTML 1.0 tags (Kurt).
  - Build-system improvements (Todd).
  - Support editing the more recent Advogato entry (Todd).
  - Support editing the most recent Atom entry (Todd).
  - Support Atom's new SSL authentication scheme (Todd).
  - Default to the last journal each account posted to (Todd).
  
  * Fixes:
  - MovableType API fixes (Bryan).
  - Fix a crash during poll creation (Jess).
  - Fix a crash when editing entries via the History dialog (Jess).
  - Remove the duplicate "http://" in the Insert Image dialog (Jess).
  - Remove warning about non-undoable action in History dialog (Jess).
  - Fix a handful of XML issues by using xmlReadMemory rather than
    xmlParseMemory (Todd).
  - Use UTC timestamps, should fix the Atom timezone issues (Todd).
    
  * Translations:
  - Update English (British) translation (David Lodge).
  - Updated Dutch translation (Taco Witte).
  - Updated Canadian English translation (Adam Weinberger).
  - Added Kinyarwanda translation (Steve Murphy).
  - Updated Spanish translation (Francisco Javier F. Serrador).
  
Drivel 1.3.2 (The "Tonik: with a K!" release)
=============================================

  * Improvements:

  - Allow the user to select the language of the spell-checking dictionary
    (Isak).
  - Support posting to Blogger's implementation of the Atom API (Todd).
  
  * Fixes:
  - Resolved hangs after login (Davyd).
  - Disabled features in the UI that are not supported by the selected
    journal system (Todd).

  * Translations:

  - Updated Dutch translation (Reinout van Schouwen).
  - Updated German translation (Frank Arnold).
  - Updated Canadian English translation (Adam Weinberger and 
    Alexander Winston).
  - Updated Czech translation (Miloslav Trmac).

Drivel 1.3.1 (The "H'Okay" release)
===================================

  * Improvements:

  - Support opening drafts from the file manager (Todd).
  - Make Drivel's use of the notification area HIG-compliant (Todd).
  - Add Advogato posting support (Todd).
  - Add support for Movable Type categories (Todd).
  - Add --disable-mime-update and --disable-desktop-update configure
    parameters, useful for binary packagers (Todd).

  * Fixes:

  - Display a custom icon for Drivel drafts (Todd).
  - Publish Blogger posts (Todd).
  - Support all of a user's blogger accounts, not just one (Todd).
  - Correctly translate the Translators field in the About dialog (Isak).

  * Translations:

  - Updated Dutch translation (Reinout van Schouwen).

Drivel 1.3.0 (The "Another drunk conquistador" release)
=======================================================

  * Improvements:
  
  - Add support for Beep Media Player (Adam).
  - Add an Offline mode (Todd).
  - Add Movable Type posting support (Todd).
  - Add Blogger posting support (Todd).
  - The F7 key quickly toggles spell-check on and off (Todd).
  - New account manager which can support multiple journal systems (Todd).
  - The build system has moved from automake-1.4 to automake-1.7 (Todd).
  
  * Fixes:
  
  - Glade translation fixes (Davyd).
  - Fix double-mnemonic in Format menu (Adam).
  - Make menus translatable (Isak).
  - Fix a crash when editing preferences (#151940) (Todd).
  - Plug a memory leak when loading user pictures (Todd).
  - Clarify auto-format tooltip (#151388) (Todd).
  - Include date information in Drivel's draft format (Davyd).
  - Update the RPM spec file to handle Scrollkeeper data (Todd).
  - Plus some severe memory leaks in the UI creation code (Todd).
  - Cleaned up lots of old, rotting code (Todd).

Drivel 1.2.0 (The "Hero of Canton" release)
===========================================

  * Improvements:
  
  - Added a user manual (Todd).
  
  * Fixes:
  
  - Fixed the oft-reported "automaticall" typo (Todd).
  - Synced eggtrayicon.* and recent-files/* with libegg to get the latest
    improvements and bug-fixes (Todd).
  
  * Translations:
  
  - Updated Canadian English translation (Adam Weinberger).
  - Updated Simplified Chinese translation (Funda Wang).
  - Updated Portuguese translation (Duarte Loreto).
  - Updated Swedish translation (Christian Rose).
  - Updated Czech translation (Miloslav Trmac).
  - Updated Dutch translation (Elros Cyriatan).
  - Updated Spanish translation (Francisco Javier F. Serrador).
  - Updated Albanian translation (Laurent Dhima).
  - Updated Brazilian Portuguese translation (Estêvão Samuel Procópio).
  
Drivel 1.1.2 (The "Betas make bubbles!" release)
================================================

  * Improvements:
  
  - Replace the RSA's reference MD5 implementation with a free one.
  - Add the GNOME Spinner to the network progress dialog.
  - Make the standard error dialog conform to the HIG.
  - Port the Network Progress and Insert Image dialogs to Glade.
  - Gave the Insert Image and Insert Link dialogs a make-over and some
    HIG-lovin'.
  - Added a Cancel button to the new Network Progress dialog.
  - Use unique names for user pictures, prevents re-downloading the
    same image again and again.
  - Add support for back-dating journal entries.
  - Add tooltips for post options.
  
  * Fixes:
  
  - Prevent the network dialog from "blinking" on short transactions.
  - Fix a crash that occured when the network dialog was closed manually.
  - Double-clicking an entry in the history dialog opens it for editing.
  - Prevent the user from selecting a row in the history list when it is
    empty, fixes a crash.
  - Fix a few strings to bring them into HIG 2.0 compliance.
  
  * Translations:
  
  - Updated Brazilian Portuguese translation (Raphael Higino and
    Estêvão Samuel Procópio).
  - Updated Czech translation (Miloslav Trmac).
  - Updated Canadian English translation (Adam Weinberger).
  - Updated British English translation (David Lodge).
  - Updated Spanish translation (Francisco Javier F. Serrador).
  
Drivel 1.1.1 (The "I'm too hung-over to be creative" release)
=============================================================

  * Improvements:
  
  - RhythmBox support for the Music entry (Davyd Madeley).
  - New and improved network layer which doesn't suck.
  - Abstracted blog API, should make it easy to support multiple blog
    systems in the future.
  - Support for EggRecent.
  - Added a "Drivel journal draft" mimetype.
  - Redesigned the Friends dialog.
  
  * Fixes:
  
  - Plugged some memory leaks.
  - Use the correct signal (enter_notify) for triggering the query_music
    function.
  - Lots of HIG-related spacing fixes.
  
  * Translations:
  
  - Updated Spanish translation (Francisco Javier F. Serrador).
  - Updated Brazilian Portuguese translation (Raphael Higino).
  - Updated Norwegian translation (Kjartan Maraas).
  - Updated Albanian translation (Laurent Dhima).
  - Updated Czech translation (Miloslav Trmac).
  - Updated British English translation (David Lodge).

  
Drivel 1.1.0 (The "Happy birthday, Stephie!" release)
=====================================================

  * Improvements:
  
  - HTML syntax highlighting (Davyd Madeley and Grahame Bowland).
  - Optional in-line spell checking support via GtkSpell.
  - Undo/Redo support (Davyd Madeley).
  - Support the new challenge/response LiveJournal authentication method.
  - Per-account autosaves.
  - Use LogJam's XML file format when saving/loading drafts (Davyd Madeley).
  - The Insert Link dialog now replaces selected text with a hyper-linked 
    version of the text.
  - Saves the filename of drafts so that the user isn't prompted each time she
    presses "Save Draft" and add a "Save Draft as..." menu command.
  - Autocomplete support for the Mood control (Davyd Madeley).
  - Lots of HIG work on the menus, dialogs, and alerts.
  
  * Fixes:
  
  - Keybinding fixes.
  - Resolve a couple of bugs in the History dialog (still requires GTK+ 2.4.4 
    or higher to work correctly) (Davyd Madeley).
  - Correct the lj-lq tag in the poll creator (Grahame Bowland).
  - Don't duplicate the protocol in the Insert Link dialog (gnome@nash.nu).
  - Fixed the autosave feature.
  - Resolved a network threading issue that prevented Drivel from working on 
    NetBSD, and possibly the other BSD variants as well.
  - Protect proxy variables with mutex locks, should resolve some more
    BSD-related threading issues.
  - Use libcurl's unescape method rather than our own, fixes a NetBSD
    character conversion problem.
  - Fix C99-ism which was preventing successfull compilation on 
    GCC-2.95 (Julio M. Merino Vidal).

  * Translations:
  
  - Added Albanian translation (Laurent Dhima).
  - Updated Czech translation (Miloslav Trmac).
  - Updated Brazilian Portuguese translation (Raphael Higino).
  - Updated British English translation (David Lodge).
  
Drivel 1.0 (Yeah, it really happened)
=====================================

  * Fixes:
  
  - Keep the network progress dialog on top of the Journal Entry during the
    login process.
  - Display user pictures the first time a user logs in.
  - Fix for detecting song titles with accented characters (Grahame Bowland).
  
  * Translations:
  
  - Added Spanish translation (Francisco Javier F. Serrador)
  - Added German translation (Christian Neumair)
  - Updated Brazilian Portuguese translation (Alexander Winston)
  - Updated British English translation (Gareth Owen)
  - Updated Canadian English translation (Gustavo Maciel Dias Vieira)
  
Drivel 0.90.0 (The "Happy birthday, cloche!" release)
=====================================================

  * Improvements:
  
  - Ported to GTK+ 2.4 / GNOME 2.6.
  - Lots of HIG work.
  - Updated artwork.
  - Ported most of UI to Glade (Davyd Madeley).
  - Revised "Detect Music" subsystem (Davyd Madeley).
  - New "Insert Poll" dialog (Davyd Madeley).
  - Display user pictures in the "Pictures" menu (Grahame Bowland).
  
  * Fixes:
  
  - Fix numerous instances of windows being displayed too soon.
  - Fix an error in calculating the number of shared journals a user has
    access to.
  - Fix a bug which caused the Entry window to be displayed immediatedly after
    it had been minimized.
  
  * Translations:
  
  - Added Swedish translation (Christian Rose).
  - Added Serbian translation (Danilo Šegan).
  - Added Canadian English translation (Adam Weinberger).
  - Added Czech translation (Miloslav Trmac).
  - Added Dutch translation (Vincent van Adrighem).
  - Added Norwegian translation (Terance Edward Sola).
  - Added Portuguese translation (Duarte Loreto).
  - Added British English translation (Gareth Owen).
  - Added Brazilian Portuguese translation (Raphael Higino).
  - Added Simplified Chinese translation (Funda Wang).
