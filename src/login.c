/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * Copyright 2002-2004 Todd Kulesza
 *
 * Authors:
 * 		Todd Kulesza <todd@dropline.net>
 */

#include <config.h>

#include <gnome.h>
#include <glade/glade.h>
#include <gdk-pixbuf/gdk-pixdata.h>
#include <string.h>

#include "drivel_request.h"
#include "blog_advogato.h"
#include "blog_atom.h"
#include "blog_blogger.h"
#include "blog_lj.h"
#include "blog_mt.h"
#include "blog_offline.h"
#include "network.h"
#include "about.h"
#include "journal.h"
#include "login.h"

extern GMutex *net_mutex;

enum
{
	TYPE_ICON,
	TYPE_NAME,
	TYPE_SERVER,
	TYPE_API,
	TYPE_COLS
};

enum
{
	LOGIN_NAME,
	LOGIN_PASSWORD,
	LOGIN_SERVER,
	LOGIN_API,
	LOGIN_SAVE_PASSWORD,
	LOGIN_AUTOLOGIN,
	LOGIN_ICON,
	LOGIN_COLS
};

/* if the user works offline, we need to remember the previous api */
DrivelBlogAPI last_api = BLOG_API_UNKNOWN;

void
about_cb(GtkWidget *widget, gpointer data)
{
	about_show (data);
	
	return;
}

static gboolean
get_sensitivity (GtkWidget *entry)
{
	gchar *text;
	gint len;
	gboolean retval;
	
	text = gtk_editable_get_chars (GTK_EDITABLE (entry), 0, -1);
	len = strlen (text);
	
	if (len > 0)
		retval = TRUE;
	else
		retval = FALSE;
	
	g_free (text);
	
	return retval;
}

static gint
user_sort (gconstpointer a, gconstpointer b)
{
	gint retval;
	DrivelUser *dua, *dub;
	
	dua = (DrivelUser*)a;
	dub = (DrivelUser*)b;
	
	retval = strcmp (dua->username, dub->username);
	if (!retval)
		retval = dua->api - dub->api;

	return retval;
}

static void
set_active_journal_type (GtkWidget *combo, DrivelBlogAPI api)
{
	GtkTreeModel *model;
	GtkTreeIter iter;
	gboolean valid;
	gint type_api;
	
	model = gtk_combo_box_get_model (GTK_COMBO_BOX (combo));
	valid = gtk_tree_model_get_iter_first (model, &iter);
	while (valid)
	{
		gtk_tree_model_get (model, &iter, TYPE_API, &type_api, -1);
		if (type_api == api)
		{
			gtk_combo_box_set_active_iter (GTK_COMBO_BOX (combo), &iter);
			valid = FALSE;
		}
		else
			valid = gtk_tree_model_iter_next (model, &iter);
	}
	
	return;
}

static void
build_journal_type_combo (GtkListStore **type_store)
{
	/* FIXME
	 * We should query registered plugins to get this information
	 */
	GtkTreeIter iter;
	GdkPixbuf *pixbuf, *pixbuf_small = NULL;
	gchar *path;
	
	/* advogato */
	gtk_list_store_append (*type_store, &iter);
	path = g_build_filename (DATADIR, "pixmaps", "drivel", "advogato.png", NULL);
	pixbuf = gdk_pixbuf_new_from_file (path, NULL);
	if (pixbuf)
	{
		pixbuf_small = gdk_pixbuf_scale_simple (pixbuf, 20, 20,
				GDK_INTERP_BILINEAR);
		g_object_unref (G_OBJECT (pixbuf));
	}
	else
		g_warning ("Could not open %s\n", path);
	g_free (path);
	gtk_list_store_set (*type_store, &iter,
			TYPE_ICON, pixbuf_small,
			TYPE_NAME, "Advogato",
			TYPE_SERVER, "http://www.advogato.org/XMLRPC",
			TYPE_API, BLOG_API_ADVOGATO,
			-1);
	
	/* atom */
	gtk_list_store_append (*type_store, &iter);
	path = g_build_filename (DATADIR, "pixmaps", "drivel", "atom.png", NULL);
	pixbuf = gdk_pixbuf_new_from_file (path, NULL);
	if (pixbuf)
	{
		pixbuf_small = gdk_pixbuf_scale_simple (pixbuf, 20, 20,
				GDK_INTERP_BILINEAR);
		g_object_unref (G_OBJECT (pixbuf));
	}
	else
		g_warning ("Could not open %s\n", path);
	g_free (path);
	gtk_list_store_set (*type_store, &iter,
			TYPE_ICON, pixbuf_small,
			TYPE_NAME, "Atom / Blogger 2.0",
			TYPE_SERVER, "https://www.blogger.com/atom",
			TYPE_API, BLOG_API_ATOM,
			-1);
	
	/* blogger */
	gtk_list_store_append (*type_store, &iter);
	path = g_build_filename (DATADIR, "pixmaps", "drivel", "blogger.png", NULL);
	pixbuf = gdk_pixbuf_new_from_file (path, NULL);
	if (pixbuf)
	{
		pixbuf_small = gdk_pixbuf_scale_simple (pixbuf, 20, 20,
				GDK_INTERP_BILINEAR);
		g_object_unref (G_OBJECT (pixbuf));
	}
	else
		g_warning ("Could not open %s\n", path);
	g_free (path);
	gtk_list_store_set (*type_store, &iter,
			TYPE_ICON, pixbuf_small,
			TYPE_NAME, "Blogger 1.0",
			TYPE_SERVER, "http://plant.blogger.com/api/RPC2",
			TYPE_API, BLOG_API_BLOGGER,
			-1);
	
	/* livejournal */
	gtk_list_store_append (*type_store, &iter);
	path = g_build_filename (DATADIR, "pixmaps", "drivel", "livejournal.png", NULL);
	pixbuf = gdk_pixbuf_new_from_file (path, NULL);
	if (pixbuf)
	{
		pixbuf_small = gdk_pixbuf_scale_simple (pixbuf, 20, 20,
				GDK_INTERP_BILINEAR);
		g_object_unref (G_OBJECT (pixbuf));
	}
	else
		g_warning ("Could not open %s\n", path);
	g_free (path);
	gtk_list_store_set (*type_store, &iter,
			TYPE_ICON, pixbuf_small,
			TYPE_NAME, "LiveJournal",
			TYPE_SERVER, "http://www.livejournal.com",
			TYPE_API, BLOG_API_LJ,
			-1);
	
	/* movable type */
	gtk_list_store_append (*type_store, &iter);
	path = g_build_filename (DATADIR, "pixmaps", "drivel", "mt.png", NULL);
	pixbuf = gdk_pixbuf_new_from_file (path, NULL);
	if (pixbuf)
	{
		pixbuf_small = gdk_pixbuf_scale_simple (pixbuf, 20, 20,
				GDK_INTERP_BILINEAR);
		g_object_unref (G_OBJECT (pixbuf));
	}
	else
		g_warning ("Could not open %s\n", path);
	g_free (path);
	gtk_list_store_set (*type_store, &iter,
			TYPE_ICON, pixbuf_small,
			TYPE_NAME, "Movable Type",
			TYPE_SERVER, "http://mt-server/cgi-bin/mt-xmlrpc.cgi",
			TYPE_API, BLOG_API_MT,
			-1);
	
	return;
}

static void
build_journal_user_combo (GtkListStore **store, GSList *list)
{
	GtkTreeIter iter;
	GSList *item;
	DrivelUser *du;
	const gchar *icon;
	gchar *path;
	GdkPixbuf *pixbuf, *pixbuf_small = NULL;

	gtk_list_store_clear (*store);
	
	/* build the list store with the list of user names*/
	for (item = list; item; item = item->next)
	{
		du = item->data;
		
		/* load the correct icon for the journal type */
		/* FIXME
		 * We should query plugins to get the images
		 */
		switch (du->api)
		{
			case BLOG_API_LJ:
			{
				icon = "livejournal.png";
				break;
			}
			case BLOG_API_MT:
			{
				icon = "mt.png";
				break;
			}
			case BLOG_API_BLOGGER:
			{
				icon = "blogger.png";
				break;
			}
			case BLOG_API_ADVOGATO:
			{
				icon = "advogato.png";
				break;
			}
			case BLOG_API_ATOM:
			{
				icon = "atom.png";
				break;
			}
			default:
			{
				icon = NULL;
				break;
			}
		}
		path = g_build_filename (DATADIR, "pixmaps", "drivel", icon, NULL);
		pixbuf = gdk_pixbuf_new_from_file (path, NULL);
		if (pixbuf)
		{
			pixbuf_small = gdk_pixbuf_scale_simple (pixbuf, 20, 20,
					GDK_INTERP_BILINEAR);
			g_object_unref (G_OBJECT (pixbuf));
		}
		g_free (path);
		
		/* add the entry to the list store */
		gtk_list_store_append (*store, &iter);
		gtk_list_store_set (*store, &iter,
				LOGIN_NAME, g_strdup (du->username),
				LOGIN_PASSWORD, g_strdup (du->password),
				LOGIN_SERVER, g_strdup (du->server),
				LOGIN_API, du->api,
				LOGIN_ICON, pixbuf_small,
				LOGIN_SAVE_PASSWORD, du->save_password,
				LOGIN_AUTOLOGIN, du->autologin,
				-1);
	}
	
	return;
}

static void
load_generic_settings (DrivelClient *dc)
{
	gtk_editable_delete_text (GTK_EDITABLE (GTK_BIN (dc->login_name)->child), 0, -1);
	gtk_combo_box_set_active (GTK_COMBO_BOX (dc->login_type), 3); /* 3 is LJ */
	
	return;
}

static void
load_last_user (GtkWidget *combo, GSList *user_list)
{
	GtkTreeModel *model;
	GtkTreeIter iter;
	GSList *item;
	DrivelUser *du;
	
	model = gtk_combo_box_get_model (GTK_COMBO_BOX (combo));
	
	for (item = user_list; item; item = item->next)
	{
		du = item->data;
		if (du->lastuser)
		{
			gboolean valid;
			
			/* find the user in the list who matches the username and api */
			valid = gtk_tree_model_get_iter_first (model, &iter);
			while (valid)
			{
				gchar *username;
				gint api;
			
				gtk_tree_model_get (model, &iter,
						LOGIN_NAME, &username,
						LOGIN_API, &api,
						-1);
				if (!strcmp (username, du->username) && api == du->api)
				{
					gtk_combo_box_set_active_iter (GTK_COMBO_BOX (combo), &iter);
					valid = FALSE;
				}
				else
					valid = gtk_tree_model_iter_next (model, &iter);
				
				g_free (username);
			}
			
			break;
		}
	}
	
	return;
}

void
set_user_login_prefs (DrivelClient *dc)
{
	gchar *username, *password, *server;
	gint api;
	gboolean save_password, autologin;
	GtkTreeModel *model;
	GtkTreeIter iter;
	
	model = gtk_combo_box_get_model (GTK_COMBO_BOX (dc->login_name));
	gtk_combo_box_get_active_iter (GTK_COMBO_BOX (dc->login_name), &iter);
	gtk_tree_model_get (model, &iter,
			LOGIN_NAME, &username,
			LOGIN_PASSWORD, &password,
			LOGIN_SERVER, &server,
			LOGIN_API, &api,
			LOGIN_SAVE_PASSWORD, &save_password,
			LOGIN_AUTOLOGIN, &autologin,
			-1);
	if (!password)
		password = g_strdup ("");
	
	gtk_entry_set_text (GTK_ENTRY (GTK_BIN (dc->login_name)->child), username);
	gtk_entry_set_text (GTK_ENTRY (dc->login_password), password);
	set_active_journal_type (dc->login_type, api);
	gtk_entry_set_text (GTK_ENTRY (dc->login_server), server);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dc->save_password), save_password);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dc->autologin), autologin);
	
	g_free (username);
	g_free (password);
	g_free (server);
	
	return;
}

static gboolean
login_focus_out_cb (GtkWidget *entry, GdkEventFocus *event, gpointer data)
{
	gchar *username;
	DrivelClient *dc = (DrivelClient *) data;
	
	username = gtk_editable_get_chars (GTK_EDITABLE (entry), 0, -1);

	if (!validate_username (username))
	{		
		g_free (username);
			
		return FALSE;
	}

	g_mutex_lock (net_mutex);
	g_free (dc->user->username);
	dc->user->username = username;
	g_mutex_unlock (net_mutex);
	
	return FALSE;
}

/* Clear the password field and desensitize related widgets */

static void
clear_password (DrivelClient *dc)
{
	gtk_editable_delete_text (GTK_EDITABLE (dc->login_password), 0, -1);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dc->save_password), FALSE);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dc->autologin), FALSE);
	gtk_widget_set_sensitive (dc->save_password, FALSE);
	gtk_widget_set_sensitive (dc->autologin, FALSE);
	gtk_widget_set_sensitive (dc->login_button, FALSE);
	gtk_widget_set_sensitive (dc->login_menu, FALSE);
	
	return;
}

static void
login_list_changed_cb (GtkWidget *entry, gpointer data)
{
	GtkTreeIter iter;
	DrivelClient *dc = (DrivelClient *) data;
	
	/* if the username was selected from the list, fill in the password, 
	   api, server, etc. */
	if (gtk_combo_box_get_active_iter (GTK_COMBO_BOX (dc->login_name), &iter))
	{
		set_user_login_prefs (dc);
		gtk_widget_set_sensitive (dc->login_remove, TRUE);
	}
	else
		gtk_widget_set_sensitive (dc->login_remove, FALSE);
	
	return;
}

static void
login_changed_cb (GtkWidget *entry, gpointer data)
{
	gboolean state;
	GtkTreeIter iter;
	gchar *username;
	DrivelClient *dc = (DrivelClient *) data;
	
	state = get_sensitivity (entry);

	if (state)
	{
		gtk_widget_set_sensitive (dc->login_password, TRUE);
		gtk_widget_set_sensitive (dc->login_type, TRUE);
		gtk_widget_set_sensitive (dc->login_server, TRUE);
		
		username = gtk_editable_get_chars (GTK_EDITABLE (entry), 0, -1);
		
		if (!validate_username (username))
		{
			display_error_dialog (dc, _("Bad username"), 
					_("Username contains invalid characters."));
		
			g_free (username);
			
			return;
		}
		
		/* if the username was selected from the list, sensitize the 'remove'
		   menu item. */
		if (gtk_combo_box_get_active_iter (GTK_COMBO_BOX (dc->login_name), &iter))
			gtk_widget_set_sensitive (dc->login_remove, TRUE);
		else
		{
			gtk_widget_set_sensitive (dc->login_remove, FALSE);
			clear_password (dc);
		}
		
		g_mutex_lock (net_mutex);
		g_free (dc->user->username);
		dc->user->username = username;
		g_mutex_unlock (net_mutex);
	}
	else
	{
		g_mutex_lock (net_mutex);
		g_free (dc->user->username);
		dc->user->username = NULL;
		g_mutex_unlock (net_mutex);
		
		clear_password (dc);
		gtk_widget_set_sensitive (dc->login_password, FALSE);
		gtk_widget_set_sensitive (dc->login_type, FALSE);
		gtk_widget_set_sensitive (dc->login_server, FALSE);
		gtk_widget_set_sensitive (dc->login_remove, FALSE);
	}

	return;
}

static void
password_changed_cb (GtkWidget *entry, gpointer data)
{
	gboolean state, username;
	DrivelClient *dc = (DrivelClient *) data;
	
	state = get_sensitivity (entry);
	
	if (state)
	{
		gtk_widget_set_sensitive (dc->save_password, TRUE);
		if (get_sensitivity (dc->login_server))
		{
			gtk_widget_set_sensitive (dc->login_button, TRUE);
			gtk_widget_set_sensitive (dc->login_menu, TRUE);
		}
		
		g_mutex_lock (net_mutex);
		if (dc->user->username)
			username = TRUE;
		else
			username = FALSE;
		g_mutex_unlock (net_mutex);
	}
	else
	{
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dc->save_password), FALSE);
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dc->autologin), FALSE);
		gtk_widget_set_sensitive (dc->save_password, FALSE);
		gtk_widget_set_sensitive (dc->autologin, FALSE);
		gtk_widget_set_sensitive (dc->login_button, FALSE);
		gtk_widget_set_sensitive (dc->login_menu, FALSE);
	}
		
	return;
}

static gboolean
password_focus_out_cb (GtkWidget *entry, GdkEventFocus *event, gpointer data)
{
	gchar *password;
	DrivelClient *dc = (DrivelClient *) data;
	
	password = gtk_editable_get_chars (GTK_EDITABLE (entry), 0, -1);

	if (!password)
		password = g_strdup ("");

	g_mutex_lock (net_mutex);
	g_free (dc->user->password);
	dc->user->password = password;
	g_mutex_unlock (net_mutex);
	
	return FALSE;
}

static void
type_changed_cb (GtkWidget *box, gpointer data)
{
	GtkTreeModel *model;
	GtkTreeIter iter;
	gchar *server;
	gint api;
	DrivelClient *dc = (DrivelClient *) data;
	
	model = gtk_combo_box_get_model (GTK_COMBO_BOX (box));
	if (gtk_combo_box_get_active_iter (GTK_COMBO_BOX (box), &iter))
	{
		gtk_tree_model_get (model, &iter, 
				TYPE_SERVER, &server, 
				TYPE_API, &api,
				-1);
		gtk_entry_set_text (GTK_ENTRY (dc->login_server), server);
		dc->user->api = api;
		g_free (server);
	}
	
	/* changing the type means the account no longer matches the account list */
	gtk_widget_set_sensitive (dc->login_remove, FALSE);
	
	return;
}

static void
server_changed_cb (GtkWidget *entry, gpointer data)
{
	gboolean state;
	DrivelClient *dc = (DrivelClient *) data;
	
	state = get_sensitivity (entry);
	
	if (state)
	{
		if (get_sensitivity (GTK_BIN (dc->login_name)->child) && 
				get_sensitivity (dc->login_password))
		{
			gtk_widget_set_sensitive (dc->login_button, TRUE);
			gtk_widget_set_sensitive (dc->login_menu, TRUE);
		}
	}
	else
	{
		gtk_widget_set_sensitive (dc->login_button, FALSE);
		gtk_widget_set_sensitive (dc->login_menu, FALSE);
	}
	
	return;
}

static gboolean
server_focus_out_cb (GtkWidget *entry, GdkEventFocus *event, gpointer data)
{
	gchar *server;
	DrivelClient *dc = (DrivelClient *) data;
	
	server = gtk_editable_get_chars (GTK_EDITABLE (entry), 0, -1);

	if (!server)
		server = g_strdup ("");
	else
	{
		gchar *tmp;
		gint len = strlen (server);
		
		if (!strstr (server, "//"))
		{
			tmp = g_strdup_printf ("http://%s", server);
			g_free (server);
			server = tmp;
		}
		if (server[len - 1] == '/')
			server[len - 1] = '\0';
	}
	
	g_free (dc->user->server);
	dc->user->server = server;
	gtk_entry_set_text (GTK_ENTRY (entry), server);
	
	return FALSE;
}

static void
save_password_cb (GtkWidget *button, gpointer data)
{
	gboolean state;
	DrivelClient *dc = (DrivelClient *) data;
	
	state = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (button));
		
	if (state)
	{
		gtk_widget_set_sensitive (dc->autologin, TRUE);
	}
	else
	{
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dc->autologin), FALSE);
		gtk_widget_set_sensitive (dc->autologin, FALSE);
	}
	
	return;
}

static void
login (DrivelClient *dc)
{
	if (last_api != BLOG_API_UNKNOWN)
	{	
		dc->user->api = last_api;
		last_api = BLOG_API_UNKNOWN;
	}
	
	g_mutex_lock (net_mutex);
	drivel_user_free (dc->user);
	dc->user = get_drivel_user (dc);
	g_mutex_unlock (net_mutex);
	
	/* build & enqueue the login request */
	
	switch (dc->user->api)
	{
	case BLOG_API_LJ:
	{
		gchar *version;
		version = g_strdup_printf ("GNOME-Drivel/%s", VERSION);
		blog_lj_build_login_request (dc->user->username, 
					     dc->user->server, version, dc->moods);
		g_free (version);
		debug ("built lj login request");
		break;
	}
	case BLOG_API_MT:
	{
		blog_mt_build_login_request (dc->user->server, 
					     dc->user->username, dc->user->password);
		debug ("built mt login request");
		break;
	}
	case BLOG_API_BLOGGER:
	{
		blog_blogger_build_login_request (dc->user->username,
						  dc->user->password, dc->user->server);
		debug ("built blogger login request");
		break;
	}
	case BLOG_API_ADVOGATO:
	{
		blog_advogato_build_login_request (dc->user->username,
						   dc->user->password, dc->user->server);
		debug ("built advogato login request");
		break;
	}
	case BLOG_API_ATOM:
	{
		blog_atom_build_login_request (dc->user->username, 
					       dc->user->password, dc->user->server);
		debug ("build atom login request");
		break;
	}
	default:
		break;
	}
	
	return;
}

static void
login_button_cb (GtkWidget *widget, gpointer data)
{
	DrivelClient *dc = (DrivelClient *)data;
	
	login (dc);
	
	return;
}

static void
work_offline (DrivelClient *dc)
{
	last_api = dc->user->api;
	
	blog_offline_fake_login (dc);
	
	return;
}

static void
work_offline_button_cb (GtkWidget *widget, gpointer data)
{
	DrivelClient *dc = (DrivelClient *)data;
	
	work_offline (dc);
	
	return;
}

static void
remove_username_cb (GtkWidget *menu, gpointer data)
{
	GtkTreeModel *model;
	GtkTreeIter iter;
	DrivelUser *to_remove;
	gchar *username, *server;
	gint api;
	GSList *item;
	DrivelClient *dc = (DrivelClient *)data;
	
	model = gtk_combo_box_get_model (GTK_COMBO_BOX (dc->login_name));
	gtk_combo_box_get_active_iter (GTK_COMBO_BOX (dc->login_name), &iter);
	gtk_tree_model_get (model, &iter,
			LOGIN_NAME, &username,
			LOGIN_SERVER, &server,
			LOGIN_API, &api,
			-1);
	
	to_remove = drivel_user_new ();
	to_remove->username = username;
	to_remove->password = NULL;
	to_remove->server = server;
	to_remove->api = api;
	
	item = find_in_user_list (dc->user_list, to_remove);
	if (item)
	{
		drivel_user_free (item->data);
		dc->user_list = g_slist_delete_link (dc->user_list, item);
		save_user_list (dc->user_list, dc->config_directory);
		user_list_changed (dc);
	}
	else
		g_warning ("Did not find the user in the list"); /* should never happen */
	
	load_generic_settings (dc);
	
	return;
}

static void
help_cb (GtkWidget *menu, gpointer data)
{
	gnome_help_display ("drivel.xml", NULL, NULL);
	
	return;
}

static void
faq_cb (GtkWidget *menu, gpointer data)
{
	gnome_help_display ("drivel.xml", "drivel-faq", NULL);
	
	return;
}

DrivelUser*
get_drivel_user (DrivelClient *dc)
{
	DrivelUser *du;
	GtkTreeIter iter;
	GtkTreeModel *model;

	du = drivel_user_new ();
	
	/* retrieve the account information from the login window */
	du->username = gtk_editable_get_chars (GTK_EDITABLE (GTK_BIN (dc->login_name)->child), 0, -1);
	du->password = gtk_editable_get_chars (GTK_EDITABLE (dc->login_password), 0, -1);
	model = gtk_combo_box_get_model (GTK_COMBO_BOX (dc->login_type));
	gtk_combo_box_get_active_iter (GTK_COMBO_BOX (dc->login_type), &iter);
	gtk_tree_model_get (model, &iter, 3, &du->api, -1);
	du->server = gtk_editable_get_chars (GTK_EDITABLE (dc->login_server), 0, -1);
	du->save_password = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dc->save_password));
	du->autologin = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dc->autologin));
	du->lastuser = TRUE;
	
	return du;
}

/* Clear and re-create the list of user accounts */

void
user_list_changed (DrivelClient *dc)
{
	GtkListStore *model;

	model = GTK_LIST_STORE (gtk_combo_box_get_model (
				GTK_COMBO_BOX (dc->login_name)));
	
	/* build new list */
	dc->user_list = g_slist_sort (dc->user_list, user_sort);
	build_journal_user_combo (&model, dc->user_list);
	
	/* select the most-recently-used user account */
	load_last_user (dc->login_name, dc->user_list);
	
	return;
}

void
login_window_build (DrivelClient *dc)
{	
	GladeXML *xml;
	GtkWidget *menubar, *statusbar, *offline;
	GtkActionGroup *action_group;
	GtkUIManager *ui_manager;
	GtkAccelGroup *accel_group;
	GtkListStore *store;
	GtkCellRenderer *renderer;
	gchar *path;
	static GtkActionEntry entries[] =
	{
		{ "JournalMenu", NULL, N_("_Journal") },
		{ "EditMenu", NULL, N_("_Edit") },
		{ "HelpMenu", NULL, N_("_Help") },
		{ "Login", "drivel-login", NULL, NULL, NULL, G_CALLBACK (login_button_cb) },
		{ "Remove", GTK_STOCK_REMOVE, N_("Remove Account"), NULL, NULL, G_CALLBACK (remove_username_cb) },
		{ "Quit", GTK_STOCK_QUIT, NULL, NULL, NULL, G_CALLBACK (exit_cb) },
		{ "Preferences", GTK_STOCK_PREFERENCES, N_("_Preferences"), NULL, NULL, G_CALLBACK (edit_preferences_cb) },
		{ "Contents", GTK_STOCK_HELP, N_("_Contents"), "F1", NULL, G_CALLBACK (help_cb) },
		{ "FAQ", NULL, N_("_Frequently Asked Questions"), NULL, NULL, G_CALLBACK (faq_cb) },
		{ "About", GNOME_STOCK_ABOUT, N_("_About"), NULL, NULL, G_CALLBACK (about_cb) }
	};
	
	static const gchar *ui_description =
	{
		"<ui>"
		"  <menubar name='MainMenu'>"
		"    <menu action='JournalMenu'>"
		"      <menuitem action='Login' />"
		"      <separator name='sep1'/>"
		"      <menuitem action='Quit' />"
		"    </menu>"
		"    <menu action='EditMenu'>"
		"      <menuitem action='Remove' />"
		"      <separator name='sep2'/>"
		"      <menuitem action='Preferences' />"
		"    </menu>"
		"    <menu action='HelpMenu'>"
		"      <menuitem action='Contents' />"
		"      <menuitem action='FAQ' />"
		"      <menuitem action='About' />"
		"    </menu>"
		"  </menubar>"
		"</ui>"
	};

	xml = load_glade_xml ("login_window");
	if (!xml)
		return;
	
	dc->login_window = glade_xml_get_widget (xml, "login_window");
	path = g_build_filename (DATADIR, "pixmaps", "drivel", "drivel-24.png", NULL);
	gtk_window_set_icon_from_file (GTK_WINDOW (dc->login_window), path, NULL);
	g_free (path);

	path = g_build_filename (DATADIR, "pixmaps", "drivel", "drivel_splash.png", NULL);
	gtk_image_set_from_file (GTK_IMAGE (glade_xml_get_widget (xml, "login_window_image")),
			path);
	g_free (path);

	/* password */
	dc->login_password = glade_xml_get_widget (xml,
			"login_window_password");
	
	/* journal type */
	dc->login_type = glade_xml_get_widget (xml, "login_window_type");
	dc->login_type_store = gtk_list_store_new (TYPE_COLS,
			GDK_TYPE_PIXBUF, G_TYPE_STRING,
			G_TYPE_STRING, G_TYPE_INT);
	gtk_combo_box_set_model (GTK_COMBO_BOX (dc->login_type),
			GTK_TREE_MODEL (dc->login_type_store));
	gtk_cell_layout_clear (GTK_CELL_LAYOUT (dc->login_type));
	
	renderer = gtk_cell_renderer_pixbuf_new ();
	gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (dc->login_type),
			renderer, FALSE);
	gtk_cell_layout_set_attributes (
			GTK_CELL_LAYOUT (dc->login_type), renderer,
			"pixbuf", TYPE_ICON,
			NULL);

	renderer = gtk_cell_renderer_text_new ();
	gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (dc->login_type), 
			renderer, TRUE);
	gtk_cell_layout_set_attributes (
			GTK_CELL_LAYOUT (dc->login_type), renderer,
			"text", TYPE_NAME,
			NULL);

	build_journal_type_combo (&(dc->login_type_store));
	
	/* journal server */
	dc->login_server = glade_xml_get_widget (xml, "login_window_server");
	
	/* account options */
	dc->save_password = glade_xml_get_widget (xml, "login_window_save_password");
	dc->autologin = glade_xml_get_widget (xml, "login_window_auto_login");
	
	/* action buttons */
	dc->login_button = glade_xml_get_widget (xml, "login_window_login");
	gtk_button_set_use_stock (GTK_BUTTON (dc->login_button), TRUE);
	offline = glade_xml_get_widget (xml, "login_window_offline");
	
	drivel_push_current_window (dc, dc->login_window);
	
	/* setup the menus */
	
	action_group = gtk_action_group_new ("MenuActions");
	gtk_action_group_set_translation_domain (action_group, GETTEXT_PACKAGE);
	gtk_action_group_add_actions (action_group, entries, G_N_ELEMENTS (entries), dc);
	
	ui_manager = gtk_ui_manager_new ();
	gtk_ui_manager_insert_action_group (ui_manager, action_group, 0);
	
	accel_group = gtk_ui_manager_get_accel_group (ui_manager);
	gtk_window_add_accel_group (GTK_WINDOW (dc->login_window), accel_group);
	
	if (!gtk_ui_manager_add_ui_from_string (ui_manager, ui_description, -1, NULL))
		g_error ("Error while building login window menu");
	
	menubar = gtk_ui_manager_get_widget (ui_manager, "/MainMenu");
	gnome_app_set_menus (GNOME_APP (dc->login_window), GTK_MENU_BAR (menubar));
	
 	dc->statusbar = gnome_appbar_new (TRUE, TRUE, GNOME_PREFERENCES_NEVER);
 	gnome_app_set_statusbar (GNOME_APP (dc->login_window), dc->statusbar);

	gtk_widget_show_all (dc->login_window);

	dc->login_menu = gtk_ui_manager_get_widget (ui_manager, "/MainMenu/JournalMenu/Login");
	dc->login_remove = gtk_ui_manager_get_widget (ui_manager, "/MainMenu/EditMenu/Remove");
	gtk_widget_set_sensitive (dc->login_menu, FALSE);
	gtk_widget_set_sensitive (dc->login_remove, FALSE);
	
	/* do this before connecting the changed signal */
	dc->user_list = load_user_list (dc->config_directory);
	dc->user_list = g_slist_sort (dc->user_list, user_sort);
	
	/* username */
	dc->login_name = glade_xml_get_widget (xml, "login_window_username");
	gtk_widget_show (dc->login_name);
	store = gtk_list_store_new (LOGIN_COLS,
			G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING,
			G_TYPE_INT, G_TYPE_INT, G_TYPE_INT,
			GDK_TYPE_PIXBUF);
	gtk_combo_box_set_model (GTK_COMBO_BOX (dc->login_name),
			GTK_TREE_MODEL (store));
	gtk_cell_layout_clear (GTK_CELL_LAYOUT (dc->login_name));

	renderer = gtk_cell_renderer_pixbuf_new ();
	gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (dc->login_name), 
			renderer, FALSE);
	gtk_cell_layout_set_attributes (GTK_CELL_LAYOUT (dc->login_name),
			renderer,
			"pixbuf", LOGIN_ICON,
			NULL);
	
	renderer = gtk_cell_renderer_text_new ();
	gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (dc->login_name),
			renderer, TRUE);
	gtk_cell_layout_set_attributes (GTK_CELL_LAYOUT (dc->login_name),
			renderer,
			"text", LOGIN_NAME,
			NULL);
	
	build_journal_user_combo (&store, dc->user_list);
	gtk_entry_set_max_length (
			GTK_ENTRY (GTK_BIN (dc->login_name)->child), 50);
	gtk_entry_set_width_chars (
			GTK_ENTRY (GTK_BIN (dc->login_name)->child), 15);
	gtk_entry_set_activates_default (
			GTK_ENTRY (GTK_BIN (dc->login_name)->child), TRUE);

	/* connect the signals */
	
	g_signal_connect (G_OBJECT (GTK_BIN (dc->login_name)->child), "changed", 
			G_CALLBACK (login_changed_cb), dc);
	g_signal_connect (G_OBJECT (dc->login_name), "changed", 
			G_CALLBACK (login_list_changed_cb), dc);
	g_signal_connect (G_OBJECT (GTK_BIN (dc->login_name)->child), "focus_out_event",
			G_CALLBACK (login_focus_out_cb), dc);
	g_signal_connect (G_OBJECT (dc->login_password), "changed",
			G_CALLBACK (password_changed_cb), dc);
	g_signal_connect (G_OBJECT (dc->login_password), "focus_out_event",
			G_CALLBACK (password_focus_out_cb), dc);
	g_signal_connect (G_OBJECT (dc->login_type), "changed",
			G_CALLBACK (type_changed_cb), dc);
	g_signal_connect (G_OBJECT (dc->login_server), "changed",
			G_CALLBACK (server_changed_cb), dc);
	g_signal_connect (G_OBJECT (dc->login_server), "focus_out_event",
			G_CALLBACK (server_focus_out_cb), dc);
	g_signal_connect (G_OBJECT (dc->save_password), "toggled",
			G_CALLBACK (save_password_cb), dc);
	g_signal_connect (G_OBJECT (dc->login_button), "clicked",
			G_CALLBACK (login_button_cb), dc);
	g_signal_connect (G_OBJECT (offline), "clicked",
			G_CALLBACK (work_offline_button_cb), dc);
	g_signal_connect (G_OBJECT (dc->login_window), "delete_event",
			G_CALLBACK (delete_event_cb), dc);

	/* first setup the window with sensible defaults... */
	load_generic_settings (dc);
	login_changed_cb (GTK_BIN (dc->login_name)->child, dc);
	
	/* ... then setup the last user's preferences (if they exist) */
	load_last_user (dc->login_name, dc->user_list);
	
	gtk_editable_select_region (GTK_EDITABLE (GTK_BIN (dc->login_name)->child), 0, -1);
	gtk_widget_grab_default (dc->login_button);
	
	if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dc->autologin)))
		login_button_cb (dc->login_button, dc);
	
	return;
}
