/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * Copyright 2005 Todd Kulesza
 *
 * Authors:
 * 		Todd Kulesza <todd@dropline.net>
 */

#ifndef _BLOG_ADVOGATO_H_
#define _BLOG_ADVOGATO_H_

#include "drivel_request.h"
#include "drivel.h"

DrivelRequest*
blog_advogato_build_login_request (const gchar *username, const gchar *password,
				   const gchar *uri);

DrivelRequest*
blog_advogato_build_postevent_request (const gchar *cookie, const gchar *uri, 
		gint index, const gchar *title, const gchar *content);

DrivelRequest*
blog_advogato_build_getevents_request (const gchar *username, const gchar *uri);

#endif /* _BLOG_ADVOGATO_H_ */
